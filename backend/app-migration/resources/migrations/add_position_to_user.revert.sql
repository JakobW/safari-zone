ALTER TABLE users
    DROP COLUMN current_tile_x,
    DROP COLUMN current_tile_y;