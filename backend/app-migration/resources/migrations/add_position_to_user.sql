ALTER TABLE users
    ADD current_tile_x INT DEFAULT 0,
    ADD current_tile_y INT DEFAULT 0;
ALTER TABLE users
    ALTER current_tile_x SET NOT NULL,
    ALTER current_tile_y SET NOT NULL,
    ALTER current_tile_x DROP DEFAULT,
    ALTER current_tile_y DROP DEFAULT;
