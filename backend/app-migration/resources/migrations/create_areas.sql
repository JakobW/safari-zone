CREATE TABLE area (
    id SERIAL PRIMARY KEY,
    name VARCHAR NOT NULL UNIQUE,
    range_x int4range NOT NULL,
    range_y int4range NOT NULL,
    EXCLUDE USING GIST (range_x WITH &&, range_y WITH &&)
)