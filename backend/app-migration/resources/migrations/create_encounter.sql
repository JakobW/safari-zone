CREATE TABLE encounter (
    id SERIAL PRIMARY KEY,
    probability REAL NOT NULL,
    area_id INTEGER NOT NULL,
    pokemon_id INTEGER NOT NULL,
    FOREIGN KEY (area_id) REFERENCES area(id),
    FOREIGN KEY (pokemon_id) REFERENCES pokemon(id),
    UNIQUE (area_id, pokemon_id)
);