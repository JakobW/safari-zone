CREATE TYPE LAYERTYPE AS ENUM ('ground', 'overlay');

CREATE TABLE tile (
    id SERIAL PRIMARY KEY,
    coord_x INT NOT NULL,
    coord_y INT NOT NULL,
    tile INT NOT NULL,
    layer INT NOT NULL,
    layerType LAYERTYPE NOT NULL,
    UNIQUE (coord_x, coord_y, layer, layerType)
);