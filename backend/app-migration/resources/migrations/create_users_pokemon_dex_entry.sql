CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    -- For andrena auth
    m_nummer VARCHAR UNIQUE,
    -- For internal auth
    internal_id INTEGER UNIQUE,
    CHECK ((m_nummer IS NOT NULL AND internal_id IS NULL)
        OR (m_nummer IS NULL AND internal_id IS NOT NULL))
);

CREATE TABLE pokemon (
    id SERIAL PRIMARY KEY,
    name VARCHAR NOT NULL,
    number INTEGER NOT NULL UNIQUE,
    description VARCHAR NOT NULL,
    primary_type VARCHAR NOT NULL,
    secondary_type VARCHAR,
    weight NUMERIC NOT NULL,
    height NUMERIC NOT NULL,
    image_name VARCHAR NOT NULL
);

CREATE TABLE dex_entry (
    id SERIAL PRIMARY KEY,
    amount_normal INTEGER NOT NULL,
    amount_shiny INTEGER NOT NULL,
    user_id INTEGER NOT NULL,
    pokemon_id INTEGER NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users(id),
    FOREIGN KEY (pokemon_id) REFERENCES pokemon(id),
    UNIQUE (user_id, pokemon_id)
);
