CREATE TABLE wild_pokemon (
    id SERIAL PRIMARY KEY,
    coord_x INT NOT NULL,
    coord_y INT NOT NULL,
    pokemon_id INT NOT NULL,
    is_shiny BOOL NOT NULL,
    despawn_time timestamp with time zone NOT NULL,
    FOREIGN KEY (pokemon_id) REFERENCES pokemon(id)
)