INSERT INTO pokemon (name, number, image_name, description, height, weight, primary_type, secondary_type)
SELECT name, number, image_name, description, height, weight, primary_type, secondary_type
FROM   json_populate_recordset(null::pokemon,  -- use same table type
          json '[
                  {
                    "name": "Bulbasaur",
                    "number": 1,
                    "image_name": "bulbasaur",
                    "description": "Bulbasaur can be seen napping in bright sunlight. There is a seed on its back. By soaking up the sun''s rays, the seed grows progressively larger.",
                    "height": 0.7,
                    "weight": 6.9,
                    "primary_type": "Grass",
                    "secondary_type": "Poison"
                  },
                  {
                    "name": "Ivysaur",
                    "number": 2,
                    "image_name": "ivysaur",
                    "description": "There is a bud on this Pokémon''s back. To support its weight, Ivysaur''s legs and trunk grow thick and strong. If it starts spending more time lying in the sunlight, it''s a sign that the bud will bloom into a large flower soon.",
                    "height": 1.0,
                    "weight": 13.0,
                    "primary_type": "Grass",
                    "secondary_type": "Poison"
                  },
                  {
                    "name": "Venusaur",
                    "number": 3,
                    "image_name": "venusaur",
                    "description": "There is a large flower on Venusaur''s back. The flower is said to take on vivid colors if it gets plenty of nutrition and sunlight. The flower''s aroma soothes the emotions of people.",
                    "height": 2.0,
                    "weight": 100.0,
                    "primary_type": "Grass",
                    "secondary_type": "Poison"
                  },
                  {
                    "name": "Charmander",
                    "number": 4,
                    "image_name": "charmander",
                    "description": "The flame that burns at the tip of its tail is an indication of its emotions. The flame wavers when Charmander is enjoying itself. If the Pokémon becomes enraged, the flame burns fiercely.",
                    "height": 0.6,
                    "weight": 8.5,
                    "primary_type": "Fire"
                  },
                  {
                    "name": "Charmeleon",
                    "number": 5,
                    "image_name": "charmeleon",
                    "description": "Charmeleon mercilessly destroys its foes using its sharp claws. If it encounters a strong foe, it turns aggressive. In this excited state, the flame at the tip of its tail flares with a bluish white color.",
                    "height": 1.1,
                    "weight": 19.0,
                    "primary_type": "Fire"
                  },
                  {
                    "name": "Charizard",
                    "number": 6,
                    "image_name": "charizard",
                    "description": "Charizard flies around the sky in search of powerful opponents. It breathes fire of such great heat that it melts anything. However, it never turns its fiery breath on any opponent weaker than itself.",
                    "height": 1.7,
                    "weight": 90.5,
                    "primary_type": "Fire",
                    "secondary_type": "Flying"
                  },
                  {
                    "name": "Squirtle",
                    "number": 7,
                    "image_name": "squirtle",
                    "description": "Squirtle''s shell is not merely used for protection. The shell''s rounded shape and the grooves on its surface help minimize resistance in water, enabling this Pokémon to swim at high speeds.",
                    "height": 0.5,
                    "weight": 9.0,
                    "primary_type": "Water"
                  },
                  {
                    "name": "Wartortle",
                    "number": 8,
                    "image_name": "wartortle",
                    "description": "Its tail is large and covered with a rich, thick fur. The tail becomes increasingly deeper in color as Wartortle ages. The scratches on its shell are evidence of this Pokémon''s toughness as a battler.",
                    "height": 1.0,
                    "weight": 22.5,
                    "primary_type": "Water"
                  },
                  {
                    "name": "Blastoise",
                    "number": 9,
                    "image_name": "blastoise",
                    "description": "Blastoise has water spouts that protrude from its shell. The water spouts are very accurate. They can shoot bullets of water with enough accuracy to strike empty cans from a distance of over 160 feet.",
                    "height": 1.6,
                    "weight": 85.5,
                    "primary_type": "Water"
                  },
                  {
                    "name": "Caterpie",
                    "number": 10,
                    "image_name": "caterpie",
                    "description": "Caterpie has a voracious appetite. It can devour leaves bigger than its body right before your eyes. From its antenna, this Pokémon releases a terrifically strong odor.",
                    "height": 0.3,
                    "weight": 2.9,
                    "primary_type": "Bug"
                  },
                  {
                    "name": "Metapod",
                    "number": 11,
                    "image_name": "metapod",
                    "description": "The shell covering this Pokémon''s body is as hard as an iron slab. Metapod does not move very much. It stays still because it is preparing its soft innards for evolution inside the hard shell.",
                    "height": 0.7,
                    "weight": 9.9,
                    "primary_type": "Bug"
                  },
                  {
                    "name": "Butterfree",
                    "number": 12,
                    "image_name": "butterfree",
                    "description": "Butterfree has a superior ability to search for delicious honey from flowers. It can even search out, extract, and carry honey from flowers that are blooming over six miles from its nest.",
                    "height": 1.1,
                    "weight": 32.0,
                    "primary_type": "Bug",
                    "secondary_type": "Flying"
                  },
                  {
                    "name": "Weedle",
                    "number": 13,
                    "image_name": "weedle",
                    "description": "Weedle has an extremely acute sense of smell. It is capable of distinguishing its favorite kinds of leaves from those it dislikes just by sniffing with its big red proboscis (nose).",
                    "height": 0.3,
                    "weight": 3.2,
                    "primary_type": "Bug",
                    "secondary_type": "Poison"
                  },
                  {
                    "name": "Kakuna",
                    "number": 14,
                    "image_name": "kakuna",
                    "description": "Kakuna remains virtually immobile as it clings to a tree. However, on the inside, it is extremely busy as it prepares for its coming evolution. This is evident from how hot the shell becomes to the touch.",
                    "height": 0.6,
                    "weight": 10.0,
                    "primary_type": "Bug",
                    "secondary_type": "Poison"
                  },
                  {
                    "name": "Beedrill",
                    "number": 15,
                    "image_name": "beedrill",
                    "description": "Beedrill is extremely territorial. No one should ever approach its nest—this is for their own safety. If angered, they will attack in a furious swarm.",
                    "height": 1.0,
                    "weight": 29.5,
                    "primary_type": "Bug",
                    "secondary_type": "Poison"
                  },
                  {
                    "name": "Pidgey",
                    "number": 16,
                    "image_name": "pidgey",
                    "description": "Pidgey has an extremely sharp sense of direction. It is capable of unerringly returning home to its nest, however far it may be removed from its familiar surroundings.",
                    "height": 0.3,
                    "weight": 1.8,
                    "primary_type": "Normal",
                    "secondary_type": "Flying"
                  },
                  {
                    "name": "Pidgeotto",
                    "number": 17,
                    "image_name": "pidgeotto",
                    "description": "Pidgeotto claims a large area as its own territory. This Pokémon flies around, patrolling its living space. If its territory is violated, it shows no mercy in thoroughly punishing the foe with its sharp claws.",
                    "height": 1.1,
                    "weight": 30.0,
                    "primary_type": "Normal",
                    "secondary_type": "Flying"
                  },
                  {
                    "name": "Pidgeot",
                    "number": 18,
                    "image_name": "pidgeot",
                    "description": "This Pokémon has a dazzling plumage of beautifully glossy feathers. Many Trainers are captivated by the striking beauty of the feathers on its head, compelling them to choose Pidgeot as their Pokémon.",
                    "height": 1.5,
                    "weight": 39.5,
                    "primary_type": "Normal",
                    "secondary_type": "Flying"
                  },
                  {
                    "name": "Rattata",
                    "number": 19,
                    "image_name": "rattata",
                    "description": "Rattata is cautious in the extreme. Even while it is asleep, it constantly listens by moving its ears around. It is not picky about where it lives—it will make its nest anywhere.",
                    "height": 0.3,
                    "weight": 3.5,
                    "primary_type": "Normal"
                  },
                  {
                    "name": "Raticate",
                    "number": 20,
                    "image_name": "raticate",
                    "description": "Raticate''s sturdy fangs grow steadily. To keep them ground down, it gnaws on rocks and logs. It may even chew on the walls of houses.",
                    "height": 0.7,
                    "weight": 18.5,
                    "primary_type": "Normal"
                  },
                  {
                    "name": "Spearow",
                    "number": 21,
                    "image_name": "spearow",
                    "description": "Spearow has a very loud cry that can be heard over half a mile away. If its high, keening cry is heard echoing all around, it is a sign that they are warning each other of danger.",
                    "height": 0.3,
                    "weight": 2.0,
                    "primary_type": "Normal",
                    "secondary_type": "Flying"
                  },
                  {
                    "name": "Fearow",
                    "number": 22,
                    "image_name": "fearow",
                    "description": "Fearow is recognized by its long neck and elongated beak. They are conveniently shaped for catching prey in soil or water. It deftly moves its long and skinny beak to pluck prey.",
                    "height": 1.2,
                    "weight": 38.0,
                    "primary_type": "Normal",
                    "secondary_type": "Flying"
                  },
                  {
                    "name": "Ekans",
                    "number": 23,
                    "image_name": "ekans",
                    "description": "Ekans curls itself up in a spiral while it rests. Assuming this position allows it to quickly respond to a threat from any direction with a glare from its upraised head.",
                    "height": 2.0,
                    "weight": 6.9,
                    "primary_type": "Poison"
                  },
                  {
                    "name": "Arbok",
                    "number": 24,
                    "image_name": "arbok",
                    "description": "This Pokémon is terrifically strong in order to constrict things with its body. It can even flatten steel oil drums. Once Arbok wraps its body around its foe, escaping its crunching embrace is impossible.",
                    "height": 3.5,
                    "weight": 65.0,
                    "primary_type": "Poison"
                  },
                  {
                    "name": "Pikachu",
                    "number": 25,
                    "image_name": "pikachu",
                    "description": "Whenever Pikachu comes across something new, it blasts it with a jolt of electricity. If you come across a blackened berry, it''s evidence that this Pokémon mistook the intensity of its charge.",
                    "height": 0.4,
                    "weight": 6.0,
                    "primary_type": "Electric"
                  },
                  {
                    "name": "Raichu",
                    "number": 26,
                    "image_name": "raichu",
                    "description": "If the electrical sacs become excessively charged, Raichu plants its tail in the ground and discharges. Scorched patches of ground will be found near this Pokémon''s nest.",
                    "height": 0.8,
                    "weight": 30.0,
                    "primary_type": "Electric"
                  },
                  {
                    "name": "Sandshrew",
                    "number": 27,
                    "image_name": "sandshrew",
                    "description": "Sandshrew''s body is configured to absorb water without waste, enabling it to survive in an arid desert. This Pokémon curls up to protect itself from its enemies.",
                    "height": 0.6,
                    "weight": 12.0,
                    "primary_type": "Ground"
                  },
                  {
                    "name": "Sandslash",
                    "number": 28,
                    "image_name": "sandslash",
                    "description": "Sandslash''s body is covered by tough spikes, which are hardened sections of its hide. Once a year, the old spikes fall out, to be replaced with new spikes that grow out from beneath the old ones.",
                    "height": 1.0,
                    "weight": 29.5,
                    "primary_type": "Ground"
                  },
                  {
                    "name": "Nidoran♀",
                    "number": 29,
                    "image_name": "nidoran-f",
                    "description": "Nidoran♀ has barbs that secrete a powerful poison. They are thought to have developed as protection for this small-bodied Pokémon. When enraged, it releases a horrible toxin from its horn.",
                    "height": 0.4,
                    "weight": 7.0,
                    "primary_type": "Poison"
                  },
                  {
                    "name": "Nidorina",
                    "number": 30,
                    "image_name": "nidorina",
                    "description": "When Nidorina are with their friends or family, they keep their barbs tucked away to prevent hurting each other. This Pokémon appears to become nervous if separated from the others.",
                    "height": 0.8,
                    "weight": 20.0,
                    "primary_type": "Poison"
                  },
                  {
                    "name": "Nidoqueen",
                    "number": 31,
                    "image_name": "nidoqueen",
                    "description": "Nidoqueen''s body is encased in extremely hard scales. It is adept at sending foes flying with harsh tackles. This Pokémon is at its strongest when it is defending its young.",
                    "height": 1.3,
                    "weight": 60.0,
                    "primary_type": "Poison",
                    "secondary_type": "Ground"
                  },
                  {
                    "name": "Nidoran♂",
                    "number": 32,
                    "image_name": "nidoran-m",
                    "description": "Nidoran♂ has developed muscles for moving its ears. Thanks to them, the ears can be freely moved in any direction. Even the slightest sound does not escape this Pokémon''s notice.",
                    "height": 0.5,
                    "weight": 9.0,
                    "primary_type": "Poison"
                  },
                  {
                    "name": "Nidorino",
                    "number": 33,
                    "image_name": "nidorino",
                    "description": "Nidorino has a horn that is harder than a diamond. If it senses a hostile presence, all the barbs on its back bristle up at once, and it challenges the foe with all its might.",
                    "height": 0.9,
                    "weight": 19.5,
                    "primary_type": "Poison"
                  },
                  {
                    "name": "Nidoking",
                    "number": 34,
                    "image_name": "nidoking",
                    "description": "Nidoking''s thick tail packs enormously destructive power. With one swing, it can topple a metal transmission tower. Once this Pokémon goes on a rampage, there is no stopping it.",
                    "height": 1.4,
                    "weight": 62.0,
                    "primary_type": "Poison",
                    "secondary_type": "Ground"
                  },
                  {
                    "name": "Clefairy",
                    "number": 35,
                    "image_name": "clefairy",
                    "description": "On every night of a full moon, groups of this Pokémon come out to play. When dawn arrives, the tired Clefairy return to their quiet mountain retreats and go to sleep nestled up against each other.",
                    "height": 0.6,
                    "weight": 7.5,
                    "primary_type": "Fairy"
                  },
                  {
                    "name": "Clefable",
                    "number": 36,
                    "image_name": "clefable",
                    "description": "Clefable moves by skipping lightly as if it were flying using its wings. Its bouncy step lets it even walk on water. It is known to take strolls on lakes on quiet, moonlit nights.",
                    "height": 1.3,
                    "weight": 40.0,
                    "primary_type": "Fairy"
                  },
                  {
                    "name": "Vulpix",
                    "number": 37,
                    "image_name": "vulpix",
                    "description": "At the time of its birth, Vulpix has one white tail. The tail separates into six if this Pokémon receives plenty of love from its Trainer. The six tails become magnificently curled.",
                    "height": 0.6,
                    "weight": 9.9,
                    "primary_type": "Fire"
                  },
                  {
                    "name": "Ninetales",
                    "number": 38,
                    "image_name": "ninetales",
                    "description": "Ninetales casts a sinister light from its bright red eyes to gain total control over its foe''s mind. This Pokémon is said to live for a thousand years.",
                    "height": 1.1,
                    "weight": 19.9,
                    "primary_type": "Fire"
                  },
                  {
                    "name": "Jigglypuff",
                    "number": 39,
                    "image_name": "jigglypuff",
                    "description": "Jigglypuff''s vocal cords can freely adjust the wavelength of its voice. This Pokémon uses this ability to sing at precisely the right wavelength to make its foes most drowsy.",
                    "height": 0.5,
                    "weight": 5.5,
                    "primary_type": "Normal",
                    "secondary_type": "Fairy"
                  },
                  {
                    "name": "Wigglytuff",
                    "number": 40,
                    "image_name": "wigglytuff",
                    "description": "Wigglytuff has large, saucerlike eyes. The surfaces of its eyes are always covered with a thin layer of tears. If any dust gets in this Pokémon''s eyes, it is quickly washed away.",
                    "height": 1.0,
                    "weight": 12.0,
                    "primary_type": "Normal",
                    "secondary_type": "Fairy"
                  },
                  {
                    "name": "Zubat",
                    "number": 41,
                    "image_name": "zubat",
                    "description": "Zubat remains quietly unmoving in a dark spot during the bright daylight hours. It does so because prolonged exposure to the sun causes its body to become slightly burned.",
                    "height": 0.8,
                    "weight": 7.5,
                    "primary_type": "Poison",
                    "secondary_type": "Flying"
                  },
                  {
                    "name": "Golbat",
                    "number": 42,
                    "image_name": "golbat",
                    "description": "Golbat loves to drink the blood of living things. It is particularly active in the pitch black of night. This Pokémon flits around in the night skies, seeking fresh blood.",
                    "height": 1.6,
                    "weight": 55.0,
                    "primary_type": "Poison",
                    "secondary_type": "Flying"
                  },
                  {
                    "name": "Oddish",
                    "number": 43,
                    "image_name": "oddish",
                    "description": "During the daytime, Oddish buries itself in soil to absorb nutrients from the ground using its entire body. The more fertile the soil, the glossier its leaves become.",
                    "height": 0.5,
                    "weight": 5.4,
                    "primary_type": "Grass",
                    "secondary_type": "Poison"
                  },
                  {
                    "name": "Gloom",
                    "number": 44,
                    "image_name": "gloom",
                    "description": "Gloom releases a foul fragrance from the pistil of its flower. When faced with danger, the stench worsens. If this Pokémon is feeling calm and secure, it does not release its usual stinky aroma.",
                    "height": 0.8,
                    "weight": 8.6,
                    "primary_type": "Grass",
                    "secondary_type": "Poison"
                  },
                  {
                    "name": "Vileplume",
                    "number": 45,
                    "image_name": "vileplume",
                    "description": "Vileplume''s toxic pollen triggers atrocious allergy attacks. That''s why it is advisable never to approach any attractive flowers in a jungle, however pretty they may be.",
                    "height": 1.2,
                    "weight": 18.6,
                    "primary_type": "Grass",
                    "secondary_type": "Poison"
                  },
                  {
                    "name": "Paras",
                    "number": 46,
                    "image_name": "paras",
                    "description": "Paras has parasitic mushrooms growing on its back called tochukaso. They grow large by drawing nutrients from this Bug Pokémon host. They are highly valued as a medicine for extending life.",
                    "height": 0.3,
                    "weight": 5.4,
                    "primary_type": "Bug",
                    "secondary_type": "Grass"
                  },
                  {
                    "name": "Parasect",
                    "number": 47,
                    "image_name": "parasect",
                    "description": "Parasect is known to infest large trees en masse and drain nutrients from the lower trunk and roots. When an infested tree dies, they move onto another tree all at once.",
                    "height": 1.0,
                    "weight": 29.5,
                    "primary_type": "Bug",
                    "secondary_type": "Grass"
                  },
                  {
                    "name": "Venonat",
                    "number": 48,
                    "image_name": "venonat",
                    "description": "Venonat is said to have evolved with a coat of thin, stiff hair that covers its entire body for protection. It possesses large eyes that never fail to spot even minuscule prey.",
                    "height": 1.0,
                    "weight": 30.0,
                    "primary_type": "Bug",
                    "secondary_type": "Poison"
                  },
                  {
                    "name": "Venomoth",
                    "number": 49,
                    "image_name": "venomoth",
                    "description": "Venomoth is nocturnal—it is a Pokémon that only becomes active at night. Its favorite prey are small insects that gather around streetlights, attracted by the light in the darkness.",
                    "height": 1.5,
                    "weight": 12.5,
                    "primary_type": "Bug",
                    "secondary_type": "Poison"
                  },
                  {
                    "name": "Diglett",
                    "number": 50,
                    "image_name": "diglett",
                    "description": "Diglett are raised in most farms. The reason is simple— wherever this Pokémon burrows, the soil is left perfectly tilled for planting crops. This soil is made ideal for growing delicious vegetables.",
                    "height": 0.2,
                    "weight": 0.8,
                    "primary_type": "Ground"
                  },
                  {
                    "name": "Dugtrio",
                    "number": 51,
                    "image_name": "dugtrio",
                    "description": "Dugtrio are actually triplets that emerged from one body. As a result, each triplet thinks exactly like the other two triplets. They work cooperatively to burrow endlessly.",
                    "height": 0.7,
                    "weight": 33.3,
                    "primary_type": "Ground"
                  },
                  {
                    "name": "Meowth",
                    "number": 52,
                    "image_name": "meowth",
                    "description": "Meowth withdraws its sharp claws into its paws to slinkily sneak about without making any incriminating footsteps. For some reason, this Pokémon loves shiny coins that glitter with light.",
                    "height": 0.4,
                    "weight": 4.2,
                    "primary_type": "Normal"
                  },
                  {
                    "name": "Persian",
                    "number": 53,
                    "image_name": "persian",
                    "description": "Persian has six bold whiskers that give it a look of toughness. The whiskers sense air movements to determine what is in the Pokémon''s surrounding vicinity. It becomes docile if grabbed by the whiskers.",
                    "height": 1.0,
                    "weight": 32.0,
                    "primary_type": "Normal"
                  },
                  {
                    "name": "Psyduck",
                    "number": 54,
                    "image_name": "psyduck",
                    "description": "Psyduck uses a mysterious power. When it does so, this Pokémon generates brain waves that are supposedly only seen in sleepers. This discovery spurred controversy among scholars.",
                    "height": 0.8,
                    "weight": 19.6,
                    "primary_type": "Water"
                  },
                  {
                    "name": "Golduck",
                    "number": 55,
                    "image_name": "golduck",
                    "description": "The webbed flippers on its forelegs and hind legs and the streamlined body of Golduck give it frightening speed. This Pokémon is definitely much faster than even the most athletic swimmer.",
                    "height": 1.7,
                    "weight": 76.6,
                    "primary_type": "Water"
                  },
                  {
                    "name": "Mankey",
                    "number": 56,
                    "image_name": "mankey",
                    "description": "When Mankey starts shaking and its nasal breathing turns rough, it''s a sure sign that it is becoming angry. However, because it goes into a towering rage almost instantly, it is impossible for anyone to flee its wrath.",
                    "height": 0.5,
                    "weight": 28.0,
                    "primary_type": "Fighting"
                  },
                  {
                    "name": "Primeape",
                    "number": 57,
                    "image_name": "primeape",
                    "description": "When Primeape becomes furious, its blood circulation is boosted. In turn, its muscles are made even stronger. However, it also becomes much less intelligent at the same time.",
                    "height": 1.0,
                    "weight": 32.0,
                    "primary_type": "Fighting"
                  },
                  {
                    "name": "Growlithe",
                    "number": 58,
                    "image_name": "growlithe",
                    "description": "Growlithe has a superb sense of smell. Once it smells anything, this Pokémon won''t forget the scent, no matter what. It uses its advanced olfactory sense to determine the emotions of other living things.",
                    "height": 0.7,
                    "weight": 19.0,
                    "primary_type": "Fire"
                  },
                  {
                    "name": "Arcanine",
                    "number": 59,
                    "image_name": "arcanine",
                    "description": "Arcanine is known for its high speed. It is said to be capable of running over 6,200 miles in a single day and night. The fire that blazes wildly within this Pokémon''s body is its source of power.",
                    "height": 1.9,
                    "weight": 155.0,
                    "primary_type": "Fire"
                  },
                  {
                    "name": "Poliwag",
                    "number": 60,
                    "image_name": "poliwag",
                    "description": "Poliwag has a very thin skin. It is possible to see the Pokémon''s spiral innards right through the skin. Despite its thinness, however, the skin is also very flexible. Even sharp fangs bounce right off it.",
                    "height": 0.6,
                    "weight": 12.4,
                    "primary_type": "Water"
                  },
                  {
                    "name": "Poliwhirl",
                    "number": 61,
                    "image_name": "poliwhirl",
                    "description": "The surface of Poliwhirl''s body is always wet and slick with a slimy fluid. Because of this slippery covering, it can easily slip and slide out of the clutches of any enemy in battle.",
                    "height": 1.0,
                    "weight": 20.0,
                    "primary_type": "Water"
                  },
                  {
                    "name": "Poliwrath",
                    "number": 62,
                    "image_name": "poliwrath",
                    "description": "Poliwrath''s highly developed, brawny muscles never grow fatigued, however much it exercises. It is so tirelessly strong, this Pokémon can swim back and forth across the ocean without effort.",
                    "height": 1.3,
                    "weight": 54.0,
                    "primary_type": "Water",
                    "secondary_type": "Fighting"
                  },
                  {
                    "name": "Abra",
                    "number": 63,
                    "image_name": "abra",
                    "description": "Abra sleeps for eighteen hours a day. However, it can sense the presence of foes even while it is sleeping. In such a situation, this Pokémon immediately teleports to safety.",
                    "height": 0.9,
                    "weight": 19.5,
                    "primary_type": "Psychic"
                  },
                  {
                    "name": "Kadabra",
                    "number": 64,
                    "image_name": "kadabra",
                    "description": "Kadabra emits a peculiar alpha wave if it develops a headache. Only those people with a particularly strong psyche can hope to become a Trainer of this Pokémon.",
                    "height": 1.3,
                    "weight": 56.5,
                    "primary_type": "Psychic"
                  },
                  {
                    "name": "Alakazam",
                    "number": 65,
                    "image_name": "alakazam",
                    "description": "Alakazam''s brain continually grows, making its head far too heavy to support with its neck. This Pokémon holds its head up using its psychokinetic power instead.",
                    "height": 1.5,
                    "weight": 48.0,
                    "primary_type": "Psychic"
                  },
                  {
                    "name": "Machop",
                    "number": 66,
                    "image_name": "machop",
                    "description": "Machop''s muscles are special—they never get sore no matter how much they are used in exercise. This Pokémon has sufficient power to hurl a hundred adult humans.",
                    "height": 0.8,
                    "weight": 19.5,
                    "primary_type": "Fighting"
                  },
                  {
                    "name": "Machoke",
                    "number": 67,
                    "image_name": "machoke",
                    "description": "Machoke''s thoroughly toned muscles possess the hardness of steel. This Pokémon has so much strength, it can easily hold aloft a sumo wrestler on just one finger.",
                    "height": 1.5,
                    "weight": 70.5,
                    "primary_type": "Fighting"
                  },
                  {
                    "name": "Machamp",
                    "number": 68,
                    "image_name": "machamp",
                    "description": "Machamp has the power to hurl anything aside. However, trying to do any work requiring care and dexterity causes its arms to get tangled. This Pokémon tends to leap into action before it thinks.",
                    "height": 1.6,
                    "weight": 130.0,
                    "primary_type": "Fighting"
                  },
                  {
                    "name": "Bellsprout",
                    "number": 69,
                    "image_name": "bellsprout",
                    "description": "Bellsprout''s thin and flexible body lets it bend and sway to avoid any attack, however strong it may be. From its mouth, this Pokémon spits a corrosive fluid that melts even iron.",
                    "height": 0.7,
                    "weight": 4.0,
                    "primary_type": "Grass",
                    "secondary_type": "Poison"
                  },
                  {
                    "name": "Weepinbell",
                    "number": 70,
                    "image_name": "weepinbell",
                    "description": "Weepinbell has a large hook on its rear end. At night, the Pokémon hooks on to a tree branch and goes to sleep. If it moves around in its sleep, it may wake up to find itself on the ground.",
                    "height": 1.0,
                    "weight": 6.4,
                    "primary_type": "Grass",
                    "secondary_type": "Poison"
                  },
                  {
                    "name": "Victreebel",
                    "number": 71,
                    "image_name": "victreebel",
                    "description": "Victreebel has a long vine that extends from its head. This vine is waved and flicked about as if it were an animal to attract prey. When an unsuspecting prey draws near, this Pokémon swallows it whole.",
                    "height": 1.7,
                    "weight": 15.5,
                    "primary_type": "Grass",
                    "secondary_type": "Poison"
                  },
                  {
                    "name": "Tentacool",
                    "number": 72,
                    "image_name": "tentacool",
                    "description": "Tentacool''s body is largely composed of water. If it is removed from the sea, it dries up like parchment. If this Pokémon happens to become dehydrated, put it back into the sea.",
                    "height": 0.9,
                    "weight": 45.5,
                    "primary_type": "Water",
                    "secondary_type": "Poison"
                  },
                  {
                    "name": "Tentacruel",
                    "number": 73,
                    "image_name": "tentacruel",
                    "description": "Tentacruel has large red orbs on its head. The orbs glow before lashing the vicinity with a harsh ultrasonic blast. This Pokémon''s outburst creates rough waves around it.",
                    "height": 1.6,
                    "weight": 55.0,
                    "primary_type": "Water",
                    "secondary_type": "Poison"
                  },
                  {
                    "name": "Geodude",
                    "number": 74,
                    "image_name": "geodude",
                    "description": "The longer a Geodude lives, the more its edges are chipped and worn away, making it more rounded in appearance. However, this Pokémon''s heart will remain hard, craggy, and rough always.",
                    "height": 0.4,
                    "weight": 20.0,
                    "primary_type": "Rock",
                    "secondary_type": "Ground"
                  },
                  {
                    "name": "Graveler",
                    "number": 75,
                    "image_name": "graveler",
                    "description": "Graveler grows by feeding on rocks. Apparently, it prefers to eat rocks that are covered in moss. This Pokémon eats its way through a ton of rocks on a daily basis.",
                    "height": 1.0,
                    "weight": 105.0,
                    "primary_type": "Rock",
                    "secondary_type": "Ground"
                  },
                  {
                    "name": "Golem",
                    "number": 76,
                    "image_name": "golem",
                    "description": "Golem live up on mountains. If there is a large earthquake, these Pokémon will come rolling down off the mountains en masse to the foothills below.",
                    "height": 1.4,
                    "weight": 300.0,
                    "primary_type": "Rock",
                    "secondary_type": "Ground"
                  },
                  {
                    "name": "Ponyta",
                    "number": 77,
                    "image_name": "ponyta",
                    "description": "Ponyta is very weak at birth. It can barely stand up. This Pokémon becomes stronger by stumbling and falling to keep up with its parent.",
                    "height": 1.0,
                    "weight": 30.0,
                    "primary_type": "Fire"
                  },
                  {
                    "name": "Rapidash",
                    "number": 78,
                    "image_name": "rapidash",
                    "description": "Rapidash usually can be seen casually cantering in the fields and plains. However, when this Pokémon turns serious, its fiery manes flare and blaze as it gallops its way up to 150 mph.",
                    "height": 1.7,
                    "weight": 95.0,
                    "primary_type": "Fire"
                  },
                  {
                    "name": "Slowpoke",
                    "number": 79,
                    "image_name": "slowpoke",
                    "description": "Slowpoke uses its tail to catch prey by dipping it in water at the side of a river. However, this Pokémon often forgets what it''s doing and often spends entire days just loafing at water''s edge.",
                    "height": 1.2,
                    "weight": 36.0,
                    "primary_type": "Water",
                    "secondary_type": "Psychic"
                  },
                  {
                    "name": "Slowbro",
                    "number": 80,
                    "image_name": "slowbro",
                    "description": "Slowbro''s tail has a Shellder firmly attached with a bite. As a result, the tail can''t be used for fishing anymore. This causes Slowbro to grudgingly swim and catch prey instead.",
                    "height": 1.6,
                    "weight": 78.5,
                    "primary_type": "Water",
                    "secondary_type": "Psychic"
                  },
                  {
                    "name": "Magnemite",
                    "number": 81,
                    "image_name": "magnemite",
                    "description": "Magnemite attaches itself to power lines to feed on electricity. If your house has a power outage, check your circuit breakers. You may find a large number of this Pokémon clinging to the breaker box.",
                    "height": 0.3,
                    "weight": 6.0,
                    "primary_type": "Electric",
                    "secondary_type": "Steel"
                  },
                  {
                    "name": "Magneton",
                    "number": 82,
                    "image_name": "magneton",
                    "description": "Magneton emits a powerful magnetic force that is fatal to mechanical devices. As a result, large cities sound sirens to warn citizens of large-scale outbreaks of this Pokémon.",
                    "height": 1.0,
                    "weight": 60.0,
                    "primary_type": "Electric",
                    "secondary_type": "Steel"
                  },
                  {
                    "name": "Farfetch''d",
                    "number": 83,
                    "image_name": "farfetchd",
                    "description": "Farfetch''d is always seen with a stalk from a plant of some sort. Apparently, there are good stalks and bad stalks. This Pokémon has been known to fight with others over stalks.",
                    "height": 0.8,
                    "weight": 15.0,
                    "primary_type": "Normal",
                    "secondary_type": "Flying"
                  },
                  {
                    "name": "Doduo",
                    "number": 84,
                    "image_name": "doduo",
                    "description": "Doduo''s two heads never sleep at the same time. Its two heads take turns sleeping, so one head can always keep watch for enemies while the other one sleeps.",
                    "height": 1.4,
                    "weight": 39.2,
                    "primary_type": "Normal",
                    "secondary_type": "Flying"
                  },
                  {
                    "name": "Dodrio",
                    "number": 85,
                    "image_name": "dodrio",
                    "description": "Watch out if Dodrio''s three heads are looking in three separate directions. It''s a sure sign that it is on its guard. Don''t go near this Pokémon if it''s being wary—it may decide to peck you.",
                    "height": 1.8,
                    "weight": 85.2,
                    "primary_type": "Normal",
                    "secondary_type": "Flying"
                  },
                  {
                    "name": "Seel",
                    "number": 86,
                    "image_name": "seel",
                    "description": "Seel hunts for prey in the frigid sea underneath sheets of ice. When it needs to breathe, it punches a hole through the ice with the sharply protruding section of its head.",
                    "height": 1.1,
                    "weight": 90.0,
                    "primary_type": "Water"
                  },
                  {
                    "name": "Dewgong",
                    "number": 87,
                    "image_name": "dewgong",
                    "description": "Dewgong loves to snooze on bitterly cold ice. The sight of this Pokémon sleeping on a glacier was mistakenly thought to be a mermaid by a mariner long ago.",
                    "height": 1.7,
                    "weight": 120.0,
                    "primary_type": "Water",
                    "secondary_type": "Ice"
                  },
                  {
                    "name": "Grimer",
                    "number": 88,
                    "image_name": "grimer",
                    "description": "Grimer''s sludgy and rubbery body can be forced through any opening, however small it may be. This Pokémon enters sewer pipes to drink filthy wastewater.",
                    "height": 0.9,
                    "weight": 30.0,
                    "primary_type": "Poison"
                  },
                  {
                    "name": "Muk",
                    "number": 89,
                    "image_name": "muk",
                    "description": "From Muk''s body seeps a foul fluid that gives off a nose-bendingly horrible stench. Just one drop of this Pokémon''s body fluid can turn a pool stagnant and rancid.",
                    "height": 1.2,
                    "weight": 30.0,
                    "primary_type": "Poison"
                  },
                  {
                    "name": "Shellder",
                    "number": 90,
                    "image_name": "shellder",
                    "description": "At night, this Pokémon uses its broad tongue to burrow a hole in the seafloor sand and then sleep in it. While it is sleeping, Shellder closes its shell, but leaves its tongue hanging out.",
                    "height": 0.3,
                    "weight": 4.0,
                    "primary_type": "Water"
                  },
                  {
                    "name": "Cloyster",
                    "number": 91,
                    "image_name": "cloyster",
                    "description": "Cloyster is capable of swimming in the sea. It does so by swallowing water, then jetting it out toward the rear. This Pokémon shoots spikes from its shell using the same system.",
                    "height": 1.5,
                    "weight": 132.5,
                    "primary_type": "Water",
                    "secondary_type": "Ice"
                  },
                  {
                    "name": "Gastly",
                    "number": 92,
                    "image_name": "gastly",
                    "description": "Gastly is largely composed of gaseous matter. When exposed to a strong wind, the gaseous body quickly dwindles away. Groups of this Pokémon cluster under the eaves of houses to escape the ravages of wind.",
                    "height": 1.3,
                    "weight": 0.1,
                    "primary_type": "Ghost",
                    "secondary_type": "Poison"
                  },
                  {
                    "name": "Haunter",
                    "number": 93,
                    "image_name": "haunter",
                    "description": "Haunter is a dangerous Pokémon. If one beckons you while floating in darkness, you must never approach it. This Pokémon will try to lick you with its tongue and steal your life away.",
                    "height": 1.6,
                    "weight": 0.1,
                    "primary_type": "Ghost",
                    "secondary_type": "Poison"
                  },
                  {
                    "name": "Gengar",
                    "number": 94,
                    "image_name": "gengar",
                    "description": "Sometimes, on a dark night, your shadow thrown by a streetlight will suddenly and startlingly overtake you. It is actually a Gengar running past you, pretending to be your shadow.",
                    "height": 1.5,
                    "weight": 40.5,
                    "primary_type": "Ghost",
                    "secondary_type": "Poison"
                  },
                  {
                    "name": "Onix",
                    "number": 95,
                    "image_name": "onix",
                    "description": "Onix has a magnet in its brain. It acts as a compass so that this Pokémon does not lose direction while it is tunneling. As it grows older, its body becomes increasingly rounder and smoother.",
                    "height": 8.8,
                    "weight": 210.0,
                    "primary_type": "Rock",
                    "secondary_type": "Ground"
                  },
                  {
                    "name": "Drowzee",
                    "number": 96,
                    "image_name": "drowzee",
                    "description": "If your nose becomes itchy while you are sleeping, it''s a sure sign that one of these Pokémon is standing above your pillow and trying to eat your dream through your nostrils.",
                    "height": 1.0,
                    "weight": 32.4,
                    "primary_type": "Psychic"
                  },
                  {
                    "name": "Hypno",
                    "number": 97,
                    "image_name": "hypno",
                    "description": "Hypno holds a pendulum in its hand. The arcing movement and glitter of the pendulum lull the foe into a deep state of hypnosis. While this Pokémon searches for prey, it polishes the pendulum.",
                    "height": 1.6,
                    "weight": 75.6,
                    "primary_type": "Psychic"
                  },
                  {
                    "name": "Krabby",
                    "number": 98,
                    "image_name": "krabby",
                    "description": "Krabby live on beaches, burrowed inside holes dug into the sand. On sandy beaches with little in the way of food, these Pokémon can be seen squabbling with each other over territory.",
                    "height": 0.4,
                    "weight": 6.5,
                    "primary_type": "Water"
                  },
                  {
                    "name": "Kingler",
                    "number": 99,
                    "image_name": "kingler",
                    "description": "Kingler has an enormous, oversized claw. It waves this huge claw in the air to communicate with others. However, because the claw is so heavy, the Pokémon quickly tires.",
                    "height": 1.3,
                    "weight": 60.0,
                    "primary_type": "Water"
                  },
                  {
                    "name": "Voltorb",
                    "number": 100,
                    "image_name": "voltorb",
                    "description": "Voltorb was first sighted at a company that manufactures Poké Balls. The link between that sighting and the fact that this Pokémon looks very similar to a Poké Ball remains a mystery.",
                    "height": 0.5,
                    "weight": 10.4,
                    "primary_type": "Electric"
                  },
                  {
                    "name": "Electrode",
                    "number": 101,
                    "image_name": "electrode",
                    "description": "Electrode eats electricity in the atmosphere. On days when lightning strikes, you can see this Pokémon exploding all over the place from eating too much electricity.",
                    "height": 1.2,
                    "weight": 66.6,
                    "primary_type": "Electric"
                  },
                  {
                    "name": "Exeggcute",
                    "number": 102,
                    "image_name": "exeggcute",
                    "description": "This Pokémon consists of six eggs that form a closely knit cluster. The six eggs attract each other and spin around. When cracks increasingly appear on the eggs, Exeggcute is close to evolution.",
                    "height": 0.4,
                    "weight": 2.5,
                    "primary_type": "Grass",
                    "secondary_type": "Psychic"
                  },
                  {
                    "name": "Exeggutor",
                    "number": 103,
                    "image_name": "exeggutor",
                    "description": "Exeggutor originally came from the tropics. Its heads steadily grow larger from exposure to strong sunlight. It is said that when the heads fall off, they group together to form Exeggcute.",
                    "height": 2.0,
                    "weight": 120.0,
                    "primary_type": "Grass",
                    "secondary_type": "Psychic"
                  },
                  {
                    "name": "Cubone",
                    "number": 104,
                    "image_name": "cubone",
                    "description": "Cubone pines for the mother it will never see again. Seeing a likeness of its mother in the full moon, it cries. The stains on the skull the Pokémon wears are made by the tears it sheds.",
                    "height": 0.4,
                    "weight": 6.5,
                    "primary_type": "Ground"
                  },
                  {
                    "name": "Marowak",
                    "number": 105,
                    "image_name": "marowak",
                    "description": "Marowak is the evolved form of a Cubone that has overcome its sadness at the loss of its mother and grown tough. This Pokémon''s tempered and hardened spirit is not easily broken.",
                    "height": 1.0,
                    "weight": 45.0,
                    "primary_type": "Ground"
                  },
                  {
                    "name": "Hitmonlee",
                    "number": 106,
                    "image_name": "hitmonlee",
                    "description": "Hitmonlee''s legs freely contract and stretch. Using these springlike legs, it bowls over foes with devastating kicks. After battle, it rubs down its legs and loosens the muscles to overcome fatigue.",
                    "height": 1.5,
                    "weight": 49.8,
                    "primary_type": "Fighting"
                  },
                  {
                    "name": "Hitmonchan",
                    "number": 107,
                    "image_name": "hitmonchan",
                    "description": "Hitmonchan is said to possess the spirit of a boxer who had been working toward a world championship. This Pokémon has an indomitable spirit and will never give up in the face of adversity.",
                    "height": 1.4,
                    "weight": 50.2,
                    "primary_type": "Fighting"
                  },
                  {
                    "name": "Lickitung",
                    "number": 108,
                    "image_name": "lickitung",
                    "description": "Whenever Lickitung comes across something new, it will unfailingly give it a lick. It does so because it memorizes things by texture and by taste. It is somewhat put off by sour things.",
                    "height": 1.2,
                    "weight": 65.5,
                    "primary_type": "Normal"
                  },
                  {
                    "name": "Koffing",
                    "number": 109,
                    "image_name": "koffing",
                    "description": "If Koffing becomes agitated, it raises the toxicity of its internal gases and jets them out from all over its body. This Pokémon may also overinflate its round body, then explode.",
                    "height": 0.6,
                    "weight": 1.0,
                    "primary_type": "Poison"
                  },
                  {
                    "name": "Weezing",
                    "number": 110,
                    "image_name": "weezing",
                    "description": "Weezing loves the gases given off by rotted kitchen garbage. This Pokémon will find a dirty, unkempt house and make it its home. At night, when the people in the house are asleep, it will go through the trash.",
                    "height": 1.2,
                    "weight": 9.5,
                    "primary_type": "Poison"
                  },
                  {
                    "name": "Rhyhorn",
                    "number": 111,
                    "image_name": "rhyhorn",
                    "description": "Rhyhorn runs in a straight line, smashing everything in its path. It is not bothered even if it rushes headlong into a block of steel. This Pokémon may feel some pain from the collision the next day, however.",
                    "height": 1.0,
                    "weight": 115.0,
                    "primary_type": "Ground",
                    "secondary_type": "Rock"
                  },
                  {
                    "name": "Rhydon",
                    "number": 112,
                    "image_name": "rhydon",
                    "description": "Rhydon''s horn can crush even uncut diamonds. One sweeping blow of its tail can topple a building. This Pokémon''s hide is extremely tough. Even direct cannon hits don''t leave a scratch.",
                    "height": 1.9,
                    "weight": 120.0,
                    "primary_type": "Ground",
                    "secondary_type": "Rock"
                  },
                  {
                    "name": "Chansey",
                    "number": 113,
                    "image_name": "chansey",
                    "description": "Chansey lays nutritionally excellent eggs on an everyday basis. The eggs are so delicious, they are easily and eagerly devoured by even those people who have lost their appetite.",
                    "height": 1.1,
                    "weight": 34.6,
                    "primary_type": "Normal"
                  },
                  {
                    "name": "Tangela",
                    "number": 114,
                    "image_name": "tangela",
                    "description": "Tangela''s vines snap off easily if they are grabbed. This happens without pain, allowing it to make a quick getaway. The lost vines are replaced by newly grown vines the very next day.",
                    "height": 1.0,
                    "weight": 35.0,
                    "primary_type": "Grass"
                  },
                  {
                    "name": "Kangaskhan",
                    "number": 115,
                    "image_name": "kangaskhan",
                    "description": "If you come across a young Kangaskhan playing by itself, you must never disturb it or attempt to catch it. The baby Pokémon''s parent is sure to be in the area, and it will become violently enraged at you.",
                    "height": 2.2,
                    "weight": 80.0,
                    "primary_type": "Normal"
                  },
                  {
                    "name": "Horsea",
                    "number": 116,
                    "image_name": "horsea",
                    "description": "Horsea eats small insects and moss off of rocks. If the ocean current turns fast, this Pokémon anchors itself by wrapping its tail around rocks or coral to prevent being washed away.",
                    "height": 0.4,
                    "weight": 8.0,
                    "primary_type": "Water"
                  },
                  {
                    "name": "Seadra",
                    "number": 117,
                    "image_name": "seadra",
                    "description": "Seadra sleeps after wriggling itself between the branches of coral. Those trying to harvest coral are occasionally stung by this Pokémon''s poison barbs if they fail to notice it.",
                    "height": 1.2,
                    "weight": 25.0,
                    "primary_type": "Water"
                  },
                  {
                    "name": "Goldeen",
                    "number": 118,
                    "image_name": "goldeen",
                    "description": "Goldeen is a very beautiful Pokémon with fins that billow elegantly in water. However, don''t let your guard down around this Pokémon—it could ram you powerfully with its horn.",
                    "height": 0.6,
                    "weight": 15.0,
                    "primary_type": "Water"
                  },
                  {
                    "name": "Seaking",
                    "number": 119,
                    "image_name": "seaking",
                    "description": "In the autumn, Seaking males can be seen performing courtship dances in riverbeds to woo females. During this season, this Pokémon''s body coloration is at its most beautiful.",
                    "height": 1.3,
                    "weight": 39.0,
                    "primary_type": "Water"
                  },
                  {
                    "name": "Staryu",
                    "number": 120,
                    "image_name": "staryu",
                    "description": "Staryu''s center section has an organ called the core that shines bright red. If you go to a beach toward the end of summer, the glowing cores of these Pokémon look like the stars in the sky.",
                    "height": 0.8,
                    "weight": 34.5,
                    "primary_type": "Water"
                  },
                  {
                    "name": "Starmie",
                    "number": 121,
                    "image_name": "starmie",
                    "description": "Starmie''s center section—the core—glows brightly in seven colors. Because of its luminous nature, this Pokémon has been given the nickname \"the gem of the sea.\"",
                    "height": 1.1,
                    "weight": 80.0,
                    "primary_type": "Water"
                  },
                  {
                    "name": "Mr. Mime",
                    "number": 122,
                    "image_name": "mr-mime",
                    "description": "Mr. Mime is a master of pantomime. Its gestures and motions convince watchers that something unseeable actually exists. Once the watchers are convinced, the unseeable thing exists as if it were real.",
                    "height": 1.3,
                    "weight": 54.5,
                    "primary_type": "Psychic"
                  },
                  {
                    "name": "Scyther",
                    "number": 123,
                    "image_name": "scyther",
                    "description": "Scyther is blindingly fast. Its blazing speed enhances the effectiveness of the twin scythes on its forearms. This Pokémon''s scythes are so effective, they can slice through thick logs in one wicked stroke.",
                    "height": 1.5,
                    "weight": 56.0,
                    "primary_type": "Bug",
                    "secondary_type": "Flying"
                  },
                  {
                    "name": "Jynx",
                    "number": 124,
                    "image_name": "jynx",
                    "description": "Jynx walks rhythmically, swaying and shaking its hips as if it were dancing. Its motions are so bouncingly alluring, people seeing it are compelled to shake their hips without giving any thought to what they are doing.",
                    "height": 1.4,
                    "weight": 40.6,
                    "primary_type": "Ice",
                    "secondary_type": "Psychic"
                  },
                  {
                    "name": "Electabuzz",
                    "number": 125,
                    "image_name": "electabuzz",
                    "description": "When a storm arrives, gangs of this Pokémon compete with each other to scale heights that are likely to be stricken by lightning bolts. Some towns use Electabuzz in place of lightning rods.",
                    "height": 1.1,
                    "weight": 30.0,
                    "primary_type": "Electric"
                  },
                  {
                    "name": "Magmar",
                    "number": 126,
                    "image_name": "magmar",
                    "description": "In battle, Magmar blows out intensely hot flames from all over its body to intimidate its opponent. This Pokémon''s fiery bursts create heat waves that ignite grass and trees in its surroundings.",
                    "height": 1.3,
                    "weight": 44.5,
                    "primary_type": "Fire"
                  },
                  {
                    "name": "Pinsir",
                    "number": 127,
                    "image_name": "pinsir",
                    "description": "Pinsir is astoundingly strong. It can grip a foe weighing twice its weight in its horns and easily lift it. This Pokémon''s movements turn sluggish in cold places.",
                    "height": 1.5,
                    "weight": 55.0,
                    "primary_type": "Bug"
                  },
                  {
                    "name": "Tauros",
                    "number": 128,
                    "image_name": "tauros",
                    "description": "This Pokémon is not satisfied unless it is rampaging at all times. If there is no opponent for Tauros to battle, it will charge at thick trees and knock them down to calm itself.",
                    "height": 1.4,
                    "weight": 88.4,
                    "primary_type": "Normal"
                  },
                  {
                    "name": "Magikarp",
                    "number": 129,
                    "image_name": "magikarp",
                    "description": "Magikarp is a pathetic excuse for a Pokémon that is only capable of flopping and splashing. This behavior prompted scientists to undertake research into it.",
                    "height": 0.9,
                    "weight": 10.0,
                    "primary_type": "Water"
                  },
                  {
                    "name": "Gyarados",
                    "number": 130,
                    "image_name": "gyarados",
                    "description": "When Magikarp evolves into Gyarados, its brain cells undergo a structural transformation. It is said that this transformation is to blame for this Pokémon''s wildly violent nature.",
                    "height": 6.5,
                    "weight": 235.0,
                    "primary_type": "Water",
                    "secondary_type": "Flying"
                  },
                  {
                    "name": "Lapras",
                    "number": 131,
                    "image_name": "lapras",
                    "description": "People have driven Lapras almost to the point of extinction. In the evenings, this Pokémon is said to sing plaintively as it seeks what few others of its kind still remain.",
                    "height": 2.5,
                    "weight": 220.0,
                    "primary_type": "Water",
                    "secondary_type": "Ice"
                  },
                  {
                    "name": "Ditto",
                    "number": 132,
                    "image_name": "ditto",
                    "description": "Ditto rearranges its cell structure to transform itself into other shapes. However, if it tries to transform itself into something by relying on its memory, this Pokémon manages to get details wrong.",
                    "height": 0.3,
                    "weight": 4.0,
                    "primary_type": "Normal"
                  },
                  {
                    "name": "Eevee",
                    "number": 133,
                    "image_name": "eevee",
                    "description": "Eevee has an unstable genetic makeup that suddenly mutates due to the environment in which it lives. Radiation from various stones causes this Pokémon to evolve.",
                    "height": 0.3,
                    "weight": 6.5,
                    "primary_type": "Normal"
                  },
                  {
                    "name": "Vaporeon",
                    "number": 134,
                    "image_name": "vaporeon",
                    "description": "Vaporeon underwent a spontaneous mutation and grew fins and gills that allow it to live underwater. This Pokémon has the ability to freely control water.",
                    "height": 1.0,
                    "weight": 29.0,
                    "primary_type": "Water"
                  },
                  {
                    "name": "Jolteon",
                    "number": 135,
                    "image_name": "jolteon",
                    "description": "Jolteon''s cells generate a low level of electricity. This power is amplified by the static electricity of its fur, enabling the Pokémon to drop thunderbolts. The bristling fur is made of electrically charged needles.",
                    "height": 0.8,
                    "weight": 24.5,
                    "primary_type": "Electric"
                  },
                  {
                    "name": "Flareon",
                    "number": 136,
                    "image_name": "flareon",
                    "description": "Flareon''s fluffy fur has a functional purpose—it releases heat into the air so that its body does not get excessively hot. This Pokémon''s body temperature can rise to a maximum of 1,650 degrees Fahrenheit.",
                    "height": 0.9,
                    "weight": 25.0,
                    "primary_type": "Fire"
                  },
                  {
                    "name": "Porygon",
                    "number": 137,
                    "image_name": "porygon",
                    "description": "Porygon is capable of reverting itself entirely back to program data and entering cyberspace. This Pokémon is copy protected so it cannot be duplicated by copying.",
                    "height": 0.8,
                    "weight": 36.5,
                    "primary_type": "Normal"
                  },
                  {
                    "name": "Omanyte",
                    "number": 138,
                    "image_name": "omanyte",
                    "description": "Omanyte is one of the ancient and long-since-extinct Pokémon that have been regenerated from fossils by people. If attacked by an enemy, it withdraws itself inside its hard shell.",
                    "height": 0.4,
                    "weight": 7.5,
                    "primary_type": "Rock",
                    "secondary_type": "Water"
                  },
                  {
                    "name": "Omastar",
                    "number": 139,
                    "image_name": "omastar",
                    "description": "Omastar uses its tentacles to capture its prey. It is believed to have become extinct because its shell grew too large and heavy, causing its movements to become too slow and ponderous.",
                    "height": 1.0,
                    "weight": 35.0,
                    "primary_type": "Rock",
                    "secondary_type": "Water"
                  },
                  {
                    "name": "Kabuto",
                    "number": 140,
                    "image_name": "kabuto",
                    "description": "Kabuto is a Pokémon that has been regenerated from a fossil. However, in extremely rare cases, living examples have been discovered. The Pokémon has not changed at all for 300 million years.",
                    "height": 0.5,
                    "weight": 11.5,
                    "primary_type": "Rock",
                    "secondary_type": "Water"
                  },
                  {
                    "name": "Kabutops",
                    "number": 141,
                    "image_name": "kabutops",
                    "description": "Kabutops swam underwater to hunt for its prey in ancient times. The Pokémon was apparently evolving from being a water dweller to living on land as evident from the beginnings of change in its gills and legs.",
                    "height": 1.3,
                    "weight": 40.5,
                    "primary_type": "Rock",
                    "secondary_type": "Water"
                  },
                  {
                    "name": "Aerodactyl",
                    "number": 142,
                    "image_name": "aerodactyl",
                    "description": "Aerodactyl is a Pokémon from the age of dinosaurs. It was regenerated from genetic material extracted from amber. It is imagined to have been the king of the skies in ancient times.",
                    "height": 1.8,
                    "weight": 59.0,
                    "primary_type": "Rock",
                    "secondary_type": "Flying"
                  },
                  {
                    "name": "Snorlax",
                    "number": 143,
                    "image_name": "snorlax",
                    "description": "Snorlax''s typical day consists of nothing more than eating and sleeping. It is such a docile Pokémon that there are children who use its expansive belly as a place to play.",
                    "height": 2.1,
                    "weight": 460.0,
                    "primary_type": "Normal"
                  },
                  {
                    "name": "Articuno",
                    "number": 144,
                    "image_name": "articuno",
                    "description": "Articuno is a legendary bird Pokémon that can control ice. The flapping of its wings chills the air. As a result, it is said that when this Pokémon flies, snow will fall.",
                    "height": 1.7,
                    "weight": 55.4,
                    "primary_type": "Ice",
                    "secondary_type": "Flying"
                  },
                  {
                    "name": "Zapdos",
                    "number": 145,
                    "image_name": "zapdos",
                    "description": "Zapdos is a legendary bird Pokémon that has the ability to control electricity. It usually lives in thunderclouds. The Pokémon gains power if it is stricken by lightning bolts.",
                    "height": 1.6,
                    "weight": 52.6,
                    "primary_type": "Electric",
                    "secondary_type": "Flying"
                  },
                  {
                    "name": "Moltres",
                    "number": 146,
                    "image_name": "moltres",
                    "description": "Moltres is a legendary bird Pokémon that has the ability to control fire. If this Pokémon is injured, it is said to dip its body in the molten magma of a volcano to burn and heal itself.",
                    "height": 2.0,
                    "weight": 60.0,
                    "primary_type": "Fire",
                    "secondary_type": "Flying"
                  },
                  {
                    "name": "Dratini",
                    "number": 147,
                    "image_name": "dratini",
                    "description": "Dratini continually molts and sloughs off its old skin. It does so because the life energy within its body steadily builds to reach uncontrollable levels.",
                    "height": 1.8,
                    "weight": 3.3,
                    "primary_type": "Dragon"
                  },
                  {
                    "name": "Dragonair",
                    "number": 148,
                    "image_name": "dragonair",
                    "description": "Dragonair stores an enormous amount of energy inside its body. It is said to alter weather conditions in its vicinity by discharging energy from the crystals on its neck and tail.",
                    "height": 4.0,
                    "weight": 16.5,
                    "primary_type": "Dragon"
                  },
                  {
                    "name": "Dragonite",
                    "number": 149,
                    "image_name": "dragonite",
                    "description": "Dragonite is capable of circling the globe in just 16 hours. It is a kindhearted Pokémon that leads lost and foundering ships in a storm to the safety of land.",
                    "height": 2.2,
                    "weight": 210.0,
                    "primary_type": "Dragon",
                    "secondary_type": "Flying"
                  },
                  {
                    "name": "Mewtwo",
                    "number": 150,
                    "image_name": "mewtwo",
                    "description": "Mewtwo is a Pokémon that was created by genetic manipulation. However, even though the scientific power of humans created this Pokémon''s body, they failed to endow Mewtwo with a compassionate heart.",
                    "height": 2.0,
                    "weight": 122.0,
                    "primary_type": "Psychic"
                  },
                  {
                    "name": "Mew",
                    "number": 151,
                    "image_name": "mew",
                    "description": "Mew is said to possess the genetic composition of all Pokémon. It is capable of making itself invisible at will, so it entirely avoids notice even if it approaches people.",
                    "height": 0.4,
                    "weight": 4.0,
                    "primary_type": "Psychic"
                  }
                ]');