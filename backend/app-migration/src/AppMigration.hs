{-# LANGUAGE TemplateHaskell #-}

module AppMigration (runAll, runAllMigrations, useDb, dbSettingsFromMap) where

import CustomPrelude
import qualified Data.Map.Strict as Map
import Database (DBSettings (..), compileWithDb)
import Database.PostgreSQL.Typed
import Language.Haskell.TH (DecsQ)
import Migration
import Resource (getResource, getResourceDir)

runAll :: IO ()
runAll =
  migrationResources >>= Migration.runInteractive dbSettingsFromMap

runAllMigrations :: PGConnection -> IO (MigrationResult String)
runAllMigrations conn = do
  migrationResources >>= Migration.runInTransaction conn

migrationResources :: IO [MigrationCommand]
migrationResources =
  (<$> migrations) . modifyFilePaths . getResource <$> $(getResourceDir)

migrations :: [MigrationCommand]
migrations =
  fmap
    fromMigrationsFile
    [ "create_users_pokemon_dex_entry",
      "create_tiles",
      "seed_pokemon",
      "create_areas",
      "add_position_to_user",
      "create_encounter",
      "create_wild_pokemon"
    ]

dbSettingsFromMap :: Map String String -> DBSettings
dbSettingsFromMap m =
  DBSettings
    { dbHost = get id "DB_HOST" "localhost",
      dbPort = get id "DB_PORT" "5432",
      dbName = get fromString "DB_NAME" "app"
    }
  where
    get :: (String -> a) -> String -> a -> a
    get transform' key deflt = maybe deflt transform' (Map.lookup key m)

useDb :: DecsQ
useDb = compileWithDb dbSettingsFromMap
