# Backend for Pokemon App

The Job of this webserver is to determine which specific tile image should be chosen based on a given layout of tiletypes.
Moreover, it has the responsibility to keep the area images up to date,
overlaying and compositing the chosen tile images.

Plan for this server:
API has Websocket for movement on map (and later chat)
Gets the initial position of the player on startup, then caches that for speed until the player disconnects
-> the position gets saved to DB
Movement is rate-limited and triggers random events leading to
pokemon encounters.

API has GET dexEntry (all of user) and GET dexEntry/{n} for Pokedex View

API has GET area to get all areas (with picture, borders)
API has GET area/{id} to get detail of one area (tiles, encounters)
API has PUT area to create a new area and POST area to change an existing one.
Areas are saved independently of tiles
To get tiles of an area, the area border is used to limit the tiles queried.
Updates to an area trigger the image builder, updating the png if tiles were changed.

Authentication is possible via andrena auth or an internal auth system.

Monitoring via Prometheus and stuff? Visible via admin login on a specific /admin route.

Routing works over nginx, serving servant for /api Requests,
the monitoring stuff for /login and Elm for all others.

