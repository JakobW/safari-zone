module Main (main) where

import CustomPrelude (IO)
import Lib (startApp)

main :: IO ()
main = startApp
