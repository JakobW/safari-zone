{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module Api.AreaApi (areaApi, AreaApi) where

import Config (AppM)
import CustomPrelude
import qualified Data.Area as Area
import Data.WithId
import qualified Db.Repo.AreaRepo as Repo
import Servant
import qualified Db.Row

type AreaApi =
  Capture "areaId" Area.AreaId :> ReqBody '[JSON] Area.Area :> PutNoContent
    :<|> ReqBody '[JSON] Area.Area :> PostCreated '[JSON] Area.AreaId
    :<|> Get '[JSON] [WithId Area.AreaId Area.Area]

areaApi :: ServerT AreaApi AppM
areaApi = updateArea :<|> createArea :<|> getAllAreas

updateArea :: Area.AreaId -> Area.Area -> AppM NoContent
updateArea areaId area = Repo.updateArea areaId (Db.Row.toRow area) >> pure NoContent

createArea :: Area.Area -> AppM Area.AreaId
createArea area = do
  createdAreaId <- Repo.createArea (Db.Row.toRow area)
  case createdAreaId of
    Right areaId -> pure areaId
    Left _ -> throwIO err400 {errBody = "Failed to create area " <> encodeUtf8 (tlshow area)}

getAllAreas :: AppM [Area.AreaWithId]
getAllAreas = fmap Db.Row.fromRow <$> Repo.getAreas
