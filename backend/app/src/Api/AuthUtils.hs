module Api.AuthUtils (extractUserId) where

import qualified Auth
import Config (AppM)
import CustomPrelude
import Data.User (UserId)
import qualified Db.Repo.UserRepo as Repo
import Has
import Servant (err500)
import qualified Katip as K

extractUserId :: Auth.JWTUser -> AppM UserId
extractUserId =
  Repo.getUserOrCreateDefault >=>
    \case
      Left err -> do
        K.logLocM K.ErrorS (K.ls err)
        throwIO err500
      Right usr ->
        pure $ getTyped usr
