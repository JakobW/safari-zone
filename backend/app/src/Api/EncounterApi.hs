{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module Api.EncounterApi (EncounterApi, encounterApi) where

import Config (AppM)
import CustomPrelude
import qualified Data.Area as Area
import Data.Encounter
import qualified Db.Repo.EncounterRepo as Repo
import Servant
import qualified Db.Row

type EncounterApi =
  "area" :> Capture "areaId" Area.AreaId :> Get '[JSON] [Encounter]
    :<|> ReqBody '[JSON] [Encounter] :> Post '[JSON] ()
    :<|> ReqBody '[JSON] EncounterIds :> Delete '[JSON] ()

encounterApi :: ServerT EncounterApi AppM
encounterApi =
  getEncounters :<|> createOrUpdateEncounters :<|> deleteEncounter

getEncounters :: Area.AreaId -> AppM [Encounter]
getEncounters = fmap (fmap Db.Row.fromRow) . Repo.getEncounters

createOrUpdateEncounters :: [Encounter] -> AppM ()
createOrUpdateEncounters =
  traverse_ Repo.upsertEncounter . fmap Db.Row.toRow

deleteEncounter :: EncounterIds -> AppM ()
deleteEncounter =
  Repo.removeEncounter . Db.Row.toRow
