module Api.Flatten2D (flatten) where

import CustomPrelude
import Data.Coord
import qualified Util
import qualified Data.List.NonEmpty as NonEmpty

flatten :: forall a. (a -> Position) -> [a] -> ([Maybe a], Unit 'X)
flatten toPosition as =
  Util.sortAndGroupWith toPosition as
    |> fmap NonEmpty.head
    |> addMissingIntermediateElements mempty
    |> (,rowLength)
  where
    rowLength :: Unit 'X
    rowLength = as
      |> fmap (toPosition .> x)
      |> maximumMay
      |> maybe 0 (+ 1)
    encodePos :: Position -> Int32
    encodePos pos = (untag (y pos) * untag rowLength) + untag (x pos)
    nextPos :: Position -> Position
    nextPos pos =
      if x pos < rowLength - 1 then over #x (+ 1) pos else (set #x 0 . over #y (+ 1)) pos
    addMissingIntermediateElements :: Position -> [a] -> [Maybe a]
    addMissingIntermediateElements pos []
      | x pos == 0 = []
      | otherwise = Nothing <$ [(encodePos pos) .. (encodePos (mkCoordLike (rowLength - 1) (y pos)))]
    addMissingIntermediateElements pos (nextTile : otherTiles) =
      (Nothing <$ [(encodePos pos) .. (encodePos (toPosition nextTile) - 1)])
        ++ (Just nextTile : callNext)
      where
        callNext = addMissingIntermediateElements (nextPos $ toPosition nextTile) otherTiles