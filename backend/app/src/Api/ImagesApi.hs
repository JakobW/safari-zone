{-# LANGUAGE DataKinds #-}

module Api.ImagesApi (imagesApi, ImagesApi, optimizedAtlasUrl) where

import Config (AppM)
import CustomPrelude
import Data.Aeson (ToJSON)
import qualified Data.Tile as Tile
import qualified Db.Repo.TileRepo as Repo (getAllLayers)
import Servant
import qualified Tile.ImageGenerator

type ImagesApi =
  Get '[JSON] ImageMetadata

data ImageMetadata = ImageMetadata
  { atlasUrl :: Text,
    atlasTileSize :: Int,
    atlasWidth :: Int,
    tileMapUrls :: [Text]
  }
  deriving (Eq, Show, Generic, ToJSON)

imagesApi :: ServerT ImagesApi AppM
imagesApi =
  imageMetadata
    <. fmap (Tile.ImageGenerator.toImagePath .> cons '/')
    <. (=<<) (toList . Tile.genAllLayers)
    <$> Repo.getAllLayers

optimizedAtlasUrl :: IsString s => s
optimizedAtlasUrl = "/images/generated/tileset-result.png"

imageMetadata :: [Text] -> ImageMetadata
imageMetadata tileMapUrls =
  ImageMetadata
    { atlasUrl = optimizedAtlasUrl,
      atlasTileSize = 16,
      atlasWidth = 8,
      tileMapUrls = tileMapUrls
    }
