{-# LANGUAGE TypeFamilies, StrictData #-}

module Api.Models.DexEntry (DexEntry(..)) where

import CustomPrelude
import Data.Aeson
import Data.Pokemon
import qualified Db.Row
import Has

data DexEntry = DexEntry
  { number :: PokemonNumber,
    amountNormal :: AmountNormal,
    amountShiny :: AmountShiny,
    pokemonName :: PokemonName,
    imageName :: PokemonImageName
  }
  deriving (Show, Eq, Generic, ToJSON, FromJSON)

instance Db.Row.RowLike DexEntry where
  type AsRow DexEntry = '[PokemonNumber, AmountNormal, AmountShiny, PokemonName, PokemonImageName]
  fromRow = DexEntry . getTyped <*> getTyped <*> getTyped <*> getTyped <*> getTyped
  toRow d = (number d, amountNormal d, amountShiny d, pokemonName d, imageName d)