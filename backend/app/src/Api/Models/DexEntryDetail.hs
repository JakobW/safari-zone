{-# LANGUAGE TypeFamilies, TypeOperators, StrictData, UndecidableInstances #-}

module Api.Models.DexEntryDetail (DexEntryDetail (..)) where

import Api.Models.PokemonData
import CustomPrelude
import Data.Aeson
import Data.Pokemon
import qualified Db.Row
import Has

data DexEntryDetail = DexEntryDetail
  { pokemon :: PokemonData,
    amountNormal :: AmountNormal,
    amountShiny :: AmountShiny
  }
  deriving (Show, Eq, Generic, ToJSON, FromJSON)

instance Db.Row.RowLike DexEntryDetail where
  type AsRow DexEntryDetail = AmountNormal ': AmountShiny ': Db.Row.AsRow PokemonData
  fromRow = DexEntryDetail . Db.Row.fromRow <*> getTyped <*> getTyped
  toRow d =
    Db.Row.addTo
      (Db.Row.addTo (Db.Row.toRow (pokemon d)) (amountShiny d))
      (amountNormal d)
