module Api.Models.MoveRules (MoveRules(..)) where

import CustomPrelude
import Data.Aeson
import Data.Coord
import Tile.MoveRule

data MoveRules = MoveRules
  { rules :: [MoveRule],
    columns :: Unit 'X
  }
  deriving (Show, Eq, Generic, ToJSON, FromJSON)
