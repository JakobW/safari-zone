{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE TemplateHaskell #-}

module Api.Models.Pokemon
  ( Pokemon (..),
    PokemonRenderData(..)
  )
where

import CustomPrelude
import Data.Aeson (FromJSON, ToJSON)
import Data.Pokemon
import Data.WildPokemon
import qualified Db.Row
import Has

data PokemonRenderData = PokemonRenderData
  { name :: PokemonName,
    imageName :: PokemonImageName
  }
  deriving (Show, Eq, Generic, ToJSON, FromJSON)

data Pokemon = Pokemon
  { pokemonId :: PokemonId,
    name :: PokemonName,
    shinyStatus :: ShinyStatus,
    wildPokemonId :: WildPokemonId,
    despawnTime :: UTCTime
  }
  deriving (Show, Eq)

$(makeFieldLabelsNoPrefix ''PokemonRenderData)
$(makeFieldLabelsNoPrefix ''Pokemon)

instance Db.Row.RowLike PokemonRenderData where
  type AsRow PokemonRenderData = '[PokemonName, PokemonImageName]
  fromRow = PokemonRenderData <$> getTyped <*> getTyped
  toRow p = (p ^. #name, p ^. #imageName)

instance Db.Row.RowLike Pokemon where
  type AsRow Pokemon = '[PokemonId, PokemonName, ShinyStatus, WildPokemonId, UTCTime]
  fromRow = Pokemon <$> getTyped <*> getTyped <*> getTyped <*> getTyped <*> getTyped
  toRow p = (p ^. #pokemonId, p ^. #name, p ^. #shinyStatus, p ^. #wildPokemonId, p ^. #despawnTime)
