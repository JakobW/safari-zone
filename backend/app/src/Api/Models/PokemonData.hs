{-# LANGUAGE DataKinds, TypeFamilies, StrictData #-}

module Api.Models.PokemonData
  ( PokemonData (..),
    PokemonDataWithId,
  )
where

import CustomPrelude
import Data.Aeson (FromJSON, ToJSON)
import Data.Pokemon
import Data.WithId (WithId(..))
import qualified Db.Row
import Has

data PokemonData = PokemonData
  { number :: PokemonNumber,
    name :: PokemonName,
    primaryType :: PokemonType,
    secondaryType :: Maybe PokemonType,
    weight :: PokemonWeight,
    height :: PokemonHeight,
    description :: PokemonDescription,
    imageName :: PokemonImageName
  }
  deriving (Show, Eq, Generic, ToJSON, FromJSON)

type PokemonDataWithId = WithId PokemonId PokemonData

instance Db.Row.RowLike PokemonData where
  type AsRow PokemonData =
    '[PokemonNumber, PokemonName, PokemonType, Maybe PokemonType, PokemonWeight, PokemonHeight, PokemonDescription, PokemonImageName]

  fromRow = PokemonData . getTyped <*> getTyped <*> getTyped <*> getTyped <*> getTyped <*> getTyped <*> getTyped <*> getTyped

  toRow p = (number p, name p, primaryType p, secondaryType p, weight p, height p, description p, imageName p)