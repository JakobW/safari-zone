{-# LANGUAGE DataKinds #-}

module Api.Models.TileInfo (TileInfo (..), mkTileInfo, mkLayerInfo, LayerInfo (ground, overlay)) where

import qualified Api.Flatten2D as Flatten
import CustomPrelude
import Data.Aeson
import Data.Coord
import Data.Tile
import Has

data TileInfo = TileInfo
  { columns :: Unit 'X,
    tiles :: [OptionalTile]
  }
  deriving (Show, Eq, Generic, FromJSON, ToJSON)

data LayerInfo = LayerInfo
  { ground :: TileInfo,
    overlay :: TileInfo
  }
  deriving (Show, Eq, Generic, FromJSON, ToJSON)

mkLayerInfo :: [(Unit 'X, Unit 'Y, Tile, LayerType)] -> LayerInfo
mkLayerInfo =
  uncurry LayerInfo
    . join bimap mkTileInfo
    . partition ((== Ground) . view _4)

mkTileInfo :: HasAll '[Unit 'X, Unit 'Y, Tile] a => [a] -> TileInfo
mkTileInfo = helper . Flatten.flatten fst . fmap convert

convert :: HasAll '[Unit 'X, Unit 'Y, Tile] a => a -> (Position, Tile)
convert a = (mkCoordLike (getTyped a) (getTyped a), getTyped a)

helper :: ([Maybe (Position, Tile)], Unit 'X) -> TileInfo
helper (items, columnNo) =
  TileInfo
    { tiles = fmap (optTileFromMaybe . fmap snd) items,
      columns = columnNo
    }
