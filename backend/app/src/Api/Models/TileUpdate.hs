{-# LANGUAGE DataKinds #-}

module Api.Models.TileUpdate (TileUpdate (..), toPositionBasedMap, mkTileUpdate) where

import CustomPrelude
import Data.Aeson
import qualified Data.List.NonEmpty as NonEmpty
import Data.Coord
import Data.TileType (TileType)
import Util

data TileUpdate = TileUpdate
  { coordX :: Unit 'X,
    coordY :: Unit 'Y,
    tile :: TileType
  }
  deriving (Show, Eq, Generic, FromJSON, ToJSON)

mkTileUpdate :: Unit 'X -> Unit 'Y -> TileType -> TileUpdate
mkTileUpdate = TileUpdate

toPositionBasedMap :: [TileUpdate] -> Map Position TileType
toPositionBasedMap = fmap (snd . NonEmpty.head) . sortAndGroupToMap fst . fmap toPositionTuple
  where
    toPositionTuple update = (mkCoordLike (coordX update) (coordY update), tile update)
