module Api.Models.WebsocketMessages
  ( WebsocketIn (..),
    WebsocketOut (..),
    AuthenticationMessage (AuthenticationMessage),
    authToken,
    authProvider,
  )
where

import Api.Models.Pokemon (PokemonRenderData)
import qualified Auth
import CustomPrelude
import Data.Aeson
import Data.Direction
import Data.WildPokemon (WildPokemonId)
import qualified Websocket.PubSub.Channel.Movement as Movement
import qualified Websocket.PubSub.Channel.Pokemon as Pokemon

data WebsocketIn
  = MoveMessage Direction
  | EncounterMessage WildPokemonId
  | BallMessage
  | BaitMessage
  | RockMessage
  deriving (Generic, FromJSON, ToJSON)

data WebsocketOut
  = PokemonEncounterMessage PokemonRenderData
  | PokemonCaughtMessage
  | PokemonBrokeOutMessage
  | PokemonFledMessage
  | PlayerMovedMessage Movement.Msg
  | PokemonMessage Pokemon.Msg
  | EncounterTooFarAway
  | ErrorMessage Text
  deriving (Show, Eq, Generic, ToJSON, FromJSON)

data AuthenticationMessage = AuthenticationMessage
  { provider :: Auth.AuthProvider,
    token :: Text
  }
  deriving (Show, Eq, Generic, FromJSON, ToJSON)

authProvider :: AuthenticationMessage -> Auth.AuthProvider
authProvider = provider

authToken :: AuthenticationMessage -> ByteString
authToken = encodeUtf8 . token
