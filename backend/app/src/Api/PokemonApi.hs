{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}

module Api.PokemonApi (PokemonApi, pokemonApi) where

import qualified Api.AuthUtils as AuthUtils
import Api.Models.DexEntry
import Api.Models.DexEntryDetail
import Api.Models.PokemonData
import qualified Auth
import Config (AppM)
import CustomPrelude
import Data.Pokemon (PokemonId, PokemonNumber)
import qualified Db.Repo.DexEntryRepo as Repo
import qualified Db.Repo.PokemonRepo as Repo
import qualified Katip as K
import Servant
import qualified Db.Row

type PokemonApi =
  Auth.Protected
    :> ( Get '[JSON] [DexEntry]
           :<|> Capture "pokemonNumber" PokemonNumber :> Get '[JSON] PokemonDataWithId
           :<|> "user" :> Capture "pokemonId" PokemonId :> Get '[JSON] DexEntryDetail
           :<|> "pokemon" :> Get '[JSON] [PokemonDataWithId]
       )

pokemonApi :: ServerT PokemonApi AppM
pokemonApi (Auth.Authenticated usr) = getDexEntries usr :<|> getPokemon :<|> getDexEntryDetail usr :<|> getAllPokemon
pokemonApi _ = Auth.throwAll err401

getAllPokemon :: AppM [PokemonDataWithId]
getAllPokemon = fmap Db.Row.fromRow <$> Repo.findAllPoke

getPokemon :: PokemonNumber -> AppM PokemonDataWithId
getPokemon =
  Repo.findFirstWithNumber >=> handleMissing . fmap Db.Row.fromRow

getDexEntries :: Auth.JWTUser -> AppM [DexEntry]
getDexEntries =
  fmap (fmap Db.Row.fromRow) . Repo.getDexEntriesOfUser <=< AuthUtils.extractUserId

getDexEntryDetail :: Auth.JWTUser -> PokemonId -> AppM DexEntryDetail
getDexEntryDetail authUser pokeId = do
  userId <- AuthUtils.extractUserId authUser
  detail <- Repo.getDexEntryDetailOfUser userId pokeId
  Db.Row.fromRow <$> handleMissingEither detail

handleMissing :: Maybe a -> AppM a
handleMissing = \case
  Just a -> return a
  Nothing -> throwIO err404

handleMissingEither :: Either Text a -> AppM a
handleMissingEither = \case
  Left err -> K.logLocM K.WarningS (K.ls err) >> throwIO err404
  Right a -> return a
