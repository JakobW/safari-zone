{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module Api.TileApi
  ( TileApi,
    tileApi,
    getAllLayers,
    getAllTilesOfLayer,
    getAllTileTypes,
    updateTiles,
  )
where

import qualified Api.Flatten2D as Flatten
import qualified Api.Models.TileInfo as Model
import qualified Api.Models.TileUpdate as Model
import Config (AppM)
import CustomPrelude
import qualified Data.Coord as C
import qualified Data.Tile as Tile
import qualified Data.Tile.GameTile as GameTile
import Data.TileType as Tile (TileType, toTileType)
import qualified Db.Repo.TileRepo as Repo
import Has
import Servant
import qualified Tile.ImageGenerator as ImageGenerator
import qualified Tile.MoveRule as MoveRule
import qualified Tile.TileChooser as Chooser
import qualified Tile.TileTypes as TileTypes
import qualified Util
import qualified Data.Map.Strict as Map
import Api.Models.MoveRules

type TileApi =
  Get '[JSON] [Tile.TileType]
    :<|> "moveRules"
    :> Get '[JSON] MoveRules
      :<|> "layer"
    :> ( Get '[JSON] [Tile.LayerNumber]
           :<|> Capture "layerNumber" Tile.LayerNumber
             :> Get '[JSON] Model.LayerInfo
           :<|> Capture "layerNumber" Tile.LayerNumber
             :> ReqBody '[JSON] [Model.TileUpdate]
             :> PutNoContent
           :<|> Capture "layerNumber" Tile.LayerNumber
             :> ReqBody '[JSON] [C.Position]
             :> Delete '[JSON] ()
       )

tileApi :: ServerT TileApi AppM
tileApi =
  getAllTileTypes
    :<|> getMoveRules
    :<|> (getAllLayers :<|> getAllTilesOfLayer :<|> updateTiles :<|> deleteTiles)

getAllTileTypes :: AppM [Tile.TileType]
getAllTileTypes = pure $ toList Util.enumerate

getAllLayers :: AppM [Tile.LayerNumber]
getAllLayers = Repo.getAllLayers

getAllTilesOfLayer :: Tile.LayerNumber -> AppM Model.LayerInfo
getAllTilesOfLayer = fmap Model.mkLayerInfo . Repo.getAllTilesOfLayerOrdered

updateTiles :: Tile.LayerNumber -> [Model.TileUpdate] -> AppM NoContent
updateTiles layerNumber updatedTiles = do
  tiles <- Repo.getAllNonOverlayTilesOfLayer layerNumber
  let simplifiedTiles = fmap toTileUpdate tiles
      tileData = Chooser.executeTileResolvers $ Model.toPositionBasedMap (updatedTiles <> simplifiedTiles)
      diffTileData = Util.diff (fmap toTileData tiles) tileData
  ImageGenerator.generateAndWriteImagesOfAllTypesOfTileMap layerNumber tileData
  void $ Repo.deleteOverlayTilesFromLayer layerNumber
  void $ Repo.insertTiles $ fmap (toInsertableTile layerNumber) diffTileData
  pure NoContent
  where
    toTileData row = TileTypes.groundTileData (C.mkCoordLike (getTyped row) (getTyped row)) (Has.getTyped row)

toInsertableTile :: Tile.LayerNumber -> TileTypes.TileData c -> (C.Unit 'C.X, C.Unit 'C.Y, c, Tile.Layer)
toInsertableTile layerNumber tileData =
  ( tileData ^. #position % #x,
    tileData ^. #position % #y,
    tileData ^. #tile,
    Tile.mkLayer layerNumber (tileData ^. #layer)
  )

deleteTiles :: Tile.LayerNumber -> [C.Position] -> AppM ()
deleteTiles layerNumber positions = do
  tiles <- Repo.getAllNonOverlayTilesOfLayer layerNumber
  let simplifiedTiles = fmap toTileUpdate tiles
      positionsToDeleteMap = Map.fromList $ fmap (,()) positions
      positionToTileMap = Map.difference (Model.toPositionBasedMap simplifiedTiles) positionsToDeleteMap
      tileData = Chooser.executeTileResolvers positionToTileMap

  ImageGenerator.generateAndWriteImagesOfAllTypesOfTileMap layerNumber tileData
  void $ Repo.deleteOverlayTilesFromLayer layerNumber
  -- TODO need to calc difference between tiles and tileData -> optimize for less db requests
  Repo.deleteGroundTiles layerNumber positions
  Repo.insertTiles $ fmap (toInsertableTile layerNumber) tileData

toTileUpdate :: (C.Unit 'C.X, C.Unit 'C.Y, Tile.Tile) -> Model.TileUpdate
toTileUpdate (cx, cy, t) = Model.mkTileUpdate cx cy (Tile.toTileType t)

getMoveRules :: AppM MoveRules
getMoveRules =
  toMoveRules
    . Flatten.flatten GameTile.position
    . GameTile.convertToGameTiles
    <$> Repo.getAllTilesOrdered
  where
    toMoveRules (maybeGameTiles, columnNo) =
      MoveRules
        { rules = fmap (maybe MoveRule.Blocked MoveRule.getMoveRule) maybeGameTiles,
          columns = columnNo
        }
