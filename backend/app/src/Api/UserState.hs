{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

module Api.UserState
  ( move,
    UserState,
    initState,
    addEncounter,
    throwRock,
    throwBait,
    getCatchRate,
    getFleeRate,
    throwBall,
    Event (..),
    UserStateError,
    Action,
    userId,
    user,
    userPosition,
    isInteractable,
  )
where

import qualified Api.Models.Pokemon as Pokemon
import Control.Monad.State hiding (state)
import CustomPrelude
import Data.Coord
import qualified Data.Direction as Direction
import Data.Tile (Tile)
import qualified Data.User as User
import Db.Client (DBM)
import qualified Db.Repo.TileRepo as Repo
import qualified Random
import qualified Tile.MoveRule as MoveRule

data UserState = UserState
  { userData :: User.UserWithId,
    currentTile :: [Tile],
    encounter :: Maybe EncounterState
  }
  deriving (Show, Eq)

type WithUserState m = MonadState UserState m

data EncounterState = EncounterState
  { pokemon :: Pokemon.Pokemon,
    catchRate :: Float,
    fleeRate :: Float
  }
  deriving (Show, Eq)

data Event
  = EncounterFled
  | EncounterCaught Pokemon.Pokemon
  | NoEvent

$(makeFieldLabelsNoPrefix ''UserState)

$(makeFieldLabelsNoPrefix ''EncounterState)

userId :: Lens' UserState User.UserId
userId = #userData % #theId

user :: Lens' UserState User.User
user = #userData % #theContent

getCatchRate :: UserState -> Maybe Float
getCatchRate = (^? #encounter % _Just % #catchRate)

getFleeRate :: UserState -> Maybe Float
getFleeRate = (^? #encounter % _Just % #fleeRate)

data UserStateError
  = PositionNotFoundError
  | PositionBlockedError
  | NoEncounterError
  deriving (Show, Eq)

initState :: (DBM m) => User.UserWithId -> m (Either UserStateError UserState)
initState usr =
  (fmap toUserState . Repo.getTilesAtPosition) pos
  where
    pos = usr ^. #theContent % #position
    toUserState [] = Left PositionNotFoundError
    toUserState tiles =
      Right $
        UserState
          { userData = usr,
            currentTile = tiles,
            encounter = Nothing
          }

userPosition :: Lens' UserState Position
userPosition = user % #position

move :: (DBM m, WithUserState m) => Direction.Direction -> m (Either UserStateError ())
move direction = do
  userState <- get
  let newPos = Direction.moveIn direction (view userPosition userState)
  tilesAtPosition <- Repo.getTilesAtPosition newPos
  case MoveRule.getMoveRule tilesAtPosition of
    MoveRule.Blocked -> pure $ Left PositionBlockedError
    _ -> do
      let setUserPosition = set userPosition newPos
          setTiles = set #currentTile tilesAtPosition
          updateState = setUserPosition . setTiles
      modify updateState
      pure $ Right ()

isInteractable :: Position -> UserState -> Bool
isInteractable pos state =
  distance pos (state ^. userPosition) <= (1 :: Int32)

addEncounter :: Pokemon.Pokemon -> UserState -> UserState
addEncounter p = set #encounter (Just $ initEncounter p)

initEncounter :: Pokemon.Pokemon -> EncounterState
initEncounter p = EncounterState {pokemon = p, catchRate = 0.3, fleeRate = 0.2}

throwRock :: (Random.RandM m, WithUserState m) => m (Either UserStateError Event)
throwRock = do
  modify (over #encounter $ fmap rock)
  encounterOnlyAction possiblyFlee

throwBait :: (Random.RandM m, WithUserState m) => m (Either UserStateError Event)
throwBait = do
  modify (over #encounter $ fmap bait)
  encounterOnlyAction possiblyFlee

rock :: EncounterState -> EncounterState
rock = modifyCatchRate (* 2) . modifyFleeRate (* 1.5)

bait :: EncounterState -> EncounterState
bait = modifyCatchRate (* 0.75) . modifyFleeRate (* 0.5)

modifyCatchRate :: (Float -> Float) -> EncounterState -> EncounterState
modifyCatchRate f = over #catchRate (clamp 0 1 . f)

modifyFleeRate :: (Float -> Float) -> EncounterState -> EncounterState
modifyFleeRate f = over #fleeRate (clamp 0 1 . f)

possiblyFlee :: (Random.RandM m, WithUserState m) => EncounterState -> m Event
possiblyFlee enc =
  Random.withCoinFlip (enc ^. #fleeRate) $
    \case
      Random.Heads -> modify flee $> EncounterFled
      Random.Tails -> pure NoEvent

flee :: UserState -> UserState
flee = set #encounter Nothing

throwBall :: (Random.RandM m, WithUserState m) => m (Either UserStateError Event)
throwBall =
  encounterOnlyAction $ \enc ->
    Random.withCoinFlip (enc ^. #catchRate) $
      \case
        Random.Heads -> do
          modify flee $> EncounterCaught (enc ^. #pokemon)
        Random.Tails -> possiblyFlee enc

type WithEvent m = m (UserState, Maybe Event)

type Action m = UserState -> Either UserStateError (WithEvent m)

encounterOnlyAction :: MonadState UserState m => (EncounterState -> m a) -> m (Either UserStateError a)
encounterOnlyAction action =
  gets encounter
    >>= \case
      Just enc -> Right <$> action enc
      Nothing -> pure $ Left NoEncounterError

clamp :: (Ord a) => a -> a -> a -> a
clamp mn mx = max mn . min mx
