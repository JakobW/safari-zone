{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module Api.Websocket (WebsocketApi, websocketApi, WebsocketIn (..)) where

import Api.Models.WebsocketMessages
import qualified Api.UserState as UserState
import qualified Api.WebsocketMonad as WSM
import qualified Auth
import Config (AppM, jwtSettings)
import qualified Control.Monad.State as State
import CustomPrelude
import Data.Aeson
import qualified Data.User as User
import qualified Db.Repo.DexEntryRepo as Repo (addPokemonToUser)
import qualified Db.Repo.UserRepo as Repo (getUserOrCreateDefault, setUserPosition)
import qualified Db.Repo.WildPokemonRepo as Repo (getAllWildPokemon, getById)
import qualified Db.Row
import qualified Katip as K
import Network.WebSockets as WS
import Servant
import qualified Servant.API.WebSocket as SWS
import qualified Util.Either as Either
import qualified Websocket.PubSub.Channel.Movement as Movement
import qualified Websocket.PubSub.Channel.Pokemon as Pokemon
import qualified Websocket.PubSub.Types as PubSub

type WebsocketApi = "websocket" :> SWS.WebSocketPending

websocketApi :: ServerT WebsocketApi AppM
websocketApi = acceptConnection
  .> flip catchAny (K.showLS .> K.logLocM K.ErrorS)

acceptConnection :: WS.PendingConnection -> AppM ()
acceptConnection pc = do
  conn <- liftIO $ WS.acceptRequest pc
  userOrErr <- authenticateUser conn
  case userOrErr of
    Left err ->
      liftIO $ WS.sendClose conn err
    Right user -> do
      initialState <- UserState.initState user
      case initialState of
        Left err -> do
          K.logLocM K.WarningS (K.showLS err)
          liftIO $ WS.sendClose conn (tshow err)
        Right userState -> do
          WSM.sendData conn <| userState ^. UserState.user % #position
          mUserState <- newIORef userState
          WSM.runInAppM mUserState (websocketApp conn)

websocketApp :: WS.Connection -> WSM.WsMonad ()
websocketApp conn =
  withSubscriptions <| do
    Repo.getAllWildPokemon
      >>= fmap (Db.Row.fromRow .> Pokemon.NewEncounterMsg .> PokemonMessage)
      .> traverse_ (WSM.sendData conn)
    withRunInIO $ \runInIO ->
      WS.withPingThread conn 30 (return ())
        <| runInIO mainWork
  where
    withSubscriptions =
      PubSub.withSubscription Movement.Channel (movementSub conn)
        .> PubSub.withSubscription Pokemon.Channel (pokemonSub conn)
    mainWork =
      forever
        ( liftIO (WS.receiveData conn) >>= handleData >>= traverse_ (WSM.sendData conn)
        )
        `catch` handleClose

movementSub :: WS.Connection -> Movement.Msg -> WSM.WsMonad ()
movementSub conn msg = do
  usrId <- State.gets (view UserState.userId)
  when (Movement.id msg /= usrId) $
    WSM.sendData conn (PlayerMovedMessage msg)

pokemonSub :: WS.Connection -> Pokemon.Msg -> WSM.WsMonad ()
pokemonSub conn = do
  WSM.sendData conn . PokemonMessage

authenticateUser :: WS.Connection -> AppM (Either Text User.UserWithId)
authenticateUser conn = do
  authResult <- liftIO $ eitherDecode <$> WS.receiveData conn
  case authResult of
    Left err -> do
      K.logLocM K.WarningS ("Failed to decode authorization: " <> K.ls err) $> Left "Unauthorized"
    Right auth -> do
      settings <- asks Config.jwtSettings
      verified <- Auth.jwtToUser settings (authProvider auth) (authToken auth)
      case verified of
        Nothing -> do
          K.logLocM K.WarningS ("Login attempt unsuccessful: " <> K.showLS auth) $> Left "Unauthorized"
        Just usr -> do
          userResult <- Repo.getUserOrCreateDefault usr
          case Db.Row.fromRow <$> userResult of
            Left err -> do
              K.logLocM K.ErrorS (K.ls err) $> Left err
            Right user -> pure $ Right user

handleData :: LByteString -> WSM.WsMonad (Maybe WebsocketOut)
handleData msg = withDefault (toWebSocketIn msg) handleError handleWebsocketIn
  where
    handleError :: Text -> Maybe WebsocketOut
    handleError = Just . ErrorMessage

toWebSocketIn :: LByteString -> Either Text WebsocketIn
toWebSocketIn = Either.mapLeft pack . eitherDecode

withDefault :: (Applicative m) => Either a b -> (a -> c) -> (b -> m c) -> m c
withDefault (Left a) mapA _ = pure $ mapA a
withDefault (Right b) _ mapB = mapB b

handleWebsocketIn :: WebsocketIn -> WSM.WsMonad (Maybe WebsocketOut)
handleWebsocketIn = \case
  MoveMessage direction -> do
    moveResult <- UserState.move direction
    case moveResult of
      Right _ -> do
        usr <- State.get
        _ <- PubSub.publishM Movement.Channel (Movement.Msg (usr ^. UserState.userId) (usr ^. UserState.user % #position))
        pure Nothing
      Left err -> pure (Just $ ErrorMessage $ tshow err)
  EncounterMessage encounterId -> do
    encounter <- Repo.getById encounterId
    case encounter of
      Right enc -> do
        state <- State.get
        if UserState.isInteractable (Db.Row.fromRow enc) state then do
          Db.Row.addTo enc encounterId |> Db.Row.fromRow |> UserState.addEncounter |> State.modify
          pure <| Just <| PokemonEncounterMessage <| Db.Row.fromRow enc
        else pure <| Just <| EncounterTooFarAway
      Left err -> do
        K.logLocM K.WarningS <| K.ls err
        pure <| Just <| ErrorMessage err

  BaitMessage ->
    UserState.throwBait >>= handleEvent
  BallMessage ->
    UserState.throwBall >>= handleEvent
  RockMessage ->
    UserState.throwRock >>= handleEvent

handleEvent :: Either UserState.UserStateError UserState.Event -> WSM.WsMonad (Maybe WebsocketOut)
handleEvent = \case
  Left err ->
    pure $ Just $ ErrorMessage $ tshow err
  Right UserState.EncounterFled ->
    pure $ Just PokemonFledMessage
  Right (UserState.EncounterCaught pokemon) -> do
    userId <- State.gets (^. UserState.userId)
    Repo.addPokemonToUser userId (pokemon ^. #pokemonId) (pokemon ^. #shinyStatus)
    pure $ Just PokemonCaughtMessage
  Right UserState.NoEvent ->
    pure $ Just PokemonBrokeOutMessage

handleClose :: WS.ConnectionException -> WSM.WsMonad ()
handleClose e = do
  state <- State.get
  _ <- Repo.setUserPosition (state ^. UserState.userId) (state ^. UserState.userPosition % to Db.Row.toRow)
  K.logLocM K.InfoS ("User " <> K.showLS (state ^. UserState.userId) <> " disconnected: " <> K.showLS e)
