{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE UndecidableInstances #-}

module Api.WebsocketMonad (runInAppM, WsMonad, sendData) where

import Api.UserState
import qualified Config
import Control.Monad.State hiding (mapM_, state)
import CustomPrelude
import Data.Aeson
import qualified Database.PostgreSQL.Typed as PG
import Db.Client
import qualified Katip as K
import Logger
import Network.WebSockets as WS
import qualified Random
import qualified Websocket.PubSub.Types as PubSub

data WsState = WsState
  { runDb :: RunDb,
    userState :: IORef UserState,
    randomGen :: Random.RandGen,
    pubSub :: PubSub.SomePubSub (LogM IO)
  }

$(makeFieldLabelsNoPrefix ''WsState)

newtype WsMonad a = WsMonad
  { runWsMonad' :: ReaderT WsState (LogM IO) a
  }
  deriving newtype
    ( Functor,
      Applicative,
      Monad,
      MonadReader WsState,
      MonadIO,
      MonadThrow,
      MonadCatch,
      K.KatipContext,
      K.Katip
    )

instance MonadUnliftIO WsMonad where
  withRunInIO go = WsMonad (withRunInIO (\k -> go (k . runWsMonad')))

runWsMonad :: WsState -> WsMonad a -> LogM IO a
runWsMonad wsState (WsMonad m) = runReaderT m wsState

runLogInWs :: LogM IO a -> WsMonad a
runLogInWs = WsMonad . lift

instance MonadState UserState WsMonad where
  get = readIORef =<< asks userState
  put s = flip writeIORef s =<< asks userState

instance Random.RandM WsMonad where
  doRandom f = asks randomGen >>= Random.ioDoRandom f

instance WithDB WsMonad where
  execQuery q = do
    (RunDb dbAccess) <- asks runDb
    liftIO $ dbAccess (`PG.pgQuery` q)

  parseQueryResult parse rows = do
    let (errors, successes) = partitionEithers $ fmap parse rows
    mapM_ (K.logLocM K.ErrorS . ("Failed parsing db row: " <>) . K.ls) errors
    pure successes

runInAppM :: IORef UserState -> WsMonad a -> Config.AppM a
runInAppM mUserState wsm = do
  rDb <- asks Config.runDb
  rng <- asks Config.randomGen
  ps <- asks Config.pubSub
  r <- replicateLogM
  liftIO $ r $ runWsMonad (WsState rDb mUserState rng ps) wsm

sendData :: (ToJSON a, MonadIO m) => WS.Connection -> a -> m ()
sendData conn = liftIO . WS.sendTextData conn . encode

instance PubSub.PubSubM WsMonad where
  publishM channel msg = do
    ps <- asks pubSub
    runLogInWs $ PubSub.publish ps channel msg

  subscribeM channel subscriber = do
    wsState <- ask
    runLogInWs <$> runLogInWs (PubSub.subscribe (pubSub wsState) channel (runWsMonad wsState . subscriber))
