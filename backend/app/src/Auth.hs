{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}

module Auth
  ( withAuthContext,
    AuthContext,
    AuthSettings,
    AuthProvider (..),
    Protected,
    JWTUser (..),
    jwtToUser,
    MultiJWTSettings,
    jwtMapFromContext,
    Auth.AuthResult (..),
    Auth.ThrowAll (..),
    testAuthContext,
    XAuthProvider (..),
  )
where

import qualified Auth.Andrena.AuthUser as Andrena
import qualified Auth.Andrena.Client as Andrena
import qualified Auth.Data.UserWithRole as Local
import Auth.JWT
import qualified Auth.Local.Client as Local
import CustomPrelude
import Data.Aeson
import qualified Data.Map.Strict as Map
import Data.Void
import qualified Env
import qualified Katip as K
import Servant
import qualified Servant.Auth.Server as Auth
import Servant.Client (BaseUrl)
import qualified Util
import qualified Time

data AuthProvider = AndrenaProvider | LocalProvider deriving (Eq, Show, Ord, Enum, Bounded)

instance FromJSON AuthProvider where
  parseJSON = Util.eitherToParser . providerFromHeader <=< parseJSON @Text

instance ToJSON AuthProvider where
  toJSON = toJSON @Text . providerToHeader

type MultiJWTSettings = JWTMap AuthProvider

type AuthSettings = '[MultiJWTSettings, Auth.JWTSettings, Auth.CookieSettings]

type AuthContext = Context AuthSettings

type Protected = Auth.Auth '[MultiJWT AuthProvider] JWTUser

data JWTUser = Andrena Andrena.AuthUser | Local Local.UserWithRole deriving (Show, Eq)

instance XAuthProvider AuthProvider where
  providerToHeader AndrenaProvider = "andrena"
  providerToHeader LocalProvider = "local"
  providerFromHeader "andrena" = Right AndrenaProvider
  providerFromHeader "local" = Right LocalProvider
  providerFromHeader other =
    Left $
      tshow other
        <> " is not a valid auth provider, expected one of "
        <> tshow (Util.enumerate @AuthProvider)

instance FromHttpApiData AuthProvider where
  parseUrlPiece = providerFromHeader

instance ToHttpApiData AuthProvider where
  toUrlPiece = providerToHeader

instance Auth.FromJWT JWTUser where
  decodeJWT jwt =
    case Andrena <$> Auth.decodeJWT jwt of
      Left _ -> Local <$> Auth.decodeJWT jwt
      Right andrena -> Right andrena

instance Auth.ToJWT JWTUser where
  encodeJWT (Andrena a) = Auth.encodeJWT a
  encodeJWT (Local a) = Auth.encodeJWT a

jwtToUser :: MonadIO m => MultiJWTSettings -> AuthProvider -> ByteString -> m (Maybe JWTUser)
jwtToUser = verifyMultiJWT

withAuthContext :: (K.Katip m, K.KatipContext m, MonadUnliftIO m) => Env.Environment -> (AuthContext -> m a) -> m a
withAuthContext env program = do
  jwtMap <- liftIO $ newTVarIO mempty
  let authContext = jwtMap :. emptyJWTSettings :. Auth.defaultCookieSettings :. EmptyContext
      scheduledJobs =
        concurrently
          (scheduleJWTSettings jwtMap LocalProvider (getLocalSettingsSchedule $ env ^. #localAuthBaseUrl))
          (scheduleJWTSettings jwtMap AndrenaProvider getAndrenaSettingsSchedule)
  removeVoid
    <$> race
      (program authContext)
      scheduledJobs
  where
    removeVoid = \case
      Left a -> a
      Right (v, _) -> absurd v

-- this should be used for tests only
testAuthContext :: IO AuthContext
testAuthContext = do
  settingsWithKeys <- mapM (\prov -> fmap (prov,) genOne) providers
  let jwtMap = Map.fromList settingsWithKeys
  (:. emptyJWTSettings :. Auth.defaultCookieSettings :. EmptyContext) <$> newTVarIO jwtMap
  where
    genOne = Auth.defaultJWTSettings <$> Auth.generateKey
    providers = toList $ Util.enumerate @AuthProvider

jwtMapFromContext :: AuthContext -> MultiJWTSettings
jwtMapFromContext (jwtMap :. _) = jwtMap

scheduleJWTSettings ::
  (K.Katip m, Time.Timeable t) =>
  MultiJWTSettings ->
  AuthProvider ->
  m (Maybe Auth.JWTSettings, t) ->
  m Void
scheduleJWTSettings jwtMap provider getSchedule = forever $ do
  (maySettings, delay) <- getSchedule
  case maySettings of
    Just jwtSettings -> liftIO $ atomically $ modifyTVar jwtMap (Map.insert provider jwtSettings)
    Nothing -> pure ()
  Time.threadDelay delay

getLocalSettingsSchedule :: (K.Katip m, K.KatipContext m) => BaseUrl -> m (Maybe Auth.JWTSettings, Time.Minutes)
getLocalSettingsSchedule baseUrl = do
  eitherLocalSettings <- Local.getLocalJWTSettings baseUrl
  case eitherLocalSettings of
    Left err ->
      K.logLocM K.ErrorS ("Could not load local jwt settings: " <> K.showLS err) $> (Nothing, 1)
    Right localSettings ->
      -- on success, run once an hour
      K.logLocM K.InfoS "Successfully reloaded local jwt settings" $> (Just localSettings, 60)

getAndrenaSettingsSchedule :: (K.Katip m, K.KatipContext m) => m (Maybe Auth.JWTSettings, Time.Minutes)
getAndrenaSettingsSchedule = do
  eitherAndrenaSettings <- Andrena.getAndrenaKeySet
  case eitherAndrenaSettings of
    Left err ->
      K.logLocM K.ErrorS ("Could not load andrena jwt settings: " <> K.showLS err) $> (Nothing, 1)
    Right andrenaSettings ->
      -- on success, run once a day
      K.logLocM K.InfoS "Successfully reloaded andrena jwt settings" $> (Just andrenaSettings, 60 * 24)

-- just for typechecking. Servant.Auth.Server won't work without this in the context
emptyJWTSettings :: Auth.JWTSettings
emptyJWTSettings = Auth.defaultJWTSettings $ Auth.fromSecret "do-not-use-this"
