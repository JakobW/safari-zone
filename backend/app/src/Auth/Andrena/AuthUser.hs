module Auth.Andrena.AuthUser (AuthUser(..)) where

import CustomPrelude
import Util.Either (mapLeft)
import Data.Aeson
import qualified Crypto.JWT as Jose
import Servant.Auth.Server (FromJWT(..), ToJWT(..))
import qualified Data.User.UserName as User
import qualified Test.QuickCheck as QC
import qualified Lens.Micro as Lens

data AuthUser = AndrenaUser {
  preferred_username :: User.UserName,
  given_name :: Text,
  family_name :: Text
} deriving (Show, Eq, Generic, FromJSON, ToJSON)

instance FromJWT AuthUser where
  decodeJWT claimSet = mapLeft pack $ eitherDecode jsonCS
    where jsonCS = encode $ claimSet Lens.^. Jose.unregisteredClaims

claim :: (ToJSON a) => Text -> a -> Jose.ClaimsSet -> Jose.ClaimsSet
claim prop a = Jose.addClaim prop (toJSON a)

instance ToJWT AuthUser where
  encodeJWT user = 
    claim "preferred_username" (preferred_username user) $
    claim "given_name" (given_name user) $
    claim "family_name" (family_name user)
    Jose.emptyClaimsSet

instance QC.Arbitrary AuthUser where
  arbitrary = AndrenaUser <$> QC.arbitrary <*> QC.arbitrary <*> QC.arbitrary