{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module Auth.Andrena.Client (getAndrenaKeySet) where

import Crypto.JOSE.JWA.JWS (Alg (RS256))
import Crypto.JOSE.JWK (JWK, JWKSet (..))
import CustomPrelude
import Network.HTTP.Client (newManager)
import Network.HTTP.Client.TLS (tlsManagerSettings)
import Servant
import Servant.Auth.Server (JWTSettings (..), defaultJWTSettings)
import Servant.Client

type AndrenaAPI = "auth" :> "realms" :> "test" :> "protocol" :> "openid-connect" :> "certs" :> Get '[JSON] JWKSet

andrenaAPI :: Proxy AndrenaAPI
andrenaAPI = Proxy

getKeySet :: ClientM JWKSet
getKeySet = client andrenaAPI

firstOfJwkSet :: JWKSet -> JWK
firstOfJwkSet (JWKSet jwks) =
  unsafeHead jwks

jwkSetToSettings :: JWKSet -> JWTSettings
jwkSetToSettings jwkSet =
  (defaultJWTSettings $ firstOfJwkSet jwkSet) {validationKeys = jwkSet, jwtAlg = Just RS256}

andrenaUrl :: (IsString s) => s
andrenaUrl = "auth.apps.andrena.de"

getAndrenaKeySet :: MonadIO m => m (Either ClientError JWTSettings)
getAndrenaKeySet = liftIO $ do
  manager' <- newManager tlsManagerSettings
  jwkSet <- runClientM getKeySet (mkClientEnv manager' (BaseUrl Https andrenaUrl 443 ""))
  pure $ fmap jwkSetToSettings jwkSet
