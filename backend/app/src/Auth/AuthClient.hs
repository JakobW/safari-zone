{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Auth.AuthClient (Token(..)) where

import Auth.JWT
import CustomPrelude hiding ((<|))
import Data.Sequence ((<|))
import Servant
import Servant.Auth.Server (Auth)
import Servant.Client
import Servant.Client.Core (requestHeaders)

-- | A simple bearer token.
newtype Token = Token {getToken :: ByteString}
  deriving newtype (Eq, Show, Read, IsString)

instance (HasClient m api, XAuthProvider keys) => HasClient m (Auth '[MultiJWT keys] a :> api) where
  type Client m (Auth '[MultiJWT keys] a :> api) = keys -> Token -> Client m api

  clientWithRoute m _ req authProvider (Token token) =
    clientWithRoute m (Proxy :: Proxy api) $
      req {requestHeaders = ("X-Auth-Provider", providerToHeader authProvider) <| ("Authorization", headerVal) <| requestHeaders req}
    where
      headerVal = "Bearer " <> token

  hoistClientMonad pm _ nt cl authProvider = hoistClientMonad pm (Proxy :: Proxy api) nt . cl authProvider