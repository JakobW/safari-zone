{-# LANGUAGE TypeFamilies #-}

module Auth.JWT (JWTMap, verifyMultiJWT, MultiJWT, XAuthProvider (..)) where

import CustomPrelude
import Network.Wai (Request, requestHeaders)
import qualified Servant.Auth.Server as Auth
import qualified Servant.Auth.Server.Internal.Class as Auth
import qualified Util
import qualified Data.Map.Strict as Map

type JWTMap keys = TVar (Map keys Auth.JWTSettings)

data MultiJWT keys

class XAuthProvider a where
  providerFromHeader :: (IsSequence s, IsString s, Eq s, Show s) => s -> Either Text a
  providerToHeader :: (IsSequence s, IsString s, Eq s, Show s) => a -> s

chooseSettings :: (XAuthProvider keys, Ord keys) => JWTMap keys -> Request -> IO (Maybe Auth.JWTSettings)
chooseSettings sMap req = do
  case Util.eitherToMaybe . providerFromHeader =<< lookup "X-Auth-Provider" (requestHeaders req) of
    Just authProvider ->
      lookupIO authProvider sMap
    Nothing ->
      pure Nothing

instance (Auth.FromJWT usr, XAuthProvider keys, Ord keys) => Auth.IsAuth (MultiJWT keys) usr where
  type AuthArgs (MultiJWT keys) = '[JWTMap keys]
  runAuth _ _ settingsMap =
    ask >>= liftIO . chooseSettings settingsMap >>= maybe mzero Auth.jwtAuthCheck 

verifyMultiJWT :: (Auth.FromJWT usr, Ord keys, MonadIO m) => JWTMap keys -> keys -> ByteString -> m (Maybe usr)
verifyMultiJWT sMap authProvider token = lookupIO authProvider sMap >>= fmap join . mapM verifyWithSettings
  where
    verifyWithSettings settings = liftIO $ Auth.verifyJWT settings token

lookupIO :: (MonadIO m, Ord k) => k -> TVar (Map k v) -> m (Maybe v)
lookupIO k = fmap (Map.lookup k) . readTVarIO
