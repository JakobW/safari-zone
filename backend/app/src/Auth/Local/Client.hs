{-# LANGUAGE AllowAmbiguousTypes #-}

module Auth.Local.Client (getLocalJWTSettings) where

import CustomPrelude
import Crypto.JOSE.JWK (JWK)
import Network.HTTP.Client (newManager)
import Network.HTTP.Client.TLS (tlsManagerSettings)
import Servant
import Servant.Auth.Server (JWTSettings (..), defaultJWTSettings)
import Servant.Client
import Auth.Api.Types (JWKApi)

jwkClient :: ClientM JWK
jwkClient = client (Proxy :: Proxy JWKApi)

getLocalJWTSettings :: MonadIO m => BaseUrl -> m (Either ClientError JWTSettings)
getLocalJWTSettings baseUrl = liftIO $ do
  manager' <- newManager tlsManagerSettings
  jwk <- runClientM jwkClient (mkClientEnv manager' baseUrl)
  pure (fmap defaultJWTSettings jwk)

