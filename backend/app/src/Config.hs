{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GeneralisedNewtypeDeriving #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE TypeFamilies #-}

module Config
  ( Config (..),
    AppM,
    runHandlerInApp,
    KatipConfig,
    writeImg,
    defaultWriteImage,
    WriteImage,
    runAppM,
    runAppOnLogM,
  )
where

import qualified Auth
import Control.Monad.Except (ExceptT (..))
import Crypto.JOSE (MonadRandom (..))
import CustomPrelude hiding (Handler)
import qualified Database.PostgreSQL.Typed as PG
import Db.Client (RunDb (..), WithDB (..), queryToString)
import qualified Draw
import Env (Environment)
import qualified Graphics.Image as Image
import qualified Katip as K
import Logger
import qualified Random
import Servant
import qualified Util.Filesystem as Util
import qualified Websocket.PubSub.Channel.Cache as Cache
import qualified Websocket.PubSub.Types as PubSub

type WriteImage =
  FilePath -> Draw.Img -> IO ()

defaultWriteImage :: WriteImage
defaultWriteImage fp img = Util.safeWrite (`Image.writeImage` img) fp

data Config = Config
  { env :: Environment,
    runDb :: RunDb,
    randomGen :: Random.RandGen,
    jwtSettings :: Auth.MultiJWTSettings,
    writeImage :: WriteImage,
    pubSub :: PubSub.SomePubSub (LogM IO),
    cache :: Cache.InMemoryCache
  }

newtype AppM a = AppM
  { runAppM' :: ReaderT Config (LogM IO) a
  }
  deriving newtype
    ( Functor,
      Applicative,
      Monad,
      MonadIO,
      MonadReader Config,
      MonadThrow,
      MonadCatch,
      K.Katip,
      K.KatipContext
    )

$(makeFieldLabelsNoPrefix ''Config)

writeImg :: Draw.Img -> FilePath -> AppM ()
writeImg img filePath = do
  write <- asks writeImage
  K.logLocM K.DebugS $ "Creating image at path " <> K.ls filePath
  liftIO $ write filePath img

instance MonadRandom AppM where
  getRandomBytes = liftIO . getRandomBytes

instance MonadUnliftIO AppM where
  withRunInIO go = AppM (withRunInIO (\k -> go (k . runAppM')))

instance Random.RandM AppM where
  doRandom f = asks randomGen >>= Random.ioDoRandom f

instance Auth.ThrowAll (AppM a) where
  throwAll = throwIO

runAppOnLogM :: Config -> AppM a -> LogM IO a
runAppOnLogM config x = runReaderT (runAppM' x) config

runLogInApp :: LogM IO a -> AppM a
runLogInApp = AppM . lift

runAppM :: MonadIO m => KatipConfig -> Config -> AppM a -> m a
runAppM katipConfig config = liftIO . runLogM katipConfig . runAppOnLogM config

instance WithDB AppM where
  execQuery q = (`K.logExceptionM` K.ErrorS) $ do
    (RunDb dbAccess) <- asks runDb
    K.logLocM K.DebugS $ "Executing query " <> K.ls (queryToString q)
    liftIO $ dbAccess (`PG.pgQuery` q)

  parseQueryResult parse rows = do
    let (errors, successes) = partitionEithers $ fmap parse rows
    mapM_ (K.logLocM K.ErrorS . ("Failed parsing db row: " <>) . K.ls) errors
    pure successes

instance Cache.HasCache AppM where
  getCache = asks cache

instance PubSub.PubSubM AppM where
  publishM channel msg = do
    pubsub <- asks pubSub
    runLogInApp $ PubSub.publish pubsub channel msg
    
  subscribeM channel subscriber = do
    cfg <- ask
    runLogInApp <$> runLogInApp (PubSub.subscribe (pubSub cfg) channel (runAppOnLogM cfg . subscriber)) 
  

runHandlerInApp :: KatipConfig -> Config -> AppM a -> Handler a
runHandlerInApp katipConfig config = Handler . ExceptT . try . runAppM katipConfig config
