{-# LANGUAGE DataKinds #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

module Data.Area
  ( module Data.Area.AreaId,
    module Data.Area.AreaName,
    module Data.Area.Range,
    Area (Area),
    AreaWithId,
    name,
    ranges,
    mkArea,
  )
where

import CustomPrelude
import Data.Aeson
import Data.Area.AreaId
import Data.Area.AreaName
import Data.Area.Range
import Data.Coord
import Data.WithId
import qualified Db.Row
import Has (getTyped)
import qualified Test.QuickCheck as QC
import qualified Util

data Area = Area
  { name :: AreaName,
    ranges :: Coordinate Range
  }
  deriving (Show, Eq)

$(makeFieldLabelsNoPrefix ''Area)

type AreaWithId = WithId AreaId Area

mkArea :: AreaName -> Range 'X -> Range 'Y -> Area
mkArea aName rx ry = Area aName $ mkCoordLike rx ry

instance FromJSON Area where
  parseJSON = withObject "Area" parseObject
    where
      parseObject obj = do
        name <- obj .: "name"
        rangeX <-
          Util.eitherToParser =<< mkRange <$> obj .: "borderLeft"
            <*> obj .: "borderRight"
        rangeY <-
          Util.eitherToParser =<< mkRange <$> obj .: "borderTop"
            <*> obj .: "borderBottom"
        pure $ mkArea name rangeX rangeY

instance ToJSON Area where
  toJSON Area {name, ranges} =
    object
      [ "name" .= name,
        "borderLeft" .= (ranges ^. #x % to lower),
        "borderRight" .= (ranges ^. #x % to upper),
        "borderTop" .= (ranges ^. #y % to lower),
        "borderBottom" .= (ranges ^. #y % to upper)
      ]

instance Db.Row.RowLike Area where
  type AsRow Area = '[AreaName, Range 'X, Range 'Y]

  fromRow = mkArea . getTyped <*> getTyped <*> getTyped

  toRow a = (name a, ranges a ^. #x, ranges a ^. #y)

instance QC.Arbitrary Area where
  arbitrary = QC.applyArbitrary3 mkArea
