{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Data.Area.AreaId (AreaId) where

import CustomPrelude
import Data.Aeson
import Database.PostgreSQL.Typed.Types (PGColumn (..), PGParameter (..))
import Servant (FromHttpApiData, ToHttpApiData)

newtype AreaId = AreaId {getAreaId :: Int32}
  deriving stock (Show, Eq)
  deriving newtype
    ( PGColumn "integer",
      PGParameter "integer",
      FromJSON,
      ToJSON,
      FromHttpApiData,
      ToHttpApiData
    )
