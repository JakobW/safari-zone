{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Data.Area.AreaName (AreaName (getAreaName), mkAreaName) where

import CustomPrelude
import Data.Aeson
import Database.PostgreSQL.Typed.Types (PGColumn (..), PGParameter (..))
import qualified Test.QuickCheck as QC
import qualified Util
import qualified Util.SecurityTextFilter

newtype AreaName = AreaName {getAreaName :: Text}
  deriving stock (Show, Eq)
  deriving newtype
    ( PGColumn "character varying",
      PGParameter "character varying",
      ToJSON,
      Semigroup,
      Monoid
    )

mkAreaName :: Text -> Either Text AreaName
mkAreaName name =
  if not (null name)
    then Right $ AreaName (Util.SecurityTextFilter.filterText name)
    else Left "Area name cannot be empty"

instance FromJSON AreaName where
  parseJSON = withText "AreaName" (Util.eitherToParser . mkAreaName)

instance QC.Arbitrary AreaName where
  arbitrary = AreaName <$> Util.SecurityTextFilter.validTextGen `QC.suchThat` (not . null)
