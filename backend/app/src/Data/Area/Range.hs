{-# LANGUAGE DataKinds #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE StrictData #-}

module Data.Area.Range (overlaps, mkRange, Range(lower, upper), elemRange) where

import CustomPrelude
import Database.PostgreSQL.Typed.Types (PGColumn(..), PGParameter(..))
import qualified Database.PostgreSQL.Typed.Range as PG
import qualified Test.QuickCheck as QC
import qualified Util
import Data.Coord (Dimension(..), Unit(untag))
import Data.String.Interpolate (i)

data Range (a :: Dimension) = Range { lower :: Int32, upper :: Int32 }
  deriving (Show, Eq)

instance PGColumn "int4range" (Range a) where
  pgDecode typeId = unsafeRight . uncurry mkRange . fromPGRange . pgDecode typeId

instance PGParameter "int4range" (Range a) where
  pgEncode typeId = pgEncode typeId . toPGRange

toPGRange :: Range a -> PG.Range Int32
toPGRange r = PG.bounded (lower r) (upper r + 1)

fromPGRange :: (Bounded a, Num a) => PG.Range a -> (a, a)
fromPGRange range = (
  (fromMaybe minBound . PG.bound . PG.lowerBound) range,
  (fromMaybe maxBound . PG.bound . PG.upperBound) range - 1)

mkRange :: Int32 -> Int32 -> Either Text (Range a)
mkRange l u
  | l <= u = Right $ Range l u
  | otherwise = Left [i|Invalid Range: #{u} is smaller than #{l}|]

overlaps :: Range a -> Range a -> Bool
overlaps r1 r2 =
  PG.overlaps (toPGRange r1) (toPGRange r2)

elemRange :: Unit a -> Range a -> Bool
elemRange unit range = untag unit >= lower range && untag unit <= upper range

instance QC.Arbitrary (Range a) where
  arbitrary = (mkRange <$> QC.arbitrary <*> QC.arbitrary) `QC.suchThatMap` Util.eitherToMaybe
  
unsafeRight :: Either Text r -> r
unsafeRight (Right r) = r
unsafeRight (Left err) = error [i|UnsafeRight failed! #{err}|]