{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

module Data.Coord
  ( Dimension (..),
    Unit (..),
    Position,
    mkCoordLike,
    x,
    y,
    Coordinate,
    FPosition,
    FUnit (..),
    addCoord,
    mapCoord,
    mapMCoord,
    unitToFUnit,
    distance
  )
where

import CustomPrelude
import Data.Aeson
import Database.PostgreSQL.Typed.Types (PGColumn, PGParameter)
import qualified Db.Row
import Has
import qualified Test.QuickCheck as QC
import Data.Coerce

data Dimension = X | Y deriving (Show)

data Coordinate f = Coordinate
  { x :: f 'X,
    y :: f 'Y
  }

$(makeFieldLabelsNoPrefix ''Coordinate)

deriving instance (Show (f 'X), Show (f 'Y)) => Show (Coordinate f)

deriving instance (Eq (f 'X), Eq (f 'Y)) => Eq (Coordinate f)

deriving instance Generic (Coordinate f)

type Position = Coordinate Unit

type FPosition = Coordinate FUnit

mkCoordLike :: f 'X -> f 'Y -> Coordinate f
mkCoordLike = Coordinate

addCoord :: (Num (f 'X), Num (f 'Y)) => Coordinate f -> Coordinate f -> Coordinate f
addCoord c1 c2 = mkCoordLike (x c1 + x c2) (y c1 + y c2)

instance (FromJSON (f 'X), FromJSON (f 'Y)) => FromJSON (Coordinate f) where
  parseJSON = withObject "Position" $ \obj ->
    mkCoordLike <$> obj .: "x" <*> obj .: "y"

instance (ToJSON (f 'X), ToJSON (f 'Y)) => ToJSON (Coordinate f) where
  toJSON Coordinate {x = x, y = y} =
    object
      [ "x" .= x,
        "y" .= y
      ]

instance Db.Row.RowLike (Coordinate f) where
  type AsRow (Coordinate f) = '[f 'X, f 'Y]
  fromRow = Coordinate . getTyped <*> getTyped
  toRow c = (x c, y c)

newtype Unit (a :: Dimension) = Unit
  { untag :: Int32
  }
  deriving newtype
    ( Num,
      Eq,
      Enum,
      Bounded,
      Ord,
      Real,
      Integral,
      PGColumn "integer",
      PGParameter "integer",
      FromJSON,
      ToJSON,
      QC.Arbitrary
    )

instance Show (Unit 'X) where
  show (Unit u) = show X <> show u

instance Show (Unit 'Y) where
  show (Unit u) = show Y <> show u

instance Show (FUnit 'X) where
  show (FUnit u) = show X <> show u

instance Show (FUnit 'Y) where
  show (FUnit u) = show Y <> show u

newtype FUnit (d :: Dimension) = FUnit {fUntag :: Float}
  deriving newtype
    ( Num,
      Eq,
      Enum,
      Ord,
      Real,
      FromJSON,
      ToJSON,
      QC.Arbitrary
    )

instance Semigroup Position where
  g1 <> g2 = mkCoordLike (x g1 + x g2) (y g1 + y g2)

instance Monoid Position where
  mempty = mkCoordLike 0 0

instance Ord Position where
  compare a b = compare (y a, x a) (y b, x b)

instance QC.Arbitrary Position where
  arbitrary = mkCoordLike <$> QC.arbitrary <*> QC.arbitrary

unitToFUnit :: Unit any -> FUnit any
unitToFUnit (Unit u) = FUnit (fromIntegral u)

mapCoord :: (forall any. f any -> g any) -> Coordinate f -> Coordinate g
mapCoord fToG c = mkCoordLike (fToG $ x c) (fToG $ y c)

mapMCoord :: Monad m => (forall any. f any -> m (g any)) -> Coordinate f -> m (Coordinate g)
mapMCoord fToMG c = do
  gx <- fToMG (x c)
  gy <- fToMG (y c)
  pure $ mkCoordLike gx gy

distance :: (Num n, Coercible (f 'X) n, Coercible (f 'Y) n) => Coordinate f -> Coordinate f -> n
distance c1 c2 = abs (coerce (x c1) - coerce (x c2)) + abs (coerce (y c1) - coerce (y c2))