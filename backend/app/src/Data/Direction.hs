{-# LANGUAGE StrictData #-}
{-# LANGUAGE OverloadedLabels #-}

module Data.Direction (Direction(..), moveIn) where

import CustomPrelude
import Data.Aeson
import Data.Coord
import qualified Test.QuickCheck as QC
import qualified Generic.Random as QC

data Direction = L | R | U | D deriving (Show, Eq, Generic, Enum, Bounded, FromJSON, ToJSON)

moveIn :: Direction -> Position -> Position
moveIn L = over #x (+ (- 1))
moveIn R = over #x (+ 1)
moveIn U = over #y (+ (- 1))
moveIn D = over #y (+ 1)

instance QC.Arbitrary Direction where
  arbitrary = QC.genericArbitrary QC.uniform

