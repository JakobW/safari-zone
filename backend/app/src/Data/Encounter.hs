{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE OverloadedLabels #-}

module Data.Encounter
  ( module Data.Encounter.EncounterProbability,
    Encounter(Encounter),
    EncounterIds(EncounterIds),
    rollEncounter,
  )
where

import CustomPrelude
import Data.Area.AreaId (AreaId)
import Data.Encounter.EncounterProbability
import Data.List.NonEmpty (NonEmpty (..))
import Data.Pokemon.PokemonId (PokemonId)
import qualified Random
import qualified Db.Row
import Has
import Data.Aeson

data Encounter = Encounter
  { areaId :: AreaId,
    pokemonId :: PokemonId,
    probability :: EncounterProbability
  }
  deriving (Show, Eq, Generic, FromJSON, ToJSON)

data EncounterIds = EncounterIds
  { areaId :: AreaId,
    pokemonId :: PokemonId
  }
  deriving (Show, Eq, Generic, FromJSON, ToJSON)

$(makeFieldLabelsNoPrefix ''Encounter)
$(makeFieldLabelsNoPrefix ''EncounterIds)

getTotalProbability :: NonEmpty Encounter -> EncounterProbability
getTotalProbability = foldl1Ex' (+) . fmap (^. #probability)

rollEncounter :: (Random.RandM m) => NonEmpty Encounter -> m Encounter
rollEncounter encounters =
  choose encounters 0 . EncounterProbability <$> Random.getRandom01
  where
    total :: EncounterProbability
    total = getTotalProbability encounters
    choose :: NonEmpty Encounter -> EncounterProbability -> EncounterProbability -> Encounter
    choose (e :| []) _ _ = e
    choose (e :| e' : es) probabilitySoFar rand =
      if rand > nextProbabilitySoFar
        then choose (e' :| es) nextProbabilitySoFar rand
        else e
      where
        nextProbabilitySoFar = probabilitySoFar + (e ^. #probability / total)

instance Db.Row.RowLike Encounter where
  type AsRow Encounter = '[PokemonId, AreaId, EncounterProbability]
  fromRow = Encounter . getTyped <*> getTyped <*> getTyped
  toRow e = (e ^. #pokemonId, e ^. #areaId, e ^. #probability)

instance Db.Row.RowLike EncounterIds where
  type AsRow EncounterIds = '[PokemonId, AreaId]
  fromRow = EncounterIds . getTyped <*> getTyped
  toRow e = (e ^. #pokemonId, e ^. #areaId)
