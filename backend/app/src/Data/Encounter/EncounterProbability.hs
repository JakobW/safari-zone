{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Data.Encounter.EncounterProbability (EncounterProbability (EncounterProbability)) where

import CustomPrelude
import Data.Aeson
import Database.PostgreSQL.Typed.Types (PGColumn, PGParameter)

newtype EncounterProbability = EncounterProbability {getProbability :: Float}
  deriving stock (Show, Eq)
  deriving newtype
    ( PGParameter "real",
      PGColumn "real",
      Num,
      Fractional,
      Ord,
      ToJSON,
      FromJSON
    )
