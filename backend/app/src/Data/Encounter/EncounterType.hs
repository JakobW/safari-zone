module Data.Encounter.EncounterType (EncounterType(..), HasEncounterType(..)) where

import CustomPrelude

data EncounterType = Grass | Cave | None deriving (Show, Eq)

instance Semigroup EncounterType where
  (<>) :: EncounterType -> EncounterType -> EncounterType
  e1 <> None = e1
  None <> e2 = e2
  Grass <> _ = Grass
  _ <> Grass = Grass
  Cave <> Cave = Cave

instance Monoid EncounterType where
  mempty :: EncounterType
  mempty = None

class HasEncounterType a where
  getEncounterType :: a -> EncounterType
