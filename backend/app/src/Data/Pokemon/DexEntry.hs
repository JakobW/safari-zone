{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Data.Pokemon.DexEntry (AmountNormal, AmountShiny) where
  
import CustomPrelude
import Data.Aeson
import Database.PostgreSQL.Typed.Types (PGColumn, PGParameter)

newtype AmountNormal = AmountNormal {getAmountNormal :: Int32}
  deriving stock (Show, Eq)
  deriving newtype (Num, PGParameter "integer", PGColumn "integer", ToJSON, FromJSON)

newtype AmountShiny = AmountShiny {getAmountShiny :: Int32}
  deriving stock (Show, Eq)
  deriving newtype (Num, PGParameter "integer", PGColumn "integer", ToJSON, FromJSON)