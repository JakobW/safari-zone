{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Data.Pokemon.PokemonFields
  ( PokemonName,
    PokemonType,
    PokemonDescription,
    PokemonHeight,
    PokemonImageName,
    PokemonWeight,
  )
where

import CustomPrelude
import Data.Aeson
import Database.PostgreSQL.Typed.Types (PGColumn, PGParameter)

newtype PokemonName = PokemonName {getPokemonName :: Text}
  deriving stock (Show, Eq)
  deriving newtype (PGParameter "character varying", PGColumn "character varying", ToJSON, FromJSON)

newtype PokemonType = PokemonType {getPokemonType :: Text}
  deriving stock (Show, Eq)
  deriving newtype (PGParameter "character varying", PGColumn "character varying", ToJSON, FromJSON)

newtype PokemonDescription = PokemonDescription {getPokemonDescription :: Text}
  deriving stock (Show, Eq)
  deriving newtype (PGParameter "character varying", PGColumn "character varying", ToJSON, FromJSON)

newtype PokemonImageName = PokemonImageName {getPokemonImageName :: Text}
  deriving stock (Show, Eq)
  deriving newtype (PGParameter "character varying", PGColumn "character varying", ToJSON, FromJSON)

newtype PokemonWeight = PokemonWeight {getPokemonWeight :: Scientific}
  deriving stock (Show, Eq)
  deriving newtype (PGParameter "numeric", PGColumn "numeric", ToJSON, FromJSON)

newtype PokemonHeight = PokemonHeight {getPokemonHeight :: Scientific}
  deriving stock (Show, Eq)
  deriving newtype (PGParameter "numeric", PGColumn "numeric", ToJSON, FromJSON)
