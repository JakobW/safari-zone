{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Data.Pokemon.PokemonId (PokemonId) where

import CustomPrelude
import Data.Aeson
import Database.PostgreSQL.Typed.Types (PGColumn (..), PGParameter (..))
import Servant (FromHttpApiData, ToHttpApiData)
import qualified Test.QuickCheck as QC

newtype PokemonId = PokemonId {getPokemonId :: Int32}
  deriving stock (Show, Eq)
  deriving newtype
    ( PGParameter "integer",
      PGColumn "integer",
      ToJSON,
      FromJSON,
      FromHttpApiData,
      ToHttpApiData,
      QC.Arbitrary
    )
