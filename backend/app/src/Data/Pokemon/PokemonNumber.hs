{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Data.Pokemon.PokemonNumber (PokemonNumber, mkPokemonNumber) where

import CustomPrelude
import Data.Aeson
import Data.String.Interpolate (i)
import Database.PostgreSQL.Typed.Types (PGColumn (..), PGParameter (..))
import Servant (FromHttpApiData(..), ToHttpApiData)
import qualified Util
import qualified Test.QuickCheck as QC

newtype PokemonNumber = PokemonNumber {getPokemonId :: Int32}
  deriving stock (Show, Eq)
  deriving newtype (PGParameter "integer", PGColumn "integer", ToJSON, ToHttpApiData)

minPokemonNumber :: Int32
minPokemonNumber = 1

maxPokemonNumber :: Int32
maxPokemonNumber = 151

mkPokemonNumber :: Int32 -> Either Text PokemonNumber
mkPokemonNumber a
  | a < minPokemonNumber = Left [i|PokemonNumber #{a} has to be > #{minPokemonNumber}|]
  | a > maxPokemonNumber = Left [i|PokemonNumber #{a} has to be < #{maxPokemonNumber}|]
  | otherwise = Right $ PokemonNumber a

instance FromHttpApiData PokemonNumber where
  parseUrlPiece = parseUrlPiece >=> mkPokemonNumber

instance FromJSON PokemonNumber where
  parseJSON = parseJSON >=> Util.eitherToParser . mkPokemonNumber

instance QC.Arbitrary PokemonNumber where
  arbitrary = PokemonNumber <$> QC.choose (minPokemonNumber, maxPokemonNumber)