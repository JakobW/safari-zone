{-# LANGUAGE StrictData #-}

module Data.Tile
  ( module Data.Tile.TH.Tile,
    module Data.Tile.TH.TileToId,
    module Data.Tile.TH.IdToTile,
    module Data.Tile.Layer,
    OptionalTile(..),
    optTileFromMaybe,
    optTileToMaybe
  )
where

import Data.Tile.TH.Tile hiding (inefficientFromDbId)
import Data.Tile.TH.TileToId
import Data.Tile.TH.IdToTile
import Data.Tile.TH.Orphans ()
import Data.Tile.Layer
import Data.Aeson
import CustomPrelude

data OptionalTile = SomeTile Tile | NoTile deriving (Show, Eq)

optTileFromMaybe :: Maybe Tile -> OptionalTile
optTileFromMaybe (Just t) = SomeTile t
optTileFromMaybe Nothing = NoTile

optTileToMaybe :: OptionalTile -> Maybe Tile
optTileToMaybe (SomeTile t) = Just t
optTileToMaybe NoTile = Nothing

instance ToJSON OptionalTile where
  toJSON (SomeTile tile) = toJSON $ tileToId @Int tile
  toJSON NoTile = toJSON (-1 :: Int)

instance FromJSON OptionalTile where
  parseJSON (Number (-1)) = pure NoTile
  parseJSON value =  SomeTile <$> parseJSON value