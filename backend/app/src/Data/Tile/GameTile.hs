{-# LANGUAGE StrictData #-}

module Data.Tile.GameTile (GameTile (..), convertToGameTiles, gameTilesAsMap) where

import CustomPrelude
import Data.Coord
import qualified Data.Encounter.EncounterType as EncounterType
import Data.List.NonEmpty (NonEmpty)
import qualified Data.Map.Strict as Map
import qualified Data.Tile as Tile
import qualified Tile.MoveRule as MoveRule
import qualified Util
import Has

data GameTile = GameTile
  { moveRule :: MoveRule.MoveRule,
    encounterType :: EncounterType.EncounterType,
    position :: Position
  }
  deriving (Show, Eq, Generic)

instance MoveRule.HasMoveRule GameTile where
  getMoveRule = moveRule

instance EncounterType.HasEncounterType GameTile where
  getEncounterType = encounterType

convertToGameTiles :: HasAll '[Unit 'X, Unit 'Y, Tile.Tile, Tile.Layer] a => [a] -> [GameTile]
convertToGameTiles = fmap toGameTile . Map.toList . Util.sortAndGroupToMap (mkCoordLike . getTyped <*> getTyped)
  where
    toGameTile (pos, as) =
      let tiles :: NonEmpty Tile.Tile
          tiles = fmap Has.getTyped as
       in GameTile
            { moveRule = Util.foldl1' (<>) (MoveRule.getMoveRule <$> tiles),
              encounterType = Util.foldl1' (<>) (EncounterType.getEncounterType <$> tiles),
              position = pos
            }
            
gameTilesAsMap :: [GameTile] -> Map Position (EncounterType.EncounterType, MoveRule.MoveRule)
gameTilesAsMap = Map.fromList . fmap (\gt -> (position gt, (encounterType gt, moveRule gt)))             
