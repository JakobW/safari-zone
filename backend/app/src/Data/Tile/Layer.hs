{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GeneralisedNewtypeDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE UndecidableInstances #-}

module Data.Tile.Layer (Layer(layerNumber, layerType), LayerNumber, LayerType (..), genAllLayers, mkLayer, stringifyLayer) where

import CustomPrelude
import Data.Aeson
import Database.PostgreSQL.Typed.Types (PGColumn, PGParameter)
import Servant (FromHttpApiData, ToHttpApiData)
import qualified Test.QuickCheck as QC
import qualified Util
import Data.Tile.LayerType

data Layer = Layer
  { layerNumber :: LayerNumber,
    layerType :: LayerType
  }
  deriving stock (Show, Eq)

mkLayer :: LayerNumber -> LayerType -> Layer
mkLayer = Layer

newtype LayerNumber = LayerNumber {getLayerNumber :: Int32}
  deriving stock (Show, Eq, Ord)
  deriving newtype
    ( PGColumn "integer",
      PGParameter "integer",
      FromJSON,
      ToJSON,
      FromHttpApiData,
      ToHttpApiData,
      QC.Arbitrary
    )

$(makeFieldLabelsNoPrefix ''Layer)

instance Ord Layer where
  l1 `compare` l2 = case layerType l1 `compare` layerType l2 of
    EQ -> layerNumber l1 `compare` layerNumber l2
    other -> other

instance QC.Arbitrary Layer where
  arbitrary = Layer <$> QC.arbitrary <*> QC.arbitrary

genAllLayers :: LayerNumber -> NonNull [Layer]
genAllLayers n = mapNonNull (mkLayer n) Util.enumerate

stringifyLayer :: Layer -> Text
stringifyLayer layer =
  (layerType layer |> stringifyLayerType) <> tshow (layerNumber layer |> getLayerNumber)

parseLayer :: Text -> Either Text Layer
parseLayer text =
  case uncons text of
    Just ('o', rest) -> createLayer Overlay <$> readLayerNumber rest
    Just ('g', rest) -> createLayer Ground <$> readLayerNumber rest
    _ -> Left $ "Expected string starting with layer type identifier 'o' or 'g' but received " <> text
  where
    readLayerNumber :: Text -> Either Text LayerNumber
    readLayerNumber str = LayerNumber <$> Util.justEither ("Expected number but got " <> str) (readMay str)
    createLayer lType number =
      Layer
        { layerNumber = number,
          layerType = lType
        }

instance ToJSON Layer where
  toJSON = toJSON . stringifyLayer

instance FromJSON Layer where
  parseJSON = withText "Layer" (Util.eitherToParser . parseLayer)
