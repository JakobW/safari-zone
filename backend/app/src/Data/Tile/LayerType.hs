{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GeneralisedNewtypeDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE UndecidableInstances #-}

module Data.Tile.LayerType (LayerType(..), stringifyLayerType) where

import CustomPrelude
import Database.PostgreSQL.Typed.Enum (dataPGEnum)
import Db.Client (useDb)
import qualified Test.QuickCheck as QC hiding (elements)
import qualified Util.QuickCheck as QC
import qualified Util
import Database.PostgreSQL.Typed.Array
import Database.PostgreSQL.Typed.Types

useDb

$( dataPGEnum "LayerType" "layertype" $
     \case
       [] -> []
       (a : as) -> toUpper [a] <> as
 )

deriving instance Show LayerType

instance PGType "layertype" => PGType "layertype[]" where
  type PGVal "layertype[]" = PGArray (PGVal "layertype")

instance PGType "layertype" => PGArrayType "layertype[]" where
  type PGElemType "layertype[]" = "layertype"

instance QC.Arbitrary LayerType where
  arbitrary = QC.elements Util.enumerate

stringifyLayerType :: LayerType -> Text
stringifyLayerType Overlay = "o"
stringifyLayerType Ground = "g"
