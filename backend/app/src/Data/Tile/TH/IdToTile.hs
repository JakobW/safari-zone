{-# LANGUAGE TemplateHaskell #-}

module Data.Tile.TH.IdToTile (idToTile) where

import qualified Data.Tile.TH.Tile as T
import CustomPrelude
import Data.Tile.TH.TileToId
import TH.Inverse

idToTile :: (Num a, Eq a) => a -> Maybe T.Tile
idToTile = $(invert (tileToId @Integer) fromInt32)