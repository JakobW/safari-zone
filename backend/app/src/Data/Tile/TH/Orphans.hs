{-# OPTIONS_GHC -fno-warn-orphans #-}

module Data.Tile.TH.Orphans () where

import Control.Monad.Fail
import CustomPrelude
import Data.Aeson
import Data.Tile.TH.Tile
import Data.Tile.TH.IdToTile

instance FromJSON Tile where
  parseJSON = withScientific "Tile" parseTile

parseTile :: MonadFail m => Scientific -> m Tile
parseTile n = case idToTile n of
  Just t -> pure t
  Nothing -> fail $ show n <> " is not a valid tile id"
