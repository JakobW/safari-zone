{-# LANGUAGE DeriveLift #-}
{-# LANGUAGE TemplateHaskellQuotes #-}
{-# LANGUAGE StrictData #-}

module Data.Tile.TH.Tile (Tile(..), TileIndex(..), inefficientFromDbId, toTileIndex) where

import CustomPrelude
import Generics.Deriving (GEnum)
import Language.Haskell.TH.Syntax (Lift)
import qualified Data.Tile.Types.ClassicTree as CT
import qualified Data.Encounter.EncounterType as EncounterType
import qualified Data.Tile.Types.Fence as F
import qualified Data.Tile.Types.LargeTree as LT
import qualified Data.Tile.Types.Meadow as M
import qualified Tile.MoveRule as MoveRule
import qualified Data.Tile.Types.Neighbour as N
import Data.List (elemIndex)

data Tile
  = Meadow M.Meadow
  | ClassicTree CT.ClassicTree
  | LargeTree LT.LargeTree
  | Sand N.NeighbourBased
  | Pavement N.NeighbourBased
  | BlueishMeadow N.NeighbourBased
  | DarkSand N.NeighbourBased
  | WhiteSand N.NeighbourBased
  | GrassMeadow
  | RowBush F.FenceBased
  | Water N.NeighbourBased
  | RockArea N.NeighbourBased
  | CutDecoration
  | BushDecoration
  | RockDecoration
  | MovableRockDecoration
  | BreakableRockDecoration
  | WoodenSignDecoration
  | PokeballDecoration
  | WhiteSignDecoration
  deriving (Show, Eq, Ord, Generic, GEnum, Lift)

data TileIndex = Selected Int | Combined Int Int
  deriving (Show, Eq, Lift)

instance MoveRule.HasMoveRule Tile where
  getMoveRule = \case
    Meadow _ -> MoveRule.Ground
    ClassicTree _ -> MoveRule.Blocked
    LargeTree _ -> MoveRule.Blocked
    Sand _ -> MoveRule.Ground
    GrassMeadow -> MoveRule.Ground
    Pavement _ -> MoveRule.Ground
    BlueishMeadow _ -> MoveRule.Ground
    DarkSand _ -> MoveRule.Ground
    WhiteSand _ -> MoveRule.Ground
    RowBush _ -> MoveRule.Blocked
    Water _ -> MoveRule.Water
    CutDecoration -> MoveRule.Blocked
    BushDecoration -> MoveRule.Blocked
    RockDecoration -> MoveRule.Blocked
    MovableRockDecoration -> MoveRule.Blocked
    BreakableRockDecoration -> MoveRule.Blocked
    WoodenSignDecoration -> MoveRule.Blocked
    PokeballDecoration -> MoveRule.Blocked
    WhiteSignDecoration -> MoveRule.Blocked
    RockArea _ -> MoveRule.Blocked

instance EncounterType.HasEncounterType Tile where
  getEncounterType = \case
    Meadow _ -> EncounterType.None
    ClassicTree _ -> EncounterType.None
    LargeTree _ -> EncounterType.None
    Sand _ -> EncounterType.None
    GrassMeadow -> EncounterType.Grass
    Pavement _ -> EncounterType.None
    BlueishMeadow _ -> EncounterType.None
    DarkSand _ -> EncounterType.None
    WhiteSand _ -> EncounterType.None
    RowBush _ -> EncounterType.None
    CutDecoration -> EncounterType.None
    BushDecoration -> EncounterType.None
    RockDecoration -> EncounterType.None
    MovableRockDecoration -> EncounterType.None
    BreakableRockDecoration -> EncounterType.None
    WoodenSignDecoration -> EncounterType.None
    PokeballDecoration -> EncounterType.None
    WhiteSignDecoration -> EncounterType.None
    RockArea _ -> EncounterType.Cave
    Water _ -> EncounterType.None

inefficientFromDbId :: Tile -> Maybe Int
inefficientFromDbId = flip elemIndex enumerateTiles

toTileIndex :: Tile -> TileIndex
toTileIndex = \case
  Meadow M.Meadow_1 -> Selected 0
  Meadow M.Meadow_2 -> Selected 1
  Meadow M.Meadow_3 -> Selected 2
  Meadow M.Meadow_4 -> Selected 3
  ClassicTree CT.TopOverlayLeft -> Selected 6
  ClassicTree CT.TopOverlayRight -> Selected 7
  GrassMeadow -> Selected 8
  CutDecoration -> Selected 9
  BushDecoration -> Selected 10
  RockDecoration -> Selected 11
  ClassicTree CT.MiddleLeft -> Selected 12
  ClassicTree CT.MiddleRight -> Selected 13
  Sand N.TopLeft -> Selected 16
  Sand N.Top -> Selected 17
  Sand N.TopRight -> Selected 18
  MovableRockDecoration -> Selected 19
  ClassicTree CT.BottomLeft -> Selected 20
  ClassicTree CT.BottomRight -> Selected 21
  Sand N.Left -> Selected 24
  Sand N.Middle -> Selected 25
  Sand N.Right -> Selected 26
  BreakableRockDecoration -> Selected 27
  WoodenSignDecoration -> Selected 28
  PokeballDecoration -> Selected 29
  LargeTree LT.TopOverlay -> Selected 30
  WhiteSignDecoration -> Selected 31
  Sand N.BottomLeft -> Selected 32
  Sand N.Bottom -> Selected 33
  Sand N.BottomRight -> Selected 34
  Sand N.CurveTopLeft -> Selected 35
  Sand N.CurveTopRight -> Selected 36
  LargeTree LT.TopLeft -> Selected 37
  LargeTree LT.TopMiddle -> Selected 38
  LargeTree LT.TopRight -> Selected 39
  Pavement N.TopLeft -> Selected 40
  Pavement N.Top -> Selected 41
  Pavement N.TopRight -> Selected 42
  Sand N.CurveBottomLeft -> Selected 43
  Sand N.CurveBottomRight -> Selected 44
  LargeTree LT.MidLeft -> Selected 45
  LargeTree LT.MidRight -> Selected 47
  Pavement N.Left -> Selected 48
  Pavement N.Middle -> Selected 49
  Pavement N.Right -> Selected 50
  Pavement N.CurveTopLeft -> Selected 51
  Pavement N.CurveTopRight -> Selected 52
  LargeTree LT.BottomLeftWithOverlay -> Selected 53
  LargeTree LT.BottomRightWithOverlay -> Selected 55
  Pavement N.BottomLeft -> Selected 56
  Pavement N.Bottom -> Selected 57
  Pavement N.BottomRight -> Selected 58
  Pavement N.CurveBottomLeft -> Selected 59
  Pavement N.CurveBottomRight -> Selected 60
  LargeTree LT.Middle -> Selected 62
  BlueishMeadow N.TopLeft -> Selected 64
  BlueishMeadow N.Top -> Selected 65
  BlueishMeadow N.TopRight -> Selected 66
  BlueishMeadow N.CurveTopLeft -> Selected 67
  BlueishMeadow N.CurveTopRight -> Selected 68
  LargeTree LT.BottomLeft -> Selected 69
  LargeTree LT.BottomMiddle -> Selected 70
  LargeTree LT.BottomRight -> Selected 71
  BlueishMeadow N.Left -> Selected 72
  BlueishMeadow N.Middle -> Selected 73
  BlueishMeadow N.Right -> Selected 74
  BlueishMeadow N.CurveBottomLeft -> Selected 75
  BlueishMeadow N.CurveBottomRight -> Selected 76
  DarkSand N.TopLeft -> Selected 77
  DarkSand N.Top -> Selected 78
  DarkSand N.TopRight -> Selected 79
  BlueishMeadow N.BottomLeft -> Selected 80
  BlueishMeadow N.Bottom -> Selected 81
  BlueishMeadow N.BottomRight -> Selected 82
  DarkSand N.CurveTopLeft -> Selected 83
  DarkSand N.CurveTopRight -> Selected 84
  DarkSand N.Left -> Selected 85
  DarkSand N.Middle -> Selected 86
  DarkSand N.Right -> Selected 87
  DarkSand N.CurveBottomLeft -> Selected 91
  DarkSand N.CurveBottomRight -> Selected 92
  DarkSand N.BottomLeft -> Selected 93
  DarkSand N.Bottom -> Selected 94
  DarkSand N.BottomRight -> Selected 95
  WhiteSand N.TopLeft -> Selected 112
  WhiteSand N.Top -> Selected 113
  WhiteSand N.TopRight -> Selected 114
  WhiteSand N.Left -> Selected 120
  WhiteSand N.Middle -> Selected 121
  WhiteSand N.Right -> Selected 122
  WhiteSand N.CurveTopLeft -> Selected 123
  WhiteSand N.CurveTopRight -> Selected 124
  WhiteSand N.BottomLeft -> Selected 128
  WhiteSand N.Bottom -> Selected 129
  WhiteSand N.BottomRight -> Selected 130
  WhiteSand N.CurveBottomLeft -> Selected 131
  WhiteSand N.CurveBottomRight -> Selected 132
  RowBush F.TopEnd -> Selected 135
  RowBush F.RightToBottom -> Selected 142
  RowBush F.LeftToBottom -> Selected 143
  RowBush F.BottomEnd -> Selected 150
  RowBush F.TopToBottom -> Selected 151
  RowBush F.RightToTop -> Selected 158
  RowBush F.LeftToTop -> Selected 159
  RowBush F.LeftEnd -> Selected 165
  RowBush F.LeftToRight -> Selected 166
  RowBush F.RightEnd -> Selected 167
  Water N.TopLeft -> Selected 213
  Water N.Top -> Selected 214
  Water N.TopRight -> Selected 215
  Water N.Left -> Selected 221
  Water N.Middle -> Selected 222
  Water N.Right -> Selected 223
  Water N.BottomLeft -> Selected 229
  Water N.Bottom -> Selected 230
  Water N.BottomRight -> Selected 231
  Water N.CurveTopLeft -> Selected 238
  Water N.CurveTopRight -> Selected 239
  Water N.CurveBottomLeft -> Selected 246
  Water N.CurveBottomRight -> Selected 247
  RockArea N.CurveBottomRight -> Selected 226
  RockArea N.CurveBottomLeft -> Selected 227
  RockArea N.CurveTopLeft -> Combined 236 243
  RockArea N.CurveTopRight -> Combined 236 245
  RockArea N.TopLeft -> Selected 235
  RockArea N.Top -> Selected 236
  RockArea N.TopRight -> Selected 237
  RockArea N.Left -> Selected 243
  RockArea N.Middle -> Selected 244
  RockArea N.Right -> Selected 245
  RockArea N.BottomLeft -> Selected 251
  RockArea N.Bottom -> Selected 252
  RockArea N.BottomRight -> Selected 253

enumerateNeighbourBased :: (N.NeighbourBased -> a) -> [a]
enumerateNeighbourBased =
  flip
    fmap
    [ N.TopLeft,
      N.Top,
      N.TopRight,
      N.Left,
      N.Middle,
      N.Right,
      N.BottomLeft,
      N.Bottom,
      N.BottomRight,
      N.CurveTopLeft,
      N.CurveTopRight,
      N.CurveBottomLeft,
      N.CurveBottomRight
    ]

enumerateTiles :: [Tile]
enumerateTiles =
  [ Meadow M.Meadow_1,
    Meadow M.Meadow_2,
    Meadow M.Meadow_3,
    Meadow M.Meadow_4,
    GrassMeadow,
    CutDecoration,
    BushDecoration,
    RockDecoration,
    MovableRockDecoration,
    BreakableRockDecoration,
    WoodenSignDecoration,
    PokeballDecoration,
    WhiteSignDecoration,
    ClassicTree CT.TopOverlayLeft,
    ClassicTree CT.TopOverlayRight,
    ClassicTree CT.MiddleLeft,
    ClassicTree CT.MiddleRight,
    ClassicTree CT.BottomLeft,
    ClassicTree CT.BottomRight
  ]
    <> enumerateNeighbourBased Sand
    <> enumerateNeighbourBased Pavement
    <> enumerateNeighbourBased BlueishMeadow
    <> enumerateNeighbourBased DarkSand
    <> enumerateNeighbourBased WhiteSand
    <> enumerateNeighbourBased Water
    <> [ LargeTree LT.TopOverlay,
         LargeTree LT.TopLeft,
         LargeTree LT.TopMiddle,
         LargeTree LT.TopRight,
         LargeTree LT.MidLeft,
         LargeTree LT.MidRight,
         LargeTree LT.BottomLeftWithOverlay,
         LargeTree LT.BottomRightWithOverlay,
         LargeTree LT.Middle,
         LargeTree LT.BottomLeft,
         LargeTree LT.BottomMiddle,
         LargeTree LT.BottomRight
       ]
    <> [ RowBush F.TopEnd,
         RowBush F.RightToBottom,
         RowBush F.LeftToBottom,
         RowBush F.BottomEnd,
         RowBush F.TopToBottom,
         RowBush F.RightToTop,
         RowBush F.LeftToTop,
         RowBush F.LeftEnd,
         RowBush F.LeftToRight,
         RowBush F.RightEnd
       ]
    <> enumerateNeighbourBased RockArea
