{-# LANGUAGE TemplateHaskell #-}

module Data.Tile.TH.TileToId (tileToId) where

import qualified Data.Tile.TH.Tile as T
import CustomPrelude
import TH.Inverse

tileToId :: (Num a) => T.Tile -> a
tileToId = $(invertToNonPartial T.inefficientFromDbId)