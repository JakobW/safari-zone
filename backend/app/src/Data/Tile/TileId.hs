{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Data.Tile.TileId (TileId) where

import CustomPrelude
import Database.PostgreSQL.Typed.Types (PGParameter, PGColumn)
import Data.Aeson

newtype TileId = TileId {getTileId :: Int32}
  deriving stock (Show, Eq)
  deriving newtype (PGColumn "integer", PGParameter "integer", FromJSON, ToJSON)