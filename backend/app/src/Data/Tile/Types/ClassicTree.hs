{-# LANGUAGE DeriveLift #-}
{-# LANGUAGE StrictData #-}

module Data.Tile.Types.ClassicTree (ClassicTree (..)) where

import CustomPrelude
import Generics.Deriving (GEnum)
import Language.Haskell.TH.Syntax (Lift)

data ClassicTree
  = MiddleLeft
  | MiddleRight
  | BottomLeft
  | BottomRight
  | TopOverlayLeft
  | TopOverlayRight
  deriving (Show, Eq, Ord, Enum, Bounded, Generic, GEnum, Lift)
