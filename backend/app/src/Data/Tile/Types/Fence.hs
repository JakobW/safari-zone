{-# LANGUAGE DeriveLift #-}
{-# LANGUAGE StrictData #-}

module Data.Tile.Types.Fence (FenceBased(..)) where

import CustomPrelude
import Generics.Deriving (GEnum)
import Language.Haskell.TH.Syntax (Lift)

data FenceBased =
  LeftEnd
  | RightEnd
  | TopEnd
  | BottomEnd
  | LeftToRight
  | TopToBottom
  | LeftToBottom
  | LeftToTop
  | RightToBottom
  | RightToTop
  deriving (Show, Eq, Ord, Enum, Bounded, Generic, GEnum, Lift)

