{-# LANGUAGE DeriveLift #-}
{-# LANGUAGE StrictData #-}

module Data.Tile.Types.LargeTree (LargeTree(..)) where

import CustomPrelude
import Generics.Deriving (GEnum)
import Language.Haskell.TH.Syntax (Lift)

data LargeTree =
  TopLeft
  | TopMiddle
  | TopRight
  | MidLeft
  | Middle
  | MidRight
  | BottomLeft
  | BottomMiddle
  | BottomRight
  | BottomLeftWithOverlay
  | BottomRightWithOverlay
  | TopOverlay
  deriving (Show, Eq, Ord, Bounded, Enum, Generic, GEnum, Lift)