{-# LANGUAGE DeriveLift #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE StrictData #-}

module Data.Tile.Types.Meadow (Meadow(..)) where

import CustomPrelude
import Generics.Deriving (GEnum)
import Language.Haskell.TH.Syntax (Lift)

data Meadow
  = Meadow_1
  | Meadow_2
  | Meadow_3
  | Meadow_4
  deriving (Show, Eq, Ord, Enum, Bounded, Generic, GEnum, Lift)