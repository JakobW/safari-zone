{-# LANGUAGE DeriveLift #-}
{-# LANGUAGE StrictData #-}

module Data.Tile.Types.Neighbour (NeighbourBased(..)) where

import CustomPrelude hiding (Left, Right)
import Generics.Deriving (GEnum)
import Language.Haskell.TH.Syntax (Lift)

data NeighbourBased
  = TopLeft
  | Top
  | TopRight
  | Left
  | Middle
  | Right
  | BottomLeft
  | Bottom
  | BottomRight
  | CurveTopLeft
  | CurveTopRight
  | CurveBottomLeft
  | CurveBottomRight
  deriving (Show, Eq, Ord, Enum, Bounded, Generic, GEnum, Lift)