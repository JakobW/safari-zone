{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE StrictData #-}

module Data.TileType
  ( TileType (..),
    toTileType,
  )
where

import CustomPrelude
import Data.Aeson
import qualified Generic.Random as QC
import TH.GenTileType
import qualified Test.QuickCheck as QC
import qualified Data.Tile.TH.Tile as Tile

$(genSimplifiedDatatype ''Tile.Tile "TileType")

deriving instance Generic TileType
deriving instance FromJSON TileType
deriving instance ToJSON TileType

instance QC.Arbitrary TileType where
  arbitrary = QC.genericArbitrary QC.uniform
