{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE UndecidableInstances #-}

module Data.User (
  module Data.User.UserId,
  module Data.User.UserName,
  User,
  UserWithId,
  position
) where

import Data.User.UserId
import Data.User.UserName
import Data.Coord
import CustomPrelude
import Data.WithId
import qualified Db.Row
import Has

newtype User = User
  { position :: Position
  }
  deriving (Show, Eq)

type UserWithId = WithId UserId User

instance Db.Row.RowLike User where
  type AsRow User = '[Unit 'X, Unit 'Y]
  fromRow = User <$> (mkCoordLike . getTyped <*> getTyped)
  toRow u = (position u ^. #x, position u ^. #y)

$(makeFieldLabelsNoPrefix ''User)