{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Data.User.UserId (UserId) where

import CustomPrelude
import Data.Aeson
import Database.PostgreSQL.Typed.Types (PGColumn, PGParameter)
import Servant

newtype UserId = UserId {getUserId :: Int32}
  deriving stock (Show, Eq)
  deriving newtype
    ( PGParameter "integer",
      PGColumn "integer",
      ToJSON,
      FromJSON,
      FromHttpApiData,
      ToHttpApiData
    )
