{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Data.User.UserName (UserName(getUserName), mkUserName)where

import CustomPrelude
import Data.Aeson
import qualified Test.QuickCheck as QC
import qualified Util.SecurityTextFilter
import Database.PostgreSQL.Typed.Types (PGParameter, PGColumn)
import qualified Util

newtype UserName = UserName {getUserName :: Text}
  deriving stock (Show, Eq)
  deriving newtype
    ( PGParameter "text",
      PGParameter "character varying",
      PGColumn "text",
      PGColumn "character varying",
      ToJSON
    )

mkUserName :: Text -> Either Text UserName
mkUserName name | not (null name) =
  (Right . UserName .  Util.SecurityTextFilter.filterText) name
mkUserName _ = Left "User name cannot be empty"

instance FromJSON UserName where
  parseJSON = withText "UserName" (Util.eitherToParser . mkUserName)

instance QC.Arbitrary UserName where
  arbitrary = Util.SecurityTextFilter.validTextGen
      `QC.suchThatMap` (Util.eitherToMaybe . mkUserName)
