{-# LANGUAGE StrictData #-}

module Data.WildPokemon (WildPokemonId, ShinyStatus(..), shinyChance, isShiny) where

import CustomPrelude
import Data.Aeson
import Database.PostgreSQL.Typed.Types
import qualified Random
import qualified Test.QuickCheck as QC

data ShinyStatus = Shiny | NonShiny deriving (Show, Eq, Generic, ToJSON, FromJSON)

isShiny :: ShinyStatus -> Bool
isShiny Shiny = True
isShiny NonShiny = False

fromBool :: Bool -> ShinyStatus
fromBool True = Shiny
fromBool False = NonShiny

shinyChance :: Random.CoinFlip -> ShinyStatus
shinyChance =
  \case
    Random.Heads -> Shiny
    Random.Tails -> NonShiny

instance PGColumn "boolean" ShinyStatus where
  pgDecode typeId = fromBool . pgDecode typeId

instance PGParameter "boolean" ShinyStatus where
  pgEncode typeId = pgEncode typeId . isShiny

newtype WildPokemonId = WildPokemonId Int32
  deriving newtype
    ( Show,
      Eq,
      FromJSON,
      ToJSON,
      PGParameter "integer",
      PGColumn "integer",
      QC.Arbitrary
    )

instance QC.Arbitrary ShinyStatus where
  arbitrary = fromBool <$> QC.arbitrary