{-# LANGUAGE GADTs #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}

module Data.WithId (WithId (WithId), withTypedId, mkWithId, theId, theContent) where

import CustomPrelude
import Data.Aeson
import qualified Data.HashMap.Strict as H
import qualified Db.Row
import Has

withTypedId :: HasType id c => c -> a -> WithId id a
withTypedId = WithId . getTyped

mkWithId :: HasType id c => (c -> a) -> c -> WithId id a
mkWithId mk c = withTypedId c (mk c)

data WithId id a = WithId {theId :: id, theContent :: a}
  deriving (Eq, Show, Functor)

$(makeFieldLabelsNoPrefix ''WithId)

instance (ToJSON a, ToJSON id) => ToJSON (WithId id a) where
  toJSON (WithId id' a) = Object $ case rest of
    Object x ->
      idPart `union` x
    _ ->
      idPart `union` H.fromList ["content" .= rest]
    where
      idPart :: H.HashMap Text Value
      idPart = H.fromList ["id" .= id']

      rest :: Value
      rest = toJSON a

instance (FromJSON a, FromJSON id) => FromJSON (WithId id a) where
  parseJSON (Object v) = do
    parsed <- WithId <$> (v .: "id")
    fmap parsed (parseJSON $ Object v)
  parseJSON _ = mzero

instance (Db.Row.Extends (Db.Row.AsRow a) id, Db.Row.RowLike a) => Db.Row.RowLike (WithId id a) where
  type AsRow (WithId id a) = (id ': Db.Row.AsRow a)
  fromRow row = WithId (getTyped row) (Db.Row.fromRow row)
  toRow (WithId i a) = Db.Row.addTo (Db.Row.toRow a) i
