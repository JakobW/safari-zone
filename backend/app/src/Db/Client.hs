{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}

module Db.Client
  ( connDb,
    runPool,
    useDb,
    pgSQL,
    PGSimpleQuery,
    PGPreparedQuery,
    WithDB (..),
    querySingleResult,
    queryToString,
    DBM,
    RunDb (..),
    pgErrorToEither,
  )
where

import CustomPrelude
import Data.Has
import Data.Pool (Pool, createPool, withResource)
import Database
import Database.PostgreSQL.Typed
import Database.PostgreSQL.Typed.Query
import Database.PostgreSQL.Typed.Types
import qualified Katip as K
import AppMigration (useDb)
import qualified Util

newtype RunDb = RunDb (forall a. (PGConnection -> IO a) -> IO a)

newtype DbException = DbException Text
  deriving stock (Show)
  deriving anyclass (Exception)

connDb :: (Has DBSettings c) => c -> IO (Pool PGConnection)
connDb ctx =
  createPool
    (connect ctx)
    pgDisconnect
    2 -- stripes
    60 -- seconds that unused connection stays open
    10 -- max 10 conns per stripe

runPool :: Pool PGConnection -> RunDb
runPool pool = RunDb $ withResource pool

type DBM m = (WithDB m, K.KatipContext m, K.Katip m, MonadCatch m)

class (Monad m, MonadCatch m) => WithDB m where
  execQuery :: PGQuery q a => q -> m [a]
  parseQueryResult :: (a -> Either Text b) -> [a] -> m [b]

pgErrorToEither :: MonadCatch m => m a -> m (Either Text a)
pgErrorToEither m = fmap Right m `catch` (\(e :: PGError) -> pure $ Left $ tshow e)

queryToString :: (PGQuery q a) => q -> ByteString
queryToString = getQueryString unknownPGTypeEnv

querySingleResult :: (WithDB m, PGQuery q a) => q -> m (Either Text a)
querySingleResult q =
  extractSingle <$> execQuery q
  where
    extractSingle result =
      Util.justEither
        ( "Expected single result from query '"
            ++ decodeUtf8 (queryToString q)
            ++ "' but received "
            ++ tshow (length result)
            ++ " results"
        )
        (headMay result)
