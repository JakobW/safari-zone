module Db.Redis.Client (fromEnv, withRedis) where

import qualified Control.Retry as Retry
import CustomPrelude
import qualified Data.Map.Strict as Map
import qualified Database.Redis as R
import qualified Katip as K

localConnectInfo :: R.ConnectInfo
localConnectInfo =
  R.defaultConnectInfo
    { -- 10 seconds connect window
      R.connectTimeout = Just 10
    }

withRedis :: (K.KatipContext m, MonadCatch m, MonadUnliftIO m) => R.ConnectInfo -> (R.Connection -> m a) -> m a
withRedis info = bracket (checkedConnectWithRetry info `K.logExceptionM` K.EmergencyS) (liftIO . R.disconnect)

fromEnv :: Map String String -> R.ConnectInfo
fromEnv env =
  localConnectInfo
    { R.connectHost =
        Map.lookup "REDIS_CONNECT_HOST" env
          |> fromMaybe (R.connectHost localConnectInfo),
      R.connectPort =
        Map.lookup "REDIS_CONNECT_PORT" env
          >>= readMay .> fmap R.PortNumber
          |> fromMaybe (R.connectPort localConnectInfo)
    }

checkedConnectWithRetry :: MonadIO m => R.ConnectInfo -> m R.Connection
checkedConnectWithRetry connInfo = liftIO $
  Retry.recoverAll (Retry.exponentialBackoff 50000 <> Retry.limitRetries 10) $
    \(Retry.RetryStatus tryNo _ _) -> do
      if tryNo > 0
        then putStrLn $ "Retrying to get a redis connection... Try: " <> tshow tryNo <> ". Backing off exponentially."
        else putStrLn "Connecting to redis..."
      R.checkedConnect connInfo
