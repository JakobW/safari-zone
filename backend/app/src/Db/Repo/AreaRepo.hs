{-# LANGUAGE DataKinds #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Db.Repo.AreaRepo (createArea, getAreas, updateArea) where

import CustomPrelude
import Data.Area
import qualified Data.Coord as C
import Db.Client

useDb

createArea :: forall m . (WithDB m) => (AreaName, Range 'C.X, Range 'C.Y) -> m (Either Text AreaId)
createArea (areaName, rangeX, rangeY) =
   (fmap join . pgErrorToEither . querySingleResult)
      [pgSQL|
    INSERT INTO area (name, range_x, range_y)
    VALUES (${areaName}, ${rangeX}, ${rangeY})
    RETURNING id
    |]

getAreas :: (WithDB m) => m [(AreaId, AreaName, Range 'C.X, Range 'C.Y)]
getAreas =
  execQuery
    [pgSQL|
      SELECT id, name, range_x, range_y
      FROM area
    |]

updateArea :: (WithDB m) => AreaId -> (AreaName, Range 'C.X, Range 'C.Y) -> m ()
updateArea id' (areaName, rangeX, rangeY) =
  void $ execQuery
    [pgSQL|
      UPDATE area
      SET name = ${areaName},
          range_x = ${rangeX},
          range_y = ${rangeY}
      WHERE id = ${id'}
    |]
