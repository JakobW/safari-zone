{-# LANGUAGE DataKinds #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Db.Repo.DexEntryRepo (getDexEntryDetailOfUser, getDexEntriesOfUser, addPokemonToUser) where

import CustomPrelude
import Db.Client
import Data.Pokemon
import Data.User (UserId)
import Data.WildPokemon (ShinyStatus(..), isShiny)

useDb

getDexEntriesOfUser :: WithDB m => UserId -> m [(PokemonNumber, AmountNormal, AmountShiny, PokemonName, PokemonImageName)]
getDexEntriesOfUser usrId =
  execQuery
    [pgSQL|SELECT number, amount_normal, amount_shiny, name, image_name FROM dex_entry
  INNER JOIN users ON users.id = dex_entry.user_id
  INNER JOIN pokemon ON pokemon.id = dex_entry.pokemon_id
  WHERE users.id = ${usrId}
  ORDER BY number
|]

getDexEntryDetailOfUser ::
  WithDB m =>
  UserId ->
  PokemonId ->
  m
    ( Either
        Text
        ( AmountNormal,
          AmountShiny,
          PokemonNumber,
          PokemonName,
          PokemonType,
          Maybe PokemonType,
          PokemonWeight,
          PokemonHeight,
          PokemonDescription,
          PokemonImageName
        )
    )
getDexEntryDetailOfUser usrId pokeId =
  querySingleResult
    [pgSQL|SELECT
      amount_normal, amount_shiny,
      number, name, primary_type, secondary_type, weight, height, description, image_name
      FROM dex_entry
      INNER JOIN users ON users.id = dex_entry.user_id
      INNER JOIN pokemon ON pokemon.id = dex_entry.pokemon_id
      WHERE users.id = ${usrId} AND pokemon.id = ${pokeId}
    |]

addPokemonToUser :: (WithDB m) => UserId -> PokemonId -> ShinyStatus -> m ()
addPokemonToUser user number shinyStatus = do
  maybeDexEntry <- getPreviousDexEntry user number
  case maybeDexEntry of
    Just (dexEntryId, amountNormal, amountShiny) ->
      setDexEntry dexEntryId $ if isShiny shinyStatus
        then (amountNormal, amountShiny + 1)
        else (amountNormal + 1, amountShiny)
    Nothing ->
      insertDexEntry
  where
    getPreviousDexEntry :: WithDB m => UserId -> PokemonId -> m (Maybe (Int32, AmountNormal, AmountShiny))
    getPreviousDexEntry usrId pokeId =
      headMay
        <$> execQuery
          [pgSQL|
        SELECT id, amount_normal, amount_shiny
        FROM dex_entry
        WHERE user_id = ${usrId}
        AND pokemon_id = ${pokeId}
      |]
    insertDexEntry :: WithDB m => m ()
    insertDexEntry =
      let amountNormal = (if isShiny shinyStatus then 0 else 1) :: Int32
          amountShiny = 1 - amountNormal
      in void $
        execQuery
          [pgSQL|
        INSERT INTO dex_entry (amount_normal, amount_shiny, user_id, pokemon_id)
        VALUES (${amountNormal}, ${amountShiny}, ${user}, ${number})
      |]
    setDexEntry :: WithDB m => Int32 -> (AmountNormal, AmountShiny) -> m ()
    setDexEntry dexEntryId (amountNormal, amountShiny) =
      void $
        execQuery
          [pgSQL|
        UPDATE dex_entry
        SET amount_normal = ${amountNormal},
            amount_shiny = ${amountShiny}
        WHERE id = ${dexEntryId}
      |]
