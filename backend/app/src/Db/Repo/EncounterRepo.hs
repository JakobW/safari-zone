{-# LANGUAGE DataKinds #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Db.Repo.EncounterRepo
  ( getEncounters,
    upsertEncounter,
    removeEncounter,
    getEncountersForPosition,
  )
where

import CustomPrelude
import Data.Area (AreaId)
import Data.Encounter
import Data.Pokemon (PokemonId)
import Db.Client
import qualified Data.Coord as C
import Has

useDb

getEncounters :: (WithDB m) => AreaId -> m [(AreaId, PokemonId, EncounterProbability)]
getEncounters areaId' =
  execQuery
    [pgSQL|
    SELECT area_id, pokemon_id, probability
    FROM encounter
    WHERE area_id = ${areaId'}
  |]

upsertEncounter :: (WithDB m, HasAll '[AreaId, PokemonId, EncounterProbability] a) => a -> m ()
upsertEncounter a =
  void $ execQuery
    [pgSQL|
      INSERT INTO encounter (area_id, pokemon_id, probability)
      VALUES (${aId}, ${pId}, ${prob})
      ON CONFLICT (area_id, pokemon_id) DO UPDATE
          SET probability = ${prob}
    |] where
      prob = getTyped @EncounterProbability a
      aId = getTyped @AreaId a
      pId = getTyped @PokemonId a

removeEncounter ::
  (WithDB m) =>
  (PokemonId, AreaId) ->
  m ()
removeEncounter (pId, aId) =
  void $ execQuery
    [pgSQL|
      DELETE FROM encounter
      WHERE area_id = ${aId}
      AND pokemon_id = ${pId}
    |]

getEncountersForPosition :: (WithDB m) => (C.Unit 'C.X, C.Unit 'C.Y) -> m [(AreaId, PokemonId, EncounterProbability)]
getEncountersForPosition (x, y) =
  execQuery
    [pgSQL|
    SELECT encounter.area_id, encounter.pokemon_id, encounter.probability
    FROM encounter JOIN area
    ON encounter.area_id = area.id
    WHERE area.range_x @> ${x}::int4
    AND area.range_y @> ${y}::int4
  |]
