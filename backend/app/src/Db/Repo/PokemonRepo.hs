{-# LANGUAGE DataKinds #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Db.Repo.PokemonRepo (findAllPoke, findFirstWithNumber, getById) where

import CustomPrelude
import Db.Client
import Data.Pokemon

useDb

findAllPoke ::
  (WithDB m) =>
  m
    [ ( PokemonId,
        PokemonNumber,
        PokemonName,
        PokemonType,
        Maybe PokemonType,
        PokemonWeight,
        PokemonHeight,
        PokemonDescription,
        PokemonImageName
      )
    ]
findAllPoke =
  execQuery
    [pgSQL|
      SELECT id, number, name, primary_type, secondary_type, weight, height, description, image_name
      FROM pokemon
    |]

findFirstWithNumber ::
  (WithDB m) =>
  PokemonNumber ->
  m
    ( Maybe
        ( PokemonId,
          PokemonNumber,
          PokemonName,
          PokemonType,
          Maybe PokemonType,
          PokemonWeight,
          PokemonHeight,
          PokemonDescription,
          PokemonImageName
        )
    )
findFirstWithNumber num =
  headMay
    <$> execQuery
      [pgSQL|
      SELECT id, number, name, primary_type, secondary_type, weight, height, description, image_name
      FROM pokemon
      WHERE number = ${num}
    |]

getById ::
  (WithDB m) =>
  PokemonId ->
  m
    ( Either
        Text
        ( PokemonId,
          PokemonNumber,
          PokemonName,
          PokemonType,
          Maybe PokemonType,
          PokemonWeight,
          PokemonHeight,
          PokemonDescription,
          PokemonImageName
        )
    )
getById id' =
  querySingleResult
    [pgSQL|
      SELECT id, number, name, primary_type, secondary_type, weight, height, description, image_name
      FROM pokemon
      WHERE id = ${id'}
    |]
