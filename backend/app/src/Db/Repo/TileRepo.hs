{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}

module Db.Repo.TileRepo
  ( getAllTilesOrdered,
    getAllTilesOfLayerOrdered,
    getAllNonOverlayTilesOfLayer,
    insertTiles,
    deleteGroundTiles,
    deleteOverlayTilesFromLayer,
    getAllLayers,
    getTilesAtPosition
  )
where

import CustomPrelude
import qualified Data.Coord as C
import Data.Tile
import Database.PostgreSQL.Typed.Array ()
import Db.Client
import qualified Websocket.PubSub.Channel.Cache as Cache
import qualified Data.Time.Clock as Clock

useDb

getAllLayers :: (WithDB m) => m [LayerNumber]
getAllLayers =
  execQuery
    [pgSQL|
    SELECT DISTINCT layer
    FROM tile
  |]

parseTile :: Int32 -> Either Text Tile
parseTile n = case idToTile n of
  Just tile -> Right tile
  Nothing -> Left $ "Cannot find tile for id " <> tshow n

parseTileRow :: (C.Unit 'C.X, C.Unit 'C.Y, Int32, LayerNumber, LayerType) -> Either Text (C.Unit 'C.X, C.Unit 'C.Y, Tile, Layer)
parseTileRow (x, y, tileId, layerN, layerT) = do
  tile <- parseTile tileId
  let layer = Data.Tile.mkLayer layerN layerT
  pure (x, y, tile, layer)

getTilesAtPosition :: WithDB m => C.Position -> m [Tile]
getTilesAtPosition pos = execQuery [pgSQL|
  SELECT tile FROM tile WHERE coord_x = ${C.x pos} AND coord_y = ${C.y pos}
|] >>= parseQueryResult parseTile

tilesCacheKey :: Text
tilesCacheKey = "tilesCache"

tilesCacheTime :: Clock.NominalDiffTime
tilesCacheTime = 6 * 60

getAllTilesOrdered :: (DBM m, Cache.CacheM m) => m [(C.Unit 'C.X, C.Unit 'C.Y, Tile, Layer)]
getAllTilesOrdered = Cache.useCache tilesCacheKey tilesCacheTime getAllTilesOrdered'

getAllTilesOrdered' :: (WithDB m) => m [(C.Unit 'C.X, C.Unit 'C.Y, Tile, Layer)]
getAllTilesOrdered' =
  execQuery
    [pgSQL|
    SELECT coord_x, coord_y, tile, layer, layerType
    FROM tile
    WHERE coord_x >= 0 AND coord_y >= 0
    ORDER BY coord_y, coord_x
   |]
    >>= parseQueryResult parseTileRow

getAllTilesOfLayerOrdered :: (WithDB m) => LayerNumber -> m [(C.Unit 'C.X, C.Unit 'C.Y, Tile, LayerType)]
getAllTilesOfLayerOrdered layerNumber =
  execQuery
    [pgSQL|
    SELECT coord_x, coord_y, tile, layerType
    FROM tile
    WHERE layer = ${layerNumber}
    AND coord_x >= 0 AND coord_y >= 0
    ORDER BY coord_y, coord_x
   |]
    >>= parseQueryResult
      (\(x, y, tileId, tileType) -> (x,y,,tileType) <$> parseTile tileId)

parseNonLayerTileRow :: (C.Unit 'C.X, C.Unit 'C.Y, Int32) -> Either Text (C.Unit 'C.X, C.Unit 'C.Y, Tile)
parseNonLayerTileRow (x, y, tileId) = (x,y,) <$> parseTile tileId

getAllNonOverlayTilesOfLayer :: (WithDB m) => LayerNumber -> m [(C.Unit 'C.X, C.Unit 'C.Y, Tile)]
getAllNonOverlayTilesOfLayer layerNumber =
  execQuery
    [pgSQL|
    SELECT coord_x, coord_y, tile
    FROM tile
    WHERE layerType = ${Ground}
    AND layer = ${layerNumber}
    AND coord_x >= 0 AND coord_y >= 0
    ORDER BY coord_y, coord_x
   |]
    >>= parseQueryResult parseNonLayerTileRow

deleteGroundTiles :: (DBM m, Cache.CacheM m) => LayerNumber -> [C.Position] -> m ()
deleteGroundTiles layerNumber = Cache.invalidateCache tilesCacheKey . deleteGroundTiles' layerNumber

deleteGroundTiles' :: forall m. (WithDB m) => LayerNumber -> [C.Position] -> m ()
deleteGroundTiles' layerNumber = void . mapM (execQuery . deleteTileQuery)
  where
    deleteTileQuery :: C.Position -> PGPreparedQuery ()
    deleteTileQuery pos =
      [pgSQL|$
        DELETE FROM tile
        WHERE coord_x = ${C.x pos}
        AND coord_y = ${C.y pos}
        AND layer = ${layerNumber}
        AND layerType = ${Ground}
      |]

deleteOverlayTilesFromLayer :: (DBM m, Cache.CacheM m) => LayerNumber -> m ()
deleteOverlayTilesFromLayer layerNumber =
  (void . Cache.invalidateCache tilesCacheKey) $
    execQuery
    [pgSQL|
      DELETE FROM tile
      WHERE layer = ${layerNumber}
      AND layerType = ${Overlay}
    |]

insertTiles :: (DBM m, Cache.CacheM m) => [(C.Unit 'C.X, C.Unit 'C.Y, Tile, Layer)] -> m ()
insertTiles = void . Cache.invalidateCache tilesCacheKey . execQuery . insertTilesQuery . fmap convertRow
  where
    convertRow :: (cx, cy, t, Layer) -> (cx, cy, t, LayerNumber, LayerType)
    convertRow (cx, cy, t, l) = (cx, cy, t, layerNumber l, layerType l)

insertTilesQuery :: [(C.Unit 'C.X, C.Unit 'C.Y, Tile, LayerNumber, LayerType)] -> PGSimpleQuery ()
insertTilesQuery vals =
  [pgSQL|
  INSERT INTO tile (coord_x, coord_y, tile, layer, layerType)
  VALUES (unnest(${fmap (view _1) vals} :: integer[]),
          unnest(${fmap (view _2) vals} :: integer[]),
          unnest(${tiles} :: integer[]),
          unnest(${fmap (view _4) vals} :: integer[]),
          unnest(${fmap (view _5) vals} :: layertype[])
        )
  ON CONFLICT (coord_x, coord_y, layer, layerType) DO UPDATE
        SET tile = excluded.tile
|]
  where
    tiles :: [Int32]
    tiles = fmap (tileToId . view _3) vals
