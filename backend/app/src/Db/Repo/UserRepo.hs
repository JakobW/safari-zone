{-# LANGUAGE DataKinds #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}

module Db.Repo.UserRepo (getUserOrCreateDefault, setUserPosition) where

import qualified Auth
import qualified Auth.Andrena.AuthUser as Andrena
import qualified Auth.Data.UserId as Local
import qualified Auth.Data.UserWithRole as Local
import CustomPrelude
import qualified Data.Coord as C
import Data.User
import Db.Client

useDb

getUserOrCreateDefault :: (WithDB m) => Auth.JWTUser -> m (Either Text (UserId, C.Unit 'C.X, C.Unit 'C.Y))
getUserOrCreateDefault (Auth.Andrena andrena) = getAndrenaUserOrCreateDefault (Andrena.preferred_username andrena)
getUserOrCreateDefault (Auth.Local localUser) = getLocalUserByIdOrCreateDefault (Local.userId localUser)

getAndrenaUserOrCreateDefault :: (WithDB m) => UserName -> m (Either Text (UserId, C.Unit 'C.X, C.Unit 'C.Y))
getAndrenaUserOrCreateDefault name = do
  eitherUser <-
    querySingleResult
      [pgSQL|
      SELECT id, current_tile_x, current_tile_y
      FROM users
      WHERE m_nummer = ${name}
    |]
  case eitherUser of
    Right usr ->
      pure (Right usr)
    Left _ -> do
      userId <- createUserWithMNummer (name, defaultTileX, defaultTileY)
      pure $ fmap (,defaultTileX,defaultTileY) userId

defaultTileX :: C.Unit 'C.X
defaultTileX = 0

defaultTileY :: C.Unit 'C.Y
defaultTileY = 0

setUserPosition :: (WithDB m) => UserId -> (C.Unit 'C.X, C.Unit 'C.Y) -> m ()
setUserPosition userId' pos =
  void $
    execQuery
      [pgSQL|
      UPDATE users
      SET current_tile_x = ${pos^._1},
          current_tile_y = ${pos^._2}
      WHERE id = ${userId'}
   |]

getLocalUserByIdOrCreateDefault :: (WithDB m) => Local.UserId -> m (Either Text (UserId, C.Unit 'C.X, C.Unit 'C.Y))
getLocalUserByIdOrCreateDefault usrId =
  getLocalUserById usrId
    >>= \case
      Right usr ->
        pure (Right usr)
      Left _ -> do
        userId <- createInternalUser usrId
        pure $ fmap (,defaultTileX,defaultTileY) userId

getLocalUserById :: (WithDB m) => Local.UserId -> m (Either Text (UserId, C.Unit 'C.X, C.Unit 'C.Y))
getLocalUserById usrId =
  querySingleResult
    [pgSQL|
      SELECT id, current_tile_x, current_tile_y
      FROM users
      WHERE internal_id = ${usrId}
   |]

createUserWithMNummer :: (WithDB m) => (UserName, C.Unit 'C.X, C.Unit 'C.Y) -> m (Either Text UserId)
createUserWithMNummer user = do
  querySingleResult
    [pgSQL|
      INSERT INTO users (m_nummer, current_tile_x, current_tile_y)
      VALUES (${user^._1}, ${user^._2}, ${user^._3})
      RETURNING id
    |]

createInternalUser :: (WithDB m) => Local.UserId -> m (Either Text UserId)
createInternalUser uId =
  querySingleResult
    [pgSQL|
    INSERT INTO users (internal_id, current_tile_x, current_tile_y)
    VALUES (${uId}, ${defaultTileX}, ${defaultTileY})
    RETURNING id
  |]
