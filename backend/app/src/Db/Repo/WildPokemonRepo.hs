{-# LANGUAGE TemplateHaskell #-}

module Db.Repo.WildPokemonRepo (getAllWildPokemon, getById, createWildPokemon, setWildPokemonPosition, deleteWildPokemon) where

import CustomPrelude
import Data.Coord
import Data.Pokemon.PokemonFields
import Data.Pokemon.PokemonId
import Data.Time.Clock
import Data.WildPokemon
import Db.Client

useDb

getAllWildPokemon :: WithDB m => m [(WildPokemonId, Unit 'X, Unit 'Y, PokemonId, ShinyStatus, UTCTime)]
getAllWildPokemon =
  execQuery
    [pgSQL|
    SELECT id, coord_x, coord_y, pokemon_id, is_shiny, despawn_time
    FROM wild_pokemon
  |]

getById :: WithDB m => WildPokemonId -> m (Either Text (PokemonName, PokemonImageName, PokemonId, ShinyStatus, UTCTime, Unit 'X, Unit 'Y))
getById wpId =
  querySingleResult
    [pgSQL|
      SELECT p.name, p.image_name, p.id, wp.is_shiny, wp.despawn_time, wp.coord_x, wp.coord_y
      FROM wild_pokemon AS wp
      JOIN pokemon AS p ON p.id = wp.pokemon_id
      WHERE wp.id = ${wpId}
    |]

createWildPokemon :: WithDB m => PokemonId -> Position -> ShinyStatus -> DiffTime -> m (Either Text WildPokemonId)
createWildPokemon pId pos shinyStatus remainingMinutes =
  querySingleResult
    [pgSQL|
  INSERT INTO wild_pokemon (coord_x, coord_y, pokemon_id, is_shiny, despawn_time)
  VALUES (${x pos}, ${y pos}, ${pId}, ${shinyStatus}, NOW() + ${remainingMinutes})
  RETURNING id
  |]

setWildPokemonPosition :: WithDB m => WildPokemonId -> Position -> m ()
setWildPokemonPosition wId pos =
  void $
    execQuery
      [pgSQL|
    UPDATE wild_pokemon
    SET coord_x = ${x pos},
        coord_y = ${y pos}
    WHERE id = ${wId}
  |]

deleteWildPokemon :: WithDB m => WildPokemonId -> m ()
deleteWildPokemon wId =
  void $
    execQuery
      [pgSQL|
    DELETE FROM wild_pokemon
    WHERE id = ${wId}
  |]
