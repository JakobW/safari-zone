{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE AllowAmbiguousTypes #-}

module Db.Row (RowLike(..), TupleRep, AddT(..), Extends) where

import Data.Kind (Type)
import Has

class RowLike a where
  type AsRow a :: [Type]
  fromRow :: HasAll (AsRow a) r => r -> a
  toRow :: a -> TupleRep (AsRow a)

type family TupleRep (row :: [Type]) where
  TupleRep '[a] = Unit a
  TupleRep (a ': as) = Added (TupleRep as) a

type Extends (as :: [Type]) a = (AddT (TupleRep as), Added (TupleRep as) a ~ TupleRep (a ': as))

newtype Unit a = Unit a

class AddT a where
  type Added a b
  addTo :: a -> b -> Added a b

instance AddT (Unit a1) where
  type Added (Unit a1) a2 = (a2, a1)
  addTo (Unit a1) a2 = (a2, a1)

instance AddT (a1, a2) where
  type Added (a1, a2) b = (b, a1, a2)
  addTo (a1, a2) b = (b, a1, a2)

instance AddT (a1, a2, a3) where
  type Added (a1, a2, a3) b = (b, a1, a2, a3)
  addTo (a1, a2, a3) b = (b, a1, a2, a3)

instance AddT (a1, a2, a3, a4) where
  type Added (a1, a2, a3, a4) b = (b, a1, a2, a3, a4)
  addTo (a1, a2, a3, a4) b = (b, a1, a2, a3, a4)

instance AddT (a1, a2, a3, a4, a5) where
  type Added (a1, a2, a3, a4, a5) b = (b, a1, a2, a3, a4, a5)
  addTo (a1, a2, a3, a4, a5) b = (b, a1, a2, a3, a4, a5)

instance AddT (a1, a2, a3, a4, a5, a6) where
  type Added (a1, a2, a3, a4, a5, a6) b = (b, a1, a2, a3, a4, a5, a6)
  addTo (a1, a2, a3, a4, a5, a6) b = (b, a1, a2, a3, a4, a5, a6)

instance AddT (a1, a2, a3, a4, a5, a6, a7) where
  type Added (a1, a2, a3, a4, a5, a6, a7) b = (b, a1, a2, a3, a4, a5, a6, a7)
  addTo (a1, a2, a3, a4, a5, a6, a7) b = (b, a1, a2, a3, a4, a5, a6, a7)

instance AddT (a1, a2, a3, a4, a5, a6, a7, a8) where
  type Added (a1, a2, a3, a4, a5, a6, a7, a8) b = (b, a1, a2, a3, a4, a5, a6, a7, a8)
  addTo (a1, a2, a3, a4, a5, a6, a7, a8) b = (b, a1, a2, a3, a4, a5, a6, a7, a8)

instance AddT (a1, a2, a3, a4, a5, a6, a7, a8, a9) where
  type Added (a1, a2, a3, a4, a5, a6, a7, a8, a9) b = (b, a1, a2, a3, a4, a5, a6, a7, a8, a9)
  addTo (a1, a2, a3, a4, a5, a6, a7, a8, a9) b = (b, a1, a2, a3, a4, a5, a6, a7, a8, a9)

instance AddT (a1, a2, a3, a4, a5, a6, a7, a8, a9, a10) where
  type Added (a1, a2, a3, a4, a5, a6, a7, a8, a9, a10) b = (b, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10)
  addTo (a1, a2, a3, a4, a5, a6, a7, a8, a9, a10) b = (b, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10)