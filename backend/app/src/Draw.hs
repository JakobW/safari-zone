{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE DataKinds #-}

module Draw (makeTileMapImage, Img) where

import CustomPrelude
import qualified Graphics.Image as Image
import qualified Data.Map.Strict as Map
import Data.Coord

type Img = Image.Image Image.VU Image.RGBA Double

tileIdToPixel :: Int32 -> Image.Pixel Image.RGBA Double
tileIdToPixel i = Image.PixelRGBA (fromIntegral i/255) 0 0 1

defaultPixel :: Image.Pixel Image.RGBA Double
defaultPixel = Image.PixelRGBA 0 0 0 0

defaultImage :: Img
defaultImage = Image.makeImage (1, 1) (const defaultPixel)

safeMakeImage :: (Int, Int) -> ((Int, Int) -> Image.Pixel Image.RGBA Double) -> Img
safeMakeImage (columns, rows)
    | rows <= 0 = const defaultImage
    | columns <= 0 = const defaultImage
    | otherwise = Image.makeImage (columns, rows)

makeTileMapImage :: Unit 'X -> Unit 'Y -> [(Position, Int32)] -> Img
makeTileMapImage rows columns tiles =
  safeMakeImage (fromIntegral columns, fromIntegral rows) toPixel2d
  where
  tileMap = Map.fromList $ fmap (first toIntTuple) tiles
  toIntTuple a = (x a, y a)
  toPixel2d (y', x') = Map.lookup (fromIntegral x', fromIntegral y') tileMap
    |> maybe defaultPixel tileIdToPixel
    