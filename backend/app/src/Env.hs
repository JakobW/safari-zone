{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE UndecidableInstances #-}

module Env (loadEnv, Environment, resource, appPort, dbSettings, toResource, localAuthBaseUrl, redisSettings) where

import CustomPrelude
import qualified Data.Map.Strict as Map
import Database (DBSettings)
import AppMigration (dbSettingsFromMap)
import System.Environment (getEnvironment)
import Resource (ResourcesDir, getResource, getResourceDir)
import Servant.Client (BaseUrl, parseBaseUrl)
import qualified Database.Redis as R
import qualified Db.Redis.Client as R

data Environment = Environment
  { appPort :: Int,
    resourcesDirectory :: ResourcesDir,
    dbSettings :: DBSettings,
    localAuthBaseUrl :: BaseUrl,
    redisSettings :: R.ConnectInfo
  }
  deriving (Show)

$(makeFieldLabelsNoPrefix ''Environment)

resource :: Environment -> (FilePath -> a) -> FilePath -> a
resource env f =
  getResource (resourcesDirectory env) .> f

toResource :: Environment -> FilePath -> FilePath
toResource =
   resourcesDirectory .> getResource

fromMap :: MonadThrow m => ResourcesDir -> Map String String -> m Environment
fromMap rDir m = do
  localAuthBaseUrl' <- parseBaseUrl $ get id "LOCAL_AUTH_URL" "http://localhost:8081"
  pure Environment
    { appPort = Map.lookup "APP_PORT" m >>= readMay |> fromMaybe 8080,
      resourcesDirectory = rDir,
      dbSettings = dbSettingsFromMap m,
      localAuthBaseUrl = localAuthBaseUrl',
      redisSettings = R.fromEnv m
    }
  where
    get :: (String -> a) -> String -> a -> a
    get transform' key deflt = maybe deflt transform' (Map.lookup key m)

loadEnv :: IO Environment
loadEnv = do
  env <- fmap Map.fromList getEnvironment
  resourceDir <- $(getResourceDir)
  fromMap resourceDir env
