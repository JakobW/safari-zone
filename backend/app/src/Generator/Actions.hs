module Generator.Actions (deleteEncounter, movePokemonRandomly) where

import CustomPrelude
import qualified Data.Direction as Direction
import Data.WildPokemon
import qualified Db.Repo.WildPokemonRepo as Repo
import qualified Generator.GenM as GenM
import qualified Katip as K
import qualified Random
import qualified Websocket.PubSub.Channel.Pokemon as Pokemon
import qualified Websocket.PubSub.Types as PubSub
import qualified Generator.MoveRules

deleteEncounter :: WildPokemonId -> GenM.GenM ()
deleteEncounter wId = do
  K.logLocM K.InfoS ("Deleted wild pokemon with id " <> K.showLS wId)
  Repo.deleteWildPokemon wId
  void $ PubSub.publishM Pokemon.Channel $ Pokemon.mkRemoveEncounterMsg wId

movePokemonRandomly :: Pokemon.NewEncounter -> GenM.GenM Pokemon.NewEncounter
movePokemonRandomly encounter = do
  let wpId = encounter ^. #encounterId
      oldPosition = encounter ^. #position
  validDirs <- Generator.MoveRules.getValidDirections oldPosition
  randomDir <- Random.getRandomItem validDirs
  case randomDir of
    Just dir -> do
      let newPosition = Direction.moveIn dir oldPosition
      Repo.setWildPokemonPosition wpId newPosition
      K.logLocM K.DebugS ("Moved wild pokemon " <> K.showLS wpId <> " to " <> K.showLS newPosition)
      void <| PubSub.publishM Pokemon.Channel (Pokemon.mkMoveEncounterMsg newPosition wpId)
      pure <| set #position newPosition encounter
    Nothing -> do
      K.logLocM K.DebugS ("Pokemon " <> K.showLS wpId <> " cannot move. Current position: " <> K.showLS oldPosition)
      pure encounter