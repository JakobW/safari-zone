module Generator.Config (shinyPercentage, pokemonLifetime, pokemonMoveInterval, pokemonSpawnInterval) where

import CustomPrelude
import qualified Time

shinyPercentage :: Float
shinyPercentage = 0.1

pokemonLifetime :: Time.Seconds
pokemonLifetime = 5 * 60 --10

pokemonMoveInterval :: Time.Seconds
pokemonMoveInterval = 10

pokemonSpawnInterval :: Time.Minutes
pokemonSpawnInterval = 1 --3
