{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

module Generator.Environment (Environment, fromMap, appPort, dbSettings, redisSettings, loadEnv) where

import CustomPrelude
import qualified Database.Redis as R
import qualified Db.Redis.Client as R
import AppMigration (dbSettingsFromMap)
import Database (DBSettings)
import qualified Data.Map.Strict as Map
import System.Environment (getEnvironment)

data Environment = Environment
  { appPort :: Int,
    dbSettings :: DBSettings,
    redisSettings :: R.ConnectInfo
  }
  deriving (Show)

$(makeFieldLabelsNoPrefix ''Environment)

loadEnv :: IO Environment
loadEnv = fromMap . Map.fromList <$> getEnvironment

fromMap :: Map String String -> Environment
fromMap m = do
  Environment
    { appPort = lookup "GENERATOR_PORT" m >>= readMay |> fromMaybe 8082,
      dbSettings = dbSettingsFromMap m,
      redisSettings = R.fromEnv m
    }
