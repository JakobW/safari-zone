{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

module Generator.GenM (Config (..), GenM, runGenOnLogM) where

import CustomPrelude
import qualified Database.PostgreSQL.Typed as PG
import Db.Client
import qualified Katip as K
import Logger
import qualified Random
import qualified Websocket.PubSub.Channel.Cache as Cache
import qualified Websocket.PubSub.Types as PubSub

data Config = Config
  { runDb :: RunDb,
    randomGen :: Random.RandGen,
    pubSub :: PubSub.SomePubSub (LogM IO),
    cache :: Cache.InMemoryCache
  }

$(makeFieldLabelsNoPrefix ''Config)

newtype GenM a = GenM
  { runGenM' :: ReaderT Config (LogM IO) a
  }
  deriving newtype
    ( Functor,
      Applicative,
      Monad,
      MonadIO,
      K.Katip,
      K.KatipContext,
      MonadCatch,
      MonadThrow,
      MonadReader Config
    )

runGenOnLogM :: Config -> GenM a -> LogM IO a
runGenOnLogM config x = runReaderT (runGenM' x) config

runLogInGen :: LogM IO a -> GenM a
runLogInGen = GenM . lift

instance MonadUnliftIO GenM where
  withRunInIO go = GenM (withRunInIO (\k -> go (k . runGenM')))

instance WithDB GenM where
  execQuery q = (`K.logExceptionM` K.ErrorS) $ do
    (RunDb dbAccess) <- asks runDb
    K.logLocM K.DebugS $ "Executing query " <> K.ls (queryToString q)
    liftIO $ dbAccess (`PG.pgQuery` q)

  parseQueryResult parse rows = do
    let (errors, successes) = partitionEithers $ fmap parse rows
    mapM_ (K.logLocM K.ErrorS . ("Failed parsing db row: " <>) . K.ls) errors
    pure successes

instance Random.RandM GenM where
  doRandom f = asks randomGen >>= Random.ioDoRandom f

instance Cache.HasCache GenM where
  getCache = asks cache

instance PubSub.PubSubM GenM where
  publishM channel msg = do
    pubsub <- asks pubSub
    runLogInGen $ PubSub.publish pubsub channel msg

  subscribeM channel subscriber = do
    cfg <- ask
    runLogInGen <$> runLogInGen (PubSub.subscribe (pubSub cfg) channel (runGenOnLogM cfg . subscriber))