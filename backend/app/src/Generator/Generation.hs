module Generator.Generation (startGenerator) where

import qualified Generator.GenM as GenM
import CustomPrelude
import Data.Area
import qualified Data.Encounter as Encounter
import qualified Data.List.NonEmpty as NonEmpty
import Data.Pokemon.PokemonId
import qualified Db.Repo.AreaRepo as Repo
import qualified Db.Repo.EncounterRepo as Repo
import qualified Db.Repo.WildPokemonRepo as Repo
import qualified Db.Row
import qualified Katip as K
import qualified Random
import qualified Time
import qualified Websocket.PubSub.Channel.Pokemon as Pokemon
import qualified Generator.MoveRules
import Data.WildPokemon
import qualified Generator.Config as Config
import qualified Data.Time.Clock as Clock
import qualified Generator.Actions as Actions
import qualified Generator.Metrics
import qualified Generator.Worker as Worker
import Has (getTyped)

startGenerator :: GenM.GenM a
startGenerator = do
  initLeftoverPokemon
  let workerThread = Generator.Metrics.measurePokemonLifetime . Worker.pokemonWorker Config.pokemonLifetime
      mainLoop = withAsync (generate >>= mapConcurrently_ workerThread) $
        \_ -> Time.threadDelay Config.pokemonSpawnInterval >> mainLoop
  mainLoop

initLeftoverPokemon :: GenM.GenM ()
initLeftoverPokemon = do
  wildPokemon <- Repo.getAllWildPokemon
  currentTime <- liftIO getCurrentTime
  let isAlreadyDespawned = (< currentTime) . getTyped
      (despawnedMons, aliveMons) = partition isAlreadyDespawned wildPokemon
      workerWrapper =
        (fmap . fmap) Generator.Metrics.measurePokemonLifetime Worker.pokemonWorker
          <$> (`Clock.diffUTCTime` currentTime) . getTyped @UTCTime
          <*> Db.Row.fromRow
  mapM_ (Actions.deleteEncounter . getTyped) despawnedMons
  mapConcurrently_ workerWrapper aliveMons

generate :: GenM.GenM [Pokemon.NewEncounter]
generate = do
  areas <- fmap Db.Row.fromRow <$> Repo.getAreas
  catMaybes <$> mapM generateForArea areas

generateForArea :: AreaWithId -> GenM.GenM (Maybe Pokemon.NewEncounter)
generateForArea area = do
  encounters <- fmap Db.Row.fromRow <$> Repo.getEncounters (area ^. #theId)
  mapM Encounter.rollEncounter (NonEmpty.nonEmpty encounters)
    >>= ( \case
            Nothing -> pure Nothing
            Just pokeId -> buildRandomizedEncounterForPokemonId (area ^. #theContent) pokeId
        )
      . fmap (view #pokemonId)

buildRandomizedEncounterForPokemonId :: Area -> PokemonId -> GenM.GenM (Maybe Pokemon.NewEncounter)
buildRandomizedEncounterForPokemonId area pokeId = do
  shinyStatus <- Random.withCoinFlipPure Config.shinyPercentage shinyChance
  mayStartTile <- Generator.MoveRules.randomTile area
  case mayStartTile of
    Just startTile ->
      Repo.createWildPokemon pokeId startTile shinyStatus (Time.toDiffTime Config.pokemonLifetime) >>= \case
        Left err ->
          K.logLocM K.ErrorS (K.ls err) $> Nothing
        Right wpId ->
          (pure . Just) $
            Pokemon.NewEncounter startTile pokeId wpId shinyStatus
    Nothing ->
      K.logLocM K.WarningS ("No possible start position found for " <> K.showLS (area ^. #name)) $> Nothing
