module Generator.Logger (
  buildKatipConfig,
  module Logger
) where

import CustomPrelude  
import Logger (KatipConfig(..), runLogM)
import qualified Katip as K
import qualified System.IO as IO

namespace :: K.Namespace
namespace = K.Namespace ["generator"]

environment :: K.Environment
environment = "development"

buildKatipConfig :: IO KatipConfig
buildKatipConfig = do
  logEnv' <- buildLogEnv
  return
    KatipConfig
      { logNamespace = namespace,
        logEnv = logEnv',
        logContext = mempty
      }

buildLogEnv :: IO K.LogEnv
buildLogEnv = do
  logEnv' <- K.initLogEnv namespace environment
  stdoutScribe <- K.mkHandleScribe K.ColorIfTerminal IO.stdout (K.permitItem K.InfoS) K.V1
  K.registerScribe "stdout" stdoutScribe K.defaultScribeSettings logEnv'