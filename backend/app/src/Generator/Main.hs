{-# LANGUAGE OverloadedLabels #-}

module Generator.Main (main) where

import CustomPrelude
import qualified Db.Client
import qualified Db.Redis.Client as Redis
import qualified Generator.Environment as Env
import qualified Generator.GenM as GenM
import qualified Generator.Generation as Gen
import Generator.Logger
import Network.Wai.Handler.Warp
import Network.Wai.Logger
import qualified Network.Wai.Middleware.Prometheus as Prometheus
import qualified Prometheus
import qualified Prometheus.Metric.GHC as Prometheus
import qualified Random
import qualified Websocket.PubSub.Channel.Cache as Cache
import qualified Websocket.PubSub.Redis as PubSub

main :: IO ()
main = do
  _ <- Prometheus.register Prometheus.ghcMetrics
  env <- Env.loadEnv
  katipConfig <- buildKatipConfig
  pool <- liftIO $ Db.Client.connDb (env ^. #dbSettings)
  rng <- Random.init
  runLogM katipConfig $ do
    Redis.withRedis (env ^. #redisSettings) $ \redisConn ->
      PubSub.withRedisPubSub redisConn $ \pubSub ->
        Cache.withCache pubSub $ \cache -> do
          let config =
                GenM.Config
                  (Db.Client.runPool pool)
                  rng
                  pubSub
                  cache

              runMetricsApp = liftIO $
                withStdoutLogger $ \logger ->
                  runSettings (setLogger logger $ setPort (env ^. #appPort) defaultSettings) Prometheus.metricsApp

              runGenerator = GenM.runGenOnLogM config Gen.startGenerator

          concurrently_ runMetricsApp runGenerator
