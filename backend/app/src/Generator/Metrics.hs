module Generator.Metrics (measurePokemonLifetime) where

import CustomPrelude
import qualified Prometheus

activePokemonMetric :: Prometheus.Gauge
{-# NOINLINE activePokemonMetric #-}
activePokemonMetric =
  Prometheus.unsafeRegister $
    Prometheus.gauge $ Prometheus.Info "pokemon_spawned_current" "Number of currently spawned pokemon"

measurePokemonLifetime :: (MonadUnliftIO m) => m () -> m ()
measurePokemonLifetime = bracket_
  (liftIO $ Prometheus.incGauge activePokemonMetric)
  (liftIO $ Prometheus.decGauge activePokemonMetric)