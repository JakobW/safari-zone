{-# LANGUAGE TypeFamilies #-}

module Generator.MoveRules (randomTile, getValidDirections) where

import CustomPrelude
import Data.Coord
import qualified Data.Direction as Direction
import qualified Data.Tile.GameTile as GameTile
import qualified Db.Repo.TileRepo as Repo
import qualified Generator.GenM as GenM
import qualified Tile.MoveRule as MoveRule
import qualified Util
import qualified Random
import Data.Area
import qualified Data.Map.Strict as Map

getMoveRules :: GenM.GenM (Map Position MoveRule.MoveRule)
getMoveRules = do
  fmap snd . GameTile.gameTilesAsMap . GameTile.convertToGameTiles <$> Repo.getAllTilesOrdered

getValidDirections :: Position -> GenM.GenM [Direction.Direction]
getValidDirections pos = do
  rules <- getMoveRules
  pure $ filter (checkDir rules) (toList Util.enumerate)
  where
    checkDir rules dir =
      MoveRule.Blocked /= fromMaybe MoveRule.Blocked (lookup (Direction.moveIn dir pos) rules)

randomTile :: Area -> GenM.GenM (Maybe Position)
randomTile area =
  Random.getRandomItem
    . Map.keys
    . Map.filterWithKey (\pos mr -> posInArea pos && validMoveRule mr)
    =<< getMoveRules
  where
    posInArea :: Position -> Bool
    posInArea pos = elemRange (pos ^. #x) (area ^. #ranges % #x) && elemRange (pos ^. #y) (area ^. #ranges % #y)
    validMoveRule :: MoveRule.MoveRule -> Bool
    validMoveRule = (/= MoveRule.Blocked)