module Generator.Worker (pokemonWorker) where

import CustomPrelude
import qualified Generator.Actions as Actions
import qualified Generator.Config as Config
import qualified Generator.GenM as GenM
import qualified Katip as K
import qualified Time
import qualified Websocket.PubSub.Channel.Pokemon as Pokemon
import qualified Websocket.PubSub.Types as PubSub

pokemonWorker :: Time.Timeable t => t -> Pokemon.NewEncounter -> GenM.GenM ()
pokemonWorker leftoverTime encounter = do
  let wId = encounter ^. #encounterId
  K.logLocM K.InfoS ("Generated wild pokemon with id " <> K.showLS wId)
  void <| PubSub.publishM Pokemon.Channel (Pokemon.NewEncounterMsg encounter)
  void <| repeatN
    (leftoverTime `Time.divide` Config.pokemonMoveInterval)
    ( \currentEncounter -> do
        newEncounter <- Actions.movePokemonRandomly currentEncounter
        Time.threadDelay Config.pokemonMoveInterval
        pure newEncounter
    )
    encounter
  Actions.deleteEncounter wId

-- Note: negative ints would loop forever
repeatN :: (Monad m, Num n, Eq n) => n -> (a -> m a) -> a -> m a
repeatN 0 _ a = pure a
repeatN n action a = action a >>= repeatN (n - 1) action
