{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Has (HasType (..), HasAll) where

import CustomPrelude
import Data.Generics.Product.Typed (HasType (..))
import Data.Kind (Constraint, Type)

type family HasAll (as :: [Type]) c :: Constraint where
  HasAll '[] _ = ()
  HasAll (a ': as) c = (HasType a c, HasAll as c)

deriving instance Generic ((,,,,,,,) a b c d e f g h)

deriving instance Generic ((,,,,,,,,) a b c d e f g h i)

deriving instance Generic ((,,,,,,,,,) a b c d e f g h i j)

deriving instance Generic ((,,,,,,,,,,) a b c d e f g h i j k)
