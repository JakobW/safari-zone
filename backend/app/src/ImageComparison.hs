{-# LANGUAGE TemplateHaskell #-}

module ImageComparison (runCompare, verifySameImagesHaveBeenWritten, deleteActualAndDiff) where

import CustomPrelude
import qualified Data.Map.Strict as Map
import Data.String.Interpolate (i)
import qualified Draw
import qualified Graphics.Image as I
import qualified System.Directory as Dir
import TH.StackRoot (embedStackRoot)
import Util.Filesystem (safeWrite)

rgbaEquals :: (Ord e, Num e) => I.Pixel I.RGBA e -> I.Pixel I.RGBA e -> Bool
rgbaEquals (I.PixelRGBA r1 b1 g1 a1) (I.PixelRGBA r2 b2 g2 a2) =
  eqScaled r1 r2 && eqScaled b1 b2 && eqScaled g1 g2 && eqScaled a1 a2
  where
    eqScaled e1 e2 = abs (e1 * 255 - e2 * 255) < 1

compareImages :: Draw.Img -> Draw.Img -> Maybe Draw.Img
compareImages img1 =
  (\diff -> if diff == img1 then Nothing else Just diff) . I.zipWith colorDifferencesRed img1
  where
    colorDifferencesRed px1 px2 =
      if rgbaEquals px1 px2
        then px1
        else I.PixelRGBA 1 0 0 1

safeWriteImage :: FilePath -> Draw.Img -> IO ()
safeWriteImage fp img = safeWrite (`I.writeImage` img) fp

takeFromResources :: (IsString s, Eq s) => [s] -> [s]
takeFromResources = reverse . takeWhile (/= "resources") . reverse

encodeFilePath :: String -> String
encodeFilePath = intercalate "_" . takeFromResources . splitSeq "/"

testImagesFolder :: FilePath
testImagesFolder = $(embedStackRoot) ++ "/app/test/images/"

actualFolderPath :: FilePath
actualFolderPath =
  testImagesFolder <> "actual"

baselineFolderPath :: FilePath
baselineFolderPath =
  testImagesFolder <> "baseline"

diffFolderPath :: FilePath
diffFolderPath =
  testImagesFolder <> "diff"

encodeTestName :: String -> String
encodeTestName = replaceSeq " " "_"

cleanDir :: FilePath -> IO ()
cleanDir fp =
  whenM (Dir.doesDirectoryExist fp) $ do
    Dir.removeDirectoryRecursive fp
    Dir.createDirectory fp

deleteActualAndDiff :: IO ()
deleteActualAndDiff = do
  cleanDir actualFolderPath
  cleanDir diffFolderPath

runCompare :: String -> TVar (Bool, Map String Int) -> FilePath -> Draw.Img -> IO ()
runCompare testName capturedImageMap' fp img = do
  (shouldCaptureImages, capturedImageMap) <- readTVarIO capturedImageMap'

  -- Result is something like /app/test/images/baseline/0-test_something-ground.png
  let encfp = encodeFilePath fp
      imageCount = fromMaybe 0 $ lookup encfp capturedImageMap
      locationTemplate basePath = [i|#{basePath}/#{imageCount}-#{encodeTestName testName}-#{encfp}|]
      baselineLocation = locationTemplate baselineFolderPath
      actualLocation = locationTemplate actualFolderPath
      diffLocation = locationTemplate diffFolderPath
      onDiff img' = safeWriteImage diffLocation img' >>= error ("Images did not match. Difference saved to " ++ diffLocation)

  atomically $ modifyTVar capturedImageMap' (second $ Map.insert encfp (imageCount + 1))

  when shouldCaptureImages $ do
    safeWriteImage actualLocation img
    imgBaseline <- fmap Just (I.readImage' baselineLocation) `catch` (\(_ :: IOException) -> return Nothing)
    case imgBaseline of
      Just baseline -> do
        let diff = compareImages baseline img
        mapM_ onDiff diff
      Nothing -> do
        putStrLn $ "Creating baseline image " ++ pack baselineLocation
        safeWriteImage baselineLocation img

verifySameImagesHaveBeenWritten :: String -> TVar (Bool, Map String Int) -> IO ()
verifySameImagesHaveBeenWritten testName capturedImageMap' = do
  (shouldCaptureImages, _) <- readTVarIO capturedImageMap'
  when shouldCaptureImages $ do
    baselineFilePaths <- Dir.listDirectory baselineFolderPath
    actualFilePaths <- Dir.listDirectory actualFolderPath
    let testInfix = [i|-#{encodeTestName testName}-|]
        testRelevantPaths = sort . filter (testInfix `isInfixOf`)
        baselineRelevant = testRelevantPaths baselineFilePaths
        actualRelevant = testRelevantPaths actualFilePaths
    if baselineRelevant == actualRelevant
      then pure ()
      else
        error
          [i|"Filenames of actual and baseline images did not match:
        Actual: #{actualRelevant},
        Baseline: #{baselineRelevant}
        |]
