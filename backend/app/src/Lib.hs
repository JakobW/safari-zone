{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeOperators #-}

module Lib
  ( startApp,
    API,
    readerServer,
    api,
    configuredServer,
    JsonApi,
  )
where

import Api.AreaApi
import Api.EncounterApi
import Api.ImagesApi
import Api.PokemonApi
import Api.TileApi
import Api.Websocket
import AppMigration (runAllMigrations)
import qualified Auth
import qualified Config
import CustomPrelude
import Data.Pool (withResource)
import Db.Client (connDb, runPool)
import qualified Db.Redis.Client as Redis
import qualified Env
import qualified Katip as K
import Logger
import Network.Wai
import Network.Wai.Handler.Warp
import Network.Wai.Logger
import qualified Network.Wai.Middleware.Cors as Cors
import Network.Wai.Middleware.Prometheus as Prometheus
import qualified Prometheus
import Prometheus.Metric.GHC as Prometheus
import qualified Random (init)
import Servant
import qualified Tile.Startup
import qualified Websocket.PubSub.Channel.Cache as Cache
import qualified Websocket.PubSub.Redis as PubSub
import qualified Tile.TileWriter as TileWriter

type JsonApi =
  "api"
    :> ( "tile" :> TileApi
           :<|> "pokemon" :> PokemonApi
           :<|> "area" :> AreaApi
           :<|> "encounter" :> EncounterApi
           :<|> "images" :> ImagesApi
       )

type API =
  "healthcheck" :> Get '[JSON] String
    :<|> JsonApi
    :<|> "api" :> WebsocketApi

corsPolicy :: Middleware
corsPolicy = Cors.simpleCors

startApp :: IO ()
startApp = do
  _ <- Prometheus.register Prometheus.ghcMetrics

  withAsync TileWriter.writeTileset $ \_ -> do
    env <- Env.loadEnv
    randGen <- Random.init
    katipConfig <- buildKatipConfig
    runLogM katipConfig $ do
      K.logLocM K.InfoS $ "Running with settings: " <> K.showLS env

      pool <- liftIO $ connDb (Env.dbSettings env)
      _ <- liftIO $ withResource pool runAllMigrations

      Auth.withAuthContext env $ \authCtx -> do
        Redis.withRedis (Env.redisSettings env) $ \redisConn -> do
          PubSub.withRedisPubSub redisConn $ \pubSub -> do
            Cache.withCache pubSub $ \cache -> do
              let config =
                    Config.Config
                      env
                      (runPool pool)
                      randGen
                      (Auth.jwtMapFromContext authCtx)
                      Config.defaultWriteImage
                      pubSub
                      cache

              Config.runAppOnLogM config Tile.Startup.generateImages
              liftIO $
                withStdoutLogger $ \logger -> do
                  let settings = setPort (config ^. #env % #appPort) $ setLogger logger defaultSettings
                      application = readerServer authCtx katipConfig config
                  (runSettings settings . corsPolicy . Prometheus.prometheus Prometheus.def) application

readerServer :: Auth.AuthContext -> KatipConfig -> Config.Config -> Application
readerServer authCtx katipConfig =
  serveWithContext api authCtx . configuredServer katipConfig

configuredServer :: KatipConfig -> Config.Config -> Server API
configuredServer katipConfig config =
  hoistServerWithContext api (Proxy :: Proxy Auth.AuthSettings) (Config.runHandlerInApp katipConfig config) server

api :: Proxy API
api = Proxy

server :: ServerT API Config.AppM
server =
  healthcheck :<|> (tileApi :<|> pokemonApi :<|> areaApi :<|> encounterApi :<|> imagesApi) :<|> websocketApi
  where
    healthcheck :: Applicative f => f String
    healthcheck = pure "ok"
