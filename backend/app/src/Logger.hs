{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE OverloadedLabels #-}

module Logger
  ( KatipConfig(..),
    buildKatipConfig,
    runLogM,
    LogM,
    replicateLogM,
    withRunInLogM
  )
where

import Control.Monad.Reader (local)
import CustomPrelude
import qualified Katip as K
import qualified System.IO as IO

data KatipConfig = KatipConfig
  { logNamespace :: K.Namespace,
    logContext :: K.LogContexts,
    logEnv :: K.LogEnv
  }

$(makeFieldLabelsNoPrefix ''KatipConfig)

namespace :: K.Namespace
namespace = K.Namespace []

environment :: K.Environment
environment = "development"

buildKatipConfig :: IO KatipConfig
buildKatipConfig = do
  logEnv' <- buildLogEnv
  return
    KatipConfig
      { logNamespace = namespace,
        logEnv = logEnv',
        logContext = mempty
      }

buildLogEnv :: IO K.LogEnv
buildLogEnv = do
  logEnv' <- K.initLogEnv namespace environment
  stdoutScribe <- K.mkHandleScribe K.ColorIfTerminal IO.stdout (K.permitItem K.InfoS) K.V1
  K.registerScribe "stdout" stdoutScribe K.defaultScribeSettings logEnv'

instance MonadIO m => K.Katip (LogM m) where
  getLogEnv = LogM (asks logEnv)
  localLogEnv f = LogM . local (over #logEnv f) . runLogM'

instance MonadIO m => K.KatipContext (LogM m) where
  getKatipContext = LogM (asks logContext)
  getKatipNamespace = LogM (asks logNamespace)
  localKatipContext f = LogM . local (over #logContext f) . runLogM'
  localKatipNamespace f = LogM . local (over #logNamespace f) . runLogM'

newtype LogM m a = LogM {
  runLogM' :: ReaderT KatipConfig m a
} deriving newtype (Functor, Applicative, Monad, MonadIO, MonadThrow, MonadCatch)

readKatipConfig :: (K.Katip m, K.KatipContext m) => m KatipConfig
readKatipConfig = do
  e <- K.getLogEnv
  c <- K.getKatipContext
  n <- K.getKatipNamespace
  pure $ KatipConfig n c e

replicateLogM :: (K.Katip m, K.KatipContext m) => m (LogM n a -> n a)
replicateLogM =
  fmap runLogM readKatipConfig

instance MonadUnliftIO m => MonadUnliftIO (LogM m) where
  withRunInIO go = LogM (withRunInIO (\k -> go (k . runLogM')))

withRunInLogM :: forall m b. (K.Katip m, K.KatipContext m, MonadUnliftIO m) => ((forall a. m a -> LogM IO a) -> LogM IO b) -> m b
withRunInLogM f = do
  conf <- readKatipConfig
  withRunInIO $ \runInIO -> do
    let
      trans :: forall a. m a -> LogM IO a
      trans = LogM . ReaderT . const . runInIO
    runLogM conf (f trans)

runLogM :: KatipConfig -> LogM m a -> m a
runLogM kc (LogM lm) = runReaderT lm kc
