module Random
  ( RandGen,
    init,
    getRandomR,
    getRandom01,
    whenHeads,
    withCoinFlipPure,
    withCoinFlip,
    CoinFlip (..),
    RandM (..),
    ioDoRandom,
    getRandomEnum,
    getRandomItem
  )
where

import Control.Concurrent.STM (stateTVar)
import CustomPrelude hiding (init)
import System.Random (Random, StdGen, getStdGen, random, randomR)
import Data.List ((!!))

type RandGen = TVar StdGen

init :: MonadIO m => m RandGen
init = do
  randomGen <- liftIO getStdGen
  newTVarIO randomGen

class Monad m => RandM m where
  doRandom :: (StdGen -> (a, StdGen)) -> m a

ioDoRandom :: (MonadIO m) => (StdGen -> (a, StdGen)) -> RandGen -> m a
ioDoRandom f gen =
  atomically $ stateTVar gen f

getRandomR :: (RandM m, Random a) => (a, a) -> m a
getRandomR = doRandom . randomR

getRandomItem :: (RandM m) => [a] -> m (Maybe a)
getRandomItem items 
  | null items = pure Nothing
  | otherwise = Just . (items !!) <$> getRandomR (0, length items - 1)

getRandom01 :: (RandM m, Random a) => m a
getRandom01 = doRandom random

getRandomEnum :: forall a m. (RandM m, Enum a, Bounded a) => m a
getRandomEnum = toEnum <$> getRandomR (fromEnum @a minBound, fromEnum @a maxBound)

data CoinFlip
  = Heads
  | Tails
  deriving (Show, Eq)

withCoinFlipPure :: RandM m => Float -> (CoinFlip -> a) -> m a
withCoinFlipPure prob onFlipResult =
  withCoinFlip prob $ pure . onFlipResult

withCoinFlip :: RandM m => Float -> (CoinFlip -> m a) -> m a
withCoinFlip prob onFlipResult = do
  rand <- getRandom01
  onFlipResult $
    if rand < prob
      then Heads
      else Tails

whenHeads :: RandM m => Float -> m a -> m (Maybe a)
whenHeads prob exec =
  withCoinFlip prob $ \case
    Heads -> fmap Just exec
    Tails -> pure Nothing
