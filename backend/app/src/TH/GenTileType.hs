{-# LANGUAGE TemplateHaskellQuotes #-}

module TH.GenTileType (genSimplifiedDatatype) where

import Control.Monad.Fail (fail)
import CustomPrelude
import Language.Haskell.TH

genSimplifiedDatatype :: Name -> String -> DecsQ
genSimplifiedDatatype name simplifiedName = do
  (TyConI (DataD _ _ _ _ constructors _)) <- reify name
  dataName <- newName simplifiedName
  funName <- newName ("to" <> simplifiedName)
  let newConstructorsAndClauses = fmap genConstructorAndClause constructors
      newConstructors = fmap (fmap fst) newConstructorsAndClauses
      newClauses = fmap (fmap snd) newConstructorsAndClauses
      derivingClause = derivClause Nothing $ fmap conT [''Eq, ''Show, ''Ord, ''Enum, ''Bounded]
  dataTH <- dataD (pure mempty) dataName [] Nothing newConstructors [derivingClause]
  funTH <- funD funName newClauses
  sigTH <- sigD funName (appT (appT arrowT $ conT name) (conT dataName))
  pure [dataTH, sigTH, funTH]

genConstructorAndClause :: Con -> Q (Con, Clause)
genConstructorAndClause (NormalC oldConName parameters) = do
  let newConName = mkName $ nameBase oldConName
  cl <- clause [conP oldConName $ fmap (const wildP) parameters] (normalB $ conE newConName) []
  con <- normalC newConName []
  pure (con, cl)
genConstructorAndClause _ = fail "This TH only works for normal constructors."
