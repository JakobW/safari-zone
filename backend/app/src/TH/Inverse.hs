{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module TH.Inverse (invert, fromInt32, invertToNonPartial) where

import CustomPrelude
import Language.Haskell.TH
import Language.Haskell.TH.Syntax (Lift)
import Generics.Deriving (GEnum, genum)
import Control.Monad.Fail (fail)

invert :: forall a b . (GEnum a, Lift a) => (a -> b) -> (b -> Lit) -> ExpQ
invert f toLiteral = do
  let genClause :: a -> MatchQ
      genClause a = match ((litP . toLiteral . f) a) (normalB [| Just a |])[]
      catchAll = match wildP (normalB [| Nothing |]) []
  varName <- newName "n"
  lamE [varP varName] $ caseE (varE varName) (fmap genClause genum ++ [catchAll])

fromInt32 :: (Integral a) => a -> Lit
fromInt32 = integerL . fromIntegral

invertToNonPartial :: forall a b. (GEnum b, Lift a, Lift b) => (b -> Maybe a) -> ExpQ
invertToNonPartial f = do
  let genClause n = case f n of
        Just b -> match (valueToPattern [|n|]) (normalB [|b|]) []
        Nothing -> fail "Function is not total"
  varName <- newName "n"
  lamE [varP varName] $ caseE (varE varName) (fmap genClause genum)

valueToPattern :: ExpQ -> PatQ
valueToPattern = (=<<) $ \case
  ConE x -> conP x []
  AppE (ConE x) y -> conP x [valueToPattern (pure y)]
  x -> fail $ "Non-Supported Pattern/Value" <> show x