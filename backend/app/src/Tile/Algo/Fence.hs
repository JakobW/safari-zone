module Tile.Algo.Fence (chooseTiles) where

import CustomPrelude
import qualified Data.Set as Set
import qualified Data.Foldable
import qualified Tile.TileTypes as TileTypes
import Data.Tile.Types.Fence
import Data.Coord

includeInput :: (a -> b) -> a -> (a, b)
includeInput f a = (a, f a)

relativeNeighbours :: [Position -> Position]
relativeNeighbours =
  [over #x (+ 1), over #x (+ (- 1)), over #y (+ 1), over #y (+ (- 1))]

chooseTiles :: (Foldable f, Functor f) => f Position -> f (TileTypes.TileData FenceBased)
chooseTiles positions =
  fmap (uncurry TileTypes.groundTileData . includeInput choose) positions
  where
  posSet = Set.fromList $ Data.Foldable.toList positions
  choose pos = case fmap ((`elem` posSet) . ($ pos)) relativeNeighbours of
    [True, False, False, False] -> LeftEnd
    [False, True, False, False] -> RightEnd
    [False, False, True, False] -> TopEnd
    [False, False, False, True] -> BottomEnd
    [True, True, False, False] -> LeftToRight
    [True, False, True, False] -> RightToBottom
    [True, False, False, True] -> RightToTop
    [False, True, True, False] -> LeftToBottom
    [False, True, False, True] -> LeftToTop
    [False, False, True, True] -> TopToBottom
    _ -> TopToBottom