module Tile.Algo.ModuloTileChooser (chooseModulo) where

import CustomPrelude
import Data.Coord
import qualified Tile.TileTypes as TileTypes

chooseModulo :: (Enum a, Bounded a, Functor f) => f Position -> f (TileTypes.TileData a)
chooseModulo =
  fmap selectWithModulo
  where
    selectWithModulo pos =
      TileTypes.groundTileData pos $
        enumToMod (pos ^. #x % to untag + pos ^. #y % to untag)

enumToMod :: forall a b. (Enum a, Bounded a, Integral b) => b -> a
enumToMod n =
  toEnum $ fromIntegral n `mod` (fromEnum (maxBound :: a) + 1)
