module Tile.Algo.Neighbour (chooseBasedOnNeighbour) where

import CustomPrelude hiding (Left, Right)
import qualified Data.Foldable
import Data.Tile.Types.Neighbour
import qualified Tile.TileTypes as TileTypes
import Data.Coord

relativeNeighbours :: [Position]
relativeNeighbours =
  uncurry mkCoordLike <$> [(-1, -1), (0, -1), (1, -1), (-1, 0), (1, 0), (-1, 1), (0, 1), (1, 1)]

includeInput :: (a -> b) -> a -> (a, b)
includeInput f a = (a, f a)

chooseBasedOnNeighbour ::
  (Foldable f, Functor f) =>
  f Position ->
  f (TileTypes.TileData NeighbourBased)
chooseBasedOnNeighbour positions =
  uncurry TileTypes.groundTileData . includeInput (chooseBasedOnNeighbourInternal (Data.Foldable.toList positions)) <$> positions

chooseBasedOnNeighbourInternal :: [Position] -> Position -> NeighbourBased
chooseBasedOnNeighbourInternal otherPositions position =
  let absoluteNeighbours = (position <>) <$> relativeNeighbours
      containedNeighbours = fmap (`elem` otherPositions) absoluteNeighbours
   in case containedNeighbours of
        [True, True, True, True, True, True, True, True] ->
          Middle
        [True, True, True, True, True, True, True, False] ->
          CurveBottomRight
        [True, True, True, True, True, False, True, True] ->
          CurveBottomLeft
        [True, True, False, True, True, True, True, True] ->
          CurveTopRight
        [False, True, True, True, True, True, True, True] ->
          CurveTopLeft
        [_, _, _, True, True, True, True, True] ->
          Top
        [True, True, True, True, True, _, _, _] ->
          Bottom
        [_, True, True, _, True, _, True, True] ->
          Left
        [True, True, _, True, _, True, True, _] ->
          Right
        [_, False, _, False, True, _, True, True] ->
          TopLeft
        [_, False, _, True, False, True, True, _] ->
          TopRight
        [_, True, True, False, True, _, False, _] ->
          BottomLeft
        [True, True, _, True, False, _, False, _] ->
          BottomRight
        _ -> Middle
