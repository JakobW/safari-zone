{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies #-}

module Tile.Algo.TreeTileChooser (chooseClassic, chooseLarge) where

import CustomPrelude hiding (below)
import Data.Coord
import qualified Data.Foldable
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import qualified Data.Tile.Types.ClassicTree as CT
import qualified Data.Tile.Types.LargeTree as LT
import qualified Tile.TileTypes as TileTypes

sortAndChooseWithFold ::
  (Foldable f, SemiSequence (f Position), Element (f Position) ~ Position) =>
  (Position -> Position -> Ordering) ->
  (Set Position -> ComputationState tiles -> Position -> ComputationState tiles) ->
  f Position ->
  [TileTypes.TileData tiles]
sortAndChooseWithFold sortFunc foldFunc positions =
  go positions
  where
    positionSet = Set.fromList $ Data.Foldable.toList positions
    go =
      (=<<) toTileData
        . Map.toList
        . Data.Foldable.foldl' (foldFunc positionSet) Map.empty
        . sortBy sortFunc

type ComputationState a = Map Position (Maybe a, Maybe a)

chooseClassic :: (Foldable f, SemiSequence (f Position), Element (f Position) ~ Position) => f Position -> [TileTypes.TileData CT.ClassicTree]
chooseClassic =
  {-- Default ordering
  0 1 2 3
  4 5 6 7
  ... --}
  sortAndChooseWithFold compare (const chooseBasedOnPreviousNeighbours)

chooseLarge :: (Foldable f, SemiSequence (f Position), Element (f Position) ~ Position) => f Position -> [TileTypes.TileData LT.LargeTree]
chooseLarge =
  {--
    90 degree flipped ordering
    0 2 4 6
    1 3 5 7
   -}
  sortAndChooseWithFold (comparing (view #x) <> comparing (view #y)) chooseFold
  where
    chooseFold allTreePositions previous current =
      let getRelatedGround :: (Position -> Position) -> Maybe LT.LargeTree
          getRelatedGround relate = Map.lookup (relate current) previous >>= fst
          addGroundAtCurrentPos ground = Map.insert current (Just ground, Nothing)
          addOverlay pos overlay =
            Map.alter
              ( \case
                  Nothing -> Just (Nothing, Just overlay)
                  Just (g, _) -> Just (g, Just overlay)
              )
              pos
       in ($ previous) $
            case (getRelatedGround above, getRelatedGround leftOf) of
              (_, Just LT.TopLeft) ->
                addOverlay (above current) LT.TopOverlay
                  . addGroundAtCurrentPos LT.TopMiddle
              (_, Just LT.MidLeft) -> addGroundAtCurrentPos LT.Middle
              (_, Just LT.Middle) -> addGroundAtCurrentPos LT.MidRight
              (_, Just LT.BottomLeft) -> addGroundAtCurrentPos LT.BottomMiddle
              (_, Just LT.BottomMiddle) -> addGroundAtCurrentPos LT.BottomRight
              (_, Just LT.BottomLeftWithOverlay) ->
                addOverlay (above current) LT.TopOverlay
                  . addGroundAtCurrentPos LT.TopMiddle
              (Nothing, Just LT.TopMiddle) -> addGroundAtCurrentPos LT.TopRight
              (Just LT.MidRight, Just LT.TopMiddle) -> addGroundAtCurrentPos LT.BottomRightWithOverlay
              (Just _, Just LT.TopMiddle) -> addGroundAtCurrentPos LT.TopRight
              (Just LT.TopLeft, _) -> addGroundAtCurrentPos LT.MidLeft
              (Just LT.TopMiddle, _) -> addGroundAtCurrentPos LT.Middle
              (Just LT.TopRight, _) -> addGroundAtCurrentPos LT.MidRight
              (Just LT.BottomLeftWithOverlay, _) -> addGroundAtCurrentPos LT.MidLeft
              (Just LT.BottomRightWithOverlay, _) -> addGroundAtCurrentPos LT.MidRight
              (Just LT.MidLeft, _) ->
                if below current `elem` allTreePositions && getRelatedGround (below .> leftOf) /= Just LT.TopLeft
                  then addGroundAtCurrentPos LT.BottomLeftWithOverlay
                  else addGroundAtCurrentPos LT.BottomLeft
              (Just LT.MidRight, _) ->
                if below current `elem` allTreePositions
                  then addGroundAtCurrentPos LT.BottomRightWithOverlay
                  else addGroundAtCurrentPos LT.BottomRight
              (_, _) ->
                addGroundAtCurrentPos LT.TopLeft

toTileData :: (Position, (Maybe a, Maybe a)) -> [TileTypes.TileData a]
toTileData (g, (tileG, tileO)) =
  catMaybes [TileTypes.groundTileData g <$> tileG, TileTypes.overlayTileData g <$> tileO]

above :: Position -> Position
above = over #y pred

below :: Position -> Position
below = over #y succ

leftOf :: Position -> Position
leftOf = over #x pred

chooseBasedOnPreviousNeighbours :: ComputationState CT.ClassicTree -> Position -> ComputationState CT.ClassicTree
chooseBasedOnPreviousNeighbours previous current =
  let getRelatedGround :: (Position -> Position) -> Maybe CT.ClassicTree
      getRelatedGround relate = Map.lookup (relate current) previous >>= fst
      addGroundAtCurrentPos ground = Map.insert current (Just ground, Nothing)
      addOverlay pos overlay =
        Map.alter
          ( \case
              Nothing -> Just (Nothing, Just overlay)
              Just (g, _) -> Just (g, Just overlay)
          )
          pos
   in ($ previous) $
        case (getRelatedGround above, getRelatedGround leftOf) of
          {--
            middleLeft | ...
            #bottomLeft# | ...
          --}
          (Just CT.MiddleLeft, _) -> addGroundAtCurrentPos CT.BottomLeft
          {--
            middleRight | ...
            #bottomRight# | ...
          --}
          (Just CT.MiddleRight, _) -> addGroundAtCurrentPos CT.BottomRight
          {--
                   ...
            bottomLeft | #bottomRight# | ...
          --}
          (_, Just CT.BottomLeft) -> addGroundAtCurrentPos CT.BottomRight
          {--
            ... | #overlayTopLeft# | ...
            ... | #middleLeft# | ...
          --}
          (Nothing, Nothing) ->
            addOverlay (above current) CT.TopOverlayLeft
              . addGroundAtCurrentPos CT.MiddleLeft
          {--
                   ... | bottomLeft + #overlayTopLeft# | ...
            ...| empty | #middleLeft# | ...
          --}
          (Just CT.BottomLeft, _) ->
            addOverlay (above current) CT.TopOverlayLeft
              . addGroundAtCurrentPos CT.MiddleLeft
          {--
                        ... | #overlayTopRight# | ...
            ...| middleLeft | #middleRight# | ...
          --}
          (_, Just CT.MiddleLeft) ->
            addOverlay (above current) CT.TopOverlayRight
              . addGroundAtCurrentPos CT.MiddleRight
          {--
                        ... | empty + #overlayTopLeft# | ...
            ...| middleRight | #middleLeft# | ...
          --}
          (Nothing, Just CT.MiddleRight) ->
            addOverlay (above current) CT.TopOverlayLeft
              . addGroundAtCurrentPos CT.MiddleLeft
          {--
                        ... | empty + #overlayTopLeft# | ...
            ...| bottomRight | #middleLeft# | ...
          --}
          (Nothing, Just CT.BottomRight) ->
            addOverlay (above current) CT.TopOverlayLeft
              . addGroundAtCurrentPos CT.MiddleLeft
          {--
                        ...       | bottomRight + #overlayTopLeft# | ...
            ...| right or nothing | #middleLeft# | ...
          --}
          (Just CT.BottomRight, _) ->
            addOverlay (above current) CT.TopOverlayLeft
              . addGroundAtCurrentPos CT.MiddleLeft
          _ ->
            addOverlay (above current) CT.TopOverlayLeft
              . addGroundAtCurrentPos CT.MiddleLeft
