{-# LANGUAGE QuasiQuotes #-}

module Tile.ImageGenerator (generateAndWriteTileMapImage, generateAndWriteImagesOfAllTypesOfTileMap, toImagePath) where

import Config (AppM, env, writeImg)
import CustomPrelude
import Data.Coord (Position)
import Data.String.Interpolate (i)
import qualified Data.Tile as Tile
import qualified Draw
import qualified Env (toResource)
import qualified Tile.TileTypes as TileTypes

generateAndWriteImagesOfAllTypesOfTileMap :: Tile.LayerNumber -> [TileTypes.TileData'] -> AppM ()
generateAndWriteImagesOfAllTypesOfTileMap layerNo tiles =
  let (overlayTiles, groundTiles) = partition TileTypes.isOverlay tiles
   in mapM_
        (uncurry generateAndWriteTileMapImage)
        [(Tile.mkLayer layerNo Tile.Overlay, overlayTiles), (Tile.mkLayer layerNo Tile.Ground, groundTiles)]

generateAndWriteTileMapImage :: Tile.Layer -> [TileTypes.TileData'] -> AppM ()
generateAndWriteTileMapImage layer = writeImage . first unpack . imageStuff layer

writeImage :: (String, Draw.Img) -> AppM ()
writeImage (name, img) = do
  environment <- asks env
  writeImg img (Env.toResource environment name)

-- determine images and names of images to be written
imageStuff :: Tile.Layer -> [TileTypes.TileData'] -> (Text, Draw.Img)
imageStuff layer tiles =
  createImage tiles
  where
    columns = maybe 0 (+ 1) $ maximumMay $ fmap (^. #position % #y) tiles
    rows = maybe 0 (+ 1) $ maximumMay $ fmap (^. #position % #x) tiles
    convert :: TileTypes.TileData' -> (Position, Int32)
    convert tile = (tile ^. #position, tile ^. #tile % to Tile.tileToId)
    createImage :: [TileTypes.TileData'] -> (Text, Draw.Img)
    createImage =
      fmap convert
        .> Draw.makeTileMapImage rows columns
        .> (toImagePath layer,)

toImagePath :: Tile.Layer -> Text
toImagePath layer =
  [i|images/generated/layer#{Tile.stringifyLayer layer}.png|]
