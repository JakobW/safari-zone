{-# LANGUAGE DataKinds, TypeFamilies, UndecidableInstances #-}

module Tile.MoveRule (MoveRule (..), HasMoveRule(..)) where

import CustomPrelude
import Data.Aeson
import Control.Monad.Fail (fail)
import Data.Aeson.Types (Parser)
import Util
import Data.List.NonEmpty (nonEmpty)

data MoveRule = Ground | Blocked | Water deriving (Show, Eq, Enum, Bounded)

instance ToJSON MoveRule where
  toJSON = Number . fromIntegral . fromEnum

instance FromJSON MoveRule where
  parseJSON = withScientific "MoveRule" (parseMaybe . (toBoundedInteger >=> parseInt))
    where
    parseMaybe :: Maybe a -> Parser a
    parseMaybe = \case
      Just a -> pure a
      Nothing ->  fail $ "Expected number between 0 and " ++ show (fromEnum $ maxBound @MoveRule)
    parseInt :: Int -> Maybe MoveRule
    parseInt int =
      if int > toEnum maxBound
      then Nothing
      else Just (toEnum int)


instance Semigroup MoveRule where
  (<>) :: MoveRule -> MoveRule -> MoveRule
  Blocked <> _ = Blocked
  _ <> Blocked = Blocked
  Ground <> Ground = Ground
  Water <> Water = Water
  Water <> Ground = Water
  Ground <> Water = Water

instance Monoid MoveRule where
  mempty :: MoveRule
  mempty = Ground

class HasMoveRule a where
  getMoveRule :: a -> MoveRule

instance HasMoveRule a => HasMoveRule [a] where
  getMoveRule = nonEmpty .> maybe Blocked (fmap getMoveRule .> Util.foldl1' (<>))