{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeApplications #-}

module Tile.Startup (generateImages) where

import Config (AppM)
import CustomPrelude
import Data.Coord
import Data.List.NonEmpty (NonEmpty)
import qualified Data.Map.Strict as Map
import qualified Data.Tile as Tile
import qualified Db.Repo.TileRepo as Repo
import Has
import qualified Tile.ImageGenerator
import Tile.TileTypes
import qualified Util

generateImages :: AppM ()
generateImages = do
  tiles <- Repo.getAllTilesOrdered
  let layeredTiles = Map.toList $ Util.sortAndGroupToMap (Has.getTyped @Tile.Layer) tiles
  mapM_ genOneLayer layeredTiles

genOneLayer :: HasAll '[Unit 'X, Unit 'Y, Tile.Tile] a => (Tile.Layer, NonEmpty a) -> AppM ()
genOneLayer (l, tiles) =
  (Tile.ImageGenerator.generateAndWriteTileMapImage l . toList . fmap toTileData) tiles
  where
    toTileData a = mkTileData (getTyped a) (mkCoordLike (getTyped a) (getTyped a)) (Tile.layerType l)
