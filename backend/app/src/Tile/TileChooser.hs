{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Tile.TileChooser (executeTileResolvers) where

import CustomPrelude
import Data.Coord (Position)
import qualified Data.Foldable (toList)
import qualified Data.List.NonEmpty as NonEmpty
import qualified Data.Map.Strict as Map
import qualified Data.Tile as Tile
import qualified Data.TileType as TileType
import qualified Tile.Algo.Fence as Fence
import qualified Tile.Algo.ModuloTileChooser as ModuloTileChooser
import qualified Tile.Algo.Neighbour as Neighbour
import qualified Tile.Algo.TreeTileChooser as TreeTileChooser
import Tile.TileTypes
import qualified Util

{--
 How is this supposed to work?
 Input is a list of updated tiles: position + tiletype
 and the previously computed tiles: position + ground + decoration + overlay

 1. Simplify the previously computed tiles to position + tiletype. Overlay is not a tiletype so it may be dropped.
    So we have [position + maybe groundT + maybe decorationT]
 2. Merge the updates into the previously computed tiles
 3. Group tiles by tiletype
 4. Compute tiles

 Considerations:
 Every tile chooser only chooses the positions it is given + overlays on potential other positions.
 Could encode this in the type to make merges unnecessary?

 Top level method could pass in the required context i.e. Neighbours/previously computed tiles
 So type would be ctx -> position -> tile + (Maybe OverlayTile, Position)
 ctx could be chosen by the method itself.
 Problem: this is weird to test.
-}

executeTileResolvers :: Map Position TileType.TileType -> [TileData']
executeTileResolvers =
  mergeTiles
    . concatMap Data.Foldable.toList
    . Map.elems
    . Map.mapWithKey tileResolver
    . Util.invertMap

tileResolver :: TileType.TileType -> TileResolver' NonEmpty.NonEmpty
tileResolver = \case
  TileType.Sand ->
    neighbourBased Tile.Sand
  TileType.Pavement ->
    neighbourBased Tile.Pavement
  TileType.DarkSand ->
    neighbourBased Tile.DarkSand
  TileType.BlueishMeadow ->
    neighbourBased Tile.BlueishMeadow
  TileType.Meadow ->
    Data.Foldable.toList . fmap (fmap Tile.Meadow) . ModuloTileChooser.chooseModulo
  TileType.GrassMeadow ->
    constantGround Tile.GrassMeadow
  TileType.ClassicTree ->
    fmap (fmap Tile.ClassicTree) . TreeTileChooser.chooseClassic
  TileType.LargeTree ->
    fmap (fmap Tile.LargeTree) . TreeTileChooser.chooseLarge
  TileType.WhiteSand ->
    neighbourBased Tile.WhiteSand
  TileType.RowBush ->
    Data.Foldable.toList . fmap (fmap Tile.RowBush) . Fence.chooseTiles
  TileType.Water ->
    neighbourBased Tile.Water
  TileType.RockArea ->
    neighbourBased Tile.RockArea
  TileType.BreakableRockDecoration ->
    constantGround Tile.BreakableRockDecoration
  TileType.BushDecoration ->
    constantGround Tile.BushDecoration
  TileType.CutDecoration ->
    constantGround Tile.CutDecoration
  TileType.PokeballDecoration ->
    constantGround Tile.PokeballDecoration
  TileType.MovableRockDecoration ->
    constantGround Tile.MovableRockDecoration
  TileType.RockDecoration ->
    constantGround Tile.RockDecoration
  TileType.WhiteSignDecoration ->
    constantGround Tile.WhiteSignDecoration
  TileType.WoodenSignDecoration ->
    constantGround Tile.WoodenSignDecoration
  where
    neighbourBased tileType = Data.Foldable.toList . fmap (fmap tileType) . Neighbour.chooseBasedOnNeighbour
    constantGround constantTile = Data.Foldable.toList . fmap (`groundTileData` constantTile)

mergeTiles :: [TileData'] -> [TileData']
mergeTiles =
  (=<<) Data.Foldable.toList
    . fmap merge
    . NonEmpty.groupBy samePosition
    . sortOn position
  where
    samePosition :: TileData' -> TileData' -> Bool
    samePosition t1 t2 = position t1 == position t2

    merge :: NonEmpty.NonEmpty TileData' -> NonEmpty.NonEmpty TileData'
    merge (a NonEmpty.:| as) = mergeRemaining as (a NonEmpty.:| [])

    mergeRemaining [] result = result
    mergeRemaining (a : as) (b NonEmpty.:| bs)
      | isOverlay a /= isOverlay b = a NonEmpty.:| [b]
      | otherwise = mergeRemaining as (b NonEmpty.:| bs)
