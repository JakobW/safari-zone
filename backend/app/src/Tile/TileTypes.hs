{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE TypeFamilies #-}

module Tile.TileTypes
  ( TileResolver,
    TileResolver',
    TileData,
    TileData',
    overlayTileData,
    groundTileData,
    layer,
    tile,
    position,
    isOverlay,
    mkTileData
  )
where

import CustomPrelude
import Data.Coord (Position)
import qualified Data.Tile

type TileData' = TileData Data.Tile.Tile

data TileData t = TileData
  { tile :: t,
    position :: Position,
    layer :: Data.Tile.LayerType
  }
  deriving (Show, Eq, Ord, Functor, Generic)

$(makeFieldLabelsNoPrefix ''TileData)

isOverlay :: TileData t -> Bool
isOverlay = layer .> (== Data.Tile.Overlay)

mkTileData :: t -> Position -> Data.Tile.LayerType -> TileData t
mkTileData = TileData

overlayTileData :: Position -> t -> TileData t
overlayTileData pos t = TileData t pos Data.Tile.Overlay

groundTileData :: Position -> t -> TileData t
groundTileData pos t = TileData t pos Data.Tile.Ground

type TileResolver a f = a -> TileResolver' f

type TileResolver' f = f Position -> [TileData']