{-# LANGUAGE TemplateHaskell #-}

module Tile.TileWriter (writeTileset) where

import CustomPrelude
import Tile.Tileset
import Api.ImagesApi (optimizedAtlasUrl)
import qualified Resource

writeTileset :: IO ()
writeTileset = do
  resourceDir <- $(Resource.getResourceDir)
  let destImagePath = Resource.getResource resourceDir optimizedAtlasUrl
  writeFile destImagePath $(embeddedTileset)