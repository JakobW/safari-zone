{-# LANGUAGE TemplateHaskell #-}

module Tile.Tileset (embeddedTileset) where

import CustomPrelude
import qualified Graphics.Image as I
import Generics.Deriving (genum)
import qualified Data.Tile as Tile
import Data.FileEmbed as Embed
import qualified Language.Haskell.TH as TH
import System.FilePath as Path

type Image = I.Image I.VS I.RGBA Double

type Pixel = I.Pixel I.RGBA Double

embeddedTileset :: TH.ExpQ
embeddedTileset =
  Embed.bsToExp =<< TH.runIO tilesetByteString

tilesetByteString :: IO ByteString
tilesetByteString = do
  let tiles = genum @Tile.Tile
      srcImagePath = Path.takeDirectory $(TH.LitE . TH.stringL . TH.loc_filename <$> TH.location) <> "/tileset-advance.png"
  (image :: Image) <- I.readImage' srcImagePath
  let newImage = I.traverse image (translateDimensions $ length tiles) translatePixels
  I.encode I.PNG [] newImage |> toStrict |> pure

translateDimensions :: Int -> (Int, Int) -> (Int, Int)
translateDimensions tileAmount (_, x) =
  (tileSize * ceiling fractionalHeight, x)
  where
    fractionalHeight :: Float
    fractionalHeight = fromIntegral tileAmount / targetWidth

targetWidth :: Num a => a
targetWidth = 8

tileSize :: Num a => a
tileSize = 16

translatePixels :: ((Int, Int) -> Pixel) -> (Int, Int) -> Pixel
translatePixels getPx (y, x) =
  case Tile.toTileIndex <$> Tile.idToTile pos of
    Just (Tile.Selected tileIndex) ->
      getPx (getCoordForTileIndex tileIndex)
    Just (Tile.Combined topLayer bottomLayer) ->
      let (I.PixelRGBA r g b a) = getPx (getCoordForTileIndex topLayer)
          (I.PixelRGBA r' g' b' a') = getPx (getCoordForTileIndex bottomLayer)
       in if a > 0 then I.PixelRGBA r g b a else I.PixelRGBA r' g' b' a'
    Nothing ->
      I.PixelRGBA 0 0 0 0
  where
    tileX = x `quot` tileSize
    tileY = y `quot` tileSize
    remX = x `rem` tileSize
    remY = y `rem` tileSize
    pos = (targetWidth * tileY) + tileX
    getCoordForTileIndex tileIndex =
      (floor (fromIntegral tileIndex / 8 :: Float) * tileSize + remY, (tileIndex `rem` 8) * tileSize + remX)
