{-# LANGUAGE GeneralisedNewtypeDeriving #-}

module Time
  ( Microseconds,
    Milliseconds,
    Seconds,
    Minutes,
    Timeable (..),
    threadDelay,
    divide,
    toDiffTime,
  )
where

import qualified Control.Concurrent (threadDelay)
import CustomPrelude
import Data.Time.Clock as Clock

newtype Microseconds = Microseconds Integer deriving newtype (Eq, Num)

instance Show Microseconds where
  show (Microseconds s) = show s <> "micros"

newtype Milliseconds = Milliseconds Integer deriving newtype (Eq, Num)

instance Show Milliseconds where
  show (Milliseconds s) = show s <> "ms"

newtype Seconds = Seconds Integer deriving newtype (Eq, Num)

instance Show Seconds where
  show (Seconds s) = show s <> "s"

newtype Minutes = Minutes Integer deriving newtype (Eq, Num)

instance Show Minutes where
  show (Minutes m) = show m <> "min"

instance Timeable Milliseconds where
  toMicros (Milliseconds ms) = Microseconds (1000 * ms)

instance Timeable Seconds where
  toMicros (Seconds s) = toMicros $ Milliseconds (1000 * s)

instance Timeable Clock.DiffTime where
  toMicros = Microseconds . (`div` 1000000) . diffTimeToPicoseconds

instance Timeable Clock.NominalDiffTime where
  toMicros = toMicros . Seconds . fst . properFraction . Clock.nominalDiffTimeToSeconds

instance Timeable Minutes where
  toMicros (Minutes m) = toMicros $ Seconds (60 * m)

class Timeable t where
  toMicros :: t -> Microseconds

instance Timeable Microseconds where
  toMicros = id

threadDelay :: (MonadIO m, Timeable t) => t -> m ()
threadDelay =
  go . toMicrosInt
  where
    go micros = do
      let maxWait = min micros $ toInteger (maxBound :: Int)
      liftIO $ Control.Concurrent.threadDelay $ fromInteger maxWait
      when (maxWait /= micros) $ go (micros - maxWait)

divide :: (Timeable t0, Timeable t1) => t0 -> t1 -> Integer
divide t0 t1 = toMicrosInt t0 `div` toMicrosInt t1

toMicrosInt :: Timeable t => t -> Integer
toMicrosInt = (\(Microseconds m) -> m) . toMicros

toDiffTime :: Seconds -> Clock.DiffTime
toDiffTime (Seconds s) = fromInteger s
