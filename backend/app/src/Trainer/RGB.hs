module Trainer.RGB (RGB (..), rgbToHex, hexToRgb) where

import CustomPrelude
import Data.Aeson
import qualified Database.PostgreSQL.Typed.Types as PG
import Numeric

data RGB = RGB
  { r :: Word8,
    g :: Word8,
    b :: Word8
  }
  deriving (Eq, Show, Ord, Generic, ToJSON, FromJSON)

instance PG.PGColumn "bpchar" RGB where
  pgDecode typeId =
    PG.pgDecode typeId .> hexToRgb .> \case
      Just rgb -> rgb
      Nothing -> error "Failed to decode rgb from database"

instance PG.PGParameter "bpchar" RGB where
  pgEncode typeId = rgbToHex .> PG.pgEncode typeId

showHex2 :: (Integral a, Show a) => a -> String -> String
showHex2 x
  | x <= 0xf = ('0' :) . showHex x
  | otherwise = showHex x

rgbToHex :: RGB -> String
rgbToHex rgb = (showHex2 (r rgb) <. showHex2 (g rgb) <. showHex2 (b rgb)) ""

hexToRgb :: String -> Maybe RGB
hexToRgb [r1, r2, g1, g2, b1, b2] = do
  r <- readHex2 r1 r2
  g <- readHex2 g1 g2
  b <- readHex2 b1 b2
  pure <| RGB r g b
hexToRgb _ = Nothing

readHex2 :: Char -> Char -> Maybe Word8
readHex2 c1 c2 =
  readHex [c1, c2]
    |> headMay .> fmap fst
