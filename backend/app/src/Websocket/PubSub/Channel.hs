{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE AllowAmbiguousTypes #-}

module Websocket.PubSub.Channel (Channel(..)) where

import CustomPrelude
import Data.Aeson

class (FromJSON (ChannelContent c), ToJSON (ChannelContent c)) => Channel c where
  type ChannelContent c
  channelToByteString :: c -> ByteString