{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeFamilies #-}

module Websocket.PubSub.Channel.Cache
  ( Channel (..),
    Msg (InvalidateMsg),
    withCache,
    useCache,
    InMemoryCache,
    CacheType (..),
    HasCache (..),
    invalidateCache,
    CacheM,
  )
where

import CustomPrelude
import Data.Aeson
import qualified Data.Dynamic as Dyn
import qualified Data.Map.Strict as Map
import qualified Data.Time.Clock as Clock
import qualified Katip as K
import qualified Websocket.PubSub.Channel as PubSub
import qualified Websocket.PubSub.Types as PubSub

data Channel = Channel

newtype Msg = InvalidateMsg
  { invalidatedCacheKey :: Text
  }
  deriving newtype (FromJSON, ToJSON)

instance PubSub.Channel Channel where
  type ChannelContent Channel = Msg
  channelToByteString _ = "cache"

class (Typeable k, Typeable (CacheT k)) => CacheType k where
  type CacheT k

newtype InMemoryCache = InMemoryCache
  { getCacheImpl :: TVar (Map Text (UTCTime, Dyn.Dynamic))
  }

withCache :: (MonadIO m, PubSub.PubSub pubsub m, MonadUnliftIO m) => pubsub -> (InMemoryCache -> m a) -> m a
withCache pubsub action = do
  cache <- newTVarIO mempty
  let subscribe = PubSub.subscribe pubsub Channel (atomically . modifyTVar cache . Map.delete . invalidatedCacheKey)
  bracket subscribe id (\_ -> action (InMemoryCache cache))

addToCache :: (MonadIO m, Typeable a) => InMemoryCache -> Text -> Clock.UTCTime -> a -> m ()
addToCache (InMemoryCache cache) cacheKey expireTime =
  atomically <. modifyTVar cache <. Map.insert cacheKey <. (expireTime,) <. Dyn.toDyn

retrieveFromCache :: forall a m. (MonadIO m, Typeable a, K.KatipContext m) => InMemoryCache -> Text -> Clock.UTCTime -> m (Maybe a)
retrieveFromCache cache cacheKey currentTime =
  (getCacheImpl cache |> readTVarIO)
    >>= retrieve .> mapM typeCast .> fmap join
  where
    retrieve :: Map Text (UTCTime, Dyn.Dynamic) -> Maybe Dyn.Dynamic
    retrieve = Map.lookup cacheKey >=> \(expireTime, cacheValue) -> if expireTime > currentTime then Just cacheValue else Nothing
    typeCast :: Dyn.Dynamic -> m (Maybe a)
    typeCast =
      Dyn.fromDynamic .> \case
        Just a -> pure (Just a)
        Nothing -> K.logLocM K.WarningS ("Failed to typecast value of key " <> K.ls cacheKey) $> Nothing

useCache' :: (Typeable a, MonadIO m, K.KatipContext m) => InMemoryCache -> Text -> Clock.NominalDiffTime -> m a -> m a
useCache' cache cacheKey timeToExpire f = do
  currentTime <- liftIO Clock.getCurrentTime
  retrieveFromCache cache cacheKey currentTime >>= \case
    Just cached ->
      K.logLocM K.DebugS ("Used cache for key " <> K.ls cacheKey) $> cached
    Nothing -> do
      result <- f
      addToCache cache cacheKey (Clock.addUTCTime timeToExpire currentTime) result
      K.logLocM K.DebugS ("Initialized cache for key " <> K.ls cacheKey) $> result

invalidateCache' :: (MonadIO m) => InMemoryCache -> Text -> m ()
invalidateCache' (InMemoryCache cache) =
  atomically <. modifyTVar cache <. Map.delete

useCache :: (Typeable a, CacheM m) => Text -> Clock.NominalDiffTime -> m a -> m a
useCache cacheKey timeToExpire wrapped = do
  cache <- getCache
  useCache' cache cacheKey timeToExpire wrapped

invalidateCache :: (CacheM m) => Text -> m a -> m a
invalidateCache cacheKey wrapped = do
  cache <- getCache
  _ <- PubSub.publishM Channel (InvalidateMsg cacheKey)
  invalidateCache' cache cacheKey
  K.logLocM K.DebugS ("Invalidated cache entry for key " <> K.ls cacheKey)
  wrapped

class MonadIO m => HasCache m where
  getCache :: m InMemoryCache

type CacheM m = (HasCache m, PubSub.PubSubM m, K.KatipContext m)
