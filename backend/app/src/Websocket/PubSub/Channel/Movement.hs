{-# LANGUAGE StrictData #-}
{-# LANGUAGE TypeFamilies #-}

module Websocket.PubSub.Channel.Movement (Channel(..), Msg(..)) where

import CustomPrelude
import Data.Aeson
import qualified Data.Coord as C
import Data.User.UserId
import qualified Websocket.PubSub.Channel as PubSub

data Channel = Channel

data Msg = Msg
  { id :: UserId,
    position :: C.Position
  }
  deriving (Show, Eq, Generic, FromJSON, ToJSON)

instance PubSub.Channel Channel where
  type ChannelContent Channel = Msg
  channelToByteString _ = "movement"
