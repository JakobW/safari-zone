{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

module Websocket.PubSub.Channel.Pokemon
  ( Channel (..),
    Msg (..),
    NewEncounter (NewEncounter),
    MoveEncounter (MoveEncounter),
    mkNewEncounterMsg,
    mkMoveEncounterMsg,
    mkRemoveEncounterMsg,
  )
where

import CustomPrelude
import Data.Aeson
import qualified Data.Coord as C
import Data.Pokemon
import Data.WildPokemon
import qualified Db.Row
import qualified Generic.Random as QC
import Has (getTyped)
import qualified Test.QuickCheck as QC
import qualified Websocket.PubSub.Channel as PubSub

data Channel = Channel

data Msg
  = NewEncounterMsg NewEncounter
  | RemoveEncounterMsg WildPokemonId
  | MoveEncounterMsg MoveEncounter
  deriving (Show, Eq, Generic, FromJSON, ToJSON)

data NewEncounter = NewEncounter
  { position :: C.Position,
    pokemonId :: PokemonId,
    encounterId :: WildPokemonId,
    shinyStatus :: ShinyStatus
  }
  deriving (Show, Eq, Generic, FromJSON, ToJSON)

data MoveEncounter = MoveEncounter
  { position :: C.Position,
    encounterId :: WildPokemonId
  }
  deriving (Show, Eq, Generic, FromJSON, ToJSON)

mkNewEncounterMsg :: C.Position -> PokemonId -> WildPokemonId -> ShinyStatus -> Msg
mkNewEncounterMsg = (fmap . fmap . fmap . fmap) NewEncounterMsg NewEncounter

mkMoveEncounterMsg :: C.Position -> WildPokemonId -> Msg
mkMoveEncounterMsg = (fmap . fmap) MoveEncounterMsg MoveEncounter

mkRemoveEncounterMsg :: WildPokemonId -> Msg
mkRemoveEncounterMsg = RemoveEncounterMsg

instance PubSub.Channel Channel where
  type ChannelContent Channel = Msg
  channelToByteString _ = "pokemon"

$(makeFieldLabelsNoPrefix ''NewEncounter)
$(makeFieldLabelsNoPrefix ''MoveEncounter)

instance Db.Row.RowLike NewEncounter where
  type AsRow NewEncounter = '[C.Unit 'C.X, C.Unit 'C.Y, PokemonId, WildPokemonId, ShinyStatus]
  fromRow = NewEncounter <$> (C.mkCoordLike . getTyped <*> getTyped) <*> getTyped <*> getTyped <*> getTyped
  toRow e = (e ^. #position % #x, e ^. #position % #y, e ^. #pokemonId, e ^. #encounterId, e ^. #shinyStatus)

instance QC.Arbitrary NewEncounter where
  arbitrary = NewEncounter <$> QC.arbitrary <*> QC.arbitrary <*> QC.arbitrary <*> QC.arbitrary

instance QC.Arbitrary MoveEncounter where
  arbitrary = MoveEncounter <$> QC.arbitrary <*> QC.arbitrary

instance QC.Arbitrary Msg where
  arbitrary = QC.genericArbitrary QC.uniform
