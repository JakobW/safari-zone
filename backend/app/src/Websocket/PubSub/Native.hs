{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE TypeApplications #-}

module Websocket.PubSub.Native (withNativePubSub, createNativePubSub) where

import CustomPrelude
import Websocket.PubSub.Types
import qualified Data.Map.Strict as Map
import Control.Concurrent.STM (stateTVar)
import Websocket.PubSub.Channel
import Data.Aeson

withNativePubSub :: MonadIO m => (SomePubSub m -> m ()) -> m ()
withNativePubSub =
  (createNativePubSub >>=)

createNativePubSub :: MonadIO m => m (SomePubSub m)
createNativePubSub = SomePubSub <$> createHPubSub @ByteString

instance MonadIO m => PubSub (HPubSub m ByteString) m where
  publish ps channel = fmap Right . hPublish ps (channelToByteString channel) . toStrict . encode
  subscribe ps channel subscriber = hSubscribe ps (channelToByteString channel) $ (\case
      Left _ -> pure ()
      Right msg -> subscriber msg
    ) . eitherDecodeStrict

createHPubSub :: forall a m. MonadIO m => m (HPubSub m a)
createHPubSub = newTVarIO mempty

type HPubSub m a = TVar (Map ByteString (Subscribers m a))

type Subscribers m a = IdMap (a -> m ())

data IdMap a = IdMap (Map Integer a) Integer deriving (Functor, Foldable, Traversable)

emptySubs :: IdMap a
emptySubs = IdMap mempty 0

addSub :: a -> IdMap a -> (Integer, IdMap a)
addSub sub (IdMap subs nextId') = (nextId', IdMap (Map.insert nextId' sub subs) (nextId' + 1))

removeSub :: Integer -> IdMap a -> IdMap a
removeSub i (IdMap subs nextId') = IdMap (Map.delete i subs) nextId'

subSize :: IdMap a -> Integer
subSize (IdMap subs _) = fromIntegral $ Map.size subs

hGetChannel :: MonadIO m => HPubSub m a -> ByteString -> m (Maybe (Subscribers m a))
hGetChannel ps channel = Map.lookup channel <$> readTVarIO ps

hPublish :: MonadIO m => HPubSub m a -> ByteString -> a -> m Integer
hPublish ps channel message = maybe (pure 0) (hPublishToSubs message) =<< hGetChannel ps channel

hPublishToSubs :: Monad m => a -> Subscribers m a -> m Integer
hPublishToSubs message = fmap subSize . mapM ($ message)

hSubscribe :: forall m a. MonadIO m => HPubSub m a -> ByteString -> (a -> m ()) -> m (m ())
hSubscribe ps channel subscriber = fmap removeSubscriber $ atomically $ stateTVar ps (Map.alterF addSubscriber channel)
  where
    addSubscriber :: Maybe (Subscribers m a) -> (Integer, Maybe (Subscribers m a))
    addSubscriber = second Just . addSub subscriber . fromMaybe emptySubs
    removeSubscriber :: Integer -> m ()
    removeSubscriber i = atomically $ modifyTVar ps (Map.alter (fmap $ removeSub i) channel)
