module Websocket.PubSub.Redis (withRedisPubSub) where

import Control.Concurrent (threadDelay)
import CustomPrelude
import Data.Aeson
import qualified Database.Redis as R
import qualified Katip as K
import Websocket.PubSub.Channel
import Websocket.PubSub.Types

newtype RedisPubSub = RedisPubSub (R.PubSubController, R.Connection)

withRedisPubSub :: (K.KatipContext m, MonadUnliftIO m, MonadCatch m) => R.Connection -> (SomePubSub m -> m ()) -> m ()
withRedisPubSub conn program = do
  pubSubCtrl <- R.newPubSubController [] []
  concurrently_
    ( forever $
        liftIO (R.pubSubForever conn pubSubCtrl (return ()))
          `catch` ( \(e :: R.ConnectionLostException) -> do
                      K.logLocM K.ErrorS ("Redis Connection Exception: " <> K.showLS e)
                      liftIO $ threadDelay $ 50 * 1000 -- TODO: use exponential backoff
                  )
    )
    $ (program . SomePubSub . RedisPubSub) (pubSubCtrl, conn)

instance (MonadUnliftIO m, K.KatipContext m) => PubSub RedisPubSub m where
  publish (RedisPubSub (_, conn)) channel =
    liftIO
      . fmap (first tshow)
      . R.runRedis conn
      . R.publish (channelToByteString channel)
      . toStrict
      . encode
  subscribe (RedisPubSub (psCtl, _)) channel subscriber =
    withRunInIO $ \runInIO ->
      liftIO <$> R.addChannels psCtl [(channelToByteString channel, runInIO . byteStringSub)] []
    where
      byteStringSub =
        ( \case
            Left err -> K.logLocM K.ErrorS (K.ls err)
            Right content -> subscriber content
        )
          . eitherDecodeStrict
