{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE TypeFamilies #-}

module Websocket.PubSub.Types (PubSub (..), SomePubSub (..), PubSubM(..), withSubscription) where

import CustomPrelude
import Websocket.PubSub.Channel

class PubSub pubsub m where
  publish :: (Channel c) => pubsub -> c -> ChannelContent c -> m (Either Text Integer)
  subscribe :: (Channel c) => pubsub -> c -> (ChannelContent c -> m ()) -> m (m ())

class PubSubM m where
  publishM :: (Channel c) => c -> ChannelContent c -> m (Either Text Integer)
  subscribeM :: (Channel c) => c -> (ChannelContent c -> m ()) -> m (m ())

data SomePubSub m = forall pubsub. (PubSub pubsub m) => SomePubSub pubsub

instance PubSub (SomePubSub m) m where
  publish (SomePubSub ps) = publish ps
  subscribe (SomePubSub ps) = subscribe ps

withSubscription :: (Channel c, PubSubM m, MonadUnliftIO m) => c -> (ChannelContent c -> m ()) -> m a -> m a
withSubscription channel subscriber program = bracket (subscribeM channel subscriber) id (const program)