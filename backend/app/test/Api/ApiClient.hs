module Api.ApiClient where

import qualified Network.HTTP.Client as Http
import qualified Servant.Client as Client
import CustomPrelude
import Servant.Client.Core (requestHeaders)
import Data.Sequence (Seq(..))
import Servant.API.Flatten (flatten)

run :: (MonadIO m) => String -> Int -> Client.ClientM a -> m a
run path port req =
  liftIO (Http.newManager Http.defaultManagerSettings)
    >>= liftIO . Client.runClientM req . flip Client.mkClientEnv baseUrl
    >>= eitherToError
  where
    baseUrl = Client.BaseUrl Client.Http "localhost" port path
    eitherToError :: (MonadIO m, Exception e) => Either e a -> m a
    eitherToError = \case
      Left err ->
        throwIO err
      Right r -> pure r
