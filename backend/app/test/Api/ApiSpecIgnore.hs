module Api.ApiSpecIgnore where

import Servant.QuickCheck
import Api.AppTestBase
import CustomPrelude
import Test.Hspec
import qualified Lib

{-
spec :: Spec
spec = around withAppContext $
  describe "the api" $ do
    it "follows best practices" $ \ctx ->
      withServantServer Lib.api (pure $ createServerFromContext ctx) $
        \port ->
          serverSatisfies Lib.api port defaultArgs
            (not500 <%> onlyJsonObjects <%> mempty)-}
