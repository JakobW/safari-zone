{-# LANGUAGE GeneralisedNewtypeDeriving #-}
{-# LANGUAGE TypeFamilies #-}

module Api.AppTestBase
  ( createJWT,
    describeWithApp,
    appIntTest,
    appIntTestWith,
    appTest,
    appTestWith,
    captureImages,
    get,
    post,
    put,
    httpDelete,
    shouldRespondWith,
    shouldRespondWithBodySatisifying,
    AppTestT,
    preparePublish
  )
where

import qualified Auth
import Config (Config (Config))
import Control.Monad.Fail (MonadFail)
import Control.Monad.Reader (MonadTrans)
import CustomPrelude
import qualified Data.Aeson as Aeson
import qualified Data.Map.Strict as Map
import Db.Client (RunDb (..))
import Env
import qualified ImageComparison
import Lib (readerServer)
import Logger
import LoggerBase
import Network.Wai (Application)
import qualified Network.Wai.Handler.Warp as Wai
import Network.Wai.Test (SResponse, simpleBody)
import Random (RandGen)
import RepoTestBase
import Servant.Auth.Server (ToJWT, makeJWT)
import System.Random (mkStdGen)
import Test.Hspec
import qualified Test.Hspec.Wai as WaiTest
import Test.Hspec.Wai.Internal (WaiSession, withApplication)
import qualified Websocket.PubSub.Channel.Cache as Cache
import qualified Websocket.PubSub.Native as PubSub
import qualified Websocket.PubSub.Types as PubSub
import Websocket.PubSub.Channel

initRandom :: Int -> IO RandGen
initRandom =
  atomically . newTVar . mkStdGen

contextToConfig :: AppContext -> PGConnection -> Cache.InMemoryCache -> String -> Config
contextToConfig ctx conn cache testName = do
  Config
    (environment ctx)
    (RunDb $ \f -> f conn)
    (randGen ctx)
    (appContextToJwtMap ctx)
    (ImageComparison.runCompare testName (capturedImages ctx))
    (pubSub ctx)
    cache

appContextToJwtMap :: AppContext -> Auth.MultiJWTSettings
appContextToJwtMap = Auth.jwtMapFromContext . authContext

application :: AppContext -> PGConnection -> Cache.InMemoryCache -> String -> Application
application ctx conn cache =
  readerServer (authContext ctx) (katipConfig ctx) . contextToConfig ctx conn cache

fromRightUnsafe :: Either l r -> r
fromRightUnsafe (Left _) = error "Either resolved to Left but expected Right."
fromRightUnsafe (Right r) = r

createJWT :: (ToJWT a, IsSequence b, Element LByteString ~ Element b, MonadIO m) => Auth.AuthProvider -> a -> AppTestT m b
createJWT authProvider a = do
  multiSettings <- asks appContextToJwtMap >>= liftIO . readTVarIO
  jwtEither <- liftIO $ makeJWT a (multiSettings Map.! authProvider) Nothing
  return $ repack (fromRightUnsafe jwtEither)

data AppContext = AppContext
  { environment :: Environment,
    dbNameState :: DbNameState,
    randGen :: RandGen,
    authContext :: Auth.AuthContext,
    katipConfig :: KatipConfig,
    capturedImages :: TVar (Bool, Map String Int),
    pubSub :: PubSub.SomePubSub (LogM IO)
  }

createAppContext :: String -> IO AppContext
createAppContext dName = do
  randGen <- initRandom 0
  authContext <- Auth.testAuthContext
  environment <- loadEnv
  katipConfig <- testKatipConfig
  capturedImages <- newTVarIO (False, mempty)
  dbNameState <- initDbNameState dName
  pubSub <- runLogM katipConfig PubSub.createNativePubSub

  return $
    AppContext
      { environment = environment,
        dbNameState = dbNameState,
        randGen = randGen,
        authContext = authContext,
        katipConfig = katipConfig,
        capturedImages = capturedImages,
        pubSub = pubSub
      }

describeWithApp :: String -> SpecWith AppContext -> Spec
describeWithApp dName = before (createAppContext dName) . describe dName

newtype AppTestT m a = AppTestT
  { runTestM :: ReaderT AppContext m a
  }
  deriving newtype (Functor, Applicative, Monad, MonadReader AppContext, MonadIO, MonadFail)

preparePublish :: (Monad m, Channel c) => AppTestT m (c -> ChannelContent c -> IO (Either Text Integer))
preparePublish = do
  ps <- asks pubSub
  katipConf <- asks katipConfig
  pure $ \channel -> runLogM katipConf . PubSub.publish ps channel

instance MonadUnliftIO m => MonadUnliftIO (AppTestT m) where
  withRunInIO = wrappedWithRunInIO AppTestT runTestM

instance MonadTrans AppTestT where
  lift = AppTestT . lift

type AppTestM = AppTestT (WaiSession ())

newtype ContextTransformer = ContextTransformer (AppContext -> IO AppContext)

noTransformer :: ContextTransformer
noTransformer = ContextTransformer pure

captureImages :: ContextTransformer
captureImages = ContextTransformer $ \ctx -> do
  capturedImages <- newTVarIO (True, mempty)
  pure $ ctx {capturedImages = capturedImages}

appTestWith ::
  (HasCallStack, Example (AppContext -> IO a)) =>
  ContextTransformer ->
  String ->
  AppTestM a ->
  SpecWith (Arg (AppContext -> IO a))
appTestWith (ContextTransformer changeContext) testName testCode = it testName testWithContext
  where
    testWithContext ctx = do
      changedCtx <- changeContext ctx
      dbName <- getDbName $ dbNameState ctx
      runLogM (katipConfig ctx) $
        withNamedDb dbName $ \conn -> do
          Cache.withCache (pubSub ctx) $ \cache -> liftIO $ do
            result <-
              withApplication
                (application changedCtx conn cache testName)
                (runReaderT (runTestM testCode) changedCtx)
            verifyImages changedCtx
            pure result

    verifyImages =
      ImageComparison.verifySameImagesHaveBeenWritten testName . capturedImages

appTest ::
  (HasCallStack, Example (AppContext -> IO a)) =>
  String ->
  AppTestM a ->
  SpecWith (Arg (AppContext -> IO a))
appTest = appTestWith noTransformer

appIntTestWith ::
  (HasCallStack, Example (AppContext -> IO ())) =>
  ContextTransformer ->
  String ->
  (Wai.Port -> AppTestT IO ()) ->
  SpecWith (Arg (AppContext -> IO ()))
appIntTestWith (ContextTransformer changeContext) testName testCode =
  it testName runWithContext
  where
    runWithContext ctx = do
      changedCtx <- changeContext ctx
      dbName <- getDbName $ dbNameState ctx
      runLogM (katipConfig ctx) $
        withNamedDb dbName $ \conn -> do
          Cache.withCache (pubSub ctx) $ \cache -> liftIO $ do
            result <-
              timeout
                -- timeout 2 seconds
                2000000
                ( Wai.testWithApplication
                    (pure $ application changedCtx conn cache testName)
                    (\port -> runReaderT (runTestM $ testCode port) changedCtx)
                )
                >>= \case
                  Nothing -> expectationFailure "Timeout!"
                  Just _ -> pure ()
            verifyImages changedCtx
            pure result

    verifyImages =
      ImageComparison.verifySameImagesHaveBeenWritten testName . capturedImages

appIntTest ::
  (HasCallStack, Example (AppContext -> IO ())) =>
  String ->
  (Wai.Port -> AppTestT IO ()) ->
  SpecWith (Arg (AppContext -> IO ()))
appIntTest = appIntTestWith noTransformer

shouldRespondWith :: HasCallStack => WaiSession st SResponse -> WaiTest.ResponseMatcher -> AppTestT (WaiSession st) ()
waiSession `shouldRespondWith` matcher = lift $ waiSession `WaiTest.shouldRespondWith` matcher

shouldRespondWithBodySatisifying ::
  (HasCallStack, Aeson.FromJSON a, Show a) =>
  WaiSession st SResponse ->
  (a -> Bool) ->
  AppTestT (WaiSession st) ()
shouldRespondWithBodySatisifying waiSession predicate = do
  body <- lift $ simpleBody <$> waiSession
  case Aeson.eitherDecode body of
    Right decoded -> liftIO $ decoded `shouldSatisfy` predicate
    Left err ->
      liftIO $ expectationFailure err

get :: ByteString -> WaiSession st SResponse
get = WaiTest.get

post :: Aeson.ToJSON a => ByteString -> a -> WaiSession st SResponse
post url body = WaiTest.request "POST" url [("Content-Type", "application/json")] (Aeson.encode body)

put :: Aeson.ToJSON a => ByteString -> a -> WaiSession st SResponse
put url body = WaiTest.request "PUT" url [("Content-Type", "application/json")] (Aeson.encode body)

httpDelete :: Aeson.ToJSON a => ByteString -> a -> WaiSession st SResponse
httpDelete url body = WaiTest.request "DELETE" url [("Content-Type", "application/json")] (Aeson.encode body)
