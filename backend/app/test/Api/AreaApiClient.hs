module Api.AreaApiClient (updateArea, createArea, getAllAreas) where

import Api.ApiClient
import qualified Api.AreaApi as AreaApi
import CustomPrelude
import Servant
import qualified Servant.Client as Client

updateArea' :<|> createArea' :<|> getAllAreas' =
  Client.client (Proxy :: Proxy AreaApi.AreaApi)

run' :: MonadIO m => Int -> Client.ClientM a -> m a
run' = run "api/area"

updateArea port id' = run' port . updateArea' id'

createArea port = run' port . createArea'

getAllAreas port = run' port getAllAreas'
