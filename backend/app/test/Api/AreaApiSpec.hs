{-# LANGUAGE QuasiQuotes #-}

module Api.AreaApiSpec where

import CustomPrelude
import Test.Hspec
import Network.Wai.Test (simpleBody)
import Api.AppTestBase
import Data.Area
import qualified Data.WithId as WithId
import Data.Aeson (decode)
import TestUtils
import Api.AreaApiClient
import Data.String.Interpolate

spec :: Spec
spec = describeWithApp "AreaApi" $ do
  appTest "can create and retrieve areas" $ do
    testArea <- genArbitrary @Area
    get "api/area" `shouldRespondWith` "[]"

    post "api/area" testArea `shouldRespondWith` 201
    areas <- lift $ simpleBody <$> get "api/area"

    liftIO $ fmap (fmap WithId.theContent) (decode @[AreaWithId] areas) `shouldBe` Just [testArea]

  appTest "can update areas" $ do
    testArea <- genArbitrary @Area
    get "api/area" `shouldRespondWith` "[]"

    areaUpdate <- genArbitrary @Area
    areaId <- lift $ simpleBody <$> post "api/area" testArea

    put [i|api/area/#{areaId}|] areaUpdate `shouldRespondWith` 204
    areas <- lift $ simpleBody <$> get "api/area"

    liftIO $ fmap (fmap WithId.theContent) (decode @[AreaWithId] areas) `shouldBe` Just [areaUpdate]



