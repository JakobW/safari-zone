module Api.EncounterApiClient (getEncounters, createOrUpdateEncounters, deleteEncounter) where

import Api.ApiClient
import qualified Api.EncounterApi as EncounterApi
import CustomPrelude
import Servant
import qualified Servant.Client as Client

getEncounters' :<|> createOrUpdateEncounters' :<|> deleteEncounter' =
  Client.client (Proxy :: Proxy EncounterApi.EncounterApi)

run' :: MonadIO m => Int -> Client.ClientM a -> m a
run' = run "api/encounter"

getEncounters port = run' port . getEncounters'

createOrUpdateEncounters port = run' port . createOrUpdateEncounters'

deleteEncounter port = run' port . deleteEncounter'
