module Api.Flatten2DSpec (spec) where

import Test.Hspec
import CustomPrelude
import Api.Flatten2D
import Data.Coord
import Test.QuickCheck

spec :: Spec
spec =
  describe "flatten" $ do
    it "does not modify a single row with y = 0" $ withMaxSuccess 10 $ property $ \(Positive n) -> do
      let row = [mkCoordLike x 0 | x <- [0 .. n]]
      flatten id row `shouldBe` (fmap Just row, n + 1)

    it "fills up leftover slots in the first row" $ withMaxSuccess 10 $ property $ \(Positive n) -> do
      let row = [mkCoordLike n 0]
          expected = [Nothing | _ <- [0 .. (n - 1)]] ++ fmap Just row
      flatten id row `shouldBe` (expected, n + 1)

    it "fills up leftover slots given two rows" $ withMaxSuccess 10 $ property $ \(Positive n) -> do
      let n0 = mkCoordLike n 0
          n1 = mkCoordLike n 1
          expected = [Nothing | _ <- [0 .. (n - 1)]] ++ [Just n0] ++ [Nothing | _ <- [0 .. (n - 1)]] ++ [Just n1]
      flatten id [n0, n1] `shouldBe` (expected, n + 1)

    let genPositions = listOf (arbitrary `suchThat` (\pos -> pos ^. #x >= 0 && pos ^. #y >= 0))
    it "always produces quadratic systems" $ withMaxSuccess 10 $ forAll genPositions $ \positions -> do
      let (mayPositions, columns) = flatten id positions
          maxX :: Unit 'X
          maxX = (maybe 0 (+ 1) . maximumMay . fmap (view #x)) positions
          maxY :: Unit 'Y
          maxY = (maybe 0 (+ 1) . maximumMay . fmap (view #y)) positions
      (fromIntegral . length) mayPositions `shouldBe` untag maxX * untag maxY
      maxX `shouldBe` columns