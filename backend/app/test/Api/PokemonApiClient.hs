module Api.PokemonApiClient (getDexEntries, getPokemon, getDexEntryDetail, getAllPokemon) where

import qualified Api.PokemonApi as PokemonApi
import CustomPrelude
import Servant
import qualified Servant.Client as Client
import Api.ApiClient
import Api.Models.DexEntry
import Api.Models.DexEntryDetail
import Api.Models.PokemonData
import Data.Pokemon (PokemonId, PokemonNumber)
import Servant.API.Flatten (flatten)
import Auth.AuthClient
import Auth (AuthProvider)

getDexEntries' :: AuthProvider -> Token -> Client.ClientM [DexEntry]
getPokemon' :: AuthProvider -> Token -> PokemonNumber -> Client.ClientM PokemonDataWithId
getDexEntryDetail' :: AuthProvider -> Token -> PokemonId -> Client.ClientM DexEntryDetail
getAllPokemon' :: AuthProvider -> Token -> Client.ClientM [PokemonDataWithId]
getDexEntries' :<|> getPokemon' :<|> getDexEntryDetail' :<|> getAllPokemon' =
  Client.client $ flatten (Proxy :: Proxy PokemonApi.PokemonApi)

run' :: MonadIO m => Int -> Client.ClientM a -> m a
run' = run "api/pokemon"

getDexEntries port ap = run' port . getDexEntries' ap

getPokemon port ap token = run' port . getPokemon' ap token

getDexEntryDetail port ap token = run' port . getDexEntryDetail' ap token

getAllPokemon port ap = run' port . getAllPokemon' ap
