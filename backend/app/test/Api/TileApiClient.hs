module Api.TileApiClient
  ( getTileTypes,
    getAllLayers,
    getAllTilesOfLayer,
    getMoveRules,
    updateTiles,
    deleteTiles
  )
where

import Api.ApiClient
import qualified Api.TileApi as TileApi
import CustomPrelude
import Servant
import qualified Servant.Client as Client

getTileTypes'
  :<|> getMoveRules'
  :<|> (getAllLayers' :<|> getAllTilesOfLayer' :<|> updateTiles' :<|> deleteTiles') =
    Client.client (Proxy :: Proxy TileApi.TileApi)

run' :: MonadIO m => Int -> Client.ClientM a -> m a
run' = run "api/tile"

getTileTypes port = run' port getTileTypes'

getAllLayers port = run' port getAllLayers'

getAllTilesOfLayer port = run' port . getAllTilesOfLayer'

updateTiles port layer = run' port . updateTiles' layer

getMoveRules port = run' port getMoveRules'

deleteTiles port layer = run' port . deleteTiles' layer
