{-# LANGUAGE TypeApplications #-}

module Api.TileApiSpec where

import Api.AppTestBase
import qualified Api.Models.TileInfo as TileInfo
import Api.Models.TileUpdate
import CustomPrelude
import Data.Aeson
import Data.Coord
import qualified Data.Tile as Tile
import qualified Data.Tile.Types.ClassicTree as CT
import qualified Data.Tile.Types.Neighbour as N
import qualified Data.TileType as TileType
import Network.Wai.Test (simpleBody)
import Test.Hspec

spec :: Spec
spec =
  describeWithApp "TileApi" $ do
    let testTile = TileUpdate 1 2 TileType.Sand
    appTestWith captureImages "can create, update and retrieve tiles" $ do
      get "api/tile/layer/0" `shouldRespondWithBodySatisifying` (null . TileInfo.tiles . TileInfo.ground)
      let expectedTileIds = replicate 5 Tile.NoTile <> [Tile.SomeTile $ Tile.Sand N.Middle]
      put "api/tile/layer/0" [testTile] `shouldRespondWith` 204
      get "api/tile/layer/0" `shouldRespondWithBodySatisifying` ((== expectedTileIds) . TileInfo.tiles . TileInfo.ground)

    appTestWith captureImages "can update existing tiles" $ do
      get "api/tile/layer/0" `shouldRespondWithBodySatisifying` (null . TileInfo.tiles . TileInfo.ground)
      put "api/tile/layer/0" [testTile] `shouldRespondWith` 204
      let testTileUpdate = testTile {tile = TileType.Pavement}
          expectedTileIds = replicate 5 Tile.NoTile <> [Tile.SomeTile $ Tile.Pavement N.Middle]
      put "api/tile/layer/0" [testTileUpdate] `shouldRespondWith` 204
      get "api/tile/layer/0" `shouldRespondWithBodySatisifying` ((== expectedTileIds) . TileInfo.tiles . TileInfo.ground)

    appTestWith captureImages "tile integration test" $ do
      (void . lift . put "api/tile/layer/0") [TileUpdate x y TileType.BlueishMeadow | x <- [0 .. 2], y <- [0 .. 2]]
      (void . lift . put "api/tile/layer/0") [TileUpdate x y TileType.BlueishMeadow | x <- [0 .. 2], y <- [3 .. 4]]
      (void . lift . put "api/tile/layer/0") [TileUpdate x y TileType.ClassicTree | x <- [3 .. 6], y <- [0 .. 3]]
      (void . lift . put "api/tile/layer/0") [TileUpdate x y TileType.Sand | x <- [3 .. 5], y <- [4 .. 5]]

      Just createdTiles <- lift $ decode . simpleBody <$> get "api/tile/layer/0"
      liftIO $ do
        (TileInfo.columns . TileInfo.ground) createdTiles `shouldBe` 7
        (length . TileInfo.tiles . TileInfo.ground) createdTiles `shouldBe` 42
        (TileInfo.columns . TileInfo.overlay) createdTiles `shouldBe` 7
        (TileInfo.tiles . TileInfo.ground) createdTiles
          `shouldStartWith` [ Tile.SomeTile $ Tile.BlueishMeadow N.TopLeft,
                              Tile.SomeTile $ Tile.BlueishMeadow N.Top,
                              Tile.SomeTile $ Tile.BlueishMeadow N.TopRight,
                              Tile.SomeTile $ Tile.ClassicTree CT.MiddleLeft,
                              Tile.SomeTile $ Tile.ClassicTree CT.MiddleRight,
                              Tile.SomeTile $ Tile.ClassicTree CT.MiddleLeft,
                              Tile.SomeTile $ Tile.ClassicTree CT.MiddleRight,
                              Tile.SomeTile $ Tile.BlueishMeadow N.Left
                            ]
        (TileInfo.tiles . TileInfo.overlay) createdTiles
          `shouldBe` replicate 10 Tile.NoTile
            <> [ Tile.SomeTile $ Tile.ClassicTree CT.TopOverlayLeft,
                 Tile.SomeTile $ Tile.ClassicTree CT.TopOverlayRight,
                 Tile.SomeTile $ Tile.ClassicTree CT.TopOverlayLeft,
                 Tile.SomeTile $ Tile.ClassicTree CT.TopOverlayRight
               ]

      (void . lift . httpDelete "api/tile/layer/0") [mkCoordLike @Unit x y | x <- [2 .. 4], y <- [0 .. 3]]
      Just afterDeletionTiles <- lift $ decode . simpleBody <$> get "api/tile/layer/0"
      liftIO $ do
        (TileInfo.tiles . TileInfo.ground) afterDeletionTiles
          `shouldStartWith` [ Tile.SomeTile $ Tile.BlueishMeadow N.TopLeft,
                              Tile.SomeTile $ Tile.BlueishMeadow N.TopRight,
                              Tile.NoTile,
                              Tile.NoTile,
                              Tile.NoTile,
                              Tile.SomeTile $ Tile.ClassicTree CT.MiddleLeft,
                              Tile.SomeTile $ Tile.ClassicTree CT.MiddleRight,
                              Tile.SomeTile $ Tile.BlueishMeadow N.Left
                            ]
        (TileInfo.tiles . TileInfo.overlay) afterDeletionTiles
          `shouldBe` replicate 12 Tile.NoTile
            <> [ Tile.SomeTile $ Tile.ClassicTree CT.TopOverlayLeft,
                 Tile.SomeTile $ Tile.ClassicTree CT.TopOverlayRight
               ]