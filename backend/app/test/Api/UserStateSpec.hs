module Api.UserStateSpec where

import qualified Api.UserState as UserState
import CustomPrelude
import Data.Coord
import Data.Tile
import qualified Data.User as User
import Test.Hspec

isRight :: Either a b -> Bool
isRight (Left _) = False
isRight (Right _) = True

spec :: Spec
spec = describe "UserState" $ do
  it "skipped" $
    (2 :: Int) `shouldBe` (2 :: Int)

{-it "successfully initializes if the users tile is in the given tiles" $
  UserState.initState testUser [testTile] `shouldSatisfy` isRight

it "initializes with an error if the users tile is not in the given tiles" $
  UserState.initState testUser [] `shouldSatisfy` not . isRight

it "keeps the user state as is if trying to move to a non-existant tile" $ do
  let (Right initialState) = UserState.initState testUser [testTile]
      (newState, errOrEncounter) = UserState.move (1, 1) initialState
  errOrEncounter `shouldSatisfy` not . isRight
  newState `shouldBe` initialState

it "updates the users position when moving to a valid tile" $ do
  let (Right initialState) =
        UserState.initState
          testUser
          [testTile, testTile {DT.position = grid 1 2}]
      (newState, errOrEncounter) = UserState.move (0, 1) initialState
  errOrEncounter `shouldSatisfy` isRight
  UserState.userData newState `shouldBe` testUser { User.position = grid 1 2 }

it "keeps track of positional changes" $ do
  let (Right initialState) =
        UserState.initState
          testUser
          [testTile, testTile {DT.position = grid 1 2}]
      (newState1, _) = UserState.move (0, 0.3) initialState
      (newState2, _) = UserState.move (0, 0.3) newState1
  UserState.userData newState1 `shouldBe` testUser
  UserState.userData newState2 `shouldBe` testUser { User.position = grid 1 2 }-}
