{-# LANGUAGE TypeApplications #-}

module Api.WebsocketSpec where

import Api.AppTestBase
import qualified Api.AreaApiClient
import qualified Api.EncounterApiClient
import qualified Api.Models.TileUpdate as TileUpdate
import qualified Api.Models.WebsocketMessages as WSMsg
import qualified Api.PokemonApiClient
import qualified Api.TileApiClient
import qualified Auth
import Auth.Andrena.AuthUser (AuthUser (..))
import Auth.AuthClient (Token (..))
import BaseTest
import Control.Concurrent (threadDelay)
import CustomPrelude
import qualified Data.Aeson as Aeson
import qualified Data.Area as Area
import Data.Coord
import qualified Data.Direction as Direction
import Data.Encounter (Encounter (..), EncounterProbability (..))
import qualified Data.Pokemon as Pokemon
import qualified Data.TileType as TileType
import Data.User (mkUserName)
import qualified Data.WithId as WithId
import qualified Network.WebSockets as WS
import qualified Network.WebSockets.Client as Client
import TestUtils (genArbitrary)
import qualified Websocket.PubSub.Channel.Pokemon as Pokemon

spec :: Spec
spec =
  describeWithApp "Websocket" $ do
    appIntTest "connecting to the websocket as a new user with no tiles in db fails" $ \port -> do
      runAuthenticated port (\_ _ -> return ())
        `shouldThrow` (const True :: Selector WS.ConnectionException)

    appIntTest "connecting to the websocket as a new user with tiles in db succeeds" $ \port -> do
      let tiles = [TileUpdate.TileUpdate 0 0 TileType.Sand]
          clientTest response _ =
            decodeJson response `shouldBe` (mkCoordLike 0 0 :: Position)
      layerNumber <- genArbitrary
      void $ Api.TileApiClient.updateTiles port layerNumber tiles
      runAuthenticated port clientTest

    appIntTest "connecting to the websocket without valid jwt fails" $ \port -> do
      runWSClient
        port
        <| \conn -> do
          sendMove conn Direction.R
          WS.receiveDataMessage conn
            `shouldThrow` ( \case
                              WS.CloseRequest _ err -> err == "Unauthorized"
                              _ -> False
                          )

    let setup01TilesWithEncounter :: Int -> AppTestT IO ()
        setup01TilesWithEncounter port = do
          jwt <- mkJWT
          let tiles = [TileUpdate.TileUpdate x y TileType.GrassMeadow | x <- [0 .. 1], y <- [0 .. 1]]

          Right area <- pure $ Area.mkArea <$> Area.mkAreaName "test" <*> Area.mkRange 0 1 <*> Area.mkRange 0 1
          Right pokemonNumber <- pure $ Pokemon.mkPokemonNumber 4
          layerNumber <- genArbitrary

          void $ Api.TileApiClient.updateTiles port layerNumber tiles
          areaId <- Api.AreaApiClient.createArea port area
          pokeWithId <- Api.PokemonApiClient.getPokemon port Auth.AndrenaProvider (Token jwt) pokemonNumber
          let pokeId = WithId.theId pokeWithId
          void $ Api.EncounterApiClient.createOrUpdateEncounters port [Encounter areaId pokeId (EncounterProbability 1)]

    appIntTest "signals moving to empty tile with an error message" $ \port -> do
      let tiles = [TileUpdate.TileUpdate 0 0 TileType.Sand]
          clientTest _ conn = do
            sendMove conn Direction.R
            response <- WS.receiveData conn
            response `shouldSatisfy` isErrorMessage
      layerNumber <- genArbitrary
      void $ Api.TileApiClient.updateTiles port layerNumber tiles
      runAuthenticated port clientTest

    appIntTest "saves position when disconnecting" $ \port -> do
      let clientTest1 _ conn = do
            sendMove conn Direction.R
            WS.sendClose conn ("Close Request" :: ByteString)
          clientTest2 pos _ =
            Aeson.decode pos `shouldBe` Just (mkCoordLike 1 0 :: Position)

      setup01TilesWithEncounter port
      runAuthenticated port clientTest1
      -- delay for 0.5s to make this less flaky
      liftIO $ threadDelay 500000
      runAuthenticated port clientTest2

    appIntTest "passes messages sent to the pokemon channel" $ \port -> do
      setup01TilesWithEncounter port
      publish <- preparePublish
      runAuthenticated port $ \_ conn -> do
        msg <- genArbitrary
        publish Pokemon.Channel msg
        decodeJson <$> WS.receiveData conn `shouldReturn` WSMsg.PokemonMessage msg

mkJWT :: AppTestT IO ByteString
mkJWT = do
  let (Right userName) = mkUserName "a"
  createJWT Auth.AndrenaProvider (AndrenaUser userName "b" "c")

runAuthenticated :: Int -> (LByteString -> Client.ClientApp ()) -> AppTestT IO ()
runAuthenticated port app = do
  jwt <- mkJWT
  runWSClient port $ \conn -> do
    liftIO $ WS.sendTextData conn (Aeson.encode $ WSMsg.AuthenticationMessage Auth.AndrenaProvider (decodeUtf8 jwt))
    WS.receiveData conn >>= (`app` conn)

sendMove :: WS.Connection -> Direction.Direction -> IO ()
sendMove conn =
  WSMsg.MoveMessage .> sendWSMsg conn

sendWSMsg :: WS.Connection -> WSMsg.WebsocketIn -> IO ()
sendWSMsg conn =
  WS.sendTextData conn . Aeson.encode

runWSClient :: MonadIO m => Int -> Client.ClientApp () -> m ()
runWSClient port = liftIO . Client.runClient "127.0.0.1" port basePath
  where
    basePath = "/api/websocket"

decodeJson :: (Aeson.FromJSON a) => LByteString -> a
decodeJson str = case Aeson.eitherDecode str of
  Left errMsg -> error errMsg
  Right a -> a

isErrorMessage :: LByteString -> Bool
isErrorMessage = ((== Just "ErrorMessage") . lookup "tag") . decodeJson @(Map String String)

fail :: Text -> a
fail = error . toList

unsafeJust :: Text -> Maybe a -> a
unsafeJust _ (Just a) = a
unsafeJust err Nothing = fail err
