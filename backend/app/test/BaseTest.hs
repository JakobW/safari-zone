module BaseTest
  ( module Test.Hspec,
    module Test.Hspec.Expectations.Lifted,
    shouldThrow
  )
where

import CustomPrelude
import Data.Typeable (typeOf)
import Test.Hspec
  ( ActionWith,
    Arg,
    Example,
    Expectation,
    Selector,
    Spec,
    SpecWith,
    after,
    afterAll,
    afterAll_,
    after_,
    around,
    aroundWith,
    around_,
    before,
    beforeAll,
    beforeAll_,
    beforeWith,
    before_,
    describe,
    example,
    fdescribe,
    fit,
    focus,
    hspec,
    it,
    parallel,
    pending,
    pendingWith,
    runIO,
    xdescribe,
    xit,
  )
import Test.Hspec.Expectations.Lifted

-- lifted copy & pasted version of shouldThrow from hspec
shouldThrow :: (HasCallStack, Exception e, MonadUnliftIO m) => m a -> Selector e -> m ()
action `shouldThrow` p = do
  r <- try action
  case r of
    Right _ ->
      liftIO $
        expectationFailure $
          "did not get expected exception: " ++ exceptionType
    Left e ->
      (unless (p e) . liftIO . expectationFailure) $
        "predicate failed on expected exception: " ++ exceptionType ++ " (" ++ show e ++ ")"
  where
    -- a string representation of the expected exception's type
    exceptionType = (show . typeOf . instanceOf) p
      where
        instanceOf :: Selector a -> a
        instanceOf _ = error "Test.Hspec.Expectations.shouldThrow: broken Typeable instance"
