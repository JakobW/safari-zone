{-# LANGUAGE AllowAmbiguousTypes #-}

module CommonProperties where

import Test.QuickCheck
import Data.Aeson
import Test.Hspec
import CustomPrelude
import Servant (FromHttpApiData(..), ToHttpApiData(..))
import Database.PostgreSQL.Typed.Types

inverseJSONInstances :: (Arbitrary a, FromJSON a, ToJSON a, Show a, Eq a) => a -> Expectation
inverseJSONInstances a = decode (encode a) `shouldBe` Just a

inverseHttpApiDataInstances :: (Arbitrary a, FromHttpApiData a, ToHttpApiData a, Show a, Eq a) => a -> Expectation
inverseHttpApiDataInstances a = parseUrlPiece (toUrlPiece a) `shouldBe` Right a

inversePGRowInstances :: forall a pgType . (Arbitrary a, PGColumn pgType a, PGParameter pgType a, Show a, Eq a) => a -> Expectation
inversePGRowInstances a = do
  let pgTypeId = PGTypeProxy :: PGTypeID pgType
  pgDecode pgTypeId (pgEncode pgTypeId a) `shouldBe` a