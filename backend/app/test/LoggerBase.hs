module LoggerBase (testKatipConfig, testLogM) where

import Logger
import CustomPrelude
import qualified Katip as K

testKatipConfig :: IO KatipConfig
testKatipConfig = over #logEnv K.clearScribes <$> buildKatipConfig

testLogM :: LogM IO a -> IO a
testLogM f = testKatipConfig >>= flip runLogM f