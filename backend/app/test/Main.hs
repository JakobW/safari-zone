module Main where

import CustomPrelude
import Database (pgDisconnect)
import AppMigration (runAllMigrations)
import RepoTestBase (connectToDb, createTestDb)
import qualified Spec
import Test.Hspec
import qualified ImageComparison

main :: IO ()
main = do
  ImageComparison.deleteActualAndDiff
  createTestDb
  _ <- bracket connectToDb pgDisconnect runAllMigrations
  hspec Spec.spec
