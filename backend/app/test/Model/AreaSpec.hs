{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeApplications #-}

module Model.AreaSpec where

import CustomPrelude
import Test.Hspec
import Data.Area
import Test.QuickCheck
import Data.Aeson
import qualified Database.PostgreSQL.Typed.Range as PG
import qualified Database.PostgreSQL.Typed.Types as PG
import CommonProperties
import TestUtils
import Data.Coord

spec :: Spec
spec = do
  describe "areaName" $ do
    it "has matching fromJSON and toJSON instances" $ property (inverseJSONInstances @AreaName)

    it "can only make an area name if its text is valid" $ do
      mkAreaName "" `shouldSatisfy` isLeft
      mkAreaName "just_an-ar3a%" `shouldSatisfy` isRight

    it "filters invalid characters" $ do
      let (Right name) = mkAreaName "wo\NULrld"
      getAreaName name `shouldBe` "world"

  describe "range" $ do
    let pgTypeId = PG.PGTypeProxy :: PG.PGTypeID "int4range"
    it "has matching pgcolumn and pgparameter instances" $ property $ \(range :: Data.Area.Range any) -> do
      PG.pgDecode pgTypeId (PG.pgEncode pgTypeId range) `shouldBe` range

    it "prevents creating inverted ranges" $ property $ \(l, u) ->
      mkRange l u `shouldSatisfy` if l <= u
        then isRight
        else (==) (Left $ "Invalid Range: " <> tshow u <> " is smaller than " <> tshow l)

    it "uses min and max bounds if for some reason the postgresql side has an unbounded range or an empty range" $ do
      let rangeUnbound = PG.pgDecode pgTypeId (PG.pgEncode pgTypeId $ PG.range @Int32 PG.Unbounded PG.Unbounded)
      upper rangeUnbound `shouldBe` (maxBound - 1)
      lower rangeUnbound `shouldBe` minBound

      let rangeEmpty = PG.pgDecode pgTypeId (PG.pgEncode pgTypeId $ PG.bounded @Int32 1 0)
      upper rangeEmpty `shouldBe` (maxBound - 1)
      lower rangeEmpty `shouldBe` minBound

  describe "area" $ do
    it "has matching fromJSON and toJSON instances" $ property (inverseJSONInstances @Area)

