module Model.TileSpec (spec) where

import CustomPrelude
import Test.Hspec
import qualified Data.Tile as Tile
import Test.QuickCheck
import Data.Aeson
import CommonProperties
import Generics.Deriving (genum)

spec :: Spec
spec = do
  describe "Layer" $ do
    it "has inverse json instances" $ property (inverseJSONInstances @Tile.Layer)

  describe "Tile" $ do
    it "can be converted to an int id and back" $
      mapM_ (\tile -> (Tile.idToTile . Tile.tileToId) tile `shouldBe` Just tile) genum



