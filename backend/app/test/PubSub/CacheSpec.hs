{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE AllowAmbiguousTypes #-}

module PubSub.CacheSpec where

import CustomPrelude
import qualified Websocket.PubSub.Channel.Cache as Cache
import qualified Websocket.PubSub.Native as PubSub
import qualified Websocket.PubSub.Types as PubSub
import BaseTest
import qualified Test.QuickCheck as QC
import Logger
import LoggerBase
import qualified Katip as K
import qualified Data.Time.Clock as Clock

spec :: Spec
spec = around withPubSubAndCache $ describe "Cache" $ do
  runSpec "can wrap any computation to be called only once" $ do
    computation <- cacheComputationNoExpire "cacheKey" (+ 1) (0 :: Int)
    computation `shouldReturn` 1
    computation `shouldReturn` 1

  runSpec "can invalidate cache to let the computation be called again" $ do
    computation <- cacheComputationNoExpire "cacheKey" (+ 1) (0 :: Int)
    computation `shouldReturn` 1
    invalidation "cacheKey"
    computation `shouldReturn` 2
    computation `shouldReturn` 2

  runSpec "invalidating cache without entry does not cause a failure" $ do
    invalidation "cacheKey" `shouldReturn` ()

  runSpec "invalidates the cache via pubSub" $ do
    computation <- cacheComputationNoExpire "cacheKey" (+ 1) (0 :: Int)
    computation `shouldReturn` 1
    PubSub.publishM Cache.Channel (Cache.InvalidateMsg "cacheKey")
    computation `shouldReturn` 2

  runSpec "can store multiple cache types with seperate keys" $ do
    otherComputation <- cacheComputationNoExpire "otherKey" (<> "a") ("" :: String)
    testComputation <- cacheComputationNoExpire "cacheKey" (+ 1) (0 :: Int)
    otherComputation `shouldReturn` "a"
    testComputation `shouldReturn` 1
    invalidation "cacheKey"
    otherComputation `shouldReturn` "a"

  runSpec "does not cache after the provided expire time passed" $ do
    testComputation <- cacheComputation "cacheKey" (0 :: Clock.NominalDiffTime) (+ 1) (0 :: Int)
    testComputation `shouldReturn` 1
    testComputation `shouldReturn` 2

withPubSubAndCache :: (Ctx -> IO ()) -> IO ()
withPubSubAndCache test = testLogM $ do
  pubSub <- PubSub.createNativePubSub
  Cache.withCache pubSub (\cache -> liftIO $ test (pubSub, cache))

type Ctx = (PubSub.SomePubSub (LogM IO), Cache.InMemoryCache)

newtype TestM a = TestM {
  runTestM' :: ReaderT Ctx (LogM IO) a
} deriving newtype (Functor, Applicative, Monad, MonadIO, MonadReader Ctx, K.KatipContext, K.Katip)

instance MonadUnliftIO TestM where
  withRunInIO go = TestM (withRunInIO (\k -> go (k . runTestM')))

runLogInTest :: LogM IO a -> TestM a
runLogInTest = TestM . lift

runTestOnLogM :: Ctx -> TestM a -> LogM IO a
runTestOnLogM ctx test = runReaderT (runTestM' test) ctx

instance Cache.HasCache TestM where
  getCache = asks snd

instance PubSub.PubSubM TestM where
  publishM channel msg = do
    pubsub <- asks fst
    runLogInTest $ PubSub.publish pubsub channel msg
  subscribeM channel subscriber = do
    ctx <- ask
    runLogInTest <$> runLogInTest (PubSub.subscribe (fst ctx) channel (runTestOnLogM ctx . subscriber))

runSpec :: (HasCallStack, Example (Ctx -> IO a)) => String -> TestM a -> SpecWith (Arg (Ctx -> IO a))
runSpec testName test = it testName (\ctx -> runTestM' test |> flip runReaderT ctx |> testLogM)

invalidation :: (Cache.CacheM m) => Text -> m ()
invalidation = flip Cache.invalidateCache (return ())

cacheComputationNoExpire :: (Cache.CacheM m, Typeable a) => Text -> (a -> a) -> a -> m (m a)
cacheComputationNoExpire cacheKey =
  cacheComputation cacheKey 10000000

cacheComputation :: (Cache.CacheM m, Typeable a) => Text -> Clock.NominalDiffTime -> (a -> a) -> a -> m (m a)
cacheComputation cacheKey timeToExpire modify initial = do
  tvar <- newTVarIO initial
  pure $ Cache.useCache cacheKey timeToExpire $ atomically $ do
    modifyTVar tvar modify
    readTVar tvar