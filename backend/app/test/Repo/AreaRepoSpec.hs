module Repo.AreaRepoSpec (spec) where

import CustomPrelude
import Data.Area
import Data.Coord
import qualified Data.WithId as WithId
import qualified Db.Repo.AreaRepo as Repo
import RepoTestBase
import Test.Hspec
import Test.QuickCheck
import Test.QuickCheck.Monadic (assert)
import TestUtils
import qualified Db.Row

spec :: Spec
spec =
  describeWithDb "Repo" $ do
    dbTest "cannot create overlapping areas" $ \conn -> do
      (area1, area2) <- liftIO $ generate (twoAreasWithDifferentNames `suchThat` overlaps2d)
      res1 <- runDb conn $ Repo.createArea $ Db.Row.toRow area1
      res2 <- runDb conn $ Repo.createArea $ Db.Row.toRow area2
      res1 `shouldSatisfy` isRight
      res2 `shouldSatisfy` isLeft

    dbTest "can create non-overlapping areas" $ \conn -> do
      (area1, area2) <- liftIO $ generate (twoAreasWithDifferentNames `suchThat` (not . overlaps2d))
      res1 <- runDb conn $ Repo.createArea $ Db.Row.toRow area1
      res2 <- runDb conn $ Repo.createArea $ Db.Row.toRow area2
      res1 `shouldSatisfy` isRight
      res2 `shouldSatisfy` isRight

    dbTest "created areas get returned by getAreas" $ \conn -> do
      (area1, area2) <- liftIO $ generate (twoAreasWithDifferentNames `suchThat` (not . overlaps2d))
      void $ runDb conn $ Repo.createArea $ Db.Row.toRow area1
      void $ runDb conn $ Repo.createArea $ Db.Row.toRow area2
      [a1WithId, a2WithId] :: [AreaWithId] <- fmap Db.Row.fromRow <$> runDb conn Repo.getAreas
      WithId.theContent a1WithId `shouldBe` area1
      WithId.theContent a2WithId `shouldBe` area2

    dbTest "update area updates all fields of an area" $ \conn -> do
      (area1 :: Area, area2 :: Area) <- genArbitrary
      Right areaId <- runDb conn $ Repo.createArea $ Db.Row.toRow area1
      void $ runDb conn $ Repo.updateArea areaId $ Db.Row.toRow area2
      [a2WithId] :: [AreaWithId] <- fmap Db.Row.fromRow <$> runDb conn Repo.getAreas
      WithId.theContent a2WithId `shouldBe` area2

overlaps2d :: (Area, Area) -> Bool
overlaps2d (a1, a2) =
  overlaps (a1 ^. #ranges % #x) (a2 ^. #ranges % #x) && overlaps (a1 ^. #ranges % #y) (a2 ^. #ranges % #y)

twoAreasWithDifferentNames :: Gen (Area, Area)
twoAreasWithDifferentNames = arbitrary `suchThat` (\(a1, a2) -> a1 ^. #name /= a2 ^. #name)
