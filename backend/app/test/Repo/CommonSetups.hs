module Repo.CommonSetups (insertRandomUser, getRandomPokemon, insertRandomArea) where

import CustomPrelude
import qualified Db.Repo.PokemonRepo as PokemonRepo
import qualified Db.Repo.UserRepo as UserRepo
import qualified Db.Repo.AreaRepo as AreaRepo
import RepoTestBase
import Control.Monad.Fail (MonadFail)
import Data.Pokemon (PokemonId, PokemonNumber)
import Data.Area (AreaId, Area)
import Data.User as User
import TestUtils (genArbitrary)
import Test.QuickCheck
import qualified Db.Row
import qualified Auth

insertRandomUser :: (MonadIO m, MonadFail m) => PGConnection -> m User.UserId
insertRandomUser conn = do
  andrenaUser <- genArbitrary
  Right user <- runDb conn $ UserRepo.getUserOrCreateDefault (Auth.Andrena andrenaUser)
  pure (user ^. _1)

getRandomPokemon :: (MonadIO m, MonadFail m) => PGConnection -> m (PokemonNumber, PokemonId)
getRandomPokemon conn = do
  pokeNumber <- genArbitrary
  pokeId <- getIdForPokemonNumber conn pokeNumber
  pure (pokeNumber, pokeId)

insertRandomArea :: (MonadIO m, MonadFail m) => PGConnection -> m AreaId
insertRandomArea conn = do
  Right areaId <- (genArbitrary @Area)
    >>= runDb conn . AreaRepo.createArea . Db.Row.toRow
  pure areaId

getIdForPokemonNumber :: (MonadIO m, MonadFail m) => PGConnection -> PokemonNumber -> m PokemonId
getIdForPokemonNumber conn pokeNumber = do
  Just pokemon <- runDb conn $ PokemonRepo.findFirstWithNumber pokeNumber
  pure $ pokemon ^. _1