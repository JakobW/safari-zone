module Repo.DexEntryRepoSpec (spec) where

import CustomPrelude
import Data.Pokemon
import qualified Db.Repo.DexEntryRepo as Repo
import Has
import RepoTestBase
import Test.Hspec
import TestUtils (isLeft)
import qualified Repo.CommonSetups as Setup
import Data.WildPokemon (ShinyStatus(..))

spec :: Spec
spec =
  describeWithDb "DexEntryRepo" $ do
    dbTest "returns no dex entries for a new user" $ \conn -> do
      userId <- Setup.insertRandomUser conn
      dexEntries <- runDb conn $ Repo.getDexEntriesOfUser userId
      dexEntries `shouldSatisfy` null

    dbTest "can add pokemon to a user and retrieve them as dex entries" $ \conn -> do
      userId <- Setup.insertRandomUser conn
      (pokeNumber, pokemonId) <- Setup.getRandomPokemon conn
      _ <- runDb conn $ Repo.addPokemonToUser userId pokemonId NonShiny

      [(rPokeNumber, rAmountNormal, rAmountShiny, _, _)] <- runDb conn $ Repo.getDexEntriesOfUser userId
      rPokeNumber `shouldBe` pokeNumber
      rAmountNormal `shouldBe` 1
      rAmountShiny `shouldBe` 0

      Right detail <- runDb conn $ Repo.getDexEntryDetailOfUser userId pokemonId
      detail ^. typed `shouldBe` (1 :: AmountNormal)
      detail ^. typed `shouldBe` (0 :: AmountShiny)
      detail ^. typed `shouldBe` pokeNumber

    dbTest "adding a shiny adds that pokemon to the shiny count" $ \conn -> do
      userId <- Setup.insertRandomUser conn
      (pokeNumber, pokemonId) <- Setup.getRandomPokemon conn
      _ <- runDb conn $ Repo.addPokemonToUser userId pokemonId Shiny

      [(rPokeNumber, rAmountNormal, rAmountShiny, _, _)] <- runDb conn $ Repo.getDexEntriesOfUser userId
      rPokeNumber `shouldBe` pokeNumber
      rAmountNormal `shouldBe` 0
      rAmountShiny `shouldBe` 1

      Right detail <- runDb conn $ Repo.getDexEntryDetailOfUser userId pokemonId
      detail ^. typed `shouldBe` (0 :: AmountNormal)
      detail ^. typed `shouldBe` (1 :: AmountShiny)
      detail ^. typed `shouldBe` pokeNumber

    dbTest "adding the same pokemon twice has the expected result" $ \conn -> do
      userId <- Setup.insertRandomUser conn
      (pokeNumber, pokemonId) <- Setup.getRandomPokemon conn
      mapM_ (runDb conn . Repo.addPokemonToUser userId pokemonId) [NonShiny, Shiny, NonShiny, Shiny]

      [(rPokeNumber, rAmountNormal, rAmountShiny, _, _)] <- runDb conn $ Repo.getDexEntriesOfUser userId
      rPokeNumber `shouldBe` pokeNumber
      rAmountNormal `shouldBe` 2
      rAmountShiny `shouldBe` 2

      Right detail <- runDb conn $ Repo.getDexEntryDetailOfUser userId pokemonId
      detail ^. typed `shouldBe` (2 :: AmountNormal)
      detail ^. typed `shouldBe` (2 :: AmountShiny)
      detail ^. typed `shouldBe` pokeNumber

    dbTest "returns an error if user does not have a dex entry for the given pokemon" $ \conn -> do
      userId <- Setup.insertRandomUser conn
      (_, pokemonId) <- Setup.getRandomPokemon conn
      runDb conn (Repo.getDexEntryDetailOfUser userId pokemonId)
        >>= (`shouldSatisfy` isLeft)
