module Repo.EncounterRepoSpec where

import CustomPrelude
import Data.Encounter
import qualified Db.Repo.EncounterRepo as Repo
import qualified Db.Repo.AreaRepo as AreaRepo
import RepoTestBase
import Test.Hspec
import Test.QuickCheck
import TestUtils (genArbitrary)
import qualified Repo.CommonSetups as Setup

spec :: Spec
spec =
  describeWithDb "EncounterRepo" $ do
    dbTest "can add and get encounters for an area" $ \conn -> do
      areaId <- Setup.insertRandomArea conn
      (pokeNumber, pokeId) <- Setup.getRandomPokemon conn
      let encounter = (areaId, pokeId, 0.5 :: EncounterProbability)
      _ <- runDb conn $ Repo.upsertEncounter encounter
      [rEncounter] <- runDb conn $ Repo.getEncounters areaId
      rEncounter `shouldBe` encounter
