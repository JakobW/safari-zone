module TestUtils (propM, genArbitrary, isLeft, isRight, monadicPropWithContext) where

import CustomPrelude
import Test.QuickCheck
import Test.QuickCheck.Monadic
import Test.Hspec


monadicPropWithContext :: (Show a) => Gen a -> (c -> a -> PropertyM IO ()) -> c -> Property
monadicPropWithContext gen testCode = withMaxSuccess 10 . monadicIO . forAllM gen . testCode

propM :: (Show a) => String -> Gen a -> (c -> a -> PropertyM IO ()) -> SpecWith c
propM testName gen = it testName . monadicPropWithContext gen

genArbitrary :: (Arbitrary a, MonadIO m) => m a
genArbitrary = liftIO (generate arbitrary)

isLeft :: Either a b -> Bool
isLeft (Left _) = True
isLeft _ = False

isRight :: Either a b -> Bool
isRight = not . isLeft