module Tile.ModuloTileChooserSpec (spec) where

import Test.Hspec
import CustomPrelude
import Data.Coord
import Tile.Algo.ModuloTileChooser
import qualified Tile.TileTypes as TileTypes

data Single = Single deriving (Enum, Bounded, Show, Eq)
data TwoOptions = Opt1 | Opt2 deriving (Enum, Bounded, Show, Eq)

spec :: Spec
spec = do
     it "works for a trivial example with a single data type" $ do
       let pos = mkCoordLike 1 1
       chooseModulo [pos] `shouldMatchList` [TileTypes.groundTileData pos Single]
     it "generates all data types equally" $ do
       let tiles = [mkCoordLike x y | x <- [1 .. 2], y <- [1 .. 2]]
       chooseModulo tiles `shouldSatisfy` any ((== Opt1) . view #tile)
       chooseModulo tiles `shouldSatisfy` any ((== Opt2) . view #tile)