module Tile.NeighbourSpec (spec) where

import CustomPrelude hiding (Left, Right)
import Data.Coord
import Data.Tile.Types.Neighbour
import Test.Hspec
import Tile.Algo.Neighbour
import qualified Tile.TileTypes as TileTypes

spec :: Spec
spec = do
  it "correctly computes the tiles needed to use for a square" $ do
    let tiles = [mkCoordLike x y | x <- [1 .. 3], y <- [1 .. 3]]
    chooseBasedOnNeighbour tiles `shouldMatchList` uncurry TileTypes.groundTileData . first (uncurry mkCoordLike)
      <$> [ ((1, 1), TopLeft),
            ((1, 2), Left),
            ((1, 3), BottomLeft),
            ((2, 1), Top),
            ((2, 2), Middle),
            ((2, 3), Bottom),
            ((3, 1), TopRight),
            ((3, 2), Right),
            ((3, 3), BottomRight)
          ]

  it "correctly computes the tiles needed to use for a cross" $ do
    let tiles = [mkCoordLike x y | x <- [1 .. 6], y <- [1 .. 6], ((2 < x) && (x < 5)) || ((2 < y) && (y < 5))]
    chooseBasedOnNeighbour tiles `shouldMatchList` uncurry TileTypes.groundTileData . first (uncurry mkCoordLike)
      <$> [ ((3, 1), TopLeft),
            ((4, 1), TopRight),
            ((3, 2), Left),
            ((4, 2), Right),
            ((1, 3), TopLeft),
            ((2, 3), Top),
            ((3, 3), CurveTopLeft),
            ((4, 3), CurveTopRight),
            ((5, 3), Top),
            ((6, 3), TopRight),
            ((1, 4), BottomLeft),
            ((2, 4), Bottom),
            ((3, 4), CurveBottomLeft),
            ((4, 4), CurveBottomRight),
            ((5, 4), Bottom),
            ((6, 4), BottomRight),
            ((3, 5), Left),
            ((4, 5), Right),
            ((3, 6), BottomLeft),
            ((4, 6), BottomRight)
          ]
