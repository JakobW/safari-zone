module Tile.TileChooserSpec (spec) where

import CustomPrelude
import qualified Data.Map.Strict as Map
import Data.Coord
import Test.Hspec
import Test.QuickCheck
import Tile.TileChooser
import Data.TileType (TileType, toTileType)
import qualified Data.Tile as Tile
import qualified Tile.TileTypes as TileTypes

spec :: Spec
spec = describe "TileChooser" $ do
  it "fills at least the tiles given by the map" $
    property $
      \(mapData :: Map Position TileType) -> do
        let filledPositions = view #position <$> executeTileResolvers mapData
            givenPositions = Map.keys mapData
        mapM_ (`shouldSatisfy` (`elem` filledPositions)) givenPositions

  it "fills precisely the tiles given by the map with ground type tiles" $
    property $
      \(mapData :: Map Position TileType) -> do
        let filledPositionsOfTypeGround =
              view  #position <$> filter (not . TileTypes.isOverlay)
                (executeTileResolvers mapData)
            givenPositions = Map.keys mapData
        filledPositionsOfTypeGround `shouldMatchList` givenPositions

  it "simplifying the resulting ground tile types goes back to the input" $
    property $
      \(mapData :: Map Position TileType) -> do
        let
            toPosTileTypePair :: TileTypes.TileData' -> (Position, TileType)
            toPosTileTypePair tileData =
              (tileData ^. #position, tileData ^. #tile % to toTileType)
            posTileTypePairs = toPosTileTypePair
                <$> filter (not . TileTypes.isOverlay) (executeTileResolvers mapData)
            givenPairs = Map.assocs mapData

        posTileTypePairs `shouldMatchList` givenPairs
