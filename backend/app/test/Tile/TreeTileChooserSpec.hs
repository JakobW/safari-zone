module Tile.TreeTileChooserSpec where

import CustomPrelude
import Data.Coord
import Data.Tile.Types.ClassicTree as CT
import Data.Tile.Types.LargeTree as LT
import Test.Hspec
import Tile.Algo.TreeTileChooser
import qualified Tile.TileTypes as TileTypes

spec :: Spec
spec = do
  it "builds a classic tree with a 4 tile square" $ do
    let tiles = [mkCoordLike x y | x <- [1 .. 2], y <- [1 .. 2]]
    chooseClassic tiles
      `shouldMatchList` [ TileTypes.overlayTileData (mkCoordLike 1 0) CT.TopOverlayLeft,
                          TileTypes.overlayTileData (mkCoordLike 2 0) CT.TopOverlayRight,
                          TileTypes.groundTileData (mkCoordLike 1 1) CT.MiddleLeft,
                          TileTypes.groundTileData (mkCoordLike 2 1) CT.MiddleRight,
                          TileTypes.groundTileData (mkCoordLike 1 2) CT.BottomLeft,
                          TileTypes.groundTileData (mkCoordLike 2 2) CT.BottomRight
                        ]
  it "builds 2 overlapping classic trees with a 8 tile square" $ do
    let tiles = [mkCoordLike x y | x <- [1 .. 2], y <- [1 .. 4]]
    chooseClassic tiles
      `shouldMatchList` [ TileTypes.overlayTileData (mkCoordLike 1 0) CT.TopOverlayLeft,
                          TileTypes.overlayTileData (mkCoordLike 2 0) CT.TopOverlayRight,
                          TileTypes.groundTileData (mkCoordLike 1 1) CT.MiddleLeft,
                          TileTypes.groundTileData (mkCoordLike 2 1) CT.MiddleRight,
                          TileTypes.groundTileData (mkCoordLike 1 2) CT.BottomLeft,
                          TileTypes.groundTileData (mkCoordLike 2 2) CT.BottomRight,
                          TileTypes.overlayTileData (mkCoordLike 1 2) CT.TopOverlayLeft,
                          TileTypes.overlayTileData (mkCoordLike 2 2) CT.TopOverlayRight,
                          TileTypes.groundTileData (mkCoordLike 1 3) CT.MiddleLeft,
                          TileTypes.groundTileData (mkCoordLike 2 3) CT.MiddleRight,
                          TileTypes.groundTileData (mkCoordLike 1 4) CT.BottomLeft,
                          TileTypes.groundTileData (mkCoordLike 2 4) CT.BottomRight
                        ]
  it "manages to build a shifted classic tree besides another" $ do
    {--
      x x
      x x
        x x
        x x
    -}
    let tiles = [mkCoordLike x y | x <- [1 .. 2], y <- [1 .. 2]] ++ [mkCoordLike x y | x <- [2 .. 3], y <- [3 .. 4]]
    chooseClassic tiles
      `shouldMatchList` [ TileTypes.overlayTileData (mkCoordLike 1 0) CT.TopOverlayLeft,
                          TileTypes.overlayTileData (mkCoordLike 2 0) CT.TopOverlayRight,
                          TileTypes.groundTileData (mkCoordLike 1 1) CT.MiddleLeft,
                          TileTypes.groundTileData (mkCoordLike 2 1) CT.MiddleRight,
                          TileTypes.groundTileData (mkCoordLike 1 2) CT.BottomLeft,
                          TileTypes.groundTileData (mkCoordLike 2 2) CT.BottomRight,
                          TileTypes.overlayTileData (mkCoordLike 2 2) CT.TopOverlayLeft,
                          TileTypes.overlayTileData (mkCoordLike 3 2) CT.TopOverlayRight,
                          TileTypes.groundTileData (mkCoordLike 2 3) CT.MiddleLeft,
                          TileTypes.groundTileData (mkCoordLike 3 3) CT.MiddleRight,
                          TileTypes.groundTileData (mkCoordLike 2 4) CT.BottomLeft,
                          TileTypes.groundTileData (mkCoordLike 3 4) CT.BottomRight
                        ]
  it "builds two classic trees beside each other" $ do
    {--
      x x x x
      x x x x
    -}
    let tiles = [mkCoordLike x y | x <- [1 .. 4], y <- [1 .. 2]]
    chooseClassic tiles
      `shouldMatchList` [ TileTypes.overlayTileData (mkCoordLike 1 0) CT.TopOverlayLeft,
                          TileTypes.overlayTileData (mkCoordLike 2 0) CT.TopOverlayRight,
                          TileTypes.overlayTileData (mkCoordLike 3 0) CT.TopOverlayLeft,
                          TileTypes.overlayTileData (mkCoordLike 4 0) CT.TopOverlayRight,
                          TileTypes.groundTileData (mkCoordLike 1 1) CT.MiddleLeft,
                          TileTypes.groundTileData (mkCoordLike 2 1) CT.MiddleRight,
                          TileTypes.groundTileData (mkCoordLike 3 1) CT.MiddleLeft,
                          TileTypes.groundTileData (mkCoordLike 4 1) CT.MiddleRight,
                          TileTypes.groundTileData (mkCoordLike 1 2) CT.BottomLeft,
                          TileTypes.groundTileData (mkCoordLike 2 2) CT.BottomRight,
                          TileTypes.groundTileData (mkCoordLike 3 2) CT.BottomLeft,
                          TileTypes.groundTileData (mkCoordLike 4 2) CT.BottomRight
                        ]
  it "builds a large tree for a 3x3 area" $ do
    {--
      x x x
      x x x
      x x x
    -}
    let tiles = [mkCoordLike x y | x <- [1 .. 3], y <- [1 .. 3]]
    chooseLarge tiles
      `shouldMatchList` [ TileTypes.overlayTileData (mkCoordLike 2 0) LT.TopOverlay,
                          TileTypes.groundTileData (mkCoordLike 1 1) LT.TopLeft,
                          TileTypes.groundTileData (mkCoordLike 2 1) LT.TopMiddle,
                          TileTypes.groundTileData (mkCoordLike 3 1) LT.TopRight,
                          TileTypes.groundTileData (mkCoordLike 1 2) LT.MidLeft,
                          TileTypes.groundTileData (mkCoordLike 2 2) LT.Middle,
                          TileTypes.groundTileData (mkCoordLike 3 2) LT.MidRight,
                          TileTypes.groundTileData (mkCoordLike 1 3) LT.BottomLeft,
                          TileTypes.groundTileData (mkCoordLike 2 3) LT.BottomMiddle,
                          TileTypes.groundTileData (mkCoordLike 3 3) LT.BottomRight
                        ]
  it "builds two large trees for a 6x3 area" $ do
    {--
      x x x x x x
      x x x x x x
      x x x x x x
    -}
    let tiles = [mkCoordLike x y | x <- [1 .. 6], y <- [1 .. 3]]
    chooseLarge tiles
      `shouldMatchList` [ TileTypes.overlayTileData (mkCoordLike 2 0) LT.TopOverlay,
                          TileTypes.groundTileData (mkCoordLike 1 1) LT.TopLeft,
                          TileTypes.groundTileData (mkCoordLike 2 1) LT.TopMiddle,
                          TileTypes.groundTileData (mkCoordLike 3 1) LT.TopRight,
                          TileTypes.groundTileData (mkCoordLike 1 2) LT.MidLeft,
                          TileTypes.groundTileData (mkCoordLike 2 2) LT.Middle,
                          TileTypes.groundTileData (mkCoordLike 3 2) LT.MidRight,
                          TileTypes.groundTileData (mkCoordLike 1 3) LT.BottomLeft,
                          TileTypes.groundTileData (mkCoordLike 2 3) LT.BottomMiddle,
                          TileTypes.groundTileData (mkCoordLike 3 3) LT.BottomRight,
                          TileTypes.overlayTileData (mkCoordLike 5 0) LT.TopOverlay,
                          TileTypes.groundTileData (mkCoordLike 4 1) LT.TopLeft,
                          TileTypes.groundTileData (mkCoordLike 5 1) LT.TopMiddle,
                          TileTypes.groundTileData (mkCoordLike 6 1) LT.TopRight,
                          TileTypes.groundTileData (mkCoordLike 4 2) LT.MidLeft,
                          TileTypes.groundTileData (mkCoordLike 5 2) LT.Middle,
                          TileTypes.groundTileData (mkCoordLike 6 2) LT.MidRight,
                          TileTypes.groundTileData (mkCoordLike 4 3) LT.BottomLeft,
                          TileTypes.groundTileData (mkCoordLike 5 3) LT.BottomMiddle,
                          TileTypes.groundTileData (mkCoordLike 6 3) LT.BottomRight
                        ]
  it "builds correct trees for a more complicated setup" $ do
    {--
        x x x
        x 1 x x x x
        x x x x 2 x
      x x x   x x x
      x 3 x
      x x x
    -}
    let tiles =
          [mkCoordLike x y | x <- [1 .. 3], y <- [1 .. 3]]
            ++ [mkCoordLike x y | x <- [4 .. 6], y <- [2 .. 4]]
            ++ [mkCoordLike x y | x <- [0 .. 2], y <- [4 .. 6]]
    chooseLarge tiles
      `shouldMatchList` [
                          -- Top Tree (labeled "1")
                          TileTypes.overlayTileData (mkCoordLike 2 0) LT.TopOverlay,
                          TileTypes.groundTileData (mkCoordLike 1 1) LT.TopLeft,
                          TileTypes.groundTileData (mkCoordLike 2 1) LT.TopMiddle,
                          TileTypes.groundTileData (mkCoordLike 3 1) LT.TopRight,
                          TileTypes.groundTileData (mkCoordLike 1 2) LT.MidLeft,
                          TileTypes.groundTileData (mkCoordLike 2 2) LT.Middle,
                          TileTypes.groundTileData (mkCoordLike 3 2) LT.MidRight,
                          TileTypes.groundTileData (mkCoordLike 1 3) LT.BottomLeft,
                          TileTypes.groundTileData (mkCoordLike 2 3) LT.BottomMiddle,
                          TileTypes.groundTileData (mkCoordLike 3 3) LT.BottomRight,
                          -- Right Tree (labeled "2")
                          TileTypes.overlayTileData (mkCoordLike 5 1) LT.TopOverlay,
                          TileTypes.groundTileData (mkCoordLike 4 2) LT.TopLeft,
                          TileTypes.groundTileData (mkCoordLike 5 2) LT.TopMiddle,
                          TileTypes.groundTileData (mkCoordLike 6 2) LT.TopRight,
                          TileTypes.groundTileData (mkCoordLike 4 3) LT.MidLeft,
                          TileTypes.groundTileData (mkCoordLike 5 3) LT.Middle,
                          TileTypes.groundTileData (mkCoordLike 6 3) LT.MidRight,
                          TileTypes.groundTileData (mkCoordLike 4 4) LT.BottomLeft,
                          TileTypes.groundTileData (mkCoordLike 5 4) LT.BottomMiddle,
                          TileTypes.groundTileData (mkCoordLike 6 4) LT.BottomRight,
                          -- Bottom Tree (labeled "3")
                          TileTypes.overlayTileData (mkCoordLike 1 3) LT.TopOverlay,
                          TileTypes.groundTileData (mkCoordLike 0 4) LT.TopLeft,
                          TileTypes.groundTileData (mkCoordLike 1 4) LT.TopMiddle,
                          TileTypes.groundTileData (mkCoordLike 2 4) LT.TopRight,
                          TileTypes.groundTileData (mkCoordLike 0 5) LT.MidLeft,
                          TileTypes.groundTileData (mkCoordLike 1 5) LT.Middle,
                          TileTypes.groundTileData (mkCoordLike 2 5) LT.MidRight,
                          TileTypes.groundTileData (mkCoordLike 0 6) LT.BottomLeft,
                          TileTypes.groundTileData (mkCoordLike 1 6) LT.BottomMiddle,
                          TileTypes.groundTileData (mkCoordLike 2 6) LT.BottomRight
                        ]
