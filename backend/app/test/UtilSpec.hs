{-# LANGUAGE NegativeLiterals #-}

module UtilSpec (spec) where

import CustomPrelude
import qualified Util
import Test.Hspec
import Data.Int (Int8)
import Test.QuickCheck
import qualified Data.Set as Set
import Generic.Random

data TestType = Key1 | Key2 | Key3 deriving (Show, Eq, Ord, Enum, Bounded, Generic)
data Single = Single deriving (Show, Eq, Enum, Bounded)

instance Arbitrary TestType where
  arbitrary = genericArbitrary uniform

spec :: Spec
spec = do
    describe "enumerate" $ do
      it "works for union types with derived enum + bounded" $ do
        Util.enumerate `shouldBe` impureNonNull [Key1, Key2, Key3]

      it "works for other bounded cases" $ do
        (Util.enumerate @Int8) `shouldBe` impureNonNull ([-128 .. 127] :: [Int8])

      it "works for other bounded cases" $ do
        (Util.enumerate @Int8) `shouldBe` impureNonNull ([-128 .. 127] :: [Int8])

      it "works for single option data types" $ do
        Util.enumerate `shouldBe` impureNonNull [Single]

    describe "sortAndGroupToMap" $ do
      it "will have a map with n entries for n different keys" $ property $ \(list :: [(TestType, Int32)]) ->
        let numberOfDifferentKeys = length $ Set.fromList $ fmap fst list
        in
        length (Util.sortAndGroupToMap fst list) `shouldBe` numberOfDifferentKeys