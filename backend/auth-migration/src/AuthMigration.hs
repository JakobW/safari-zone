{-# LANGUAGE TemplateHaskell #-}

module AuthMigration (runAll, useDb, dbSettingsFromMap, runAllMigrations) where

import CustomPrelude
import qualified Data.Map.Strict as Map
import Database (DBSettings(..), compileWithDb)
import Database.PostgreSQL.Typed
import Resource (getResource, getResourceDir)
import Migration
import Language.Haskell.TH (DecsQ)

runAll :: IO ()
runAll = migrationResources >>= Migration.runInteractive dbSettingsFromMap

runAllMigrations :: PGConnection -> IO (MigrationResult String)
runAllMigrations conn =
  migrationResources >>= Migration.runInTransaction conn

migrationResources :: IO [MigrationCommand]
migrationResources =
   (<$> migrations) . modifyFilePaths . getResource <$> $(getResourceDir)

migrations :: [MigrationCommand]
migrations =
  fmap
    fromMigrationsFile
    [ "create_users"
    ]

dbSettingsFromMap :: Map String String -> DBSettings
dbSettingsFromMap m =
  DBSettings
    { dbHost = get id "AUTH_DB_HOST" "localhost",
      dbPort = get id "AUTH_DB_PORT" "5432",
      dbName = get fromString "AUTH_DB_NAME" "auth"
    }
  where
    get :: (String -> a) -> String -> a -> a
    get transform' key deflt = maybe deflt transform' (Map.lookup key m)

useDb :: DecsQ
useDb = compileWithDb dbSettingsFromMap