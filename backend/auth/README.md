#Auth

This module is a standalone web server providing authentication services.
Currently it should support jwt-based andrena authentification via the external keycloak server
and jwt-based standard authentification where user credentials are stored in the
associated postgresql database.