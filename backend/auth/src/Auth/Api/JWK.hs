module Auth.Api.JWK (jwkApi, JWKApi, getJWK) where

import Auth.Api.Types
import Auth.AuthM
import qualified Auth.Config
import qualified Crypto.JOSE as Jose
import CustomPrelude
import qualified Lens.Micro as Lens
import Servant

jwkApi :: ServerT JWKApi AuthM
jwkApi = getJWK

getJWK :: AuthM Jose.JWK
getJWK = do
  asks
    ( Auth.Config.jwkSettings
        .> (Lens.^. Jose.asPublicKey)
        .> fromMaybe (error "RSA Private Key should have private contents")
    )
