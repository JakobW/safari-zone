{-# LANGUAGE TypeOperators #-}

module Auth.Api.Login (loginApi, LoginApi, handleLogin, handleCreate) where

import Auth.AuthM
import qualified Auth.Cookie as Cookie
import Auth.Data.AccessTokenData
import Auth.Data.UserData
import Auth.Data.UserWithRole
import qualified Auth.Repo.Queries as Queries
import CustomPrelude
import Servant
import qualified Servant.Auth.Server as Auth

type LoginApi =
  ReqBody '[JSON] (UserData 'Plain)
    :> Post
         '[JSON]
         ( Headers '[Header "Set-Cookie" Auth.SetCookie] AccessTokenData
         )
    :<|> ReqBody '[JSON] (UserData 'Plain)
      :> PutCreated '[JSON] NoContent

loginApi :: ServerT LoginApi AuthM
loginApi = handleLogin :<|> handleCreate

handleLogin :: UserData 'Plain -> AuthM (Headers '[Header "Set-Cookie" Auth.SetCookie] AccessTokenData)
handleLogin user = do
  maybeUser <- headMay <$> execQuery (Queries.getByName $ username user)
  case maybeUser of
    Nothing ->
      throwIO err401
    Just (userId, userRole, hashedPass) ->
      case comparePasswords (password user) hashedPass of
        Left _ -> throwIO err401
        Right _ -> do
          let userWithRole = UserWithRole userId userRole
          tokenDataOrError <- createAccessTokenData userWithRole
          mayCookie <- Cookie.makeRefreshCookie userWithRole
          case (tokenDataOrError, mayCookie) of
            (Right tokenData, Just cookie) -> do
              (pure . addHeader cookie) tokenData
            _ -> throwIO err401

handleCreate :: UserData 'Plain -> AuthM NoContent
handleCreate user = do
  securedUser <- secureUserData user
  execQuery (Queries.createUser securedUser)
    $> NoContent
