{-# LANGUAGE TypeOperators #-}

module Auth.Api.Logout (LogoutApi, logoutApi, handleLogout) where

import Auth.AuthM
import qualified Auth.Cookie as Cookie
import Auth.Data.UserWithRole
import CustomPrelude
import Servant
import qualified Servant.Auth.Server as Auth

type LogoutApi = Auth.Auth '[Auth.Cookie] UserWithRole :> Post '[JSON] (Headers '[Header "Set-Cookie" Auth.SetCookie] NoContent)

logoutApi :: ServerT LogoutApi AuthM
logoutApi = handleLogout

handleLogout :: Auth.AuthResult UserWithRole -> AuthM (Headers '[Header "Set-Cookie" Auth.SetCookie] NoContent)
handleLogout (Auth.Authenticated user) =
  Cookie.expireCookie user
    >>= \case
      Just cookie -> pure $ addHeader cookie NoContent
      Nothing -> throwIO err401
handleLogout _ = throwIO err401
