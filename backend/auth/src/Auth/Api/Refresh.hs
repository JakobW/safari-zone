{-# LANGUAGE TypeOperators #-}

module Auth.Api.Refresh (refreshApi, RefreshApi, handleRefresh) where

import Auth.AuthM
import Auth.Data.AccessTokenData
import Auth.Data.UserWithRole
import CustomPrelude
import Servant
import qualified Servant.Auth.Server as Auth

type RefreshApi = Auth.Auth '[Auth.Cookie] UserWithRole :> Post '[JSON] AccessTokenData

refreshApi :: ServerT RefreshApi AuthM
refreshApi = handleRefresh

handleRefresh :: Auth.AuthResult UserWithRole -> AuthM AccessTokenData
handleRefresh (Auth.Authenticated user) =
  createAccessTokenData user >>= \case
    Left _ -> throwIO err401
    Right d -> pure d
handleRefresh _ = throwIO err401
