{-# LANGUAGE TypeOperators #-}

module Auth.Api.Role (RoleApi, roleApi) where

import Auth.AuthM
import Auth.Data.Role
import Auth.Data.UserId
import Auth.Data.UserWithRole
import qualified Auth.Repo.Queries as Queries
import CustomPrelude
import Servant
import qualified Servant.Auth.Server as Auth

type RoleApi =
  Auth.Auth '[Auth.JWT] UserWithRole
    :> ( ReqBody '[JSON] UserWithRole :> Post '[JSON] NoContent
           :<|> Capture "userId" UserId :> Get '[JSON] Role
       )

roleApi :: ServerT RoleApi AuthM
roleApi (Auth.Authenticated user)
  | role user == Admin =
    changeRole :<|> getRole
roleApi _ = Auth.throwAll err401

changeRole :: UserWithRole -> AuthM NoContent
changeRole =
  ($> NoContent) . execQuery . Queries.changeRole

getRole :: UserId -> AuthM Role
getRole =
  fmap (fromMaybe NormalUser . headMay) . execQuery . Queries.getRole
