{-# LANGUAGE UndecidableInstances #-}

module Auth.AuthM (AuthM, runHandlerInAuthM, runAuthM, execQuery) where

import Auth.Config
import Control.Monad.Base (MonadBase)
import Control.Monad.Except (ExceptT (..))
import Control.Monad.Trans.Control (MonadBaseControl)
import qualified Crypto.JOSE as Jose
import CustomPrelude
import qualified Database.PostgreSQL.Typed.Query as PG
import qualified Servant (Handler (..))
import qualified Servant.Auth.Server as Auth

execQuery :: (PG.PGQuery q a) => q -> AuthM [a]
execQuery q = do
  run <- asks runWithDb
  run $ liftIO . (`PG.pgQuery` q)

newtype AuthM a = AuthM
  { runAuthM' :: ReaderT AuthConfig IO a
  }
  deriving newtype
    ( Functor,
      Applicative,
      Monad,
      MonadReader AuthConfig,
      MonadIO,
      MonadThrow,
      MonadCatch,
      MonadUnliftIO,
      MonadBase IO,
      MonadBaseControl IO,
      Jose.MonadRandom
    )

runAuthM :: (MonadIO m) => AuthConfig -> AuthM a -> m a
runAuthM ctx x = liftIO $ runReaderT (runAuthM' x) ctx

runHandlerInAuthM :: AuthConfig -> AuthM a -> Servant.Handler a
runHandlerInAuthM ctx = Servant.Handler . ExceptT . try . runAuthM ctx

instance Auth.ThrowAll (AuthM a) where
  throwAll = throwIO
