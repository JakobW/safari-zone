{-# LANGUAGE RankNTypes #-}

module Auth.Config (AuthConfig (..), getJWTSettings, authCookieSettings, acquireJWK) where

import Control.Monad.Trans.Control (MonadBaseControl)
import qualified Crypto.JOSE as Jose
import Crypto.PubKey.RSA as CRSA
import CustomPrelude
import qualified Data.Map.Strict as Map
import qualified Database.PostgreSQL.Typed as PG
import qualified Servant.Auth.Server as Auth

data AuthConfig = MkAuthConfig
  { runWithDb :: forall m a. (MonadBaseControl IO m) => (PG.PGConnection -> m a) -> m a,
    jwkSettings :: Jose.JWK
  }

authJWTSettings :: Jose.JWK -> Auth.JWTSettings
authJWTSettings = Auth.defaultJWTSettings

getJWTSettings :: AuthConfig -> Auth.JWTSettings
getJWTSettings = authJWTSettings . jwkSettings

authCookieSettings :: Auth.CookieSettings
authCookieSettings =
  Auth.defaultCookieSettings
    { Auth.cookieMaxAge = Just (365 * 24 * 60 * 60 * 1000),
      Auth.cookieSameSite = Auth.SameSiteStrict,
      Auth.cookiePath = Just "/auth",
      Auth.sessionCookieName = "refresh_token",
      Auth.cookieXsrfSetting = Nothing
    }

acquireJWK :: Map String String -> IO Jose.JWK
acquireJWK env =
  Jose.fromRSA <$> case Map.lookup "AUTH_JWK_SECRET" env >>= readMay of
    Just secret -> pure secret
    Nothing -> snd <$> CRSA.generate 512 3
