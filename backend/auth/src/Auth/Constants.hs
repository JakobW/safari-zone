module Auth.Constants (jwtExpireTime) where

import CustomPrelude
import Data.Time.Clock

jwtExpireTime :: NominalDiffTime
jwtExpireTime = 15 * oneMinute

oneMinute :: NominalDiffTime
oneMinute = 60
