module Auth.Cookie (makeRefreshCookie, expireCookie) where

import Auth.AuthM
import qualified Auth.Config
import CustomPrelude
import qualified Data.Time.Calendar as Time
import qualified Data.Time.Clock as Time
import qualified Servant.Auth.Server as Auth

makeRefreshCookie :: (Auth.ToJWT a) => a -> AuthM (Maybe Auth.SetCookie)
makeRefreshCookie =
  makeCookieWithSettings Auth.Config.authCookieSettings

expireCookie :: (Auth.ToJWT a) => a -> AuthM (Maybe Auth.SetCookie)
expireCookie =
  makeCookieWithSettings expireCookieSettings

expireCookieSettings :: Auth.CookieSettings
expireCookieSettings =
  Auth.Config.authCookieSettings
    { Auth.cookieMaxAge = Just 0,
      Auth.cookieExpires = Just $ Time.UTCTime (Time.ModifiedJulianDay 5000) 0
    }

makeCookieWithSettings :: (Auth.ToJWT a) => Auth.CookieSettings -> a -> AuthM (Maybe Auth.SetCookie)
makeCookieWithSettings cookieSettings a = do
  jwtSettings <- asks Auth.Config.getJWTSettings
  liftIO $ Auth.makeSessionCookie cookieSettings jwtSettings a
