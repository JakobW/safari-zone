{-# LANGUAGE StrictData #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

module Auth.Data.AccessTokenData (AccessTokenData, createAccessTokenData, lifespan, accessToken) where

import Auth.AuthM
import qualified Auth.Config
import Auth.Constants
import Auth.Data.UserWithRole
import qualified Crypto.JOSE as Jose
import CustomPrelude
import Data.Aeson
import qualified Data.Time.Clock as Time
import qualified Servant.Auth.Server as Auth

data AccessTokenData = AccessTokenData
  { accessToken :: LText,
    lifespan :: Time.NominalDiffTime
  }
  deriving (Eq, Show, Generic, FromJSON, ToJSON)

$(makeFieldLabelsNoPrefix ''AccessTokenData)

createAccessTokenData :: UserWithRole -> AuthM (Either Jose.Error AccessTokenData)
createAccessTokenData =
  createJWT .> fmap (fmap jwtToAccessTokenData)

createJWT :: (Auth.ToJWT a) => a -> AuthM (Either Jose.Error LByteString)
createJWT toJWT = do
  settings <- asks Auth.Config.getJWTSettings
  jwtTime
    >>= ( Just
            .> Auth.makeJWT toJWT settings
            .> liftIO
        )

jwtToAccessTokenData :: LByteString -> AccessTokenData
jwtToAccessTokenData jwt =
  AccessTokenData (decodeUtf8 jwt) jwtExpireTime

jwtTime :: MonadIO m => m Time.UTCTime
jwtTime = Time.addUTCTime jwtExpireTime <$> liftIO Time.getCurrentTime
