{-# LANGUAGE StrictData #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

module Auth.Data.UserData
  ( UserData (..),
    HashStatus (..),
    secureUserData,
    comparePasswords,
    Password,
    mkPlainPassword,
    Authenticated,
  )
where

import Auth.Data.UserName
import qualified Crypto.KDF.BCrypt as BCrypt
import Crypto.Random (MonadRandom)
import CustomPrelude
import Data.Aeson
import Database.PostgreSQL.Typed.Types
import qualified Test.QuickCheck as QC
import Test.QuickCheck.Instances ()
import qualified Util
import qualified Util.Either

data HashStatus = Hashed | Plain

data Authenticated = Authenticated

data UserData (s :: HashStatus) = UserData
  { username :: UserName,
    password :: Password s
  }
  deriving (Eq, Generic)

deriving instance FromJSON (UserData 'Plain)

newtype Password (s :: HashStatus) = Password
  { unPassword :: ByteString
  }
  deriving stock (Eq)

$(makeFieldLabelsNoPrefix ''UserData)

mkPlainPassword :: ByteString -> Either Text (Password 'Plain)
mkPlainPassword pass
  | length pass < 7 = Left "Password must be at least 7 characters long."
  | length pass > 30 = Left "Password cannot be longer than 30 characters."
  | otherwise = Right $ Password pass

instance FromJSON (Password 'Plain) where
  parseJSON = parseJSON >=> encodeUtf8 .> mkPlainPassword .> Util.eitherToParser

instance PGParameter "character varying" (Password 'Hashed) where
  pgEncode typeId = unPassword .> pgEncode typeId

instance PGColumn "character varying" (Password 'Hashed) where
  pgDecode typeId = pgDecode typeId .> Password

-- The cost parameter can be between 4 and 31 inclusive, but anything less than 10 is probably not strong enough.
-- High values may be prohibitively slow depending on your hardware.
-- I tried 20 and it was incredibly slow.
securityLevel :: Int
securityLevel = 12

secureUserData :: (MonadRandom m) => UserData 'Plain -> m (UserData 'Hashed)
secureUserData user = do
  hashedPassword <- BCrypt.hashPassword securityLevel (unPassword $ password user)
  pure
    UserData
      { username = username user,
        password = Password hashedPassword
      }

comparePasswords :: Password 'Plain -> Password 'Hashed -> Either Text Authenticated
comparePasswords (Password plain) (Password hashed) = do
  isAuthenticated <- Util.Either.mapLeft pack $ BCrypt.validatePasswordEither plain hashed
  if isAuthenticated then Right Authenticated else Left "Wrong password"

instance QC.Arbitrary (Password 'Plain) where
  arbitrary = QC.arbitrary `QC.suchThatMap` (Util.eitherToMaybe . mkPlainPassword)

instance QC.Arbitrary (UserData 'Plain) where
  arbitrary = UserData <$> QC.arbitrary <*> QC.arbitrary
