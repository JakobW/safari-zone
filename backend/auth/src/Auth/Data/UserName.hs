{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Auth.Data.UserName (UserName (getUserName), mkUserName) where

import CustomPrelude
import Data.Aeson
import Database.PostgreSQL.Typed.Types (PGColumn, PGParameter)
import qualified Test.QuickCheck as QC
import qualified Util
import qualified Util.SecurityTextFilter

newtype UserName = UserName {getUserName :: Text}
  deriving stock (Show, Eq)
  deriving newtype
    ( PGParameter "text",
      PGParameter "character varying",
      PGColumn "text",
      PGColumn "character varying",
      ToJSON
    )

mkUserName :: Text -> Either Text UserName
mkUserName name
  | null name = Left "User name cannot be empty."
  | length name > 20 = Left "User name cannot be longer than 20 characters."
  | otherwise = (Right . UserName . Util.SecurityTextFilter.filterText) name

instance FromJSON UserName where
  parseJSON = withText "UserName" (Util.eitherToParser . mkUserName)

instance QC.Arbitrary UserName where
  arbitrary =
    Util.SecurityTextFilter.validTextGen
      `QC.suchThatMap` (Util.eitherToMaybe . mkUserName)
