{-# LANGUAGE TemplateHaskell #-}

module Auth.Repo.Queries (getByName, createUser, getRole, changeRole) where

import Auth.Data.Role
import Auth.Data.UserData
import Auth.Data.UserId
import Auth.Data.UserName
import Auth.Data.UserWithRole
import qualified AuthMigration
import qualified Database.PostgreSQL.Typed.Query as PG

AuthMigration.useDb

getByName :: UserName -> PG.PGSimpleQuery (UserId, Role, Password 'Hashed)
getByName userName =
  [PG.pgSQL|
    SELECT id, role, password FROM users
    WHERE username = ${userName}
  |]

createUser :: UserData 'Hashed -> PG.PGSimpleQuery ()
createUser securedUser =
  [PG.pgSQL|
    INSERT INTO users (username, password, role)
    VALUES (${username securedUser}, ${password securedUser}, ${NormalUser})
  |]

changeRole :: UserWithRole -> PG.PGSimpleQuery ()
changeRole user =
  [PG.pgSQL|
   UPDATE users
   SET role = ${role user}
   WHERE id = ${userId user}
 |]

getRole :: UserId -> PG.PGSimpleQuery Role
getRole uId =
  [PG.pgSQL|
    SELECT role FROM users
    WHERE id = ${uId}
  |]
