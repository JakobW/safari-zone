{-# LANGUAGE DataKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeOperators #-}

module Auth.Server (startApp) where

import qualified Auth.Api.JWK as Api
import qualified Auth.Api.Login as Api
import qualified Auth.Api.Logout as Api
import qualified Auth.Api.Refresh as Api
import qualified Auth.Api.Role as Api
import Auth.AuthM
import Auth.Config
import qualified AuthMigration
import Control.Monad.Trans.Control (MonadBaseControl)
import CustomPrelude hiding (Handler)
import qualified Data.Map.Strict as Map
import Data.Pool
import qualified Database
import qualified Database.PostgreSQL.Typed as PG
import Network.Wai
import Network.Wai.Handler.Warp
import Network.Wai.Logger
import Network.Wai.Middleware.Prometheus as Prometheus
import qualified Prometheus
import Prometheus.Metric.GHC as Prometheus
import Servant
import qualified Servant.Auth.Server as Auth
import System.Environment (getEnvironment)

type API =
  Api.JWKApi
    :<|> "auth"
    :> ( "login" :> Api.LoginApi
           :<|> "logout" :> Api.LogoutApi
           :<|> "refresh" :> Api.RefreshApi
           :<|> "roles" :> Api.RoleApi
       )

startApp :: IO ()
startApp = do
  _ <- Prometheus.register Prometheus.ghcMetrics
  env <- Map.fromList <$> getEnvironment
  connPool <- connDb (AuthMigration.dbSettingsFromMap env)
  _ <- withResource connPool AuthMigration.runAllMigrations
  jwk <- acquireJWK env
  let context = Auth.defaultJWTSettings jwk :. authCookieSettings :. EmptyContext
  withStdoutLogger $ \logger -> do
    let settings = setPort 8081 $ setLogger logger defaultSettings
        config = MkAuthConfig (runWithDbPool connPool) jwk
    runSettings settings $ Prometheus.prometheus Prometheus.def $ configuredServer context config

configuredServer :: Context '[Auth.JWTSettings, Auth.CookieSettings] -> AuthConfig -> Application
configuredServer ctx config =
  serveWithContext api ctx $
    hoistServerWithContext
      api
      (Proxy :: Proxy '[Auth.JWTSettings, Auth.CookieSettings])
      (runHandlerInAuthM config)
      server

api :: Proxy API
api = Proxy

server :: ServerT API AuthM
server =
  Api.jwkApi
    :<|> ( Api.loginApi
             :<|> Api.logoutApi
             :<|> Api.refreshApi
             :<|> Api.roleApi
         )

runWithDbPool :: Pool PG.PGConnection -> (forall m a. (MonadBaseControl IO m) => (PG.PGConnection -> m a) -> m a)
runWithDbPool = withResource

connDb :: Database.DBSettings -> IO (Pool PG.PGConnection)
connDb ctx =
  createPool
    (Database.connect ctx)
    Database.pgDisconnect
    2 -- stripes
    60 -- seconds that unused connection stays open
    10 -- max 10 conns per stripe
