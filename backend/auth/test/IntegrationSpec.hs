{-# LANGUAGE TypeApplications #-}

module IntegrationSpec (spec) where

import qualified Auth.Api.JWK as Api
import qualified Auth.Api.Login as Api
import qualified Auth.Api.Logout as Api
import qualified Auth.Api.Refresh as Api
import Auth.AuthM
import Auth.Config (AuthConfig)
import Auth.Data.AccessTokenData
import qualified Auth.Data.Role as Role
import Auth.Data.UserData
import Auth.Data.UserId
import Auth.Data.UserName
import Auth.Data.UserWithRole
import qualified Auth.Repo.Queries as Queries
import qualified Crypto.JWT as JWT
import CustomPrelude
import RepoTestBase
import Servant
import qualified Servant.Auth.Server as Auth
import Test.Hspec
import Test.QuickCheck
import qualified Web.Cookie as SetCookie
import GHC.TypeLits

shouldBeWellTypedHeader :: forall a s. (KnownSymbol s) => ResponseHeader s a -> IO ()
shouldBeWellTypedHeader (Header _) = pure ()
shouldBeWellTypedHeader MissingHeader = error ("Missing header: " <> symbolVal (Proxy @s))
shouldBeWellTypedHeader (UndecodableHeader _) = error ("Missing header: " <> symbolVal (Proxy @s))

spec :: Spec
spec = describeWithDb "IntegrationTest" $ do
  dbTest "can create a user and verify it with the public jwk" $ \ctx -> do
    userData <- createRandomUser ctx
    resWithHeaders <- runAuthM ctx (Api.handleLogin userData)
    shouldBeWellTypedHeader @SetCookie.SetCookie $ lookupResponseHeader @"Set-Cookie" resWithHeaders
    let
        token :: ByteString
        token = (toStrict . encodeUtf8 . accessToken . getResponse) resWithHeaders
    getResponse resWithHeaders `shouldSatisfy` ((== 900) . lifespan)
    publicJWK <- runAuthM ctx Api.getJWK
    userId <- getUserIdByName ctx (username userData)
    Auth.verifyJWT (Auth.defaultJWTSettings publicJWK) token `shouldReturn` Just (UserWithRole userId Role.NormalUser)

  dbTest "cannot create a valid token with the public jwk" $ \ctx -> do
    userData <- createRandomUser ctx
    publicJWK <- runAuthM ctx Api.getJWK
    userId <- getUserIdByName ctx (username userData)
    Auth.makeJWT (UserWithRole userId Role.Admin) (Auth.defaultJWTSettings publicJWK) Nothing
      `shouldReturn` Left (JWT.KeyMismatch "not an RSA private key")

  dbTest "can refresh the token using the cookie" $ \ctx -> do
    userData <- createRandomUser ctx
    Header cookie <- lookupResponseHeader @"Set-Cookie" <$> runAuthM ctx (Api.handleLogin userData)
    let cookieValue = SetCookie.setCookieValue cookie
    publicJWK <- runAuthM ctx Api.getJWK
    Just userWithRole <- Auth.verifyJWT (Auth.defaultJWTSettings publicJWK) cookieValue
    res <- runAuthM ctx (Api.handleRefresh (Auth.Authenticated userWithRole))
    res `shouldSatisfy` ((== 900) . lifespan)

  dbTest "logout expires the cookie" $ \ctx -> do
    userData <- createRandomUser ctx
    userId <- getUserIdByName ctx (username userData)
    let authentication = Auth.Authenticated $ UserWithRole userId Role.NormalUser
    Header cookie <- lookupResponseHeader @"Set-Cookie" <$> runAuthM ctx (Api.handleLogout authentication)
    SetCookie.setCookieMaxAge cookie `shouldBe` Just 0

getUserIdByName :: MonadIO m => AuthConfig -> UserName -> m UserId
getUserIdByName ctx =
  fmap (view _1 . unsafeHead)
    . runAuthM ctx
    . execQuery
    . Queries.getByName

createRandomUser :: MonadIO m => AuthConfig -> m (UserData 'Plain)
createRandomUser ctx = do
  userData <- liftIO (generate arbitrary)
  runAuthM ctx (Api.handleCreate userData) $> userData
