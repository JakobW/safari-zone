module Main where

import CustomPrelude
import Database (pgDisconnect)
import AuthMigration (runAllMigrations)
import RepoTestBase (connectToDb, createTestDb)
import qualified Spec
import Test.Hspec

main :: IO ()
main = do
  createTestDb
  _ <- bracket connectToDb pgDisconnect runAllMigrations
  hspec Spec.spec
