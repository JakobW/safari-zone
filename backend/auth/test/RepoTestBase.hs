{-# LANGUAGE GeneralisedNewtypeDeriving #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}

module RepoTestBase
  ( describeWithDb,
    getDbName,
    initDbNameState,
    DbNameState,
    dbTest,
    connectToDb,
    createTestDb,
    withNamedDb,
    connFromDbNameState,
  )
where

import Auth.Config
import qualified AuthMigration
import CustomPrelude
import qualified Data.Map.Strict as Map
import Data.String.Interpolate (i)
import Database (DBSettings, connectNoRetry, pgDisconnect)
import qualified Database.PostgreSQL.Typed as PG
import qualified Database.PostgreSQL.Typed.Query as PG
import System.Environment (getEnvironment)
import Test.Hspec

testDbName :: (IsString s) => s
testDbName = "test"

connectToDb :: (MonadIO m) => m PG.PGConnection
connectToDb = connectToDbWithName testDbName

connectWithSettings :: (MonadIO m) => (DBSettings -> DBSettings) -> m PG.PGConnection
connectWithSettings modifySettings = liftIO $ do
  env <- AuthMigration.dbSettingsFromMap . Map.fromList <$> getEnvironment
  connectNoRetry $ modifySettings env

connectToDbWithName :: (MonadIO m) => ByteString -> m PG.PGConnection
connectToDbWithName databaseName = connectWithSettings (set #dbName databaseName)

createTestDb :: IO ()
createTestDb = createDbWithName testDbName

createDbWithName :: String -> IO ()
createDbWithName dbName =
  bracket (connectToDbWithName "postgres") pgDisconnect tryCreateDb
  where
    dropDbQuery = PG.rawPGSimpleQuery [i|DROP DATABASE #{dbName}|]
    createDbQuery = PG.rawPGSimpleQuery [i|CREATE DATABASE #{dbName}|]
    tryCreateDb conn = do
      void (PG.pgQuery conn dropDbQuery)
        `catch` ( \(_ :: SomeException) ->
                    putStrLn [i|Could not delete Database #{dbName}|]
                )
      void (PG.pgQuery conn createDbQuery)

withNamedDb :: (MonadUnliftIO m) => ByteString -> (PG.PGConnection -> m a) -> m a
withNamedDb dbName action =
  bracket (connectToDbWithName "postgres") (liftIO . pgDisconnect) $ \mainConn -> do
    (void . liftIO) $
      PG.pgQuery mainConn $
        PG.rawPGSimpleQuery
          [i|CREATE DATABASE #{dbName} WITH TEMPLATE #{testDbName :: ByteString}|]
    bracket
      (connectToDbWithName dbName)
      ( \conn -> do
          liftIO $ pgDisconnect conn
          (void . liftIO) $ PG.pgQuery mainConn $ PG.rawPGSimpleQuery [i|DROP DATABASE #{dbName}|]
      )
      action

newtype DbParseException = ParseException [Text] deriving (Show, Typeable)

instance Exception DbParseException

data DbNameState = DbNameState
  { baseName :: String,
    counter :: TVar Int
  }

initDbNameState :: (MonadIO m) => String -> m DbNameState
initDbNameState name = do
  c <- newTVarIO 0
  pure $
    DbNameState
      { baseName = name,
        counter = c
      }

getDbName :: (MonadIO m) => DbNameState -> m ByteString
getDbName ctx = do
  currentCount <- atomically $ do
    tmp <- readTVar (counter ctx)
    modifyTVar (counter ctx) (+ 1)
    pure tmp
  (pure . encodeUtf8 . toLower . pack) $ baseName ctx <> show currentCount

describeWithDb :: String -> SpecWith DbNameState -> Spec
describeWithDb testName = beforeAll (initDbNameState testName) . describe testName

dbTest :: (HasCallStack, Example (DbNameState -> IO a)) => String -> (AuthConfig -> IO a) -> SpecWith (Arg (DbNameState -> IO a))
dbTest testName = it testName . flip (connFromDbNameState @IO)

connFromDbNameState :: (MonadIO m, MonadUnliftIO m) => DbNameState -> (AuthConfig -> m a) -> m a
connFromDbNameState nameState theTest = do
  dbName <- getDbName nameState
  jwk <- liftIO $ acquireJWK mempty
  withNamedDb dbName (\conn -> theTest (MkAuthConfig ($ conn) jwk))
