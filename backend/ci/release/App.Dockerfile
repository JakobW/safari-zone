FROM gruebel/upx:latest as compressor
COPY ci/release/backend-exe ./executable
RUN upx --best --lzma -o /executable-compressed executable

# Run exe on alpine
FROM alpine:latest
RUN apk add ca-certificates gmp
ENV RESOURCES_DIRECTORY /resources
COPY app/resources resources
COPY app-migration/resources resources
COPY --from=compressor executable-compressed /usr/local/bin/
CMD ["/usr/local/bin/executable-compressed"]