FROM gruebel/upx:latest as compressor
COPY generator-exe ./executable
RUN upx --best --lzma -o /executable-compressed executable

# Run exe on alpine
FROM alpine:latest
RUN apk add ca-certificates gmp
COPY --from=compressor executable-compressed /usr/local/bin/
CMD ["/usr/local/bin/executable-compressed"]