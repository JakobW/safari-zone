# Common

This subproject is mostly there to export a custom prelude module.

## Thought process and decisions

- The custom prelude is based on the classy-prelude package. It wraps a lot of common basic functionality surrounding different string types, number types, container types etc. with typeclass based functions. This makes the functions polymorphic and easier to use. However, sometimes explicit type annotations are necessary.
- Optics has been added here for its excellent record support. Records are one of Haskells pain points and with Optics and Overloaded Labels, its pretty awesome to work with them. Just use Template Haskell to derive the Instances for Overloaded Labels and then use #fieldName to access fields with Lens Syntax (view, set, modify).
- Flow has been added for better readability. It essentially exports .> and <. for function composition and |> and <| for function application. The arrows pointing in the correct direction to read the compositions in makes it easier to understand than "$" and ".".

