{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module Auth.Api.Types (JWKApi) where

import qualified Crypto.JOSE as Jose
import Servant

type JWKApi = "auth" :> "jwk" :> Get '[JSON] Jose.JWK
