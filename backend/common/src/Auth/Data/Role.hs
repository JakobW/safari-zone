{-# LANGUAGE StrictData #-}
{-# LANGUAGE DataKinds #-}

module Auth.Data.Role (Role(..)) where

import CustomPrelude
import qualified Database.PostgreSQL.Typed.Types as PG
import Data.Aeson
import qualified Servant.Auth.Server as Auth

data Role = Admin | NormalUser
  deriving
    ( Show,
      Eq,
      Generic,
      FromJSON,
      ToJSON,
      Auth.FromJWT,
      Auth.ToJWT
    )

instance PG.PGColumn "character varying" Role where
  pgDecode typeId = (PG.pgDecode @_ @ByteString) typeId .>
    \case
        "Admin" -> Admin
        _ -> NormalUser
    
    
instance PG.PGParameter "character varying" Role where
  pgEncode typeId =
    (PG.pgEncode @_ @ByteString) typeId <. \case
      Admin -> "Admin"
      NormalUser -> "NormalUser"