{-# LANGUAGE DataKinds #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

module Auth.Data.UserWithRole (UserWithRole (..)) where

import Auth.Data.Role
import Auth.Data.UserId
import CustomPrelude
import Data.Aeson
import qualified Servant.Auth.Server as Auth

data UserWithRole = UserWithRole
  { userId :: UserId,
    role :: Role
  }
  deriving (Eq, Show, Generic, FromJSON, ToJSON, Auth.FromJWT, Auth.ToJWT)

$(makeFieldLabelsNoPrefix ''UserWithRole)
