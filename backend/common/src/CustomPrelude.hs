module CustomPrelude (
  module ClassyPrelude,
  module Data.Scientific,
  module Control.Monad.Catch,
  uncurryN,
  module Optics,
  module Flow,
  makeFieldLabelsNoPrefix
  ) where

import ClassyPrelude hiding (catch, (<|))
import Data.Scientific (Scientific, toBoundedInteger)
import Control.Monad.Catch (catch, MonadThrow, MonadCatch)
import Data.Tuple.Curry (uncurryN)
import Optics hiding (cons, uncons, (<|), (|>), snoc, unsnoc, Index)
import Flow
import Language.Haskell.TH

makeFieldLabelsNoPrefix :: Name -> DecsQ
makeFieldLabelsNoPrefix = makeFieldLabelsWith noPrefixFieldLabels



