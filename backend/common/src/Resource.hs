{-# LANGUAGE TemplateHaskell #-}

module Resource (ResourcesDir, getResource, getResourceDir) where

import CustomPrelude
import System.Environment (lookupEnv)
import Data.FileEmbed (makeRelativeToProject)
import Language.Haskell.TH

newtype ResourcesDir = ResourcesDir String deriving (Show)

getResource :: ResourcesDir -> FilePath -> FilePath
getResource (ResourcesDir d) fp = d <> "/" <> fp

getResourceDir :: ExpQ
getResourceDir = do
  defaultResourceDir <- makeRelativeToProject "resources"
  [e| ResourcesDir . fromMaybe defaultResourceDir <$> lookupEnv "RESOURCES_DIRECTORY" |]

  --ResourcesDir . fromMaybe $(LitE . StringL <$> makeRelativeToProject "resources") <$> lookupEnv "RESOURCES_DIRECTORY"
