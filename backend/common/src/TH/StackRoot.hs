{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module TH.StackRoot (embedStackRoot) where

import CustomPrelude
import System.Directory
import System.FilePath
import Language.Haskell.TH

getStackRoot :: IO FilePath
getStackRoot = do
  dir <- getCurrentDirectory
  go dir
  where
    go fp = do
      files <- listDirectory fp
      if "stack.yaml" `elem` files
        then return fp
        else go (takeDirectory fp)
        
embedStackRoot :: ExpQ
embedStackRoot = do
  root <- runIO getStackRoot
  [| root |]