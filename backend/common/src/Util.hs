module Util
  ( enumerate,
    justEither,
    eitherToMaybe,
    sortAndGroupToMap,
    sortAndGroupWith,
    foldl1',
    diff,
    eitherToParser,
    invertMap,
  )
where

import Control.Monad.Fail (MonadFail, fail)
import CustomPrelude
import qualified Data.List.NonEmpty as NonEmpty
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set

enumerate :: forall a. (Enum a, Bounded a) => NonNull [a]
enumerate = fromNonEmpty $ minBound NonEmpty.:| 
  (if fromEnum (minBound @a) == fromEnum (maxBound @a) then [] else [succ minBound ..])

justEither :: forall a b. b -> Maybe a -> Either b a
justEither _ (Just a) = Right a
justEither err Nothing = Left err

sortAndGroupWith :: (Ord b) => (a -> b) -> [a] -> [NonEmpty.NonEmpty a]
sortAndGroupWith part = NonEmpty.groupWith part . sortOn part

foldl1' :: (a -> a -> a) -> NonEmpty.NonEmpty a -> a
foldl1' f (a NonEmpty.:| as) = foldl' f a as

sortAndGroupToMap :: (Ord b) => (a -> b) -> [a] -> Map b (NonEmpty.NonEmpty a)
sortAndGroupToMap part =
  Map.fromList
    . fmap (\a -> (part $ NonEmpty.head a, a))
    . sortAndGroupWith part

invertMap :: (Ord v) => Map k v -> Map v (NonEmpty.NonEmpty k)
invertMap =
  fmap (fmap fst) . sortAndGroupToMap snd . Map.assocs

diff :: Ord a => [a] -> [a] -> [a]
diff old new =
  Set.toList $ difference (Set.fromList new) (Set.fromList old)

eitherToParser :: MonadFail m => Either Text a -> m a
eitherToParser (Left err) = fail <| unpack err
eitherToParser (Right a) = pure a

eitherToMaybe :: Either a b -> Maybe b
eitherToMaybe = \case
  Right a -> Just a
  _ -> Nothing
