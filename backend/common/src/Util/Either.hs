module Util.Either (partition, mapLeft, whenLeft, fromRight) where

import CustomPrelude hiding (partition)

partition :: [Either a b] -> ([a], [b])
partition =  foldr go ([],[])
  where
  go (Left a) (as, bs) = (a:as, bs)
  go (Right b) (as, bs) = (as, b:bs)

mapLeft :: (a -> b) -> Either a c -> Either b c
mapLeft f = \case
  Left l -> Left $ f l
  Right r -> Right r
  
whenLeft :: Applicative m => (a -> m ()) -> Either a b -> m ()
whenLeft f = \case
  Left l -> f l
  Right _ -> pure ()

fromRight :: r -> Either l r -> r
fromRight _ (Right r) = r
fromRight r _ = r