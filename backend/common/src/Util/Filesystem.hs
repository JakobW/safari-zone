module Util.Filesystem (safeWrite) where

import CustomPrelude
import System.Directory (createDirectoryIfMissing)
import System.FilePath (takeDirectory)

safeWrite :: (FilePath -> IO a) ->  FilePath -> IO a
safeWrite write fp = createDirectoryIfMissing True (takeDirectory fp)
  >> write fp