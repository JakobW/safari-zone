module Util.QuickCheck (elements) where
  
import CustomPrelude hiding (elements)
import qualified Test.QuickCheck as QC  

elements :: NonNull [a] -> QC.Gen a
elements = QC.elements . toList
