module Util.SecurityTextFilter (validTextGen, filterText) where

import CustomPrelude
import qualified Test.QuickCheck as QC

validTextGen :: QC.Gen Text
validTextGen = pack <$> QC.listOf (QC.arbitrary `QC.suchThat` (/= '\NUL'))

filterText :: Text -> Text
filterText = filter (/= '\NUL')
