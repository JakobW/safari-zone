# Migration

This subproject is responsible for the Database Connection to an PostgreSQL
database and having schemas and seeded data on a consistent state.
Since it would be wasteful to include `postgresql-simple` with the libpq dependency
in here, we use `postgresql-typed` with its haskell hdbc backend.
The queries that do the migration are obviously not typesafe, therefore they use an escape-hatch.