{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE DataKinds #-}

module Database
  ( DBSettings(..),
    compileWithDb,
    connect,
    connectNoRetry,
    pgDisconnect,
  )
where

import qualified Control.Retry as Retry
import CustomPrelude
import Data.Has
import qualified Data.Map.Strict as Map
import Database.PostgreSQL.Typed (PGConnection, PGDatabase (..), defaultPGDatabase, pgConnect, pgDisconnect, useTPGDatabase)
import Language.Haskell.TH (DecsQ, runIO)
import System.Environment (getEnvironment)

data DBSettings = DBSettings
  { dbHost :: String,
    dbPort :: String,
    dbName :: ByteString
  }
  deriving (Show)

makeFieldLabelsNoPrefix ''DBSettings

compileWithDb :: (Map String String -> DBSettings) -> DecsQ
compileWithDb fromEnv = do
  runIO getEnvironment >>= Map.fromList .> fromEnv .> (pgDatabase .> useTPGDatabase)

pgDatabase :: (Has DBSettings c) => c -> PGDatabase
pgDatabase = pgDatabaseFromSettings . getter

pgDatabaseFromSettings :: DBSettings -> PGDatabase
pgDatabaseFromSettings settings =
  defaultPGDatabase
    { pgDBAddr = Left (settings ^. #dbHost, settings ^. #dbPort),
      pgDBName = settings ^. #dbName,
      pgDBPass = "postgres"
    }

connectNoRetry :: Has DBSettings c => c -> IO PGConnection
connectNoRetry = pgConnect . pgDatabase

connect :: (Has DBSettings c) => c -> IO PGConnection
connect = retry . connectNoRetry

retry :: IO a -> IO a
retry action = Retry.recoverAll (Retry.exponentialBackoff 50000 <> Retry.limitRetries 10) $
  \(Retry.RetryStatus tryNo _ _) -> do
    if tryNo > 0
      then putStrLn $ "Retrying to get a postgres connection... Try: " <> tshow tryNo <> ". Backing off exponentially."
      else putStrLn "Connecting to postgres..."
    action
