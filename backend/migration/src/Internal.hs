{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE NamedFieldPuns #-}

module Internal
  ( -- * Migration actions
    runMigration,
    runMigrations,
    sequenceMigrations,
    modifyFilePaths,
    rollback,
    executeNextMigration,

    -- * Migration types
    MigrationContext (..),
    MigrationCommand (..),
    MigrationResult (..),
    ScriptName,
    Checksum,
    File(..),

    -- * Migration result actions
    getMigrations,

    -- * Migration result types
    SchemaMigration (..),
  )
where

import CustomPrelude hiding (pack, putStrLn)
import qualified Crypto.Hash.MD5 as MD5 (hash)
import qualified Data.ByteString.Base64 as B64 (encode)
import qualified Data.ByteString.Char8 as Char8
import Data.Time (LocalTime)
import Database.PostgreSQL.Typed
import Database.PostgreSQL.Typed.Query (rawPGSimpleQuery)
import Database.PostgreSQL.Typed.Types (PGTypeID(PGTypeProxy), PGValue, PGValues, pgDecodeValue, unknownPGTypeEnv)
import Prelude (putStrLn)

-- | Executes migrations inside the provided 'MigrationContext'.
--
-- Returns 'MigrationSuccess' if the provided 'MigrationCommand' executes
-- without error. If an error occurs, execution is stopped and
-- a 'MigrationError' is returned.
--
-- It is recommended to wrap 'runMigration' inside a database transaction.
runMigration :: MigrationContext -> IO (MigrationResult String)
runMigration (MigrationContext cmd verbose con) = case cmd of
  MigrationFile File { name, file } ->
    executeMigration con verbose name =<< readFile file
  MigrationCommands commands ->
    runMigrations verbose con commands

-- | Execute a sequence of migrations
--
-- Returns 'MigrationSuccess' if all of the provided 'MigrationCommand's
-- execute without error. If an error occurs, execution is stopped and the
-- 'MigrationError' is returned.
--
-- It is recommended to wrap 'runMigrations' inside a database transaction.
runMigrations ::
  -- | Run in verbose mode
  Bool ->
  -- | The postgres connection to use
  PGConnection ->
  -- | The commands to run
  [MigrationCommand] ->
  IO (MigrationResult String)
runMigrations verbose con commands =
  sequenceMigrations
    (initMigrationSchema :
      [runMigration (MigrationContext c verbose con) | c <- commands])
  where
  initMigrationSchema = initializeSchema con verbose

-- | Run a sequence of contexts, stopping on the first failure
sequenceMigrations :: Monad m => [m (MigrationResult e)] -> m (MigrationResult e)
sequenceMigrations =
  foldMigrations stopOnFirstError
  where
  stopOnFirstError (MigrationError e) _ = pure (MigrationError e)
  stopOnFirstError _ next = next

foldMigrations :: forall m e . Monad m => (MigrationResult e -> m (MigrationResult e)
  -> m (MigrationResult e)) -> [m (MigrationResult e)] -> m (MigrationResult e)
foldMigrations f =
  foldl' go (pure MigrationNotExecuted)
  where
  go :: (m (MigrationResult e) -> m (MigrationResult e) -> m (MigrationResult e))
  go acc new = do
    a <- acc
    f a new

executeNextMigration :: PGConnection -> Bool -> [MigrationCommand] -> IO (MigrationResult String)
executeNextMigration con verbose commands =
  foldMigrations stopAtFirstErrorOrSuccess
    (initMigrationSchema :
      [runMigration (MigrationContext c verbose con) | c <- commands])
  where
  initMigrationSchema = initializeSchema con verbose

  stopAtFirstErrorOrSuccess (MigrationError e) _ = pure (MigrationError e)
  stopAtFirstErrorOrSuccess MigrationSuccess _ = pure MigrationSuccess
  stopAtFirstErrorOrSuccess _ next = next

execRaw :: PGConnection -> ByteString -> IO [PGValues]
execRaw con = pgQuery con . rawPGSimpleQuery

-- | Executes a generic SQL migration for the provided script 'name' with
-- content 'contents'.
executeMigration :: PGConnection -> Bool -> ScriptName -> ByteString -> IO (MigrationResult String)
executeMigration con verbose name contents = do
  let checksum = md5Hash contents
  let q = "insert into schema_migrations(filename, checksum) values('" <> Char8.pack name <> "', '" <> checksum <> "')"
  checkScript con name checksum >>= \case
    ScriptOk -> do
      when verbose $ putStrLn $ "Ok:\t" ++ name
      return MigrationNotExecuted
    ScriptNotExecuted -> do
      void $ pgQuery con contents
      void $ pgQuery con $ rawPGSimpleQuery q
      when verbose $ putStrLn $ "Execute:\t" ++ name
      return MigrationSuccess
    ScriptModified _ -> do
      when verbose $ putStrLn $ "Fail:\t" ++ name
      return (MigrationError name)

-- | Initializes the database schema with a helper table containing
-- meta-information about executed migrations.
initializeSchema :: PGConnection -> Bool -> IO (MigrationResult e)
initializeSchema con verbose = do
  when verbose $ putStrLn "Initializing schema"
  void $
    execRaw con $
      mconcat
        [ "create table if not exists schema_migrations ",
          "( filename varchar(512) not null",
          ", checksum varchar(32) not null",
          ", executed_at timestamp without time zone not null default now() ",
          ");"
        ]
  pure MigrationNotExecuted

rollback :: PGConnection -> [MigrationCommand] -> Bool -> Int -> IO (MigrationResult String)
rollback con migrations verbose rollbackSteps = do
    executedMigrationNames <- fmap schemaMigrationName <$> getMigrations con
    tryRollback (reverse executedMigrationNames) (reverse migrationFiles) rollbackSteps
    where
    tryRollback :: [ByteString] -> [File] -> Int -> IO (MigrationResult String)
    tryRollback _ _ 0 = pure MigrationSuccess
    tryRollback [] _ n = do
      putStrLn $ "WARN: Tried rolling back " <> show n <> " non-existing migrations."
      pure MigrationSuccess
    tryRollback execMigrations [] _ =
      pure $ MigrationError $ "Given MigrationCommands and executed migrations did not match: There were " <> show (length execMigrations) <> " unknown migrations."
    tryRollback (em:ems) (m:ms) n = do
      print em
      print m
      if unpack (decodeUtf8 em) == name m
      then (do
        void $ readFile (rollbackFile m) >>= executeRollback con verbose (name m)
        tryRollback ems ms (n - 1)
        )
        `catch` (\(e :: SomeException) -> pure $ MigrationError $ show e)
      else tryRollback (em:ems) ms n

    migrationFiles = foldMap flattenToFiles migrations

    flattenToFiles :: MigrationCommand -> [File]
    flattenToFiles = \case
      MigrationFile f -> [f]
      MigrationCommands cs -> foldMap flattenToFiles cs

executeRollback :: PGConnection -> Bool -> ScriptName -> ByteString -> IO ()
executeRollback con verbose scriptName contents = do
  let q = "Delete from schema_migrations where filename = '" <> Char8.pack scriptName <> "'"
  print q
  void $ execRaw con q
  void $ execRaw con contents
  when verbose $ putStrLn $ "Rollback: " <> scriptName


-- | Checks the status of the script with the given name 'name'.
-- If the script has already been executed, the checksum of the script
-- is compared against the one that was executed.
-- If there is no matching script entry in the database, the script
-- will be executed and its meta-information will be recorded.
checkScript :: PGConnection -> ScriptName -> Checksum -> IO CheckScriptResult
checkScript con name checksum =
  execRaw con (getChecksumQuery name) >>= \case
    [] ->
      return ScriptNotExecuted
    (actualChecksum : _) : _
      | checksum == decodeVarchar actualChecksum ->
        return ScriptOk
    (actualChecksum : _) : _ ->
      return (ScriptModified $ decodeVarchar actualChecksum)
    _ ->
      error "Checksum Query did not work"

decodeVarchar :: PGValue -> ByteString
decodeVarchar =
  pgDecodeValue unknownPGTypeEnv (PGTypeProxy :: PGTypeID "character varying")

getChecksumQuery :: String -> ByteString
getChecksumQuery name =
  mconcat
    [ "select checksum from schema_migrations ",
      "where filename = '",
      Char8.pack name,
      "' limit 1"
    ]

-- | Calculates the MD5 checksum of the provided bytestring in base64
-- encoding.
md5Hash :: ByteString -> Checksum
md5Hash = B64.encode . MD5.hash

-- | The checksum type of a migration script.
type Checksum = ByteString

-- | The name of a script. Typically the filename or a custom name
-- when using Haskell migrations.
type ScriptName = String

data File = File
  { name :: ScriptName, file :: FilePath, rollbackFile :: FilePath } deriving (Eq, Show, Read, Ord)

-- | 'MigrationCommand' determines the action of the 'runMigration' script.
data MigrationCommand
  = -- | Executes a migration based on script located at the provided
    -- 'FilePath'.
    MigrationFile File
  | -- | Performs a series of 'MigrationCommand's in sequence.
    MigrationCommands [MigrationCommand]
  deriving (Show, Eq, Read, Ord)

instance Semigroup MigrationCommand where
  (MigrationCommands xs) <> (MigrationCommands ys) = MigrationCommands (xs ++ ys)
  (MigrationCommands xs) <> y = MigrationCommands (xs ++ [y])
  x <> (MigrationCommands ys) = MigrationCommands (x : ys)
  x <> y =  MigrationCommands [x, y]

instance Monoid MigrationCommand where
  mempty = MigrationCommands []

-- | A sum-type denoting the result of a single migration.
data CheckScriptResult
  = -- | The script has already been executed and the checksums match.
    -- This is good.
    ScriptOk
  | -- | The script has already been executed and there is a checksum
    -- mismatch. This is bad.
    ScriptModified Checksum
  | -- | The script has not been executed, yet. This is good.
    ScriptNotExecuted
  deriving (Show, Eq, Read, Ord)

-- | A sum-type denoting the result of a migration.
data MigrationResult a
  = -- | There was an error in script migration.
    MigrationError a
  | -- | All scripts have been executed successfully.
    MigrationSuccess
  | -- | No scripts have been executed, because they were already executed
    MigrationNotExecuted
  deriving (Show, Eq, Read, Ord, Functor, Foldable, Traversable)

-- | The 'MigrationContext' provides an execution context for migrations.
data MigrationContext = MigrationContext
  { -- | The action that will be performed by 'runMigration'
    migrationContextCommand :: MigrationCommand,
    -- | Verbosity of the library.
    migrationContextVerbose :: Bool,
    -- | The PostgreSQL connection to use for migrations.
    migrationContextConnection :: PGConnection
  }

-- | Produces a list of all executed 'SchemaMigration's.
getMigrations :: PGConnection -> IO [SchemaMigration]
getMigrations conn = fmap decodeSchemaMigration <$> execRaw conn q
  where
    q =
      mconcat
        [ "select filename, checksum, executed_at ",
          "from schema_migrations order by executed_at asc"
        ]

modifyFilePaths :: (FilePath -> FilePath) -> MigrationCommand -> MigrationCommand
modifyFilePaths f = \case
  MigrationFile File { name, file, rollbackFile } -> MigrationFile File
    { name = name, file = f file, rollbackFile = f rollbackFile}
  MigrationCommands cmds -> MigrationCommands $ fmap (modifyFilePaths f) cmds


decodeTime :: PGValue -> LocalTime
decodeTime = pgDecodeValue unknownPGTypeEnv (PGTypeProxy :: PGTypeID "timestamp without time zone")

decodeSchemaMigration :: PGValues -> SchemaMigration
decodeSchemaMigration = \case
  (name:checksum:timestamp:_) ->
      SchemaMigration (decodeVarchar name) (decodeVarchar checksum) (decodeTime timestamp)
  _ ->
    error "SchemaMigration did not have the correct columns"

-- | A product type representing a single, executed 'SchemaMigration'.
data SchemaMigration = SchemaMigration
  { -- | The name of the executed migration.
    schemaMigrationName :: ByteString,
    -- | The calculated MD5 checksum of the executed script.
    schemaMigrationChecksum :: Checksum,
    -- | A timestamp without timezone of the date of execution of the script.
    schemaMigrationExecutedAt :: LocalTime
  }
  deriving (Show, Eq, Read)
