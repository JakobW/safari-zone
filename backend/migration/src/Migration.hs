module Migration (runInTransaction, runInteractive, run, fromMigrationsFile, MigrationCommand (..), MigrationResult, modifyFilePaths) where

import CustomPrelude
import qualified Data.Map.Strict as Map
import Database (DBSettings (..), connect)
import Database.PostgreSQL.Typed
import Database.PostgreSQL.Typed.TemplatePG (withTransaction)
import Internal
import System.Environment (getEnvironment)

run :: (Map String String -> DBSettings) -> [MigrationCommand] -> IO ()
run dbSettingsFromMap migrationCommands = do
  settings <- dbSettingsFromMap . Map.fromList <$> getEnvironment
  conn <- connect settings
  void $ runInTransaction conn migrationCommands

runInTransaction :: PGConnection -> [MigrationCommand] -> IO (MigrationResult String)
runInTransaction conn = withTransaction conn . runMigrations True conn

runInteractive :: (Map String String -> DBSettings) -> [MigrationCommand] -> IO ()
runInteractive dbSettingsFromMap migrationCommands = do
  settings <- dbSettingsFromMap . Map.fromList <$> getEnvironment
  args <- getArgs
  conn <- connect settings
  if "--interactive" `elem` args
    then cmdApp conn
    else void $ runInTransaction conn migrationCommands
  where
    cmdApp conn = do
      putStrLn $
        "Interactive Migration Mode. "
          <> "Press 'u' to execute the next migration, or 'd' to revert the last migration. "
          <> "Exit with 'exit'"
      input <- getLine
      case input of
        "exit" -> putStrLn "Leaving interactive migration..."
        "d" -> do
          let doRollback = rollback conn migrationCommands True 1
          withTransaction conn doRollback >>= print
          cmdApp conn
        "u" -> do
          executeNextMigration conn True migrationCommands >>= print
          cmdApp conn
        _ -> putStrLn ("Unsupported operation: " <> input) >> cmdApp conn

fromMigrationsFile :: String -> MigrationCommand
fromMigrationsFile fileName =
  MigrationFile
    File
      { name = fileName,
        file = "migrations/" <> fileName <> ".sql",
        rollbackFile = "migrations/" <> fileName <> ".revert.sql"
      }
