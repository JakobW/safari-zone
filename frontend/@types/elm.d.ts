/* eslint-disable @typescript-eslint/no-explicit-any */
interface Sub<T> {
  subscribe: (onMsg: (msg: T) => void) => void;
  unsubscribe: (onMsg: (msg: T) => void) => void;
}
interface Cmd<T> {
  send: (msg: T) => void;
}

interface Port<Out, In> {
  subscribe: Sub<Out>['subscribe'];
  send: Cmd<In>['send'];
}

interface ElmApp<T extends Record<keyof T, Cmd<any> | Sub<any> | undefined>> {
  ports: {
    [k in keyof T]: T[k];
  };
}

type ElmExport<
  PATH extends string,
  FLAGS,
  PORTS extends Record<keyof PORTS, Cmd<any> | Sub<any> | undefined>
> = PATH extends `${infer SEGMENT}.${infer REST}`
  ? { [k in SEGMENT]: ElmExport<REST, FLAGS, PORTS> }
  : {
      [k in PATH]: {
        init: (params: { flags: FLAGS }) => ElmApp<PORTS>;
      };
    };

declare namespace Elm {
  type Token = string;

  type Provider = 'andrena' | 'local';

  interface Authentification {
    provider: 'andrena' | 'local';
    token: Token;
    lifespan: number;
  }

  interface Flags {
    language: string;
    x: number;
    y: number;
    auth: Authentification;
    backendUrl?: string;
  }

  type WebsocketOut = Connect | Disconnect | Send;

  interface Connect {
    tag: 'connect';
    url: string;
    auth: Authentification;
  }

  interface Disconnect {
    tag: 'disconnect';
  }

  interface Send {
    tag: 'send';
    message: unknown;
  }

  interface Connected {
    tag: 'connected';
    data: string;
  }

  interface Message {
    tag: 'message';
    data: string;
  }

  interface Close {
    tag: 'closed';
  }

  interface Error {
    tag: 'error';
  }

  type WebsocketIn = Connected | Message | Close | Error;

  interface Ports {
    sendWebSocket: Sub<WebsocketOut>;
    receiveWebSocket: Cmd<WebsocketIn>;
  }
}

declare module '*/Main.elm' {
  const Elm: ElmExport<'Main', Elm.Flags, Elm.Ports>;
  export { Elm };
}

declare module '*/WebSocketEchoWorker.elm' {
  const Elm: ElmExport<
    'WebSocket.WebSocketEchoWorker',
    Elm.Authentification,
    Elm.Ports & { sendWsWorkerResult: Sub<string> }
  >;
  export { Elm };
}
