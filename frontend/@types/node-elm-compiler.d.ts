declare module 'node-elm-compiler' {
  interface Options {
    output: '.js';
  }

  const compileToStringSync: (file: string, options: Options) => string;
  export { compileToStringSync };
}
