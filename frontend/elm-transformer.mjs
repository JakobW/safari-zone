import elmCompiler from 'node-elm-compiler';

const render = function (file) {
  const code = elmCompiler.compileToStringSync(file, {
    output: '.js',
  });
  return code;
};

export default {
  process: function (_src, filename) {
    return render(filename);
  },
};
