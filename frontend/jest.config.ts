import type { Config } from '@jest/types';

const config: Config.InitialOptions = {
  testMatch: ['**/tests/**/*.test.[jt]s'],
  transform: { '\\.ts$': 'ts-jest', '\\.elm$': './elm-transformer.mjs' },
  testURL: 'http://localhost:9001',
  setupFilesAfterEnv: ['./tests/setupTests.ts'],
  testEnvironment: 'jsdom',
  globals: {
    'ts-jest': {
      tsconfig: 'tsconfig.test.json',
    },
  },
};

export default config;
