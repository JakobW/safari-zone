import fs from "fs";
import path from "path";
import { generateModule } from "svg2elm";
import { promisify } from "util";

const readDir = promisify(fs.readdir);
const writeFile = promisify(fs.writeFile);

interface Options {
  moduleName: string;
  svgDir: string;
  targetDir: string;
}

const inlineIcons = async ({ moduleName, svgDir, targetDir }: Options) => {
  const filePaths = (await readDir(svgDir)).map((file) =>
    path.join(svgDir, file)
  );
  const output = await generateModule(moduleName, filePaths);
  const fixedOutput = output
    .replace(/List \(Attribute msg\) -> Svg.Svg msg/g, "Icon msg")
    .concat("\n\ntype alias Icon msg = List (Attribute msg) -> Svg.Svg msg");
  const moduleNameParts = moduleName.split(".");
  const fileName = moduleNameParts[moduleNameParts.length - 1];

  await writeFile(path.join(targetDir, `${fileName}.elm`), fixedOutput, {
    encoding: "utf-8",
  });
};

const svgInlineGenerationDir = path.join(
  __dirname,
  "..",
  "src",
  "main",
  "Generated"
);

inlineIcons({
  moduleName: "Generated.Icons",
  svgDir: path.join(__dirname, "icons"),
  targetDir: svgInlineGenerationDir,
});
