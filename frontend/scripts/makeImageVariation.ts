import { RGB, RGBA } from "@jimp/core";
import Chalk from "chalk";
import Jimp from "jimp";
import Minimist from "minimist";
import * as Path from "path";

interface ColorReplacement {
  original: RGB;
  replacement: RGBA;
}

interface Args {
  path: string;
  colorReplacements: ColorReplacement[];
  name?: string;
}

const parseColorReplacement = (s: string): ColorReplacement => {
  const [orig, repl] = s.split("|");
  if (!(orig && repl)) {
    throw new Error('Need string with two components split by "|"');
  }
  const [or, og, ob] = orig.split(",");
  if (!(or !== undefined && og !== undefined && ob !== undefined)) {
    throw new Error('First part needs to be three numbers split by ","');
  }
  const [rr, rg, rb, ra] = repl.split(",");
  if (!(rr !== undefined && rg !== undefined && rb !== undefined)) {
    throw new Error(
      'Seconds part needs to be three or four numbers split by ","'
    );
  }
  const realRa = ra === undefined ? 255 : +ra;
  return {
    original: { r: +or, g: +og, b: +ob },
    replacement: { r: +rr, g: +rg, b: +rb, a: realRa },
  };
};

const parseArgsOrError = (): Args => {
  const { path, colors, name } = Minimist(process.argv.slice(2));
  console.log(colors);
  if (typeof path == "string") {
    if (typeof colors == "string") {
      return { path, colorReplacements: [parseColorReplacement(colors)], name };
    }
    if (typeof colors == "object" && colors instanceof Array) {
      return {
        path,
        colorReplacements: colors.map(parseColorReplacement),
        name,
      };
    }
    return { path, colorReplacements: [], name };
  }
  throw new Error("Args incomplete!");
};

const args = parseArgsOrError();

const basePath = Path.join(__dirname, "..", "sprites", "selected");

const destPath = Path.join(__dirname, "..", "public", "images", "trainer");

const makePath = (subPath: string) => Path.join(basePath, subPath);

interface ColorDistribution {
  color: RGB;
  amount: number;
}

const printColored = ({ color: { r, g, b }, amount }: ColorDistribution) => {
  console.log(Chalk.bgRgb(r, g, b).bold(`${r},${g},${b}|${amount}`));
};

const printColors = (baseImage: Jimp) => {
  const width = baseImage.getWidth();
  const height = baseImage.getHeight();
  const colors: number[] = [];
  for (let i = 0; i < width; i += 1) {
    for (let j = 0; j < height; j += 1) {
      const pixelColor = baseImage.getPixelColor(i, j);
      if (colors[pixelColor] === undefined) {
        colors[pixelColor] = 0;
      } else {
        colors[pixelColor] = colors[pixelColor] + 1;
      }
    }
  }
  const distribution = Object.entries(colors)
    .sort(([_c1, a1], [_c2, a2]) => a2 - a1)
    .map(([color, amount]) => ({ color: Jimp.intToRGBA(+color), amount }))
    .filter(({ color }) => color.a !== 0);
  distribution.map(printColored);
};

const go = async () => {
  const baseImage = await Jimp.read(makePath(args.path));
  if (args.colorReplacements.length === 0) {
    printColors(baseImage);
  } else {
    const colorReplacements = args.colorReplacements.map(
      ({ original, replacement }) => ({
        original: Jimp.rgbaToInt(original.r, original.g, original.b, 255),
        replacement: Jimp.rgbaToInt(
          replacement.r,
          replacement.g,
          replacement.b,
          replacement.a
        ),
      })
    );
    const width = baseImage.getWidth();
    const height = baseImage.getHeight();
    for (let i = 0; i < width; i += 1) {
      for (let j = 0; j < height; j += 1) {
        const pixelColor = baseImage.getPixelColor(i, j);
        const replacement = colorReplacements.find(
          (c) => c.original === pixelColor
        )?.replacement;
        if (typeof replacement === "number") {
          baseImage.setPixelColor(replacement, i, j);
        }
      }
    }
    await baseImage.writeAsync(Path.join(destPath, args.name || "000.png"));
  }
};

go();
