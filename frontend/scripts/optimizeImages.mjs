import fs from "fs";
import imagemin from "imagemin";
import imageminWebp from "imagemin-webp";
import path from "path";
import pkgUp from "pkg-up";
import { promisify } from "util";

const sourceDir = path.join("public", "images");
const targetDir = path.join("dist", "images");

const writeFile = promisify(fs.writeFile);
const mkDir = promisify(fs.mkdir);

(async () => {
  const rootDir = path.join(await pkgUp(), "..");
  await imagemin([path.join(rootDir, sourceDir, "**/*.{jpg,png}")], {
    plugins: [imageminWebp({ quality: 75 })],
  }).then((files) =>
    files.forEach(async (file) => {
      const source = path.parse(file.sourcePath);
      const destinationPath = path.join(
        source.dir.replace(sourceDir, targetDir),
        source.base + ".webp"
      );
      await mkDir(path.dirname(destinationPath), { recursive: true });
      await writeFile(destinationPath, file.data);
    })
  );

  console.log("Images optimized");
})();
