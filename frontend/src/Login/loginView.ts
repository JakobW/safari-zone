const storageKey = 'preferred_login';

type Token = string;

type Provider = 'andrena' | 'local';

export interface Authentification {
  provider: 'andrena' | 'local';
  token: Token;
  lifespan: number;
}

export const clearStorage = () => localStorage.removeItem(storageKey);

export const setStorage = (auth: Authentification): void =>
  localStorage.setItem(storageKey, auth.provider);

export const checkStorage = (): Provider | undefined => {
  const savedProvider = localStorage.getItem(storageKey);
  if (savedProvider === 'andrena' || savedProvider === 'local') {
    return savedProvider;
  }
  return undefined;
};

// expected form: #something=value&else=other...
const hashToMap = (hash: string): Record<string, string> =>
  Object.fromEntries(
    hash
      .slice(1)
      .split('&')
      .map((pair) => pair.split('='))
  );

const parseInt = (val: string): number | undefined => {
  const parsed = Number.parseInt(val);
  return Number.isNaN(parsed) ? undefined : parsed;
};

export const extractToken = (hash: string): Authentification | undefined => {
  const { expires_in, access_token } = hashToMap(hash);
  if (!access_token) {
    return undefined;
  }
  return {
    token: access_token,
    provider: 'andrena',
    lifespan: parseInt(expires_in) || 900,
  };
};

export const andrenaLoginHandler = async (): Promise<void> => {
  const { default: Keycloak } = await import('keycloak-js');
  const keycloak = Keycloak({
    realm: 'test',
    clientId: 'testclient',
    url: 'https://auth.apps.andrena.de/auth/',
  });
  await keycloak.init({
    onLoad: 'login-required',
    flow: 'implicit',
  });
};

interface TokenData {
  accessToken: Token;
  lifespan: number;
}

interface Credentials {
  username: string;
  password: string;
}

const validateObject = (json: unknown): json is Record<string, unknown> =>
  typeof json === 'object' && json !== null;

const validateTokenData = (json: unknown): json is TokenData =>
  validateObject(json) &&
  typeof json.accessToken === 'string' &&
  typeof json.lifespan === 'number';

const tokenDataToAuthentification = ({
  accessToken,
  lifespan,
}: TokenData): Authentification => ({
  token: accessToken,
  provider: 'local',
  lifespan,
});

const handleJsonResponse = (res: Response): unknown => {
  if (res.status >= 200 && res.status < 300) {
    return res.json() as Promise<TokenData>;
  }
  if (res.status === 401) {
    throw new Error('Wrong username/password');
  }
  throw new Error(`Received error code ${res.status} from server`);
};

export const localLoginHandler = async (
  credentials: Credentials
): Promise<Authentification> =>
  fetch('/auth/login', {
    method: 'POST',
    body: JSON.stringify(credentials),
    headers: { 'Content-Type': 'application/json' },
  })
    .then(handleJsonResponse)
    .then((json) => {
      if (validateTokenData(json)) {
        return tokenDataToAuthentification(json);
      }
      throw new Error(
        `Expected object with keys accessToken, lifespan but received ${json}`
      );
    });

export const createLocalAccount = async (
  credentials: Credentials
): Promise<void> => {
  await fetch('/auth/login', {
    method: 'PUT',
    body: JSON.stringify(credentials),
    headers: { 'Content-Type': 'application/json' },
  }).then(handleJsonResponse);
};

export const refreshLocalToken = async (): Promise<
  Authentification | undefined
> =>
  fetch('/auth/refresh', {
    method: 'POST',
  })
    .then((res) => res.json())
    .then(tokenDataToAuthentification)
    .catch((_err) => undefined);
