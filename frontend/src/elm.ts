import { Authentification } from './Login/loginView';

export interface Flags {
  language: string;
  x: number;
  y: number;
  auth: Authentification;
  backendUrl?: string;
}

export interface App {
  ports: {
    sendWebSocket: SubWrapper<WebsocketOut>;
    receiveWebSocket: CmdWrapper<WebsocketIn>;
  };
}

export type WebsocketOut = Connect | Disconnect | Send;

interface Connect {
  tag: 'connect';
  url: string;
  auth: Authentification;
}

interface Disconnect {
  tag: 'disconnect';
}

interface Send {
  tag: 'send';
  message: unknown;
}

interface Connected {
  tag: 'connected';
  data: string;
}

interface Message {
  tag: 'message';
  data: string;
}

interface Close {
  tag: 'closed';
}

interface Error {
  tag: 'error';
}

export type WebsocketIn = Connected | Message | Close | Error;

export type SessionStorageIn = { key: string; value: string };

export type SessionStorageOut =
  | { tag: 'get'; key: string }
  | { tag: 'set'; key: string; value: string };

export interface Elm {
  Main: {
    init: (args: { flags: Flags }) => App;
  };
}

export interface SubWrapper<Out> {
  subscribe: Sub<Out>;
}
export interface CmdWrapper<Out> {
  send: Cmd<Out>;
}

export type Sub<Out> = (handler: (msgOut: Out) => void) => void;
export type Cmd<In> = (msgIn: In) => void;

export interface Port<Out, In> {
  subscribe: Sub<Out>;
  send: Cmd<In>;
}
