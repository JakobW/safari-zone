import App from './Login/LoginForm.svelte';
import {
  Authentification,
  checkStorage,
  extractToken,
  refreshLocalToken,
} from './Login/loginView';
import { startSPA } from './start';

const startTileManager = (auth: Authentification) =>
  import(/* webpackChunkName: "tile-manager"*/ './tile-manager/src/index').then(
    ({ default: tileManager }) => tileManager(auth)
  );

const init = async () => {
  let token = extractToken(window.location.hash);
  const preferredLoginProvider = checkStorage();
  if (!token && preferredLoginProvider === 'local') {
    token = await refreshLocalToken();
  }
  if (token) {
    history.replaceState(
      '',
      document.title,
      window.location.pathname + window.location.search
    );
    if (window.location.pathname === '/tile-manager') {
      startTileManager(token);
    } else {
      startSPA(token);
    }
  } else {
    new App({ target: document.body });
  }
};

init();
