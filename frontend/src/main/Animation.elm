module Animation exposing (..)

import Animator
import Browser exposing (Document)
import Html exposing (Html)
import Shared.Types.Generic exposing (Lift, Setter)
import Time


type alias State =
    { navProgress : Float, isNavOpen : Bool }


type alias Model a =
    { a | timeline : Animator.Timeline State }


init : Animator.Timeline State
init =
    Animator.init { navProgress = 0, isNavOpen = False }


subscriptions : (Time.Posix -> msg) -> Model a -> Sub msg
subscriptions callback model =
    animator
        |> Animator.toSubscription callback model


animator : Animator.Animator (Model a)
animator =
    Animator.animator
        |> Animator.watching .timeline (\timeline model -> { model | timeline = timeline })


liftAnimation : Lift (Animator.Timeline State) (Model a)
liftAnimation f m =
    { m | timeline = f m.timeline }


toggleNav : Setter (Animator.Timeline State)
toggleNav state =
    let
        currentState =
            Animator.current state
    in
    if currentState.isNavOpen then
        Animator.go Animator.verySlowly { currentState | navProgress = 0, isNavOpen = False } state

    else
        Animator.go Animator.verySlowly { currentState | navProgress = 1, isNavOpen = True } state


onTick : Time.Posix -> Model a -> Model a
onTick newTime model =
    Animator.update newTime animator model


wrapDocument : (List (Html msg) -> List (Html msg)) -> Document msg -> Document msg
wrapDocument f doc =
    { doc | body = f doc.body }
