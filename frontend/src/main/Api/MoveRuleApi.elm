module Api.MoveRuleApi exposing (getMoveRules)

import Shared.Request exposing (AuthorizedRequest)
import Domain.MoveRule exposing (MoveRules)
import Http


getMoveRules : AuthorizedRequest Http.Error MoveRules
getMoveRules =
    Shared.Request.get "/api/tile/moveRules" Domain.MoveRule.decodeMoveRules
