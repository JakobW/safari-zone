module Api.PokemonApi exposing (getAllDexEntries, getDexEntryDetail, getPokemonData)

import Shared.Request exposing (AuthorizedRequest)
import Domain.DexEntry exposing (DexEntry)
import Domain.DexEntryDetail exposing (DexEntryDetail)
import Domain.PokemonDetail exposing (PokemonDetail)
import Http
import Json.Decode as D


getAllDexEntries : AuthorizedRequest Http.Error (List DexEntry)
getAllDexEntries =
    Shared.Request.get "/api/pokemon" (D.list Domain.DexEntry.decoder)


getDexEntryDetail : Int -> AuthorizedRequest Http.Error DexEntryDetail
getDexEntryDetail number =
    Shared.Request.get ("/api/pokemon/user/" ++ String.fromInt number) Domain.DexEntryDetail.decoder


getPokemonData : Int -> AuthorizedRequest Http.Error PokemonDetail
getPokemonData number =
    Shared.Request.get ("/api/pokemon/" ++ String.fromInt number) Domain.PokemonDetail.decoder
