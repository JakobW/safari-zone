module Api.Textures exposing (..)

import Api.PokemonApi
import Config
import Domain.Coordinate exposing (Coordinate)
import Domain.PokemonDetail
import Http
import Json.Decode as D
import Json.Decode.Pipeline as D
import Math.Vector2 exposing (Vec2, vec2)
import Shared.Request exposing (AuthorizedRequest)
import Task
import WebGL.Texture as Texture exposing (Texture, nonPowerOfTwoOptions)


type SpriteMap
    = SpriteMap Texture (Coordinate Int)


type alias Textures =
    { tileSet : Texture
    , tileMaps : List Texture
    , trainer : SpriteMap
    }


type alias TextureMetadata =
    { atlasUrl : String
    , atlasTileSize : Int
    , atlasWidth : Int
    , tileMapUrls : List String
    }


textureMetadataDecoder : D.Decoder TextureMetadata
textureMetadataDecoder =
    D.succeed TextureMetadata
        |> D.required "atlasUrl" D.string
        |> D.required "atlasTileSize" D.int
        |> D.required "atlasWidth" D.int
        |> D.required "tileMapUrls" (D.list D.string)


getTextureMetadata : AuthorizedRequest Http.Error TextureMetadata
getTextureMetadata =
    Shared.Request.get "/api/images" textureMetadataDecoder


type Error
    = TextureError Texture.Error
    | HttpError Http.Error


errorToString : Error -> String
errorToString e =
    case e of
        HttpError _ ->
            "Http Error"

        TextureError _ ->
            "Texture Error"


loadTextures : AuthorizedRequest Error Textures
loadTextures =
    getTextureMetadata
        |> Shared.Request.mapError HttpError
        |> Shared.Request.andThenTask loadTexturesTask


loadTexturesTask : TextureMetadata -> Task.Task Error Textures
loadTexturesTask { atlasUrl, tileMapUrls } =
    [ Texture.loadWith { nonPowerOfTwoOptions | magnify = Texture.nearest, minify = Texture.nearest } atlasUrl
    , Texture.loadWith Texture.nonPowerOfTwoOptions "/images/overworldash.png"
    ]
        ++ List.map (Texture.loadWith Texture.nonPowerOfTwoOptions) tileMapUrls
        |> Task.sequence
        |> Task.andThen
            (\textures ->
                case textures of
                    tileSet :: trainer :: tileMaps ->
                        Task.succeed
                            { tileSet = tileSet
                            , trainer = SpriteMap trainer Config.trainerSpriteSize
                            , tileMaps = tileMaps
                            }

                    _ ->
                        Task.fail Texture.LoadError
            )
        |> Task.mapError TextureError


indexToTextureData : Int -> SpriteMap -> { row : Int, col : Int, spritePercent : Vec2, texture : Texture }
indexToTextureData index (SpriteMap texture spriteSize) =
    let
        ( x, y ) =
            Texture.size texture

        cols =
            x // spriteSize.x

        col =
            modBy cols index

        row =
            floor (toFloat index / toFloat cols)

        spritePercentX =
            toFloat spriteSize.x / toFloat x

        spritePercentY =
            toFloat spriteSize.y / toFloat y
    in
    { row = row
    , col = col
    , spritePercent = vec2 spritePercentX spritePercentY
    , texture = texture
    }


loadPokemonTextureTask : Int -> AuthorizedRequest Error ( Domain.PokemonDetail.PokemonDetail, Texture )
loadPokemonTextureTask pokemonId =
    Api.PokemonApi.getPokemonData pokemonId
        |> Shared.Request.mapError HttpError
        |> Shared.Request.andThenTask
            (\detail ->
                loadPokemonTextureByName detail.imageName
                    |> Task.map (Tuple.pair detail)
            )


loadPokemonTextureByName : String -> Task.Task Error Texture
loadPokemonTextureByName imageName =
    "/images/overworld/"
        ++ imageName
        ++ ".png"
        |> Texture.loadWith Texture.nonPowerOfTwoOptions
        |> Task.mapError TextureError
