module Api.WebsocketApi exposing (..)

import Domain.Direction exposing (Direction)
import Domain.Encounter exposing (PokemonChanged)
import Domain.Player exposing (PlayerChanged)
import Domain.PokemonEncounter exposing (PokemonEncounter)
import Json.Decode as D
import Json.Decode.Pipeline as D
import Json.Encode as E
import Ports.WebSocket exposing (Connection)


type WebsocketIn
    = MoveMessage Direction
    | TurnMessage Turn
    | EncounterMessage Int


type Turn
    = Ball
    | Bait
    | Rock


sendMove : Connection -> Direction -> Cmd msg
sendMove conn =
    MoveMessage
        >> encode
        >> Ports.WebSocket.sendMessage conn


sendEncounterTurn : Connection -> Turn -> Cmd msg
sendEncounterTurn conn =
    TurnMessage
        >> encode
        >> Ports.WebSocket.sendMessage conn


sendEncounter : Connection -> Int -> Cmd msg
sendEncounter conn =
    EncounterMessage >> encode >> Ports.WebSocket.sendMessage conn


encode : WebsocketIn -> E.Value
encode webIn =
    case webIn of
        MoveMessage dir ->
            E.object [ ( "tag", E.string "MoveMessage" ), ( "contents", Domain.Direction.encoder dir ) ]

        EncounterMessage encounterId ->
            E.object [ ( "tag", E.string "EncounterMessage" ), ( "contents", E.int encounterId ) ]

        TurnMessage Bait ->
            E.object [ ( "tag", E.string "BaitMessage" ) ]

        TurnMessage Ball ->
            E.object [ ( "tag", E.string "BallMessage" ) ]

        TurnMessage Rock ->
            E.object [ ( "tag", E.string "RockMessage" ) ]


type WebsocketOut
    = PokemonEncounterMessage PokemonEncounter
    | EncounterTooFarAway
    | PokemonFledMessage
    | PokemonCaughtMessage
    | PokemonBrokeOutMessage
    | PlayerMovedMessage PlayerChanged
    | PokemonChangedMessage PokemonChanged


decoder : D.Decoder WebsocketOut
decoder =
    D.field "tag" D.string
        |> D.andThen
            (\tag ->
                case tag of
                    "PokemonCaughtMessage" ->
                        D.succeed PokemonCaughtMessage

                    "PokemonEncounterMessage" ->
                        D.succeed PokemonEncounterMessage
                            |> D.required "contents" Domain.PokemonEncounter.decoder

                    "PokemonFledMessage" ->
                        D.succeed PokemonFledMessage

                    "PokemonBrokeOutMessage" ->
                        D.succeed PokemonBrokeOutMessage

                    "PlayerMovedMessage" ->
                        D.succeed PlayerMovedMessage
                            |> D.required "contents" Domain.Player.decoder

                    "PokemonMessage" ->
                        D.succeed PokemonChangedMessage
                            |> D.required "contents" Domain.Encounter.decoder

                    "EncounterTooFarAway" ->
                        D.succeed EncounterTooFarAway

                    _ ->
                        D.fail <| "Failed to parse tag " ++ tag
            )
