module Config exposing (..)

import Domain.Coordinate exposing (Coordinate)
import Domain.Hitbox exposing (Hitbox)


tileSize : number
tileSize =
    64


trainerSpriteSize : Coordinate Int
trainerSpriteSize =
    { x = 68, y = 72 }


trainerSize : Coordinate number
trainerSize =
    { x = 90, y = 90 }


pokemonSpriteSize : Coordinate number
pokemonSpriteSize =
    { x = 32, y = 32 }


pokemonSize : Coordinate number
pokemonSize =
    { x = 90, y = 90 }


trainerRelativeHitbox : Hitbox
trainerRelativeHitbox =
    { left = 0.2
    , right = 0.2
    , bottom = 0.3
    , top = 0.3
    }


trainerSpeed : Float
trainerSpeed =
    0.3



-- less is faster here


trainerAnimationSpeed : number
trainerAnimationSpeed =
    10
