module Data.Frame exposing (..)

import Config
import Fuzz exposing (Fuzzer)


type Frame
    = Frame Float


fuzzMax1Tile : Fuzzer Float
fuzzMax1Tile =
    Fuzz.floatRange 0 Config.tileSize
