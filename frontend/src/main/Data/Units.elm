module Data.Units exposing (..)


type TileUnit
    = TileUnit Float


type WebGLUnit
    = WebGLUnit Float


unTileUnit : TileUnit -> Float
unTileUnit (TileUnit t) =
    t


unWebGLUnit : WebGLUnit -> Float
unWebGLUnit (WebGLUnit w) =
    w
