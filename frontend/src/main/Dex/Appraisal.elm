module Dex.Appraisal exposing (..)

import Api.PokemonApi
import Dex.Appraisal.Types.Model as Model
import Dex.Appraisal.Types.Msg as Msg exposing (Msg(..))
import Dex.Appraisal.Update exposing (update)
import Dex.Appraisal.View exposing (view)
import Main.SubApp exposing (SubApp)
import Shared.Request
import Shared.Session exposing (Session)


type alias Model =
    Model.Model


type alias Msg =
    Msg.Msg


app : SubApp () Msg Model Session
app =
    { init =
        \_ s ->
            ( Model.init s
            , Api.PokemonApi.getAllDexEntries
                |> Shared.Request.send GotDex s
            )
    , update = update
    , view = view
    , getSession = Model.getSession
    , setSession = Model.setSession
    , subscriptions = \_ -> Sub.none
    }
