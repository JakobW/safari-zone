module Dex.Appraisal.Types.Model exposing (..)

import Shared.Session exposing (Session, defaultGetSession, defaultModifySession, defaultSetSession)
import Shared.Types.Generic exposing (Lift, Setter)


type alias Model =
    { session : Session }


init : Session -> Model
init session =
    { session = session }


getSession : Model -> Session
getSession =
    defaultGetSession


setSession : Session -> Setter Model
setSession =
    defaultSetSession


modifySession : Lift Session Model
modifySession =
    defaultModifySession
