module Dex.Appraisal.Types.Msg exposing (..)
import Http
import Domain.DexEntry exposing (DexEntry)

type Msg = 
  GotDex (Result Http.Error (List DexEntry))
  | GoBack