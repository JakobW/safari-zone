module Dex.Appraisal.Update exposing (..)

import Dex.Appraisal.Types.Model as Model exposing (Model)
import Dex.Appraisal.Types.Msg exposing (Msg(..))
import Shared.Effect exposing (Effect(..))
import Shared.Session as Session


update : Msg -> Model -> ( Model, Effect Msg )
update msg model =
    case msg of
        GotDex (Err _) ->
            ( model, NoEffect )

        GotDex (Ok dexEntries) ->
            ( (Model.modifySession <| Session.setDexEntries dexEntries) model, NoEffect )

        GoBack ->
            ( model, Shared.Effect.goBack )
