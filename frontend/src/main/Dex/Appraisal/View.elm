module Dex.Appraisal.View exposing (view)

import Shared.View.Colors
import Dex.Appraisal.Types.Model exposing (Model)
import Dex.Appraisal.Types.Msg exposing (Msg(..))
import Dex.View.ModalHeadline exposing (modalHeadline)
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Html.Attributes
import Shared.I18n as I18n
import Shared.Session exposing (Session)
import Shared.Types.Document exposing (Document)
import Shared.View.Button as Button
import Svg
import Svg.Attributes


view : Model -> Document Msg
view { session } =
    { title = "Appraisal", body = dexCompletionInfoModal session }


dexCompletionInfoModal : Session -> Element Msg
dexCompletionInfoModal session =
    case Element.classifyDevice session |> .orientation of
        Element.Portrait ->
            Element.column [ width fill, padding 10, spacing 10 ]
                [ Button.back GoBack
                , professorImage
                , modalHeadline <| I18n.t .dexAppraisalHeadline session.i18n
                , speechBubble <| dexCompletionText session
                ]

        Element.Landscape ->
            Element.row [ width fill, padding 10 ]
                [ professorImage
                , Element.column [ width <| fillPortion 2, spacing 10, alignTop ]
                    [ Button.back GoBack
                    , modalHeadline <| I18n.t .dexAppraisalHeadline session.i18n
                    , speechBubble <| dexCompletionText session
                    ]
                ]


triangle : Element msg
triangle =
    Element.html <|
        Svg.svg [ Html.Attributes.width 20, Html.Attributes.height 20, Svg.Attributes.viewBox "0 0 100 100" ]
            [ Svg.polygon [ Svg.Attributes.fill <| Shared.View.Colors.toHex Shared.View.Colors.white, Svg.Attributes.points "0,0 100,0, 100,100" ] []
            ]


professorImage : Element msg
professorImage =
    Element.image
        [ width fill ]
        { src = "/images/professorOak.png", description = "Professor Oak" }


speechBubble : String -> Element msg
speechBubble text =
    Element.paragraph
        [ padding 20
        , Border.rounded 20
        , Background.color Shared.View.Colors.white
        , Element.behindContent <| Element.el [ alignLeft, moveLeft 20, moveDown 20 ] triangle
        ]
        [ Element.text text ]


dexCompletionText : Session -> String
dexCompletionText { i18n, dexEntries } =
    let
        number =
            List.length dexEntries

        i18nKey =
            if number < 10 then
                .dexAppraisal0

            else if number < 30 then
                .dexAppraisal1

            else if number < 60 then
                .dexAppraisal2

            else if number < 100 then
                .dexAppraisal3

            else if number < 151 then
                .dexAppraisal4

            else
                .dexAppraisal5
    in
    I18n.t i18nKey i18n
