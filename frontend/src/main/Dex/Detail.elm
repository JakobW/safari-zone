module Dex.Detail exposing (..)

import Api.PokemonApi
import Dex.Detail.Types.Model as Model
import Dex.Detail.Types.Msg as Msg exposing (Msg(..))
import Dex.Detail.Update exposing (update)
import Dex.Detail.View exposing (view)
import Main.SubApp exposing (SubApp)
import Shared.Request
import Shared.Session exposing (Session)


type alias Model =
    Model.Model


type alias Msg =
    Msg.Msg


app : SubApp Int Msg Model Session
app =
    { init =
        \id s ->
            ( Model.init id s
            , Api.PokemonApi.getDexEntryDetail id
                |> Shared.Request.send GotDexEntryDetail s
            )
    , update = update
    , view = view
    , getSession = Model.getSession
    , setSession = Model.setSession
    , subscriptions = \_ -> Sub.none
    }
