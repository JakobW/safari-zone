module Dex.Detail.Types.Model exposing (..)

import Domain.DexEntryDetail exposing (DexEntryDetail)
import Http
import Shared.ErrorHandling exposing (WithLoadAndError(..))
import Shared.Session exposing (Session, defaultGetSession, defaultSetSession)
import Shared.Types.Generic exposing (Setter)


type alias Model =
    { session : Session
    , state : WithLoadAndError Http.Error DexEntryDetail
    }


init : Int -> Session -> Model
init _ session =
    { session = session
    , state = Loading
    }


getSession : Model -> Session
getSession =
    defaultGetSession


setSession : Session -> Setter Model
setSession =
    defaultSetSession
