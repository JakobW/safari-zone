module Dex.Detail.Types.Msg exposing (..)

import Domain.DexEntryDetail exposing (DexEntryDetail)
import Http


type Msg
    = CloseDetail
    | GotDexEntryDetail (Result Http.Error DexEntryDetail)
