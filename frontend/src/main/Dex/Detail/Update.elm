module Dex.Detail.Update exposing (..)

import Dex.Detail.Types.Model exposing (Model)
import Dex.Detail.Types.Msg exposing (Msg(..))
import Shared.Effect exposing (Effect(..))
import Shared.ErrorHandling exposing (WithLoadAndError(..))
import Route


update : Msg -> Model -> ( Model, Effect Msg )
update msg model =
    case msg of
        CloseDetail ->
            ( model, Route.navigate Route.DexOverview )

        GotDexEntryDetail (Ok detail) ->
            ( { model | state = Success detail }, NoEffect )

        GotDexEntryDetail (Err err) ->
            ( { model | state = Error err }, NoEffect )
