module Dex.Detail.View exposing (..)

import Shared.View.Colors
import Dex.Detail.Types.Model exposing (Model)
import Dex.Detail.Types.Msg exposing (Msg(..))
import Dex.Overview.View as Overview
import Dex.View.ModalHeadline as Modal
import Domain.DexEntryDetail exposing (DexEntryDetail)
import Domain.Type exposing (Type)
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Shared.ErrorHandling as ErrorHandling exposing (WithLoadAndError(..))
import Shared.Session exposing (W_Viewport)
import Shared.Types.Document exposing (Document)
import Shared.View.Button as Button
import Shared.View.PokemonImage as PokemonImage


view : Model -> Document Msg
view { state, session } =
    { title = ErrorHandling.title (.pokemon >> .name >> (++) "Dex: ") state
    , body =
        Element.el [ Element.behindContent (Overview.mainContent Nothing session), centerX, centerY, width fill, height fill, clip ] <|
            case state of
                Loading ->
                    Element.text "Loading..."

                Success detail ->
                    showDexEntryDetail session detail

                Error err ->
                    Element.el
                        [ width fill
                        , height fill
                        , padding 10
                        , Background.color Shared.View.Colors.lightGray
                        ]
                    <|
                        ErrorHandling.viewHttp err
    }


showDexEntryDetail : W_Viewport a -> DexEntryDetail -> Element Msg
showDexEntryDetail deps { amountNormal, amountShiny, pokemon } =
    let
        types =
            case pokemon.secondaryType of
                Just t ->
                    [ pokemon.primaryType, t ]

                Nothing ->
                    [ pokemon.primaryType ]

        images =
            [ PokemonImage.showImage [ width fill ]
                pokemon
                (if amountNormal > 0 then
                    PokemonImage.Normal

                 else
                    PokemonImage.Missing
                )
            , PokemonImage.showImage [ width fill ]
                pokemon
                (if amountShiny > 0 then
                    PokemonImage.Shiny

                 else
                    PokemonImage.Missing
                )
            ]

        outerElementStyling =
            [ height fill, padding 10, Background.color Shared.View.Colors.white, Border.rounded 15 ]

        textualContents =
            [ Modal.modalHeadline pokemon.name
            , Element.row [ centerX, spacing 10 ] <|
                List.map presentType types
            , Element.row [ width fill ]
                [ Element.column [ spacing 10, width fill, centerX ]
                    [ Element.el [ centerX ] <| Element.text "Height"
                    , Element.el [ Font.color Shared.View.Colors.darkBlue, centerX ] <| Element.text <| String.fromFloat pokemon.height ++ "m"
                    ]
                , Element.column [ spacing 10, width fill, centerX ]
                    [ Element.el [ centerX ] <| Element.text "Weight"
                    , Element.el [ Font.color Shared.View.Colors.darkBlue, centerX ] <| Element.text <| String.fromFloat pokemon.weight ++ "kg"
                    ]
                ]
            , Element.paragraph [ centerX, padding 10 ] [ Element.text pokemon.description ]
            , Element.row [ width fill ]
                [ Element.el [ width fill, Font.center ] <| Element.text <| "Caught: " ++ (String.fromInt <| amountNormal + amountShiny)
                , Element.el [ width fill, Font.center ] <| Element.text <| "Shinies: " ++ String.fromInt amountShiny
                ]
            ]
    in
    Element.el
        [ width (fill |> Element.maximum 800)
        , centerX
        , centerY
        , padding 10
        , Background.gradient { angle = 0, steps = List.map Shared.View.Colors.fromType types }
        ]
    <|
        case Element.classifyDevice deps |> .orientation of
            Element.Portrait ->
                Element.column outerElementStyling
                    [ Button.back CloseDetail
                    , Element.row [ width fill ] images
                    , Element.column [ width fill, Element.spacing 10 ] textualContents
                    ]

            Element.Landscape ->
                Element.row
                    outerElementStyling
                    [ Element.column [ width (Element.fillPortion deps.height) ] images
                    , Element.column
                        [ width (Element.fillPortion (2 * deps.width))
                        , Element.spacing 10
                        ]
                        (Button.back CloseDetail :: textualContents)
                    ]


presentType : Type -> Element msg
presentType t =
    Element.el [ padding 10, Border.width 2, Border.color <| Shared.View.Colors.fromType t, Border.rounded 15 ] <|
        Element.text <|
            Domain.Type.toString t
