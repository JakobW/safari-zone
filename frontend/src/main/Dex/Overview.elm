module Dex.Overview exposing (..)

import Api.PokemonApi
import Dex.Overview.Types.Model as Model
import Dex.Overview.Types.Msg as Msg
import Dex.Overview.Update exposing (update)
import Dex.Overview.View exposing (view)
import Main.SubApp exposing (SubApp)
import Shared.Request
import Shared.Session exposing (Session)


type alias Model =
    Model.Model


type alias Msg =
    Msg.Msg


app : SubApp () Msg Model Session
app =
    { init = \_ s -> ( Model.init s, Api.PokemonApi.getAllDexEntries |> Shared.Request.send Msg.GotDex s )
    , update = update
    , view = view
    , getSession = Model.getSession
    , setSession = Model.setSession
    , subscriptions = \_ -> Sub.none
    }
