module Dex.Overview.Types.Model exposing (..)

import Shared.Session exposing (Session, defaultGetSession, defaultModifySession, defaultSetSession)
import Shared.Types.Generic exposing (Lift, Setter)


type State
    = ShowPokedex
    | ShowAppraisal


initialState : State
initialState =
    ShowPokedex


type alias Model =
    { session : Session }


getSession : Model -> Session
getSession =
    defaultGetSession


setSession : Session -> Setter Model
setSession =
    defaultSetSession


modifySession : Lift Session Model
modifySession =
    defaultModifySession


init : Session -> Model
init session =
    { session = session }
