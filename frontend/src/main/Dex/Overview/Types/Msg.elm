module Dex.Overview.Types.Msg exposing (..)

import Domain.DexEntry exposing (DexEntry)
import Http


type Msg
    = OpenAppraisal
    | GetDexEntryDetail Int
    | GotDex (Result Http.Error (List DexEntry))
