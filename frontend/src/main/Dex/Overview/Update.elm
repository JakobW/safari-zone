module Dex.Overview.Update exposing (update)

import Dex.Overview.Types.Model as Model exposing (Model)
import Dex.Overview.Types.Msg exposing (Msg(..))
import Route
import Shared.Effect exposing (Effect(..))
import Shared.Session as Session


update : Msg -> Model -> ( Model, Effect Msg )
update msg model =
    case msg of
        GetDexEntryDetail id ->
            ( model
            , Route.navigate <| Route.DexDetail id
            )

        OpenAppraisal ->
            ( model, Route.navigate Route.DexAppraisal )

        GotDex (Err _) ->
            ( Session.addErrorS "Http Error" model, NoEffect )

        GotDex (Ok dexEntries) ->
            ( (Model.modifySession <| Session.setDexEntries dexEntries) model, NoEffect )
