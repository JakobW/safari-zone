module Dex.Overview.View exposing (mainContent, view)

import Dex.Overview.Types.Model exposing (Model)
import Dex.Overview.Types.Msg exposing (Msg(..))
import Dex.Overview.View.Progressbar as Progressbar
import Domain.DexEntry exposing (DexEntry)
import Element exposing (Element, centerX, fill, height, padding, px, spacing, width)
import Element.Events as Events
import Element.Font as Font
import Element.Region as Region
import Maybe.Extra as Maybe
import Shared.Session exposing (Session, W_Viewport)
import Shared.Types.Document exposing (Document)
import Shared.View.PokemonImage as PokemonImage


title : String
title =
    "Pokédex"


view : Model -> Document Msg
view model =
    { title = title
    , body =
        mainContent (Just events) model.session
    }


type alias Events msg =
    { appraisalClick : msg
    , dexEntryClick : Int -> msg
    }


events : Events Msg
events =
    { appraisalClick = OpenAppraisal
    , dexEntryClick = GetDexEntryDetail
    }


mainContent : Maybe (Events msg) -> Session -> Element msg
mainContent mayEvents session =
    Element.column [ width fill, height fill ]
        [ pokedexGrade (Maybe.map .appraisalClick mayEvents) session session.dexEntries
        , Element.el [ responsiveWidth session.width, centerX ] <|
            PokemonImage.showPokedex
                { imageSize = ( 100, 100 )
                , pokemonAttributes =
                    \pokemon ->
                        case mayEvents of
                            Just { dexEntryClick } ->
                                [ Events.onClick <| dexEntryClick pokemon.number ]

                            Nothing ->
                                []
                , missingEntryAttributes = []
                }
                session.dexEntries
        ]


responsiveWidth : Int -> Element.Attribute msg
responsiveWidth viewPortWidth =
    (viewPortWidth // 100)
        * 100
        |> (\w ->
                if w > 700 then
                    w - 100

                else
                    w
           )
        |> px
        |> width


pokedexGrade : Maybe msg -> W_Viewport s -> List DexEntry -> Element msg
pokedexGrade onClick { width } entries =
    let
        total =
            151

        done =
            List.length entries

        barWidth =
            width * total // 200
    in
    Element.column
        ([ spacing 10
         , padding 10
         , centerX
         ]
            ++ (Maybe.map Events.onClick onClick |> Maybe.toList)
        )
        [ headline, Progressbar.progressbar { done = done, total = total, width = barWidth } ]


headline : Element msg
headline =
    Element.paragraph [ Region.heading 1, Font.center ] [ Element.text <| "Pokédex" ]
