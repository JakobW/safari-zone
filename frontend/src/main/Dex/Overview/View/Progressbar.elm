module Dex.Overview.View.Progressbar exposing (..)

import Shared.View.Colors
import Element exposing (Element, centerX, centerY, clip, px)
import Element.Background as Background
import Element.Border as Borders
import Element.Font as Font


type alias Options =
    { done : Int
    , total : Int
    , width : Int
    }


progressbar : Options -> Element msg
progressbar { done, total, width } =
    let
        completionWidth =
            px <| floor <| (toFloat done / toFloat total) * toFloat width

        height =
            px 50
    in
    Element.el
        [ centerX
        , clip
        , Element.width (px width)
        , Element.height height
        , Background.color Shared.View.Colors.darkGray
        , Borders.rounded 20
        , Element.inFront <| Element.el [ centerX, centerY, Font.color Shared.View.Colors.white ] <| completionText done total
        ]
    <|
        Element.el
            [ Element.width completionWidth
            , Element.height height
            , Background.color Shared.View.Colors.blue
            ]
        <|
            Element.text ""


completionText : Int -> Int -> Element a
completionText done total =
    Element.text <| String.fromInt done ++ " / " ++ String.fromInt total