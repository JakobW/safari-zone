module Dex.View.ModalHeadline exposing (..)

import Shared.View.Colors
import Element exposing (Element, centerX, padding)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Region as Region


modalHeadline : String -> Element msg
modalHeadline text =
    Element.paragraph
        [ Region.heading 1
        , centerX
        , Background.color Shared.View.Colors.blue
        , Font.color Shared.View.Colors.white
        , padding 10
        , Font.center
        , Border.rounded 15
        ]
        [ Element.text text ]
