module Domain.ConstrainedPosition exposing
    ( ConstrainedPosition
    , addCoord
    , getCurrentDirection
    , getExact
    , hasMoved
    , init
    , initNoMove
    , interact
    , move
    , noPosition
    , shouldSendMessageAfterMove
    , updateMoveRules
    )

import Data.Units exposing (TileUnit)
import Domain.Coordinate as Coordinate exposing (Coordinate)
import Domain.Direction exposing (Direction)
import Domain.MoveRule exposing (MoveRules, canMoveFromTo)
import Domain.Position exposing (Position(..))
import Shared.Types.FrameBased exposing (current)


type ConstrainedPosition
    = ConstrainedPosition Position MoveRules
    | JustRules MoveRules
    | JustCoord (Coordinate Int)
    | NoPosition


noPosition : ConstrainedPosition
noPosition =
    NoPosition


init : MoveRules -> Coordinate Int -> ConstrainedPosition
init rules current =
    ConstrainedPosition (Domain.Position.init current) rules


initNoMove : Coordinate Int -> ConstrainedPosition
initNoMove =
    JustCoord


updateMoveRules : MoveRules -> ConstrainedPosition -> ConstrainedPosition
updateMoveRules rules p =
    case p of
        NoPosition ->
            JustRules rules

        JustRules _ ->
            JustRules rules

        JustCoord coord ->
            init rules coord

        ConstrainedPosition pos _ ->
            ConstrainedPosition pos rules


shouldSendMessageAfterMove : ConstrainedPosition -> ConstrainedPosition -> Bool
shouldSendMessageAfterMove prev curr =
    case ( prev, curr ) of
        ( ConstrainedPosition prevPos _, ConstrainedPosition currPos _ ) ->
            Domain.Position.shouldSendMessageAfterMove prevPos currPos

        _ ->
            False


getCurrentDirection : ConstrainedPosition -> Maybe Direction
getCurrentDirection p =
    case p of
        ConstrainedPosition pos _ ->
            Just <| Domain.Position.getCurrentDirection pos

        _ ->
            Nothing


move : Maybe Direction -> Float -> ConstrainedPosition -> ConstrainedPosition
move dir dt p =
    case p of
        ConstrainedPosition pos rules ->
            case ( pos, dir ) of
                ( Moving _ _, _ ) ->
                    ConstrainedPosition (Domain.Position.move dir dt pos) rules

                ( OnTile currentCoord _, Just newDir ) ->
                    let
                        constrainedDt =
                            if canMoveFromTo currentCoord (Domain.Direction.directionToCoord newDir |> Coordinate.plus currentCoord) rules then
                                dt

                            else
                                0
                    in
                    ConstrainedPosition (Domain.Position.move dir constrainedDt pos) rules

                ( OnTile _ _, Nothing ) ->
                    p

        _ ->
            p


hasMoved : ConstrainedPosition -> ConstrainedPosition -> Bool
hasMoved p1 p2 =
    case ( p1, p2 ) of
        ( ConstrainedPosition pos1 _, ConstrainedPosition pos2 _ ) ->
            Domain.Position.hasMoved pos1 pos2

        _ ->
            False


addCoord : Coordinate Int -> ConstrainedPosition -> ConstrainedPosition
addCoord c p =
    case p of
        NoPosition ->
            JustCoord c

        JustCoord _ ->
            JustCoord c

        ConstrainedPosition _ rules ->
            init rules c

        JustRules rules ->
            init rules c


getExact : ConstrainedPosition -> Maybe (Coordinate TileUnit)
getExact p =
    case p of
        ConstrainedPosition current _ ->
            Just <| Domain.Position.getExact current

        _ ->
            Nothing


interact : ConstrainedPosition -> Maybe (Coordinate Int)
interact p =
    case p of
        ConstrainedPosition pos _ ->
            Domain.Position.interact pos

        _ ->
            Nothing
