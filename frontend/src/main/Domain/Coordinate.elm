module Domain.Coordinate exposing (..)

import Json.Decode as D
import Json.Decode.Pipeline as D
import Json.Encode as E
import Basics
import Fuzz


type alias Coordinate number =
    { x : number
    , y : number
    }


minus : Coordinate number -> Coordinate number -> Coordinate number
minus c1 c2 =
    { x = c1.x - c2.x, y = c1.y - c2.y }


plus : Coordinate number -> Coordinate number -> Coordinate number
plus c1 c2 =
    { x = c1.x + c2.x, y = c1.y + c2.y }


times : number -> Coordinate number -> Coordinate number
times n c1 =
    { x = c1.x * n, y = c1.y * n }


map : (a -> b) -> Coordinate a -> Coordinate b
map f { x, y } =
    { x = f x, y = f y }

toFloat : Coordinate Int -> Coordinate Float
toFloat = map Basics.toFloat

divide : Coordinate Float -> Float -> Coordinate Float
divide { x, y } d =
    { x = x / d, y = y / d }


toTuple : Coordinate number -> ( number, number )
toTuple { x, y } =
    ( x, y )


decoder : D.Decoder (Coordinate Int)
decoder =
    D.succeed Coordinate
        |> D.required "x" D.int
        |> D.required "y" D.int


encoder : Coordinate Float -> E.Value
encoder { x, y } =
    E.object [ ( "x", E.float x ), ( "y", E.float y ) ]

fuzzer : Fuzz.Fuzzer (Coordinate Int)
fuzzer = Fuzz.map2 Coordinate Fuzz.int Fuzz.int    
