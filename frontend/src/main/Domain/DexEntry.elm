module Domain.DexEntry exposing (..)

import Json.Decode as D
import Json.Decode.Pipeline as D


type alias DexEntry =
    { number : Int
    , name : String
    , imageName : String
    , amountNormal : Int
    , amountShiny : Int
    }


decoder : D.Decoder DexEntry
decoder =
    D.succeed DexEntry
        |> D.required "number" D.int
        |> D.required "pokemonName" D.string
        |> D.required "imageName" D.string
        |> D.required "amountNormal" D.int
        |> D.required "amountShiny" D.int
