module Domain.DexEntryDetail exposing (..)

import Domain.PokemonDetail exposing (PokemonDetail)
import Json.Decode as D
import Json.Decode.Pipeline as D


type alias DexEntryDetail =
    { amountNormal : Int
    , amountShiny : Int
    , pokemon : PokemonDetail
    }


decoder : D.Decoder DexEntryDetail
decoder =
    D.succeed DexEntryDetail
        |> D.required "amountNormal" D.int
        |> D.required "amountShiny" D.int
        |> D.required "pokemon" Domain.PokemonDetail.decoder
