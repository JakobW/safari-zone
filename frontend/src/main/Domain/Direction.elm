module Domain.Direction exposing (..)

import Domain.Coordinate exposing (Coordinate)
import Fuzz
import Json.Decode as D
import Json.Encode as E


type Direction
    = L
    | R
    | U
    | D


default : Direction
default =
    D


all : List Direction
all =
    [ L, R, U, D ]


directionToCoord : Direction -> Coordinate number
directionToCoord dir =
    case dir of
        L ->
            { x = -1, y = 0 }

        R ->
            { x = 1, y = 0 }

        U ->
            { x = 0, y = -1 }

        D ->
            { x = 0, y = 1 }


coordToDirection : Coordinate Int -> Maybe Direction
coordToDirection { x, y } =
    -- workaround, you cannot match on negative ints :/
    case ( x + 1, y + 1 ) of
        ( 0, 1 ) ->
            Just L

        ( 2, 1 ) ->
            Just R

        ( 1, 0 ) ->
            Just U

        ( 1, 2 ) ->
            Just D

        _ ->
            Nothing


decoder : D.Decoder Direction
decoder =
    D.string
        |> D.andThen
            (\str ->
                case str of
                    "L" ->
                        D.succeed L

                    "R" ->
                        D.succeed R

                    "U" ->
                        D.succeed U

                    "D" ->
                        D.succeed D

                    _ ->
                        D.fail <| "Expected one of L, R, U, D but was: " ++ str
            )


encoder : Direction -> E.Value
encoder dir =
    E.string <|
        case dir of
            L ->
                "L"

            R ->
                "R"

            U ->
                "U"

            D ->
                "D"


fuzzer : Fuzz.Fuzzer Direction
fuzzer =
    Fuzz.oneOf <| List.map Fuzz.constant all
