module Domain.Encounter exposing (..)

import Config
import Shared.Types.FrameBased exposing (FrameBased)
import Data.Units exposing (unTileUnit)
import Dict exposing (Dict)
import Domain.Coordinate exposing (Coordinate)
import Domain.Direction as Direction exposing (Direction)
import Domain.QueueablePosition exposing (QueueablePosition)
import Json.Decode as D
import Json.Decode.Pipeline as D


type alias Encounter =
    { position : QueueablePosition
    , encounterId : Int
    , pokemonId : Int
    , shinyStatus : Bool
    , action : Action
    , direction : Direction
    }


type alias Action =
    FrameBased CurrentAction


type CurrentAction
    = StepLeft
    | StepRight


rotate : CurrentAction -> CurrentAction
rotate a =
    case a of
        StepLeft ->
            StepRight

        StepRight ->
            StepLeft


type alias NewEncounter =
    { position : Coordinate Int
    , encounterId : Int
    , pokemonId : Int
    , shinyStatus : Bool
    }


type alias MoveEncounter =
    { encounterId : Int
    , position : Coordinate Int
    }


type PokemonChanged
    = MkNewEncounter NewEncounter
    | MkMoveEncounter MoveEncounter
    | MkDeleteEncounter Int


type alias Encounters =
    Dict Int Encounter


getEncounters : Encounters -> List Encounter
getEncounters =
    Dict.values


getEncounter : Int -> Encounters -> Maybe Encounter
getEncounter encounterId =
    Dict.get encounterId


fromCoordinate : Coordinate Int -> Encounters -> Maybe Encounter
fromCoordinate coord =
    getEncounters
        >> List.filter
            (.position
                >> Domain.QueueablePosition.getExact
                >> Domain.Coordinate.map (unTileUnit >> round)
                >> (==) coord
            )
        >> List.head


handleChange : PokemonChanged -> Encounters -> Encounters
handleChange event =
    case event of
        MkNewEncounter { pokemonId, encounterId, position, shinyStatus } ->
            Dict.insert encounterId
                { position = Domain.QueueablePosition.init position
                , encounterId = encounterId
                , pokemonId = pokemonId
                , shinyStatus = shinyStatus
                , action = Shared.Types.FrameBased.initFrameBased StepLeft rotate Config.trainerAnimationSpeed
                , direction = Direction.D
                }

        MkMoveEncounter moveEncounter ->
            Dict.update moveEncounter.encounterId <|
                Maybe.map (updateEncounterWithMove moveEncounter.position)

        MkDeleteEncounter encounterId ->
            Dict.remove encounterId


continueMoves : Float -> Encounters -> Encounters
continueMoves dt =
    Dict.map (\_ enc -> { enc | position = Domain.QueueablePosition.move Nothing dt enc.position })


updateEncounterWithMove : Coordinate Int -> Encounter -> Encounter
updateEncounterWithMove coord encounter =
    let
        newPos =
            Domain.QueueablePosition.moveToCoord coord encounter.position
    in
    { encounter
        | position = newPos
        , direction =
            Domain.QueueablePosition.getCurrentDirection newPos
    }


decoder : D.Decoder PokemonChanged
decoder =
    D.field "tag" D.string
        |> D.andThen
            (\tag ->
                case tag of
                    "NewEncounterMsg" ->
                        D.field "contents" newEncounterDecoder
                            |> D.map MkNewEncounter

                    "MoveEncounterMsg" ->
                        D.field "contents" moveEncounterDecoder
                            |> D.map MkMoveEncounter

                    "RemoveEncounterMsg" ->
                        D.field "contents" D.int |> D.map MkDeleteEncounter

                    x ->
                        D.fail <| "Expected one of NewEncounterMsg, MoveEncounterMsg and RemoveEncounterMsg, but got " ++ x
            )


newEncounterDecoder : D.Decoder NewEncounter
newEncounterDecoder =
    D.succeed NewEncounter
        |> D.required "position" Domain.Coordinate.decoder
        |> D.required "encounterId" D.int
        |> D.required "pokemonId" D.int
        |> D.required "shinyStatus" shinyStatusDecoder


moveEncounterDecoder : D.Decoder MoveEncounter
moveEncounterDecoder =
    D.succeed MoveEncounter
        |> D.required "encounterId" D.int
        |> D.required "position" Domain.Coordinate.decoder


shinyStatusDecoder : D.Decoder Bool
shinyStatusDecoder =
    D.string
        |> D.map
            (\str ->
                case str of
                    "Shiny" ->
                        True

                    _ ->
                        False
            )
