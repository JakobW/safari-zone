module Domain.Hitbox exposing (..)


type alias Hitbox =
    { top : Float
    , right : Float
    , bottom : Float
    , left : Float
    }
