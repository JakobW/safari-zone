module Domain.MoveRule exposing (MoveRule(..), MoveRules, blockedMoveRules, canMoveFromTo, decodeMoveRules)

import Dict exposing (Dict)
import Domain.Coordinate as Coordinate exposing (Coordinate)
import Json.Decode as D
import Json.Decode.Pipeline as D


type MoveRule
    = Blocked
    | Ground
    | Water


type alias MoveRules =
    Dict ( Int, Int ) MoveRule


blockedMoveRules : MoveRules
blockedMoveRules =
    Dict.empty


canMoveFromTo : Coordinate Int -> Coordinate Int -> MoveRules -> Bool
canMoveFromTo c1 c2 rules =
    isAllowed (getPosition (Coordinate.toTuple c1) rules) (getPosition (Coordinate.toTuple c2) rules)


getPosition : comparable -> Dict comparable MoveRule -> MoveRule
getPosition pos =
    Dict.get pos >> Maybe.withDefault Blocked


isAllowed : MoveRule -> MoveRule -> Bool
isAllowed from to =
    case ( from, to ) of
        ( _, Blocked ) ->
            False

        ( Blocked, _ ) ->
            False

        ( Ground, Ground ) ->
            True

        ( Water, Water ) ->
            True

        ( _, Water ) ->
            False

        ( Water, Ground ) ->
            True


decodeMoveRules : D.Decoder MoveRules
decodeMoveRules =
    D.succeed Tuple.pair
        |> D.required "rules" (D.list decoder)
        |> D.required "columns" D.int
        |> D.map listTo2Dim


listTo2Dim : ( List a, Int ) -> Dict ( Int, Int ) a
listTo2Dim ( list, columns ) =
    let
        transformIndex : Int -> ( Int, Int )
        transformIndex i =
            ( modBy columns i, i // columns )
    in
    List.indexedMap (Tuple.pair << transformIndex) list
        |> Dict.fromList


decoder : D.Decoder MoveRule
decoder =
    D.int
        |> D.andThen
            (\i ->
                case i of
                    0 ->
                        D.succeed Ground

                    1 ->
                        D.succeed Blocked

                    2 ->
                        D.succeed Water

                    _ ->
                        D.fail <| "Expected number between 0 and 2"
            )
