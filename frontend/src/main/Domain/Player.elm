module Domain.Player exposing (..)

import Dict exposing (Dict)
import Domain.Coordinate exposing (Coordinate)
import Domain.QueueablePosition exposing (QueueablePosition)
import Json.Decode as D
import Json.Decode.Pipeline as D
import Shared.Types.TrainerState as TrainerState exposing (TrainerState)


type alias Player =
    { id : Int
    , position : QueueablePosition
    , state : TrainerState
    }


type alias PlayerChanged =
    { id : Int
    , position : Coordinate Int
    }


type alias Players =
    Dict Int Player


addOrMovePlayer : PlayerChanged -> Players -> Players
addOrMovePlayer { id, position } =
    Dict.update id <|
        \val ->
            case val of
                Just player ->
                    let
                        newPos =
                            Domain.QueueablePosition.moveToCoord position player.position

                        dir =
                            Domain.QueueablePosition.getCurrentDirection newPos
                    in
                    Just { player | position = newPos, state = TrainerState.changeDirection dir player.state }

                Nothing ->
                    Just { id = id, position = Domain.QueueablePosition.init position, state = TrainerState.init }


continueMoves : Float -> Players -> Players
continueMoves dt =
    Dict.map (\_ -> movePlayer dt)


movePlayer : Float -> Player -> Player
movePlayer dt player =
    let
        newPos =
            Domain.QueueablePosition.move Nothing dt player.position

        action =
            if Domain.QueueablePosition.hasMoved player.position newPos then
                TrainerState.move

            else
                TrainerState.stop

        newState =
            player.state
                |> TrainerState.changeDirection (Domain.QueueablePosition.getCurrentDirection newPos)
                |> action
    in
    { player | position = newPos, state = newState }


getPlayers : Players -> List Player
getPlayers =
    Dict.values


decoder : D.Decoder PlayerChanged
decoder =
    D.succeed PlayerChanged
        |> D.required "id" D.int
        |> D.required "position" Domain.Coordinate.decoder
