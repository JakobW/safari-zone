module Domain.PokemonDetail exposing (..)

import Domain.Type exposing (Type)
import Json.Decode as D
import Json.Decode.Pipeline as D


type alias PokemonDetail =
    { number : Int
    , name : String
    , primaryType : Type
    , secondaryType : Maybe Type
    , weight : Float
    , height : Float
    , description : String
    , imageName : String
    }


decoder : D.Decoder PokemonDetail
decoder =
    D.succeed PokemonDetail
        |> D.required "number" D.int
        |> D.required "name" D.string
        |> D.required "primaryType" Domain.Type.decoder
        |> D.required "secondaryType" (D.nullable Domain.Type.decoder)
        |> D.required "weight" D.float
        |> D.required "height" D.float
        |> D.required "description" D.string
        |> D.required "imageName" D.string
