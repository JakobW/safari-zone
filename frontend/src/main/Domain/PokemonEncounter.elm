module Domain.PokemonEncounter exposing (..)

import Json.Decode as D
import Json.Decode.Pipeline as D


type alias PokemonEncounter =
    { name : String, imageName : String }


decoder : D.Decoder PokemonEncounter
decoder =
    D.succeed PokemonEncounter
        |> D.required "name" D.string
        |> D.required "imageName" D.string
