module Domain.Position exposing
    ( Position(..)
    , getCurrentDirection
    , getExact
    , getNextTile
    , hasMoved
    , init
    , interact
    , move
    , setTile
    , shouldSendMessageAfterMove
    )

import Config
import Data.Units exposing (TileUnit(..))
import Domain.Coordinate as Coordinate exposing (Coordinate)
import Domain.Direction exposing (Direction(..))


type Position
    = OnTile (Coordinate Int) Direction
    | Moving (Coordinate Float) Direction



-- Coordinate is tile coordinate


init : Coordinate Int -> Position
init c =
    OnTile c Domain.Direction.default


floorCoord : Direction -> Coordinate Float -> Coordinate Int
floorCoord dir =
    Coordinate.map
        (case dir of
            L ->
                ceiling

            R ->
                floor

            U ->
                ceiling

            D ->
                floor
        )


ceilCoord : Direction -> Coordinate Float -> Coordinate Int
ceilCoord dir =
    Coordinate.map
        (case dir of
            L ->
                floor

            R ->
                ceiling

            U ->
                floor

            D ->
                ceiling
        )


shouldSendMessageAfterMove : Position -> Position -> Bool
shouldSendMessageAfterMove prev curr =
    case ( prev, curr ) of
        ( OnTile _ _, Moving _ _ ) ->
            True

        _ ->
            False


getCurrentDirection : Position -> Direction
getCurrentDirection pos =
    case pos of
        OnTile _ dir ->
            dir

        Moving _ dir ->
            dir


getNextTile : Position -> Coordinate Int
getNextTile pos =
    case pos of
        Moving currentPos dir ->
            ceilCoord dir currentPos

        OnTile tile _ ->
            tile


move : Maybe Direction -> Float -> Position -> Position
move maybeNewDir dt pos =
    case ( pos, maybeNewDir ) of
        ( Moving currentPos dir, _ ) ->
            let
                newPos =
                    Domain.Direction.directionToCoord dir
                        |> Coordinate.times (min (dt / Config.tileSize) 1)
                        |> Coordinate.plus currentPos
            in
            if floorCoord dir newPos == floorCoord dir currentPos then
                Moving newPos dir

            else
                OnTile (floorCoord dir newPos) dir

        ( OnTile currentPos _, Just dir ) ->
            let
                newPos =
                    Domain.Direction.directionToCoord dir
                        |> Coordinate.times (dt / Config.tileSize)
                        |> Coordinate.plus (Coordinate.toFloat currentPos)

                flooredTarget =
                    floorCoord dir newPos
            in
            if dt == 0 then
                OnTile currentPos dir

            else if flooredTarget == currentPos then
                Moving newPos dir

            else
                OnTile flooredTarget dir

        ( OnTile _ _, Nothing ) ->
            pos


hasMoved : Position -> Position -> Bool
hasMoved pos1 pos2 =
    getExact pos1 /= getExact pos2


getExact : Position -> Coordinate TileUnit
getExact pos =
    case pos of
        Moving coord _ ->
            Coordinate.map TileUnit coord

        OnTile coord _ ->
            Coordinate.map (toFloat >> TileUnit) coord


setTile : Coordinate Int -> Position -> Position
setTile coord =
    getCurrentDirection >> OnTile coord


interact : Position -> Maybe (Coordinate Int)
interact position =
    case position of
        Moving _ _ ->
            Nothing

        OnTile pos dir ->
            Domain.Direction.directionToCoord dir
                |> Coordinate.plus pos
                |> Just
