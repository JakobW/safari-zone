module Domain.QueueablePosition exposing
    ( QueueablePosition
    , getCurrentDirection
    , getExact
    , hasMoved
    , init
    , move
    , moveToCoord
    , setTile
    )

import Data.Units exposing (TileUnit)
import Domain.Coordinate as Coord exposing (Coordinate)
import Domain.Direction exposing (Direction)
import Domain.Position exposing (Position(..))


type alias QueueablePosition =
    { position : Position
    , directionQueue : List Direction
    }


init : Coordinate Int -> QueueablePosition
init c =
    { position = Domain.Position.init c
    , directionQueue = []
    }


move : Maybe Direction -> Float -> QueueablePosition -> QueueablePosition
move maybeNewDir dt pos =
    let
        { position, directionQueue } =
            pos

        speedup =
            toFloat (List.length directionQueue + 1)

        addedToQueue =
            case maybeNewDir of
                Just d ->
                    d :: directionQueue

                Nothing ->
                    directionQueue

        moveInDir mayDir =
            Domain.Position.move mayDir (dt * speedup) position
    in
    case ( List.head pos.directionQueue, position ) of
        ( Just queuedDir, OnTile _ _ ) ->
            { position = moveInDir (Just queuedDir), directionQueue = addedToQueue }

        ( Nothing, OnTile _ _ ) ->
            { pos | position = moveInDir maybeNewDir }

        ( _, Moving _ _ ) ->
            { position = moveInDir maybeNewDir, directionQueue = addedToQueue }


hasMoved : QueueablePosition -> QueueablePosition -> Bool
hasMoved qp1 qp2 =
    Domain.Position.hasMoved qp1.position qp2.position


moveToCoord : Coordinate Int -> QueueablePosition -> QueueablePosition
moveToCoord coord pos =
    case getTarget pos |> Coord.minus coord |> Domain.Direction.coordToDirection of
        Just dir ->
            move (Just dir) 0.1 pos

        Nothing ->
            setTile coord pos


getTarget : QueueablePosition -> Coordinate Int
getTarget { position, directionQueue } =
    let
        go dirs coord =
            case dirs of
                [] ->
                    coord

                dir :: rest ->
                    Domain.Direction.directionToCoord dir
                        |> Coord.plus coord
                        |> go rest
    in
    go directionQueue (Domain.Position.getNextTile position)


getExact : QueueablePosition -> Coordinate TileUnit
getExact =
    .position >> Domain.Position.getExact


getCurrentDirection : QueueablePosition -> Direction
getCurrentDirection =
    .position >> Domain.Position.getCurrentDirection


setTile : Coordinate Int -> QueueablePosition -> QueueablePosition
setTile coord { position } =
    { position = Domain.Position.setTile coord position, directionQueue = [] }
