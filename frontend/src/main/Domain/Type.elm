module Domain.Type exposing (..)

import Json.Decode as D
import Json.Encode as E


type Type
    = Fire
    | Grass
    | Normal
    | Flying
    | Psychic
    | Fighting
    | Bug
    | Water
    | Ice
    | Fairy
    | Steel
    | Poison
    | Ground
    | Rock
    | Dragon
    | Electric
    | Ghost


encoder : Type -> E.Value
encoder =
    E.string << toString


toString : Type -> String
toString t =
    case t of
        Fire ->
            "Fire"

        Grass ->
            "Grass"

        Normal ->
            "Normal"

        Flying ->
            "Flying"

        Psychic ->
            "Psychic"

        Fighting ->
            "Fighting"

        Bug ->
            "Bug"

        Water ->
            "Water"

        Ice ->
            "Ice"

        Fairy ->
            "Fairy"

        Steel ->
            "Steel"

        Poison ->
            "Poison"

        Ground ->
            "Ground"

        Rock ->
            "Rock"

        Dragon ->
            "Dragon"

        Electric ->
            "Electric"

        Ghost ->
            "Ghost"


decoder : D.Decoder Type
decoder =
    D.string
        |> D.andThen
            (\string ->
                case string of
                    "Fire" ->
                        D.succeed Fire

                    "Grass" ->
                        D.succeed Grass

                    "Normal" ->
                        D.succeed Normal

                    "Flying" ->
                        D.succeed Flying

                    "Psychic" ->
                        D.succeed Psychic

                    "Fighting" ->
                        D.succeed Fighting

                    "Bug" ->
                        D.succeed Bug

                    "Water" ->
                        D.succeed Water

                    "Ice" ->
                        D.succeed Ice

                    "Fairy" ->
                        D.succeed Fairy

                    "Steel" ->
                        D.succeed Steel

                    "Poison" ->
                        D.succeed Poison

                    "Ground" ->
                        D.succeed Ground

                    "Rock" ->
                        D.succeed Rock

                    "Dragon" ->
                        D.succeed Dragon

                    "Electric" ->
                        D.succeed Electric

                    "Ghost" ->
                        D.succeed Ghost

                    _ ->
                        D.fail <| "Type of pokemon " ++ string ++ " is unknown"
            )
