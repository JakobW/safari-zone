module Main exposing (main)

import Browser exposing (Document)
import Browser.Navigation as Nav
import Html
import Json.Decode as D
import Main.Model exposing (Model(..))
import Main.Msg exposing (Msg(..))
import Main.Subscriptions as Subscriptions
import Main.Update as Update exposing (initializePage)
import Main.View as View
import Shared.Effect as Effect
import Shared.Session
import Url exposing (Url)
import Shared.I18n as I18n
import Shared.Effect exposing (Effect(..))


init : D.Value -> Url -> Nav.Key -> ( MainModel, Cmd Msg )
init jsFlags url key =
    Shared.Session.init jsFlags url
        |> Result.map
            (\session ->
                let
                    ( model, effect ) =
                        initializePage session.route (NotFoundModel session)
                    getI18n = I18n.getProperties GotI18n session |> CommandEffect  
                in
                ( ( model, key ), Effect.runEffect key <| Effects [effect, getI18n] )
            )
        |> orNoCmd


orNoCmd : Result x ( a, Cmd msg ) -> ( Result x a, Cmd msg )
orNoCmd r =
    case r of
        Ok ( a, cmd ) ->
            ( Ok a, cmd )

        Err err ->
            ( Err err, Cmd.none )


type alias MainModel =
    Result D.Error ( Model, Nav.Key )


update : Msg -> MainModel -> ( MainModel, Cmd Msg )
update msg m =
    case m of
        Err _ ->
            ( m, Cmd.none )

        Ok ( model, key ) ->
            Update.update msg model |> Tuple.mapBoth (\new -> ( new, key ) |> Ok) (Effect.runEffect key)


subscriptions : MainModel -> Sub Msg
subscriptions m =
    case m of
        Err _ ->
            Sub.none

        Ok ( model, _ ) ->
            Subscriptions.subscriptions model


view : MainModel -> Document Msg
view m =
    case m of
        Err err ->
            { title = "Something went wrong", body = [ Html.text <| D.errorToString err ] }

        Ok ( model, _ ) ->
            View.view model


main : Program D.Value (Result D.Error ( Model, Nav.Key )) Msg
main =
    Browser.application
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        , onUrlRequest = OnUrlRequest
        , onUrlChange = OnUrlChange
        }
