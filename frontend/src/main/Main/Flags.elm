module Main.Flags exposing (Flags, readFlags)

import Shared.Types.BackendUrl exposing (BackendUrl)
import Shared.Types.Auth as Auth
import Json.Decode as D
import Json.Decode.Pipeline as D


type alias Flags =
    { language : String
    , x : Int
    , y : Int
    , auth : Auth.Authentification
    , backendUrl : BackendUrl
    }


flagDecoder : D.Decoder Flags
flagDecoder =
    D.succeed Flags
        |> D.optional "language" D.string "de"
        |> D.optional "x" D.int 0
        |> D.optional "y" D.int 0
        |> D.required "auth" Auth.decodeAuthentification
        |> D.required "backendUrl" Shared.Types.BackendUrl.backendUrlDecoder


readFlags : D.Value -> Result D.Error Flags
readFlags =
    D.decodeValue flagDecoder
