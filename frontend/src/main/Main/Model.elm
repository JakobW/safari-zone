module Main.Model exposing (Model(..), getSession, modifySession, setSession)

import Dex.Appraisal
import Dex.Detail
import Dex.Overview
import Safari.Overworld
import Shared.Session exposing (Session)
import Shared.Types.Generic exposing (Lift)
import TrainerSelect.Detail
import TrainerSelect.Overview


type Model
    = DexOverviewModel Dex.Overview.Model
    | DexDetailModel Dex.Detail.Model
    | DexAppraisalModel Dex.Appraisal.Model
    | SafariModel Safari.Overworld.Model
    | TrainerOverviewModel TrainerSelect.Overview.Model
    | TrainerDetailModel TrainerSelect.Detail.Model
    | NotFoundModel Session


getSession : Model -> Session
getSession model =
    case model of
        DexOverviewModel m ->
            Dex.Overview.app.getSession m

        DexDetailModel m ->
            Dex.Detail.app.getSession m

        DexAppraisalModel m ->
            Dex.Appraisal.app.getSession m

        SafariModel m ->
            Safari.Overworld.app.getSession m

        TrainerOverviewModel m ->
            TrainerSelect.Overview.app.getSession m

        TrainerDetailModel m ->
            TrainerSelect.Detail.app.getSession m

        NotFoundModel session ->
            session


setSession : Session -> Model -> Model
setSession session model =
    case model of
        DexOverviewModel m ->
            DexOverviewModel <| Dex.Overview.app.setSession session m

        DexDetailModel m ->
            DexDetailModel <| Dex.Detail.app.setSession session m

        DexAppraisalModel m ->
            DexAppraisalModel <| Dex.Appraisal.app.setSession session m

        SafariModel m ->
            SafariModel <| Safari.Overworld.app.setSession session m

        TrainerOverviewModel m ->
            TrainerOverviewModel <| TrainerSelect.Overview.app.setSession session m

        TrainerDetailModel m ->
            TrainerDetailModel <| TrainerSelect.Detail.app.setSession session m

        NotFoundModel _ ->
            NotFoundModel session


modifySession : Lift Session Model
modifySession modify model =
    setSession (modify <| getSession model) model
