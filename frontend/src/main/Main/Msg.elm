module Main.Msg exposing (Msg(..))

import Api.Textures
import Browser exposing (UrlRequest)
import Dex.Appraisal
import Dex.Detail
import Dex.Overview
import Domain.PokemonDetail exposing (PokemonDetail)
import Http
import Ports.WebSocket as WS
import Safari.Overworld
import Shared.I18n exposing (I18n)
import Shared.Types.Auth exposing (Authentification)
import Time
import TrainerSelect.Detail
import TrainerSelect.Overview
import Url exposing (Url)
import WebGL.Texture exposing (Texture)


type Msg
    = -- Navigation
      OnUrlChange Url
    | OnUrlRequest UrlRequest
    | Tick Time.Posix
    | ClickedNav
    | AuthRefreshTime
    | AuthRefreshed (Result Http.Error Authentification)
    | GotI18n (Result Http.Error I18n)
      -- Websocket
    | WebSocketIn WS.WebsocketIn
    | LoadedEncounterTexture Int (Result Api.Textures.Error ( PokemonDetail, Texture ))
      -- Resizing
    | Resize Int Int
      -- Pages
    | DexOverviewMessage Dex.Overview.Msg
    | DexDetailMessage Dex.Detail.Msg
    | DexAppraisalMessage Dex.Appraisal.Msg
    | SafariPageMessage Safari.Overworld.Msg
    | TrainerOverviewPageMessage TrainerSelect.Overview.Msg
    | TrainerDetailPageMessage TrainerSelect.Detail.Msg
