module Main.SubApp exposing (..)

import Element
import Shared.Effect as Effect exposing (Effect)
import Shared.Types.Document exposing (Document)
import Shared.Types.Generic exposing (Setter)


type alias SubApp flags msg model session =
    { init : flags -> session -> ( model, Effect msg )
    , update : msg -> model -> ( model, Effect msg )
    , view : model -> Document msg
    , subscriptions : model -> Sub msg
    , getSession : model -> session
    , setSession : session -> Setter model
    }


subInit : (msg -> mainMsg) -> (model -> mainModel) -> SubApp flags msg model session -> flags -> session -> ( mainModel, Effect mainMsg )
subInit liftMsg liftModel subApp flags =
    subApp.init flags >> Tuple.mapBoth liftModel (Effect.map liftMsg)


subUpdate : (msg -> mainMsg) -> (model -> mainModel) -> SubApp flags msg model session -> msg -> model -> ( mainModel, Effect mainMsg )
subUpdate liftMsg liftModel subApp msg model =
    subApp.update msg model
        |> Tuple.mapBoth liftModel (Effect.map liftMsg)


subView : (msg -> mainMsg) -> SubApp flags msg model session -> model -> Document mainMsg
subView liftMsg subApp =
    subApp.view >> mapView liftMsg


subSubs : (msg -> mainMsg) -> SubApp flags msg model session -> model -> Sub mainMsg
subSubs liftMsg subApp =
    subApp.subscriptions >> Sub.map liftMsg


mapView : (msg -> msg2) -> Document msg -> Document msg2
mapView f { title, body } =
    { title = title, body = Element.map f body }
