module Main.SubApps exposing
    ( dexAppraisal
    , dexDetail
    , dexOverview
    , safari
    , trainerDetail
    , trainerOverview
    )

import Dex.Appraisal
import Dex.Detail
import Dex.Overview
import Main.Model exposing (Model(..))
import Main.Msg exposing (Msg(..))
import Main.SubApp as SubApp exposing (SubApp)
import Safari.Overworld
import Shared.Effect exposing (Effect)
import Shared.Session exposing (Session)
import Shared.Types.Document exposing (Document)
import Shared.Types.Generic exposing (Setter)
import TrainerSelect.Detail
import TrainerSelect.Overview


type alias App flags msg model =
    { init : flags -> Session -> ( Model, Effect Msg )
    , update : msg -> model -> ( Model, Effect Msg )
    , view : model -> Document Msg
    , subscriptions : model -> Sub Msg
    , getSession : model -> Session
    , setSession : Session -> Setter model
    }


dexOverview : App () Dex.Overview.Msg Dex.Overview.Model
dexOverview =
    liftSubApp DexOverviewMessage DexOverviewModel Dex.Overview.app


dexDetail : App Int Dex.Detail.Msg Dex.Detail.Model
dexDetail =
    liftSubApp DexDetailMessage DexDetailModel Dex.Detail.app


dexAppraisal : App () Dex.Appraisal.Msg Dex.Appraisal.Model
dexAppraisal =
    liftSubApp DexAppraisalMessage DexAppraisalModel Dex.Appraisal.app


safari : App () Safari.Overworld.Msg Safari.Overworld.Model
safari =
    liftSubApp SafariPageMessage SafariModel Safari.Overworld.app


trainerOverview : App () TrainerSelect.Overview.Msg TrainerSelect.Overview.Model
trainerOverview =
    liftSubApp TrainerOverviewPageMessage TrainerOverviewModel TrainerSelect.Overview.app


trainerDetail : App Int TrainerSelect.Detail.Msg TrainerSelect.Detail.Model
trainerDetail =
    liftSubApp TrainerDetailPageMessage TrainerDetailModel TrainerSelect.Detail.app


liftSubApp : (msg -> Msg) -> (model -> Model) -> SubApp flags msg model Session -> App flags msg model
liftSubApp liftMsg liftModel subApp =
    { init = SubApp.subInit liftMsg liftModel subApp
    , update = SubApp.subUpdate liftMsg liftModel subApp
    , view = SubApp.subView liftMsg subApp
    , subscriptions = SubApp.subSubs liftMsg subApp
    , getSession = subApp.getSession
    , setSession = subApp.setSession
    }
