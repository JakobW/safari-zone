module Main.Subscriptions exposing (..)

import Animation
import Browser.Events
import Main.Model as Model exposing (Model(..))
import Main.Msg exposing (Msg(..))
import Main.SubApps as SubApps
import Ports.WebSocket as WS
import Shared.Types.Auth


subscriptions : Model -> Sub Msg
subscriptions model =
    let
        session =
            Model.getSession model

        pageSubscriptions =
            case model of
                DexOverviewModel dexModel ->
                    SubApps.dexOverview.subscriptions dexModel

                DexDetailModel dexModel ->
                    SubApps.dexDetail.subscriptions dexModel

                DexAppraisalModel dexModel ->
                    SubApps.dexAppraisal.subscriptions dexModel

                SafariModel safariModel ->
                    SubApps.safari.subscriptions safariModel

                TrainerOverviewModel trainerModel ->
                    SubApps.trainerOverview.subscriptions trainerModel

                TrainerDetailModel trainerModel ->
                    SubApps.trainerDetail.subscriptions trainerModel

                NotFoundModel _ ->
                    Sub.none
    in
    Sub.batch
        [ WS.receive WebSocketIn
        , Browser.Events.onResize Resize
        , Animation.subscriptions Tick (Model.getSession model)
        , Shared.Types.Auth.registerRefreshReminder session.auth AuthRefreshTime
        , pageSubscriptions
        ]
