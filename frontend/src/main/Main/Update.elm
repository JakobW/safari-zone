module Main.Update exposing (..)

import Animation
import Api.Textures
import Api.WebsocketApi
import Browser
import Browser.Navigation as Nav
import Dict
import Domain.Coordinate
import Domain.Encounter
import Domain.Player
import Json.Decode as D
import Main.Model as Model exposing (Model(..))
import Main.Msg exposing (Msg(..))
import Main.SubApps as SubApps
import Ports.WebSocket as WS
import Route exposing (Route)
import Safari.Overworld.Types.Msg as SafariMsg
import Shared.Effect exposing (Effect(..), withNoEffect)
import Shared.I18n as I18n
import Shared.Request
import Shared.Session as Session
import Shared.Types.Auth as Auth
import Url exposing (Url)


update : Msg -> Model -> ( Model, Effect Msg )
update msg model =
    case ( msg, model ) of
        ( DexOverviewMessage dexMessage, DexOverviewModel dexModel ) ->
            SubApps.dexOverview.update dexMessage dexModel

        ( DexDetailMessage dexMessage, DexDetailModel dexModel ) ->
            SubApps.dexDetail.update dexMessage dexModel

        ( DexAppraisalMessage dexMessage, DexAppraisalModel dexModel ) ->
            SubApps.dexAppraisal.update dexMessage dexModel

        ( SafariPageMessage safariPageMessage, SafariModel safariModel ) ->
            SubApps.safari.update safariPageMessage safariModel

        ( TrainerOverviewPageMessage trainerMessage, TrainerOverviewModel trainerModel ) ->
            SubApps.trainerOverview.update trainerMessage trainerModel

        ( TrainerDetailPageMessage trainerMessage, TrainerDetailModel trainerModel ) ->
            SubApps.trainerDetail.update trainerMessage trainerModel

        ( WebSocketIn (WS.Connected connection data), _ ) ->
            case D.decodeString Domain.Coordinate.decoder data of
                Ok pos ->
                    ( Model.modifySession (Session.setConnection connection pos) model, NoEffect )

                Err err ->
                    ( Model.modifySession (Session.addError <| D.errorToString err) model, NoEffect )

        ( WebSocketIn WS.Closed, _ ) ->
            ( model, Model.getSession model |> .auth >> WS.connect >> CommandEffect )

        ( WebSocketIn (WS.Message wsMsg), _ ) ->
            case D.decodeString Api.WebsocketApi.decoder wsMsg of
                Ok decodedMsg ->
                    handleWebsocketMessage decodedMsg model

                Err err ->
                    ( (Model.modifySession << Session.addError) (D.errorToString err) model, NoEffect )

        ( WebSocketIn (WS.Error err), _ ) ->
            ( (Model.modifySession << Session.addError) (WS.errorToString err) model, NoEffect )

        ( OnUrlChange url, _ ) ->
            handleRouteChange url model

        ( OnUrlRequest (Browser.Internal url), _ ) ->
            ( model, UseKey <| \key -> Url.toString url |> Nav.pushUrl key )

        ( OnUrlRequest (Browser.External url), _ ) ->
            ( model, CommandEffect <| Nav.load url )

        ( Resize x y, _ ) ->
            ( Model.modifySession (Session.setViewport ( x, y )) model, NoEffect )

        ( Tick time, _ ) ->
            ( Model.modifySession (Animation.onTick time) model, NoEffect )

        ( AuthRefreshTime, _ ) ->
            ( model, Model.getSession model |> (\{ auth, backendUrl } -> Auth.refreshAuth auth AuthRefreshed backendUrl) |> CommandEffect )

        ( AuthRefreshed (Ok auth), _ ) ->
            ( Model.modifySession (Session.setAuth auth) model, NoEffect )

        ( AuthRefreshed (Err _), _ ) ->
            ( model, NoEffect )

        ( ClickedNav, _ ) ->
            ( Model.modifySession (Animation.liftAnimation Animation.toggleNav) model, NoEffect )

        ( LoadedEncounterTexture pokemonId (Ok ( _, texture )), _ ) ->
            ( (Model.modifySession <| Session.addPokemonTexture pokemonId texture) model, NoEffect )

        ( LoadedEncounterTexture _ (Err err), _ ) ->
            ( (Model.modifySession << Session.addError) (Api.Textures.errorToString err) model, NoEffect )

        ( GotI18n (Ok i18n), _ ) ->
            ( (Model.modifySession <| I18n.setI18n i18n) model, NoEffect )

        _ ->
            ( model, NoEffect )


handleRouteChange : Url -> Model -> ( Model, Effect Msg )
handleRouteChange url model =
    initializePage (Route.toRoute url) model


initializePage : Route -> Model -> ( Model, Effect Msg )
initializePage route model =
    let
        session =
            Model.getSession model

        initConnection effs =
            case session.connection of
                Just _ ->
                    effs

                Nothing ->
                    Effects [ effs, WS.connect session.auth |> CommandEffect ]
    in
    Tuple.mapSecond initConnection <|
        case route of
            Route.DexOverview ->
                SubApps.dexOverview.init () session

            Route.DexDetail id ->
                SubApps.dexDetail.init id session

            Route.DexAppraisal ->
                SubApps.dexAppraisal.init () session

            Route.Safari ->
                SubApps.safari.init () session

            Route.NotFound _ ->
                ( NotFoundModel session, NoEffect )

            Route.TrainerOverview ->
                SubApps.trainerOverview.init () session

            Route.TrainerDetail id ->
                SubApps.trainerDetail.init id session


handleWebsocketMessage : Api.WebsocketApi.WebsocketOut -> Model -> ( Model, Effect Msg )
handleWebsocketMessage wsMsg model =
    case wsMsg of
        Api.WebsocketApi.PokemonCaughtMessage ->
            ( model, Route.navigate Route.DexOverview )

        Api.WebsocketApi.PokemonFledMessage ->
            ( model, Route.navigate Route.Safari )

        Api.WebsocketApi.PokemonBrokeOutMessage ->
            case model of
                SafariModel safariModel ->
                    SubApps.safari.update (SafariMsg.GotRunningMsg SafariMsg.BrokeOut) safariModel

                _ ->
                    withNoEffect model

        Api.WebsocketApi.PlayerMovedMessage player ->
            ( (Model.modifySession << Session.modifyPlayers) (Domain.Player.addOrMovePlayer player) model, NoEffect )

        Api.WebsocketApi.PokemonChangedMessage pokemonMsg ->
            let
                session =
                    Model.getSession model

                effect =
                    case pokemonMsg of
                        Domain.Encounter.MkNewEncounter { pokemonId } ->
                            if Dict.member pokemonId session.pokemonTextures then
                                NoEffect

                            else
                                Api.Textures.loadPokemonTextureTask pokemonId
                                    |> Shared.Request.send (LoadedEncounterTexture pokemonId) session

                        _ ->
                            NoEffect
            in
            ( (Model.modifySession << Session.modifyEncounters) (Domain.Encounter.handleChange pokemonMsg) model, effect )

        Api.WebsocketApi.PokemonEncounterMessage encounter ->
            case model of
                SafariModel safariModel ->
                    SubApps.safari.update (SafariMsg.GotEncounter encounter |> SafariMsg.GotRunningMsg) safariModel

                _ ->
                    withNoEffect model

        Api.WebsocketApi.EncounterTooFarAway ->
            ( model, CommandEffect Nav.reload )
