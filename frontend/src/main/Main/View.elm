module Main.View exposing (..)

import Browser exposing (Document)
import Main.Model as Model exposing (Model(..))
import Main.Msg exposing (Msg)
import Main.SubApps as SubApps
import Shared.Types.Document as Document
import Shared.View.Nav as Nav
import Shared.View.Page404 as Page404


view : Model -> Document Msg
view model =
    let
        session =
            Model.getSession model
    in
    Document.render (Nav.view session) <|
        case model of
            DexOverviewModel dexModel ->
                SubApps.dexOverview.view dexModel

            DexDetailModel dexModel ->
                SubApps.dexDetail.view dexModel

            DexAppraisalModel dexModel ->
                SubApps.dexAppraisal.view dexModel

            SafariModel safariModel ->
                SubApps.safari.view safariModel

            NotFoundModel notFoundModel ->
                Page404.view notFoundModel

            TrainerOverviewModel trainerModel ->
                SubApps.trainerOverview.view trainerModel

            TrainerDetailModel trainerModel ->
                SubApps.trainerDetail.view trainerModel
