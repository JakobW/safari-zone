port module Ports.WebSocket exposing (Connection, Msg(..), WebsocketIn(..), connect, errorToString, receive, sendMessage)

import Json.Decode as D
import Json.Encode as E
import Shared.Types.Auth as Auth


port receiveWebSocket : (D.Value -> msg) -> Sub msg


port sendWebSocket : E.Value -> Cmd msg


type Connection
    = Connection


type Msg
    = Connect String Auth.Authentification
    | Disconnect
    | Send E.Value


type WebsocketIn
    = Connected Connection String
    | Closed
    | Error Error
    | Message String


type Error
    = DecodeError D.Error
    | OtherError


errorToString : Error -> String
errorToString err =
    case err of
        DecodeError d ->
            D.errorToString d

        OtherError ->
            "Error from websocket"


decodeWebsocketIn : D.Decoder WebsocketIn
decodeWebsocketIn =
    D.field "tag" D.string
        |> D.andThen
            (\s ->
                case s of
                    "connected" ->
                        D.field "data" D.string |> D.map (Connected Connection)

                    "message" ->
                        D.field "data" D.string |> D.map Message

                    "closed" ->
                        D.succeed Closed

                    "error" ->
                        D.succeed (Error OtherError)

                    _ ->
                        D.fail <| "Expected tag to be one of connected, message, closed, but was " ++ s
            )


unifyError : Result D.Error WebsocketIn -> WebsocketIn
unifyError res =
    case res of
        Ok ok ->
            ok

        Err err ->
            Error (DecodeError err)


receive : (WebsocketIn -> msg) -> Sub msg
receive onIn =
    receiveWebSocket (D.decodeValue decodeWebsocketIn >> unifyError >> onIn)


send : Msg -> Cmd msg
send =
    encode >> sendWebSocket


sendMessage : Connection -> E.Value -> Cmd msg
sendMessage _ =
    send << Send


connect : Auth.Authentification -> Cmd msg
connect =
    send << Connect "api/websocket"


encode : Msg -> E.Value
encode msg =
    case msg of
        Connect url auth ->
            E.object [ ( "tag", E.string "connect" ), ( "url", E.string url ), ( "auth", Auth.encodeAuthentification auth ) ]

        Disconnect ->
            E.object [ ( "tag", E.string "disconnect" ) ]

        Send value ->
            E.object [ ( "tag", E.string "send" ), ( "message", value ) ]
