module Route exposing (..)

import Browser.Navigation as Nav
import Shared.Effect exposing (Effect(..))
import Url exposing (Url)
import Url.Builder exposing (relative)
import Url.Parser exposing ((</>), Parser, map, oneOf, parse, s, top)


type Route
    = DexOverview
    | DexDetail Int
    | DexAppraisal
    | Safari
    | TrainerOverview
    | TrainerDetail Int
    | NotFound Url


route : Parser (Route -> a) a
route =
    oneOf
        [ map DexOverview top
        , map DexDetail (s "dex" </> Url.Parser.int)
        , map DexAppraisal (s "dex" </> s "appraisal")
        , map Safari (s "safari")
        , map TrainerOverview (s "trainer")
        , map TrainerDetail (s "trainer" </> Url.Parser.int)
        ]


navigate : Route -> Effect msg
navigate r =
    UseKey <| \key -> fromRoute r |> Nav.pushUrl key


toRoute : Url -> Route
toRoute url =
    parse route url
        |> Maybe.withDefault (NotFound url)


fromRoute : Route -> String
fromRoute r =
    "/"
        ++ (case r of
                DexOverview ->
                    relative [] []

                DexDetail id ->
                    relative [ "dex", String.fromInt id ] []

                DexAppraisal ->
                    relative [ "dex", "appraisal" ] []

                Safari ->
                    relative [ "safari" ] []

                TrainerOverview ->
                    relative [ "trainer" ] []

                TrainerDetail id ->
                    relative [ "trainer", String.fromInt id ] []

                NotFound url ->
                    Url.toString url
           )
