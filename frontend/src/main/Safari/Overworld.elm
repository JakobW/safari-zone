module Safari.Overworld exposing (..)

import Api.MoveRuleApi
import Api.Textures exposing (Textures)
import Domain.MoveRule exposing (MoveRules)
import Main.SubApp exposing (SubApp)
import Safari.Overworld.Subscriptions exposing (subscriptions)
import Safari.Overworld.Types.Model as Model
import Safari.Overworld.Types.Msg as Msg
import Safari.Overworld.Update exposing (update)
import Safari.Overworld.View exposing (view)
import Shared.Effect exposing (Effect)
import Shared.Request exposing (AuthorizedRequest)
import Shared.Session exposing (Session)


type alias Msg =
    Msg.Msg


type alias Model =
    Model.Model


app : SubApp () Msg Model Session
app =
    { init = \_ s -> ( Model.init s, initCmd s )
    , update = update
    , view = view
    , subscriptions = subscriptions
    , getSession = Model.getSession
    , setSession = Model.setSession
    }


beforeInit : AuthorizedRequest Api.Textures.Error ( Textures, MoveRules )
beforeInit =
    Api.MoveRuleApi.getMoveRules
        |> Shared.Request.mapError Api.Textures.HttpError
        |> Shared.Request.combine Api.Textures.loadTextures


initCmd : Session -> Effect Msg
initCmd session =
    beforeInit |> Shared.Request.send Msg.GotInitMsg session
