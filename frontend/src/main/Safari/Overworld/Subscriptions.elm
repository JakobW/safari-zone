module Safari.Overworld.Subscriptions exposing (subscriptions)

import Animator
import Browser.Events
import Safari.Overworld.Types.Gamepad
import Json.Decode as D
import Safari.Overworld.Types.Interaction exposing (interactionDecoder)
import Safari.Overworld.Types.Model as Model exposing (Model)
import Safari.Overworld.Types.Msg exposing (Msg(..), RunningMsg(..))


subscriptions : Model -> Sub Msg
subscriptions model =
    case model of
        Model.Loading _ ->
            Sub.none

        Model.Encounter encounter ->
            Animator.toSubscription (Tick >> GotRunningMsg) encounter Model.animator

        Model.Initialized _ ->
            Sub.batch
                [ Sub.map (GamepadMsg >> GotRunningMsg) <| Safari.Overworld.Types.Gamepad.subscriptions
                , Browser.Events.onAnimationFrameDelta (Frame >> GotRunningMsg)
                , interact
                ]


interact : Sub Msg
interact =
    Browser.Events.onKeyPress
        (D.field "key" D.string
            |> D.andThen interactionDecoder
            |> D.map (GotInteraction >> GotRunningMsg)
        )
