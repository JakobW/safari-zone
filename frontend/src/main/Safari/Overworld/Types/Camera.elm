module Safari.Overworld.Types.Camera exposing (..)

import Config
import Data.Units exposing (TileUnit(..), WebGLUnit(..))
import Domain.Coordinate exposing (Coordinate)
import Shared.Session exposing (W_Viewport)



{-
   There are three different coordinate systems we have to work with here.
   This module attempts to make it easy to distinguish them.

   1. Screen Pixels. Something like 1200x800. Is relevant because we want to render entities at consistent sizes regardless of screen size.
      All entities have a size given in pixels.

   2. Tile Coordinates. Something like 12x15. Is relevant because players and pokemon cannot stop between tiles. Also moveability is determined
     on tile basis.

   3. WebGL Canvas Coordinates. Something like 0.6x0.3. Is relevant because this is how the graphics are rendered in the end. Contrary to
     the other 2 coordinte systems, 0x0 is not in the top left but in the middle. Goes from -1 to +1 in both dimensions,
     x from left to right, y from down to up.

   The camera is always centered on the player unless the player approaches the edge of the map.
   Then, the camera stops following the player until he leaves the edge again.
-}


type alias OneDCamera =
    { player : TileUnit
    , screenSizePx : Int
    , tileMapSize : TileUnit
    }


type alias Camera =
    Coordinate OneDCamera


mkCamera : Coordinate TileUnit -> W_Viewport a -> Coordinate TileUnit -> Camera
mkCamera player { height, width } tileMap =
    { x = { player = player.x, screenSizePx = width, tileMapSize = tileMap.x }
    , y = { player = player.y, screenSizePx = height, tileMapSize = tileMap.y }
    }


calcTileOffset : Camera -> Coordinate TileUnit
calcTileOffset =
    Domain.Coordinate.map calcTileOffset1d


getScreenWidth : Camera -> Int
getScreenWidth =
    .x >> .screenSizePx


getScreenHeight : Camera -> Int
getScreenHeight =
    .y >> .screenSizePx


calcWebGLCoord : Camera -> Coordinate TileUnit -> Coordinate WebGLUnit
calcWebGLCoord camera tile =
    let
        tileOffset =
            calcTileOffset camera

        withOffset =
            { x = addTile tileOffset.x tile.x, y = addTile tileOffset.y tile.y }
    in
    { x = tileToWebGL camera.x.screenSizePx withOffset.x
    , y = tileToWebGL camera.y.screenSizePx withOffset.y |> invertWebGL
    }


tileToWebGL : Int -> TileUnit -> WebGLUnit
tileToWebGL screenSizePx (TileUnit tu) =
    let
        tilesOnScreen =
            toFloat screenSizePx / Config.tileSize
    in
    WebGLUnit <| 2 * tu / tilesOnScreen - 1


calcTileOffset1d : OneDCamera -> TileUnit
calcTileOffset1d { player, screenSizePx, tileMapSize } =
    let
        tilesOnScreen =
            toFloat screenSizePx / Config.tileSize

        (TileUnit pTile) =
            player

        (TileUnit mapSize) =
            tileMapSize

        tileOffset =
            clamp (tilesOnScreen - mapSize) 0 (tilesOnScreen / 2 - pTile)
    in
    TileUnit tileOffset


addTile : TileUnit -> TileUnit -> TileUnit
addTile (TileUnit t1) (TileUnit t2) =
    TileUnit (t1 + t2)


invertWebGL : WebGLUnit -> WebGLUnit
invertWebGL (WebGLUnit w) =
    WebGLUnit -w


clampWithRemainder : number -> number -> number -> ( number, number )
clampWithRemainder minVal maxVal n =
    case ( minVal > n, maxVal < n ) of
        ( True, True ) ->
            ( n, 0 )

        -- should not occur since minVal <= maxVal
        ( True, False ) ->
            ( minVal, minVal - n )

        ( False, True ) ->
            ( maxVal, maxVal - n )

        ( False, False ) ->
            ( n, 0 )
