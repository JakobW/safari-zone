module Safari.Overworld.Types.CatchState exposing (..)


type CatchState
    = Idle
    | StartThrow
    | Throw
    | Falling
    | Catching
    | Crawling
