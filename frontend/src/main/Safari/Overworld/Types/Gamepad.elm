module Safari.Overworld.Types.Gamepad exposing (Button, Gamepad, Msg, getDirection, init, subscriptions, update)

import Browser.Events
import Domain.Direction exposing (Direction(..))
import Json.Decode as D


type Msg
    = Pressed Button
    | Released Button


type Button
    = GoLeft
    | GoRight
    | GoUp
    | GoDown


getDirection : Gamepad -> Maybe Direction
getDirection (Gamepad pad) =
    List.head pad
        |> Maybe.map buttonToDirection


buttonToDirection : Button -> Direction
buttonToDirection button =
    case button of
        GoLeft ->
            L

        GoRight ->
            R

        GoUp ->
            U

        GoDown ->
            D


type Gamepad
    = Gamepad (List Button)


init : Gamepad
init =
    Gamepad []


pressButton : Button -> Gamepad -> Gamepad
pressButton button (Gamepad pad) =
    Gamepad <| button :: pad


releaseButton : Button -> Gamepad -> Gamepad
releaseButton button (Gamepad pad) =
    Gamepad <| List.filter (\btn -> button /= btn) pad


subscriptions : Sub Msg
subscriptions =
    Sub.batch
        [ Browser.Events.onKeyDown (D.map Pressed decodeButton)
        , Browser.Events.onKeyUp (D.map Released decodeButton)
        ]


update : Msg -> Gamepad -> Gamepad
update msg =
    case msg of
        Pressed button ->
            pressButton button

        Released button ->
            releaseButton button


decodeButton : D.Decoder Button
decodeButton =
    D.andThen toButton
        (D.field "key" D.string)


toButton : String -> D.Decoder Button
toButton string =
    case string of
        "ArrowLeft" ->
            D.succeed GoLeft

        "ArrowRight" ->
            D.succeed GoRight

        "ArrowDown" ->
            D.succeed GoDown

        "ArrowUp" ->
            D.succeed GoUp

        _ ->
            D.fail "Skip"
