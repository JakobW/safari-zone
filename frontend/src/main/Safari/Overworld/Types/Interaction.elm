module Safari.Overworld.Types.Interaction exposing (..)

import Json.Decode as D


type Interaction
    = A


interactionDecoder : String -> D.Decoder Interaction
interactionDecoder str =
    case String.toLower str of
        "a" ->
            D.succeed A

        _ ->
            D.fail "Skip"
