module Safari.Overworld.Types.Model exposing (..)

import Animator
import Api.Textures exposing (Textures)
import Domain.ConstrainedPosition
import Domain.Coordinate exposing (Coordinate)
import Domain.Encounter as Encounter
import Domain.MoveRule exposing (MoveRules)
import Domain.PokemonEncounter exposing (PokemonEncounter)
import Safari.Overworld.Types.CatchState as CatchState exposing (CatchState)
import Safari.Overworld.Types.Gamepad exposing (Gamepad)
import Shared.Session exposing (Session)
import Shared.Types.Generic exposing (Lift, Setter, getAndSet)
import Shared.Types.TrainerState as TrainerState exposing (TrainerState)


type Model
    = Loading Session
    | Initialized InitModel
    | Encounter EncounterModel


init : Session -> Model
init =
    Loading


type alias InitModel =
    { trainer : TrainerState
    , playerOffset : Coordinate Float
    , gamepad : Gamepad
    , textures : Textures
    , session : Session
    }


type alias EncounterModel =
    { session : Session
    , encounter : EncounterState
    , trainerState : Animator.Timeline CatchState
    , zoom : Animator.Timeline Float
    }


loaded : ( Textures, MoveRules ) -> Model -> Model
loaded ( textures, moveRules ) model =
    let
        session =
            getSession model
    in
    Initialized
        { trainer = TrainerState.init
        , playerOffset = { x = toFloat session.width / 2, y = toFloat session.height / 2 }
        , gamepad = Safari.Overworld.Types.Gamepad.init
        , textures = textures
        , session = { session | position = Domain.ConstrainedPosition.updateMoveRules moveRules session.position }
        }


toEncounter : Encounter.Encounter -> Model -> Model
toEncounter { encounterId, pokemonId, shinyStatus } model =
    let
        session =
            getSession model
    in
    Encounter
        { session = session
        , encounter =
            JustIds { pokemonId = pokemonId, encounterId = encounterId, shinyStatus = shinyStatus }
        , trainerState = Animator.init CatchState.Idle
        , zoom = Animator.init 1
        }


getSession : Model -> Session
getSession model =
    case model of
        Loading s ->
            s

        Initialized { session } ->
            session

        Encounter { session } ->
            session


setSession : Session -> Setter Model
setSession session model =
    case model of
        Loading _ ->
            Loading session

        Initialized m ->
            Initialized { m | session = session }

        Encounter m ->
            Encounter { m | session = session }


modifySession : Lift Session Model
modifySession =
    getAndSet getSession setSession


modifySessionInInit : Lift Session InitModel
modifySessionInInit =
    getAndSet .session (\s m -> { m | session = s })


type EncounterState
    = JustIds (WithIds {})
    | Loaded Loaded


type alias Loaded =
    WithIds { name : String, imageName : String }


addMetadata : PokemonEncounter -> WithIds a -> Loaded
addMetadata md enc =
    { name = md.name, imageName = md.imageName, encounterId = enc.encounterId, pokemonId = enc.pokemonId, shinyStatus = enc.shinyStatus }


type alias WithIds a =
    { a | pokemonId : Int, encounterId : Int, shinyStatus : Bool }


animator : Animator.Animator EncounterModel
animator =
    Animator.animator
        |> Animator.watching .trainerState (\s m -> { m | trainerState = s })
        |> Animator.watching .zoom (\s m -> { m | zoom = s })
