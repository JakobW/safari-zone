module Safari.Overworld.Types.Msg exposing (..)

import Api.Textures exposing (Textures)
import Domain.MoveRule exposing (MoveRules)
import Domain.PokemonEncounter exposing (PokemonEncounter)
import Safari.Overworld.Types.Gamepad as Gamepad
import Safari.Overworld.Types.Interaction exposing (Interaction)
import Time


type Msg
    = GotInitMsg (Result Api.Textures.Error ( Textures, MoveRules ))
    | GotRunningMsg RunningMsg


type RunningMsg
    = GamepadMsg Gamepad.Msg
    | Frame Float
    | GotInteraction Interaction
    | GotEncounter PokemonEncounter
    | GoNearer
    | ThrowBall
    | Reset
    | BrokeOut
    | Tick Time.Posix
