module Safari.Overworld.Update exposing (update)

import Animator
import Api.WebsocketApi
import Config
import Domain.ConstrainedPosition
import Domain.Encounter as Encounter
import Domain.Player as Player
import Ports.WebSocket as WS
import Safari.Overworld.Types.CatchState as CatchState
import Safari.Overworld.Types.Gamepad
import Safari.Overworld.Types.Interaction as Interaction
import Safari.Overworld.Types.Model as Model exposing (EncounterModel, InitModel, Model(..))
import Safari.Overworld.Types.Msg exposing (Msg(..), RunningMsg(..))
import Shared.Effect as Effect exposing (Effect(..), withNoEffect)
import Shared.Session as Session exposing (Session)
import Shared.Types.TrainerState as TrainerState


update : Msg -> Model -> ( Model, Effect Msg )
update msg model =
    case ( msg, model ) of
        ( GotInitMsg (Ok ( textures, moveRules )), Loading _ ) ->
            ( Model.loaded ( textures, moveRules ) model, NoEffect )

        ( GotInitMsg (Err _), _ ) ->
            ( model, NoEffect )

        ( GotRunningMsg runningMsg, _ ) ->
            updateInitialized runningMsg model
                |> Tuple.mapSecond (Effect.map GotRunningMsg)

        _ ->
            ( model, NoEffect )


updateInitialized : RunningMsg -> Model -> ( Model, Effect RunningMsg )
updateInitialized msg model =
    case ( msg, model ) of
        ( Frame dt, Initialized initModel ) ->
            handleFrame dt initModel |> Tuple.mapFirst Initialized

        ( GamepadMsg gamepadMsg, Initialized initModel ) ->
            ( Initialized { initModel | gamepad = Safari.Overworld.Types.Gamepad.update gamepadMsg initModel.gamepad }, NoEffect )

        ( GotInteraction interaction, Initialized initModel ) ->
            case interaction of
                Interaction.A ->
                    case
                        Domain.ConstrainedPosition.interact initModel.session.position
                            |> Maybe.andThen (\coord -> Encounter.fromCoordinate coord initModel.session.encounters)
                    of
                        Just encounter ->
                            ( Model.toEncounter encounter model
                            , sendWS initModel.session.connection (\c -> Api.WebsocketApi.sendEncounter c encounter.encounterId)
                            )

                        Nothing ->
                            ( model, NoEffect )

        ( GoNearer, Model.Encounter encounterModel ) ->
            ( goNearer encounterModel |> Model.Encounter
            , sendWS encounterModel.session.connection (\c -> Api.WebsocketApi.sendEncounterTurn c Api.WebsocketApi.Rock)
            )

        ( ThrowBall, Model.Encounter encounterModel ) ->
            ( throw encounterModel |> Model.Encounter
            , sendWS encounterModel.session.connection (\c -> Api.WebsocketApi.sendEncounterTurn c Api.WebsocketApi.Ball)
            )

        ( Reset, Model.Encounter encounterModel ) ->
            ( endThrow encounterModel |> Model.Encounter, NoEffect )

        ( BrokeOut, Model.Encounter encounterModel ) ->
            ( endThrow encounterModel |> Model.Encounter, NoEffect )

        ( Tick time, Model.Encounter encounterModel ) ->
            ( Animator.update time Model.animator encounterModel |> Model.Encounter, NoEffect )

        ( GotEncounter metadata, Model.Encounter encounterModel ) ->
            case encounterModel.encounter of
                Model.JustIds wIds ->
                    Model.Encounter { encounterModel | encounter = Model.Loaded <| Model.addMetadata metadata wIds } |> withNoEffect

                _ ->
                    withNoEffect model

        _ ->
            withNoEffect model


sendWS : Maybe WS.Connection -> (WS.Connection -> Cmd msg) -> Effect msg
sendWS mayConn doWithConn =
    case mayConn of
        Just conn ->
            CommandEffect <| doWithConn conn

        Nothing ->
            NoEffect


handleFrame : Float -> InitModel -> ( InitModel, Effect RunningMsg )
handleFrame dt model =
    let
        ( updatedSession, cmds ) =
            movement dt model

        mayDir =
            case Domain.ConstrainedPosition.getCurrentDirection updatedSession.position of
                Just d ->
                    Just d

                Nothing ->
                    Safari.Overworld.Types.Gamepad.getDirection model.gamepad

        action =
            if Domain.ConstrainedPosition.hasMoved model.session.position updatedSession.position then
                TrainerState.move

            else
                TrainerState.stop

        updatedModel =
            { model
                | trainer =
                    case mayDir of
                        Just dir ->
                            action model.trainer
                                |> TrainerState.changeDirection dir

                        Nothing ->
                            action model.trainer
                , session = updatedSession
            }
    in
    ( updatedModel, cmds )


movement : Float -> InitModel -> ( Session, Effect msg )
movement dt model =
    let
        { session } =
            model

        newPos =
            Domain.ConstrainedPosition.move (Safari.Overworld.Types.Gamepad.getDirection model.gamepad) (dt * Config.trainerSpeed) session.position

        cmds =
            case ( session.connection, Domain.ConstrainedPosition.shouldSendMessageAfterMove session.position newPos ) of
                ( Just conn, True ) ->
                    case Safari.Overworld.Types.Gamepad.getDirection model.gamepad of
                        Just dir ->
                            CommandEffect <| Api.WebsocketApi.sendMove conn dir

                        Nothing ->
                            NoEffect

                _ ->
                    NoEffect
    in
    ( { session
        | position = newPos
      }
        |> Session.modifyEncounters (Encounter.continueMoves <| Config.trainerSpeed * dt)
        |> Session.modifyPlayers (Player.continueMoves <| Config.trainerSpeed * dt)
    , cmds
    )


goNearer : EncounterModel -> EncounterModel
goNearer model =
    { model
        | trainerState =
            Animator.queue
                [ Animator.event Animator.quickly CatchState.Crawling
                , Animator.wait <| Animator.seconds 1
                , Animator.event Animator.quickly CatchState.Idle
                ]
                model.trainerState
        , zoom = Animator.go Animator.verySlowly (Animator.current model.zoom * 1.2) model.zoom
    }


throw : EncounterModel -> EncounterModel
throw model =
    { model
        | trainerState =
            Animator.queue
                [ Animator.event (Animator.millis 800) CatchState.StartThrow
                , Animator.event (Animator.millis 800) CatchState.Throw
                , Animator.wait Animator.slowly
                , Animator.event (Animator.millis 400) CatchState.Falling
                , Animator.event (Animator.millis 400) CatchState.Catching
                ]
                model.trainerState
    }


endThrow : EncounterModel -> EncounterModel
endThrow model =
    { model
        | trainerState =
            Animator.queue [ Animator.event Animator.immediately CatchState.Idle ] model.trainerState
    }
