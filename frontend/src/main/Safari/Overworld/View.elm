module Safari.Overworld.View exposing (..)

import Element exposing (clip, fill, height, width)
import Safari.Overworld.Types.Model exposing (EncounterState(..), Model(..))
import Safari.Overworld.Types.Msg exposing (Msg(..))
import Safari.Overworld.View.Encounter as Encounter
import Safari.Overworld.View.GameCanvas as GameCanvas
import Shared.I18n as I18n
import Shared.Types.Document exposing (Document)


view : Model -> Document Msg
view m =
    case m of
        Loading _ ->
            { title = "", body = Element.text "Loading" }

        Initialized model ->
            { title = "Catch em all"
            , body =
                Element.column
                    [ width fill
                    , height fill
                    , clip
                    , Element.behindContent (Element.html <| GameCanvas.view model.session model)
                    ]
                    []
            }

        Encounter eModel ->
            case eModel.encounter of
                Loaded enc ->
                    { title = I18n.t (\t -> t.safariEncounterTitle enc.name) eModel.session.i18n, body = Element.map GotRunningMsg <| Encounter.view enc eModel }

                _ ->
                    { title = "Encounter", body = Element.text "Rip" }
