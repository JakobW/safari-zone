module Safari.Overworld.View.Encounter exposing (view)

import Animator
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Html.Attributes
import Safari.Overworld.Types.CatchState as CatchState
import Safari.Overworld.Types.Model exposing (EncounterModel, Loaded)
import Safari.Overworld.Types.Msg exposing (RunningMsg(..))
import Safari.Overworld.View.PokeballSprite as PokeballSprite
import Safari.Overworld.View.TrainerImage
import Shared.View.Colors as Colors
import Shared.View.PokemonImage as PokemonImage


pokemonWidth : number
pokemonWidth =
    300


pokeballWidth : number
pokeballWidth =
    120


pokemonImage : Loaded -> EncounterModel -> Element RunningMsg
pokemonImage encounter model =
    let
        scaling state =
            case state of
                CatchState.Catching ->
                    0

                CatchState.Falling ->
                    0

                _ ->
                    1

        animatedScaling =
            Animator.linear model.trainerState (scaling >> Animator.at)

        actualWidth =
            pokemonWidth * animatedZoom model
    in
    PokemonImage.showImage
        [ width (px <| round <| animatedScaling * actualWidth)
        , Element.moveLeft <| (1 - animatedScaling) * actualWidth / 2
        , Element.moveDown <| (fallProgress model * 100) + (1 - animatedScaling) * actualWidth / 2
        , Element.alignRight
        ]
        -- Load Encounter Data previously
        { imageName = "bulbasaur", name = "Bulbasaur" }
        (if encounter.shinyStatus then
            PokemonImage.Shiny

         else
            PokemonImage.Normal
        )


animatedZoom : EncounterModel -> Float
animatedZoom model =
    Animator.linear model.zoom Animator.at


fallProgress : EncounterModel -> Float
fallProgress model =
    Animator.linear model.trainerState
        (\state ->
            case state of
                CatchState.Falling ->
                    Animator.at 1

                CatchState.Catching ->
                    Animator.at 1

                _ ->
                    Animator.at 0 |> Animator.leaveSmoothly 0.6
        )


view : Loaded -> EncounterModel -> Element RunningMsg
view encounter model =
    let
        throwProgress =
            Animator.linear model.trainerState
                (\state ->
                    case state of
                        CatchState.Throw ->
                            Animator.at 1

                        CatchState.Catching ->
                            Animator.at 1

                        CatchState.Falling ->
                            Animator.at 1

                        _ ->
                            Animator.at 0
                )
    in
    Element.column
        [ width fill
        , height fill
        , Element.behindContent <|
            Element.image
                [ width fill
                , height fill
                , Element.scale (animatedZoom model)
                , Element.htmlAttribute <| Html.Attributes.style "transform-origin" "top right"

                -- Hacky fix to make image extend past viewport
                , Element.htmlAttribute <| Html.Attributes.style "display" "flex"
                ]
                { src = "/images/bg-grass.png", description = "Background" }
        , Element.behindContent (pokemonImage encounter model)
        , Element.behindContent
            (PokeballSprite.pokeballImage
                [ Element.moveRight (throwProgress * (toFloat model.session.width - (pokeballWidth + animatedZoom model * pokemonWidth) / 2) - 100)
                , Element.moveDown (progressToY model throwProgress + fallProgress model * (animatedZoom model * (pokemonWidth - 50)) / 2 - 100)

                --, Element.transparent (throwProgress < 0.1)
                ]
                pokeballWidth
                model.trainerState
            )
        ]
        [ Safari.Overworld.View.TrainerImage.trainerImage [ Element.alignBottom, Element.moveDown 100, Element.moveLeft 50 ] 400 model.trainerState
        , displayOptions model
        ]



-- Using the formula (2e + 2s - 4m)x² + (4m - 3s -e) + s where s is the value at start, e at end and m at 0.5
-- 150 as magical constant


progressToY : EncounterModel -> Float -> Float
progressToY model x =
    let
        s =
            toFloat model.session.height - 150

        m =
            20

        e =
            (animatedZoom model * pokemonWidth - pokeballWidth) / 2
    in
    (2 * e + 2 * s - 4 * m) * x ^ 2 + (4 * m - 3 * s - e) * x + s


displayOptions : EncounterModel -> Element RunningMsg
displayOptions _ =
    Element.row
        [ alignBottom
        , Border.roundEach { topLeft = 20, topRight = 20, bottomLeft = 0, bottomRight = 0 }
        , Background.color Colors.darkGray
        , width fill
        , padding 20
        , Border.shadow { offset = ( 2, -2 ), size = 2, blur = 5, color = Colors.black }
        , spacing 20
        ]
        [ displayOption "Throw Ball" ThrowBall, displayOption "Go Nearer" GoNearer, displayOption "Reset" Reset ]


displayOption : String -> RunningMsg -> Element RunningMsg
displayOption label onPress =
    Input.button
        [ padding 20
        , Background.color Colors.midGray
        , Border.rounded 10
        , width fill
        , Font.center
        ]
        { label = Element.text label, onPress = Just onPress }
