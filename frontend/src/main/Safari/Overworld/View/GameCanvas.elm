module Safari.Overworld.View.GameCanvas exposing (view)

import Api.Textures exposing (Textures)
import Safari.Overworld.Types.Camera as Camera
import Data.Units exposing (TileUnit(..))
import Dict
import Domain.ConstrainedPosition
import Domain.Encounter
import Domain.Player
import Domain.QueueablePosition
import Html exposing (Html)
import Html.Attributes exposing (height, style, width)
import Safari.Overworld.View.MapEntity as MapEntity
import Safari.Overworld.View.PokemonEntity as PokemonEntity
import Safari.Overworld.View.TrainerEntity as TrainerEntity
import Shared.Session exposing (Session)
import Shared.Types.TrainerState exposing (TrainerState)
import WebGL
import WebGL.Texture


type alias Model a =
    { a
        | trainer : TrainerState
        , textures : Textures
    }


view : Session -> Model b -> Html msg
view session model =
    case ( List.head model.textures.tileMaps, Domain.ConstrainedPosition.getExact session.position ) of
        ( Just tileMapLayer, Just pos ) ->
            let
                ( totalTilesX, totalTilesY ) =
                    tileMapLayer |> WebGL.Texture.size |> Tuple.mapBoth toFloat toFloat

                camera =
                    Camera.mkCamera pos session { x = TileUnit totalTilesX, y = TileUnit totalTilesY }
            in
            WebGL.toHtml
                [ width session.width
                , height session.height
                , style "display" "block"
                , style "position" "fixed"
                , style "left" "0"
                , style "top" "0"
                ]
            <|
                List.map
                    (\tileMap ->
                        MapEntity.entity camera
                            model.textures.tileSet
                            tileMap
                            { x = TileUnit 0, y = TileUnit 0 }
                    )
                    model.textures.tileMaps
                    ++ (Domain.Encounter.getEncounters session.encounters
                            |> List.filterMap
                                (\enc ->
                                    Dict.get enc.pokemonId session.pokemonTextures
                                        |> Maybe.map (PokemonEntity.pokemonEntity camera enc)
                                )
                       )
                    ++ (Domain.Player.getPlayers session.players
                            |> List.map (\{ state, position } -> TrainerEntity.entity camera model.textures.trainer { state = state, position = Domain.QueueablePosition.getExact position })
                       )
                    ++ [ TrainerEntity.entity camera
                            model.textures.trainer
                            { state = model.trainer, position = pos }
                       ]

        _ ->
            Html.text "Failed!"
