module Safari.Overworld.View.MapEntity exposing (entity)

import Safari.Overworld.View.CommonGL as CommonGL
import Config
import Safari.Overworld.Types.Camera as Camera exposing (Camera)
import Data.Units exposing (TileUnit, unWebGLUnit)
import Domain.Coordinate exposing (Coordinate)
import Math.Matrix4 as Mat4 exposing (Mat4)
import Math.Vector2 exposing (Vec2, vec2)
import WebGL
import WebGL.Texture as Texture exposing (Texture)


entity : Camera -> Texture -> Texture -> Coordinate TileUnit -> WebGL.Entity
entity camera tileSet tileMap coord =
    let
        ( totalTilesX, totalTilesY ) =
            Texture.size tileMap |> Tuple.mapBoth toFloat toFloat

        scalingX =
            totalTilesX * Config.tileSize / toFloat (Camera.getScreenWidth camera)

        scalingY =
            totalTilesY * Config.tileSize / toFloat (Camera.getScreenHeight camera)

        trans =
            Camera.calcWebGLCoord camera coord

        translateX =
            unWebGLUnit trans.x / scalingX

        translateY =
            unWebGLUnit trans.y / scalingY
    in
    WebGL.entityWith CommonGL.settings
        CommonGL.vertexShader
        fragmentShader
        CommonGL.mesh
        { perspective =
            Mat4.identity
                |> Mat4.scale3 scalingX scalingY 1
                --|> Mat4.translate3 ((scalingX - 1) / scalingX) (-(scalingY - 1) / scalingY) 0
                |> Mat4.translate3 (translateX + 1) (translateY - 1) 0
        , texture = tileSet
        , spriteSheetSize = CommonGL.textureSize tileSet
        , spriteSize = vec2 16 16
        , encodedMap = tileMap
        , tileSize = vec2 Config.tileSize Config.tileSize
        , encodedMapSize = vec2 totalTilesX totalTilesY
        }


type alias Uniforms =
    { perspective : Mat4
    , texture : Texture
    , encodedMap : Texture
    , encodedMapSize : Vec2
    , spriteSheetSize : Vec2
    , spriteSize : Vec2
    , tileSize : Vec2
    }


fragmentShader : WebGL.Shader {} Uniforms { vcoord : Vec2 }
fragmentShader =
    [glsl|
        precision mediump float;
        uniform sampler2D texture;
        uniform sampler2D encodedMap;
        uniform vec2 encodedMapSize;
        uniform vec2 spriteSheetSize;   // In px
        uniform vec2 spriteSize;        // In px
        uniform vec2 tileSize;
        varying vec2 vcoord;

        vec2 getIndex(in vec2 coord) {
          vec2 tile = floor(vec2(coord.x, 1.0 - coord.y) * encodedMapSize);
          float indexX = (tile.x + 0.5) / encodedMapSize.x;
          float indexY = 1.0 - ((tile.y + 0.5) / encodedMapSize.y);
          return vec2(indexX, indexY); 
        }

        void main () {
          vec2 indexXY = getIndex(vcoord);
          vec4 encodedColor = texture2D(encodedMap, indexXY);
          if (encodedColor.a == 0.0 || indexXY.x > 1.0 || indexXY.y < 0.0) {
              discard;
          }

          float index = floor(encodedColor.r * 256.0);

          float w = spriteSheetSize.x;
          float h = spriteSheetSize.y;
          //-- Normalize sprite size (0.0-1.0) - Percentage a sprite takes up of the sprite sheet
          float dx = spriteSize.x / w;
          float dy = spriteSize.y / h;
          float cols = w / spriteSize.x;
          //-- From linear index to row/col pair
          float col = mod(index, cols);
          float row = floor(index / cols);
          float xxx = fract(vcoord.x * encodedMapSize.x);
          //-- magic constants fix issue with overlapping tiles due to floating point precision
          float yyy = 0.99 * fract((1.0 - vcoord.y) * encodedMapSize.y);
          vec2 uv = vec2(dx * (col + xxx), 0.99999 - dy * (row + yyy));
          gl_FragColor = texture2D(texture, uv);
        }
    |]
