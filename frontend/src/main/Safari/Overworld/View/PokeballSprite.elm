module Safari.Overworld.View.PokeballSprite exposing (pokeballImage)

import Animator
import Element exposing (Element, height, px, width)
import Element.Background
import Html.Attributes
import Safari.Overworld.Types.CatchState as CatchState exposing (CatchState)



-- Pokeball Sprite Image dimensions: (29px, 499px), 17 images


pokeballImage : List (Element.Attribute msg) -> Int -> Animator.Timeline CatchState -> Element msg
pokeballImage extraAttrs w timeline =
    let
        part =
            Animator.step timeline stateToFrame

        animatedScale =
            round <| toFloat w * Animator.linear timeline stateToScale
    in
    Element.el
        (extraAttrs
            ++ [ width (px animatedScale)
               , height (px animatedScale)
               , Element.Background.image "/images/sprites-pokeball.png"
               , Element.htmlAttribute <| Html.Attributes.style "background-position" ("0 -" ++ String.fromInt (part * (animatedScale + 1)) ++ "px")
               ]
        )
        Element.none


frames : List (Animator.Frames Int)
frames =
    List.map Animator.frame [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]


stateToScale : CatchState -> Animator.Movement
stateToScale state =
    case state of
        CatchState.Throw ->
            Animator.at 0.7

        CatchState.Catching ->
            Animator.at 0.7

        CatchState.Falling ->
            Animator.at 0.7

        _ ->
            Animator.at 1


stateToFrame : CatchState -> Animator.Frames Int
stateToFrame state =
    case state of
        CatchState.Throw ->
            Animator.walk 0 frames

        CatchState.Catching ->
            Animator.framesWith
                { resting =
                    Animator.cycle (Animator.fps 5)
                        [ Animator.hold 400 11
                        , Animator.hold 80 12
                        , Animator.hold 80 13
                        , Animator.hold 80 14
                        , Animator.hold 80 15
                        , Animator.hold 80 16
                        ]
                , transition = Animator.frame 0
                }

        CatchState.Falling ->
            Animator.walk 10 [ Animator.hold 2 0 ]

        _ ->
            Animator.frame 0
