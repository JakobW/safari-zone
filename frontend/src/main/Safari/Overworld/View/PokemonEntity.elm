module Safari.Overworld.View.PokemonEntity exposing (..)

import Api.Textures exposing (SpriteMap(..))
import Safari.Overworld.View.CommonGL as CommonGL
import Config
import Safari.Overworld.Types.Camera as Camera exposing (Camera)
import Shared.Types.FrameBased
import Data.Units exposing (unWebGLUnit)
import Domain.Direction as Direction exposing (Direction)
import Domain.Encounter exposing (Action, CurrentAction(..), Encounter)
import Domain.QueueablePosition
import Math.Matrix4 as Mat4 exposing (Mat4)
import Math.Vector2 exposing (Vec2)
import WebGL
import WebGL.Texture exposing (Texture)


pokemonEntity : Camera -> Encounter -> Texture -> WebGL.Entity
pokemonEntity camera { direction, action, shinyStatus, position } spriteMap =
    let
        index =
            dirToIndex direction action shinyStatus

        { row, col, spritePercent, texture } =
            Api.Textures.indexToTextureData index (SpriteMap spriteMap Config.pokemonSpriteSize)

        scalingX =
            Config.pokemonSize.x / toFloat (Camera.getScreenWidth camera)

        scalingY =
            Config.pokemonSize.y / toFloat (Camera.getScreenHeight camera)

        pixelFixX =
            Config.trainerSize.x / (2 * toFloat (Camera.getScreenWidth camera))

        pixelFixY =
            Config.trainerSize.y / (2 * toFloat (Camera.getScreenHeight camera))

        trans =
            Camera.calcWebGLCoord camera (Domain.QueueablePosition.getExact position)
    in
    WebGL.entityWith
        CommonGL.settings
        CommonGL.vertexShader
        fragmentShader
        CommonGL.mesh
        { perspective =
            Mat4.makeScale3 scalingX scalingY 1
                |> Mat4.translate3 ((unWebGLUnit trans.x + pixelFixX) / scalingX) ((unWebGLUnit trans.y - pixelFixY) / scalingY) 0
        , row = toFloat row
        , col = toFloat col
        , spriteSheet = texture
        , spritePercent = spritePercent
        }


type alias Uniforms =
    { perspective : Mat4
    , spriteSheet : Texture
    , spritePercent : Vec2
    , row : Float
    , col : Float
    }


fragmentShader : WebGL.Shader {} Uniforms { vcoord : Vec2 }
fragmentShader =
    [glsl|
        precision mediump float;
        uniform sampler2D spriteSheet;
        uniform vec2 spritePercent;
        uniform float row;
        uniform float col;
        varying vec2 vcoord;

        void main() {
          vec2 uv = vec2(spritePercent.x * (col + vcoord.x), 1.0 - spritePercent.y * (row + 1.0 - vcoord.y));
          gl_FragColor = texture2D(spriteSheet, uv);
        }
    |]


dirToIndex : Direction -> Action -> Bool -> Int
dirToIndex dir action isShiny =
    let
        normalIndex =
            case ( dir, Shared.Types.FrameBased.current action ) of
                ( Direction.L, StepLeft ) ->
                    0

                ( Direction.L, StepRight ) ->
                    1

                ( Direction.R, StepLeft ) ->
                    5

                ( Direction.R, StepRight ) ->
                    6

                ( Direction.U, StepLeft ) ->
                    9

                ( Direction.U, StepRight ) ->
                    10

                ( Direction.D, StepLeft ) ->
                    13

                ( Direction.D, StepRight ) ->
                    14
    in
    if isShiny then
        normalIndex + 2

    else
        normalIndex
