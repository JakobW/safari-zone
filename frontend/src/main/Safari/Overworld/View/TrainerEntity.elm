module Safari.Overworld.View.TrainerEntity exposing (entity)

import Api.Textures exposing (SpriteMap, indexToTextureData)
import Config
import Data.Units exposing (TileUnit, unWebGLUnit)
import Domain.Coordinate exposing (Coordinate)
import Math.Matrix4 as Mat4 exposing (Mat4)
import Math.Vector2 exposing (Vec2)
import Safari.Overworld.Types.Camera as Camera exposing (Camera)
import Safari.Overworld.View.CommonGL as CommonGL
import Shared.Types.TrainerState as TrainerState exposing (TrainerState)
import WebGL
import WebGL.Texture exposing (Texture)


entity : Camera -> SpriteMap -> { a | state : TrainerState, position : Coordinate TileUnit } -> WebGL.Entity
entity camera trainer { state, position } =
    let
        index =
            TrainerState.dirToIndex state

        { row, col, spritePercent, texture } =
            indexToTextureData index trainer

        scalingX =
            Config.trainerSize.x / toFloat (Camera.getScreenWidth camera)

        scalingY =
            Config.trainerSize.y / toFloat (Camera.getScreenHeight camera)

        pixelFixX =
            Config.trainerSize.x / (2 * toFloat (Camera.getScreenWidth camera))

        pixelFixY =
            Config.trainerSize.y / (2 * toFloat (Camera.getScreenHeight camera))

        trans =
            Camera.calcWebGLCoord camera position
    in
    WebGL.entityWith
        CommonGL.settings
        CommonGL.vertexShader
        fragmentShader
        CommonGL.mesh
        { perspective =
            Mat4.makeScale3 scalingX scalingY 1
                |> Mat4.translate3 ((unWebGLUnit trans.x + pixelFixX) / scalingX) ((unWebGLUnit trans.y - pixelFixY) / scalingY) 0
        , row = toFloat row
        , col = toFloat col
        , spriteSheet = texture
        , spritePercent = spritePercent
        }


type alias Uniforms =
    { perspective : Mat4
    , spriteSheet : Texture
    , spritePercent : Vec2
    , row : Float
    , col : Float
    }


fragmentShader : WebGL.Shader {} Uniforms { vcoord : Vec2 }
fragmentShader =
    [glsl|
        precision mediump float;
        uniform sampler2D spriteSheet;
        uniform vec2 spritePercent;
        uniform float row;
        uniform float col;
        varying vec2 vcoord;

        void main() {
          vec2 uv = vec2(spritePercent.x * (col + vcoord.x), 1.0 - spritePercent.y * (row + 1.0 - vcoord.y));
          gl_FragColor = texture2D(spriteSheet, uv);
        }
    |]
