module Safari.Overworld.View.TrainerImage exposing (..)

import Animator
import Element exposing (Element, height, px, width)
import Element.Background
import Html.Attributes
import Safari.Overworld.Types.CatchState as CatchState exposing (CatchState)



-- Trainer Image dimensions: (645px, 118px), 5 images


trainerImage : List (Element.Attribute msg) -> Int -> Animator.Timeline CatchState -> Element msg
trainerImage extraAttrs w timeline =
    let
        part =
            Animator.step timeline stateToFrame

        h =
            round <| toFloat w * 118 / 129
    in
    Element.el
        (extraAttrs
            ++ [ width (px w)
               , height (px h)
               , Element.Background.image "/images/sprites-catch.png"
               , Element.htmlAttribute <| Html.Attributes.style "background-position" ("-" ++ String.fromInt (part * w + 4) ++ "px 0")
               , Element.moveRight <| sin (Animator.move timeline crawling) * 50
               , Element.moveDown <| sin (Animator.move timeline crawling) * 20
               ]
        )
        Element.none


crawling : CatchState -> Animator.Movement
crawling state =
    case state of
        CatchState.Crawling ->
            Animator.loop Animator.verySlowly (Animator.zigzag 0 1)

        _ ->
            Animator.at 0


stateToFrame : CatchState -> Animator.Frames Int
stateToFrame state =
    case state of
        CatchState.StartThrow ->
            Animator.walk 0 (List.map Animator.frame [ 1, 2, 3 ])

        CatchState.Throw ->
            Animator.frame 4

        _ ->
            Animator.frame 0
