module Shared.Effect exposing (..)

import Browser.Navigation



{-
   This module defines a small effect system for oversights in Elms Command Design System.
   In essence, any Program with Navigation is not testable, since it is impossible to obtain a navigation key outside of Browser.application.
   So instead we define an effect to navigate to a certain route.
   For compatability, there is an effect to run an arbitrary Command.
-}


type Effect msg
    = UseKey (Browser.Navigation.Key -> Cmd msg)
    | Effects (List (Effect msg))
    | NoEffect
    | CommandEffect (Cmd msg)


goBack : Effect msg
goBack =
    UseKey (\key -> Browser.Navigation.back key 1)


runEffect : Browser.Navigation.Key -> Effect msg -> Cmd msg
runEffect key effect =
    case effect of
        UseKey f ->
            f key

        Effects effs ->
            List.map (runEffect key) effs |> Cmd.batch

        NoEffect ->
            Cmd.none

        CommandEffect cmd ->
            cmd


map : (a -> b) -> Effect a -> Effect b
map f eff =
    case eff of
        UseKey uk ->
            UseKey (uk >> Cmd.map f)

        Effects effs ->
            Effects <| List.map (map f) effs

        NoEffect ->
            NoEffect

        CommandEffect cmd ->
            CommandEffect <| Cmd.map f cmd


withNoEffect : model -> ( model, Effect msg )
withNoEffect model =
    ( model, NoEffect )
