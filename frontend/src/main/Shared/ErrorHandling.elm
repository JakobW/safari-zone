module Shared.ErrorHandling exposing (..)

import Element exposing (Element, centerX, centerY)
import Element.Font as Font
import Http exposing (Error(..))


type WithLoadAndError x a
    = Loading
    | Success a
    | Error x


title : (a -> String) -> WithLoadAndError x a -> String
title successToString state =
    case state of
        Loading ->
            "Loading..."

        Success a ->
            successToString a

        Error _ ->
            "Something went wrong"


describeHttpError : Http.Error -> String
describeHttpError err =
    case err of
        BadBody errMsg ->
            errMsg

        BadUrl badUrl ->
            "Bad Url: " ++ badUrl

        Timeout ->
            "Ran into timeout"

        NetworkError ->
            "Lost connection"

        BadStatus status ->
            "Received error status: " ++ String.fromInt status


viewHttp : Http.Error -> Element msg
viewHttp err =
    case err of
        Http.BadUrl badUrl ->
            Element.text <| "Bad Url: " ++ badUrl

        Http.Timeout ->
            Element.column [ centerX, centerY ]
                [ Element.image [ centerX ] { src = "/images/slowpoke.png", description = "Slowpoke indicating a timeout" }
                , bigCenteredText "Request timeout."
                ]

        Http.NetworkError ->
            Element.column [ centerX, centerY ]
                [ Element.image [ centerX ] { src = "/images/slowpoke.png", description = "Slowpoke indicating a network error" }
                , bigCenteredText "Connection lost."
                ]

        Http.BadStatus status ->
            Element.column [ centerX, centerY ]
                [ Element.image [ centerX ] { src = "/images/slowbro.png", description = "Slowbro indicating a bad status code" }
                , bigCenteredText (String.fromInt status)
                ]

        Http.BadBody decodeErrorMsg ->
            Element.column [ centerX, centerY ]
                [ Element.image [ centerX ] { src = "/images/psyduck.png", description = "Psyduck indicating a decoding error" }
                , Element.paragraph [] [ Element.text decodeErrorMsg ]
                ]


bigCenteredText : String -> Element msg
bigCenteredText =
    Element.el [ centerX, Font.size 50, Font.bold ] << Element.text
