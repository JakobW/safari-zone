module Shared.I18n exposing (I18n, W_I18n, getProperties, init, setI18n, t)

import Http
import Json.Decode as D
import Json.Decode.Pipeline as D
import Shared.Types.BackendUrl as BackendUrl exposing (BackendUrl)
import Shared.Types.Generic exposing (Setter)
import Shared.Types.Language as Language exposing (Language)
import Shared.Types.Placeholder exposing (placeholder1)


type alias W_I18n s =
    { s | i18n : I18n }


setI18n : I18n -> Setter (W_I18n s)
setI18n i18n s =
    { s | i18n = i18n }


type I18n
    = Loaded I18nInstance
    | Pending


init : I18n
init =
    Pending


type alias I18nInstance =
    { dexAppraisal0 : String
    , dexAppraisal1 : String
    , dexAppraisal2 : String
    , dexAppraisal3 : String
    , dexAppraisal4 : String
    , dexAppraisal5 : String
    , dexAppraisalHeadline : String
    , safariEncounterTitle : String -> String
    }


noVarKey : List String -> D.Decoder (String -> b) -> D.Decoder b
noVarKey key =
    D.requiredAt key D.string


singleVarKey : List String -> D.Decoder ((String -> String) -> b) -> D.Decoder b
singleVarKey key =
    placeholder1 |> D.requiredAt key


decoder : D.Decoder I18nInstance
decoder =
    D.succeed I18nInstance
        |> noVarKey [ "dex", "appraisal", "0" ]
        |> noVarKey [ "dex", "appraisal", "1" ]
        |> noVarKey [ "dex", "appraisal", "2" ]
        |> noVarKey [ "dex", "appraisal", "3" ]
        |> noVarKey [ "dex", "appraisal", "4" ]
        |> noVarKey [ "dex", "appraisal", "5" ]
        |> noVarKey [ "dex", "appraisal", "headline" ]
        |> singleVarKey [ "safari", "encounter", "title" ]


t : (I18nInstance -> String) -> I18n -> String
t key i18n =
    case i18n of
        Pending ->
            "..."

        Loaded instance ->
            key instance


getProperties : (Result Http.Error I18n -> msg) -> { s | language : Language, backendUrl : BackendUrl } -> Cmd msg
getProperties callback { language, backendUrl } =
    Http.get
        { url = BackendUrl.toString backendUrl ++ "i18n/" ++ Language.toString language ++ ".json"
        , expect = Http.expectJson callback (decoder |> D.map Loaded)
        }
