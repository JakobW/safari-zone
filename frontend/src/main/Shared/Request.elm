module Shared.Request exposing (AuthorizedRequest, W_Api, andThen, andThenTask, combine, get, map, mapError, send)

import Shared.Types.BackendUrl exposing (BackendUrl)
import Http
import Json.Decode as D
import Shared.Effect exposing (Effect(..))
import Shared.Types.Auth as Auth
import Task exposing (Task)


type alias W_Api a =
    { a | auth : Auth.Authentification, backendUrl : BackendUrl }


type AuthorizedRequest x a
    = AuthorizedRequest (BackendUrl -> Auth.Authentification -> Task x a)


getTask : AuthorizedRequest x a -> BackendUrl -> Auth.Authentification -> Task x a
getTask (AuthorizedRequest task) =
    task


get : String -> D.Decoder a -> AuthorizedRequest Http.Error a
get url =
    requestTask "GET" Http.emptyBody url >> AuthorizedRequest


lift : (Task x a -> Task y b) -> AuthorizedRequest x a -> AuthorizedRequest y b
lift f request =
    AuthorizedRequest <|
        \backendUrl -> f << getTask request backendUrl


map : (a -> b) -> AuthorizedRequest x a -> AuthorizedRequest x b
map =
    lift << Task.map


combine : AuthorizedRequest x a -> AuthorizedRequest x b -> AuthorizedRequest x ( a, b )
combine ar =
    andThen (\b -> map (\a -> ( a, b )) ar)


andThen : (a -> AuthorizedRequest x b) -> AuthorizedRequest x a -> AuthorizedRequest x b
andThen nextRequest firstRequest =
    AuthorizedRequest <|
        \url token -> getTask firstRequest url token |> Task.andThen (\a -> getTask (nextRequest a) url token)


andThenTask : (a -> Task x b) -> AuthorizedRequest x a -> AuthorizedRequest x b
andThenTask nextTask firstRequest =
    AuthorizedRequest <|
        \token -> getTask firstRequest token >> Task.andThen nextTask


mapError : (x -> y) -> AuthorizedRequest x a -> AuthorizedRequest y a
mapError f =
    lift <| Task.mapError f


send : (Result x a -> msg) -> W_Api model -> AuthorizedRequest x a -> Effect msg
send callback { backendUrl, auth } request =
    getTask request backendUrl auth |> Task.attempt callback |> CommandEffect


requestTask : String -> Http.Body -> String -> D.Decoder a -> BackendUrl -> Auth.Authentification -> Task Http.Error a
requestTask method body path decoder backendUrl auth =
    Http.task
        { url = Shared.Types.BackendUrl.setPath path backendUrl |> Shared.Types.BackendUrl.toString
        , method = method
        , body = body
        , headers = Auth.authToHeaders auth
        , resolver = resolver decoder
        , timeout = Nothing
        }


resolver : D.Decoder a -> Http.Resolver Http.Error a
resolver decoder =
    Http.stringResolver <|
        \res ->
            case res of
                Http.BadUrl_ url ->
                    Err (Http.BadUrl url)

                Http.Timeout_ ->
                    Err Http.Timeout

                Http.NetworkError_ ->
                    Err Http.NetworkError

                Http.BadStatus_ metadata _ ->
                    Err (Http.BadStatus metadata.statusCode)

                Http.GoodStatus_ _ body ->
                    D.decodeString decoder body
                        |> Result.mapError (Http.BadBody << D.errorToString)
