module Shared.Session exposing (..)

import Animation
import Animator
import Dict exposing (Dict)
import Domain.ConstrainedPosition exposing (ConstrainedPosition)
import Domain.Coordinate exposing (Coordinate)
import Domain.DexEntry exposing (DexEntry)
import Domain.Encounter exposing (Encounters)
import Domain.Player exposing (Players)
import Json.Decode as D
import Main.Flags
import Ports.WebSocket as WS
import Route exposing (Route)
import Shared.I18n as I18n
import Shared.Types.Auth exposing (Authentification)
import Shared.Types.BackendUrl exposing (BackendUrl)
import Shared.Types.Generic exposing (Lift, Setter, getAndSet)
import Shared.Types.Language as Language exposing (Language)
import TrainerSelect.Types.TrainerMetadata exposing (TrainerMetadata)
import Url
import WebGL.Texture exposing (Texture)


type alias W_Viewport a =
    { a | width : Int, height : Int }


type alias W_WS a =
    { a | connection : Maybe WS.Connection }


type alias W_Position a =
    { a | position : ConstrainedPosition }


type alias W_Route a =
    { a | route : Route }


type alias W_ErrorDump a =
    { a | errorDump : List String }


type alias Session =
    { language : Language
    , i18n : I18n.I18n
    , timeline : Animator.Timeline Animation.State
    , dexEntries : List DexEntry
    , players : Players
    , encounters : Encounters
    , pokemonTextures : Dict Int Texture
    , position : ConstrainedPosition
    , selectedTrainer : Maybe ( Int, TrainerMetadata )
    , height : Int
    , width : Int
    , route : Route
    , auth : Authentification
    , backendUrl : BackendUrl
    , connection : Maybe WS.Connection
    , errorDump : List String
    }


init : D.Value -> Url.Url -> Result D.Error Session
init jsFlags url =
    Main.Flags.readFlags jsFlags
        |> Result.map
            (\flags ->
                { width = flags.x
                , height = flags.y
                , language = Language.fromString flags.language
                , i18n = I18n.init
                , dexEntries = []
                , route = Route.toRoute url
                , backendUrl = flags.backendUrl
                , auth = flags.auth
                , players = Dict.empty
                , encounters = Dict.empty
                , pokemonTextures = Dict.empty
                , connection = Nothing
                , selectedTrainer = Nothing
                , timeline = Animation.init
                , position = Domain.ConstrainedPosition.noPosition
                , errorDump = []
                }
            )


defaultGetSession : { m | session : Session } -> Session
defaultGetSession =
    .session


defaultSetSession : Session -> Setter { m | session : Session }
defaultSetSession session model =
    { model | session = session }


defaultModifySession : Lift Session { m | session : Session }
defaultModifySession =
    getAndSet defaultGetSession defaultSetSession


setAuth : Authentification -> Setter { a | auth : Authentification }
setAuth auth session =
    { session | auth = auth }


addError : String -> Setter (W_ErrorDump a)
addError err a =
    { a | errorDump = err :: a.errorDump }


addErrorS : String -> Setter { a | session : W_ErrorDump b }
addErrorS err a =
    { a | session = addError err a.session }


setViewport : ( Int, Int ) -> Setter (W_Viewport a)
setViewport ( width, height ) model =
    { model | width = width, height = height }


setConnection : WS.Connection -> Coordinate Int -> Setter (W_Position (W_WS a))
setConnection conn coord model =
    { model | connection = Just conn, position = Domain.ConstrainedPosition.addCoord coord model.position }


setRoute : Route -> Setter (W_Route a)
setRoute route model =
    { model | route = route }


setDexEntries : List DexEntry -> Setter { s | dexEntries : List DexEntry }
setDexEntries dexEntries s =
    { s | dexEntries = dexEntries }


modifyEncounters : Lift Encounters { a | encounters : Encounters }
modifyEncounters f a =
    { a | encounters = f a.encounters }


modifyPlayers : Lift Players { a | players : Players }
modifyPlayers f a =
    { a | players = f a.players }


selectTrainer : trainer -> Setter { s | selectedTrainer : Maybe trainer }
selectTrainer trainer s =
    { s | selectedTrainer = Just trainer }


addPokemonTexture : Int -> Texture -> Setter { s | pokemonTextures : Dict Int Texture }
addPokemonTexture k v =
    modifyPokemonTextures (Dict.insert k v)


modifyPokemonTextures : Lift (Dict Int Texture) { s | pokemonTextures : Dict Int Texture }
modifyPokemonTextures f a =
    { a | pokemonTextures = f a.pokemonTextures }
