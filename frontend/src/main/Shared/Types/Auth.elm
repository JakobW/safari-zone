module Shared.Types.Auth exposing
    ( AuthProvider
    , Authentification
    , Token
    , authToHeaders
    , decodeAuthentification
    , encodeAuthentification
    , refreshAuth
    , registerRefreshReminder
    )

import Shared.Types.BackendUrl exposing (BackendUrl)
import Http
import Json.Decode as D
import Json.Decode.Pipeline as D
import Json.Encode as E
import Time


type Token
    = Token String


type AuthProvider
    = AndrenaProvider
    | LocalProvider


authProviderToString : AuthProvider -> String
authProviderToString provider =
    case provider of
        AndrenaProvider ->
            "andrena"

        LocalProvider ->
            "local"


decodeAuthProvider : D.Decoder AuthProvider
decodeAuthProvider =
    D.string
        |> D.andThen
            (\s ->
                case s of
                    "andrena" ->
                        D.succeed AndrenaProvider

                    "local" ->
                        D.succeed LocalProvider

                    x ->
                        D.fail <| x ++ " is not a valid auth provider"
            )


authProviderToHeader : AuthProvider -> Http.Header
authProviderToHeader =
    Http.header "X-Auth-Provider" << authProviderToString


encodeAuthProvider : AuthProvider -> E.Value
encodeAuthProvider =
    authProviderToString >> E.string


type alias Authentification =
    { token : Token
    , provider : AuthProvider
    , lifespan : Int
    }


registerRefreshReminder : Authentification -> msg -> Sub msg
registerRefreshReminder { lifespan } =
    always >> Time.every (1000 * 2 * toFloat lifespan / 3)


refreshAuth : Authentification -> (Result Http.Error Authentification -> msg) -> BackendUrl -> Cmd msg
refreshAuth auth =
    case auth.provider of
        LocalProvider ->
            refreshLocalToken

        _ ->
            \_ _ -> Cmd.none


refreshLocalToken : (Result Http.Error Authentification -> msg) -> BackendUrl -> Cmd msg
refreshLocalToken callback backendUrl =
    Http.post
        { url = Shared.Types.BackendUrl.setPath "/auth/refresh" backendUrl |> Shared.Types.BackendUrl.toString
        , expect = Http.expectJson callback (D.map localAuthToAuth decodeLocalAuthData)
        , body = Http.emptyBody
        }


localAuthToAuth : LocalAuthData -> Authentification
localAuthToAuth { accessToken, lifespan } =
    { token = accessToken, lifespan = lifespan, provider = LocalProvider }


type alias LocalAuthData =
    { accessToken : Token
    , lifespan : Int
    }


decodeLocalAuthData : D.Decoder LocalAuthData
decodeLocalAuthData =
    D.succeed LocalAuthData
        |> D.required "accessToken" decodeToken
        |> D.required "lifespan" D.int


encodeAuthentification : Authentification -> E.Value
encodeAuthentification { token, provider } =
    E.object [ ( "token", encodeToken token ), ( "provider", encodeAuthProvider provider ) ]


decodeAuthentification : D.Decoder Authentification
decodeAuthentification =
    D.succeed Authentification
        |> D.required "token" decodeToken
        |> D.required "provider" decodeAuthProvider
        |> D.required "lifespan" D.int


encodeToken : Token -> E.Value
encodeToken (Token token) =
    E.string token


decodeToken : D.Decoder Token
decodeToken =
    D.string |> D.map Token


tokenToHeader : Token -> Http.Header
tokenToHeader (Token token) =
    Http.header "authorization" ("Bearer " ++ token)


authToHeaders : Authentification -> List Http.Header
authToHeaders { token, provider } =
    [ tokenToHeader token
    , authProviderToHeader provider
    ]
