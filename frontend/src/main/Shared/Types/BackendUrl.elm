module Shared.Types.BackendUrl exposing (..)

import Json.Decode as D
import Url


type BackendUrl
    = BackendUrl Url.Url


backendUrlDecoder : D.Decoder BackendUrl
backendUrlDecoder =
    D.string
        |> D.andThen
            (\maybeUrl ->
                case Url.fromString maybeUrl of
                    Just url ->
                        D.succeed <| BackendUrl url

                    Nothing ->
                        D.fail "Given URL is not of the correct format."
            )


setPath : String -> BackendUrl -> BackendUrl
setPath path (BackendUrl url) =
    BackendUrl { url | path = path }


toString : BackendUrl -> String
toString =
    toUrl >> Url.toString


toUrl : BackendUrl -> Url.Url
toUrl (BackendUrl url) =
    url
