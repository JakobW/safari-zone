module Shared.Types.Document exposing (..)

import Browser
import Element exposing (Element)
import Element.Background as Background
import Shared.View.Colors


type alias Document msg =
    { body : Element msg
    , title : String
    }


render : List (Element.Attribute msg) -> Document msg -> Browser.Document msg
render extraAttrs { body, title } =
    { body =
        [ Element.layout
            (Background.color Shared.View.Colors.midGray
                :: extraAttrs
            )
            body
        ]
    , title = title
    }
