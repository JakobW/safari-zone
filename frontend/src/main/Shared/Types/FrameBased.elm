module Shared.Types.FrameBased exposing (..)


type alias FrameBased a =
    { curr : a
    , next : a -> a
    , maxFrames : Int
    , currFrames : Int
    }


current : FrameBased a -> a
current =
    .curr


initFrameBased : a -> (a -> a) -> Int -> FrameBased a
initFrameBased curr next maxFrames =
    { curr = curr
    , next = next
    , maxFrames = maxFrames
    , currFrames = 0
    }


nextFrame : FrameBased a -> FrameBased a
nextFrame fb =
    if fb.currFrames + 1 > fb.maxFrames then
        { fb | curr = fb.next fb.curr, currFrames = 0 }

    else
        { fb | currFrames = fb.currFrames + 1 }

immediately : FrameBased a -> a -> FrameBased a
immediately fb immediate =
  { fb | curr = immediate,  currFrames = 0}        
