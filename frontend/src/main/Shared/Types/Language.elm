module Shared.Types.Language exposing (..)

type Language
    = DE
    | EN


toString : Language -> String
toString lang =
    case lang of
        DE ->
            "de"

        EN ->
            "en"


fromString : String -> Language
fromString str =
    case String.toLower str of
        "de" ->
            DE

        "de-de" ->
            DE

        _ ->
            EN