module Shared.Types.Placeholder exposing (placeholder1, placeholder2)

import Array
import Json.Decode as D
import Parser exposing (..)


type alias Placeholder =
    { segments : List ( String, Int )
    , end : String
    }


addSegment : ( String, Int ) -> Placeholder -> Placeholder
addSegment segment ph =
    { ph | segments = segment :: ph.segments }


emptyPlaceholder : String -> Placeholder
emptyPlaceholder start =
    { end = start, segments = [] }


placeholder1 : D.Decoder (String -> String)
placeholder1 =
    commonDecoder placeholder1Parser


placeholder2 : D.Decoder (String -> String -> String)
placeholder2 =
    commonDecoder placeholder2Parser


commonDecoder : Parser a -> D.Decoder a
commonDecoder parser =
    D.string |> D.andThen (parserToDecoder parser)


placeholder1Parser : Parser (String -> String)
placeholder1Parser =
    placeholderParserGeneric 1 <| \ph p0 -> fillPlaceholdersUnsafe [ p0 ] ph


placeholder2Parser : Parser (String -> String -> String)
placeholder2Parser =
    placeholderParserGeneric 2 <| \ph p0 p1 -> fillPlaceholdersUnsafe [ p0, p1 ] ph


placeholderParserGeneric : Int -> (Placeholder -> a) -> Parser a
placeholderParserGeneric i fromPlaceholder =
    placeholderParser
        |> Parser.andThen
            (\ph ->
                let
                    sortedPlaceholderNumbers =
                        List.map Tuple.second ph.segments |> List.sort
                in
                if sortedPlaceholderNumbers == List.range 0 (i - 1) then
                    succeed (fromPlaceholder ph)

                else
                    problem <| "Expected placeholders {i} (0 <= i <= " ++ String.fromInt i ++ ") to be present."
            )


fillPlaceholdersUnsafe : List String -> Placeholder -> String
fillPlaceholdersUnsafe strs { segments, end } =
    let
        array =
            Array.fromList strs
    in
    List.map (\( before, ref ) -> before ++ (Array.get ref array |> Maybe.withDefault "")) segments
        |> String.concat
        |> (\str -> str ++ end)


placeholderParser : Parser Placeholder
placeholderParser =
    chompUntilEndOr "{"
        |> getChompedString
        |> Parser.andThen
            (\begin ->
                oneOf
                    [ Parser.succeed (emptyPlaceholder begin) |. Parser.end
                    , Parser.succeed (Tuple.pair begin >> addSegment)
                        |. token "{"
                        |= Parser.int
                        |. token "}"
                        |= Parser.lazy (\_ -> placeholderParser)
                    ]
            )


parserToDecoder : Parser a -> String -> D.Decoder a
parserToDecoder parser =
    Parser.run parser
        >> Result.mapError Parser.deadEndsToString
        >> resultToDecoder


resultToDecoder : Result String a -> D.Decoder a
resultToDecoder res =
    case res of
        Ok a ->
            D.succeed a

        Err err ->
            D.fail err
