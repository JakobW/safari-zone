module Shared.View.Button exposing (accept, back)

import Shared.View.Colors
import Element exposing (Element, alignRight, alignTop)
import Element.Input as Input
import Element.Region
import Generated.Icons as Icons
import Svg.Attributes


accept :
    { onPress : msg
    , label : Element msg
    }
    -> Element msg
accept { onPress, label } =
    Input.button [] { onPress = Just onPress, label = label }


back : msg -> Element msg
back onPress =
    Input.button [ alignRight, alignTop, Element.Region.description "Back" ]
        { onPress = Just onPress
        , label =
            Element.html <|
                Icons.backArrow [ Svg.Attributes.fill <| Shared.View.Colors.toHex Shared.View.Colors.darkGray ]
        }
