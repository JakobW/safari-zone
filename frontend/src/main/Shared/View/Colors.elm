module Shared.View.Colors exposing (..)

import Domain.Type exposing (Type(..))
import Element exposing (Color, fromRgb, rgb, rgba, toRgb)
import Hex
import Shared.Types.Generic exposing (Setter)



toHex : Color -> String
toHex =
    let
        parts =
            [ .red, .green, .blue ]
    in
    toRgb
        >> (\rgb -> List.map ((|>) rgb >> (*) 255 >> floor) parts)
        >> List.map Hex.toString
        >> String.concat >> ((++) "#")


lightGray : Color
lightGray =
    rgb 0.9 0.9 0.9


withAlpha : Float -> Color -> Color
withAlpha alpha =
    toRgb >> setAlpha alpha >> fromRgb


setAlpha : a -> Setter { color | alpha : a }
setAlpha alpha color =
    { color | alpha = alpha }


midGray : Color
midGray =
    rgb 0.7 0.7 0.7


darkGray : Color
darkGray =
    rgb 0.6 0.6 0.6


black : Color
black =
    rgb 0 0 0


white : Color
white =
    rgb 1 1 1


red : Color
red =
    rgb 1 0.2 0.2


blue : Color
blue =
    rgb 0.2 0.2 0.8


darkGreen : Color
darkGreen =
    rgb 0.1 0.2 0.1


transparent : Color
transparent =
    rgba 0 0 0 0


green : Color
green =
    rgb 0.3 0.8 0.3


lightGreen : Color
lightGreen =
    rgb 0.5 0.8 0.2


lightBlue : Color
lightBlue =
    rgb 0.5 0.5 1


darkBlue : Color
darkBlue =
    rgb 0.1 0.1 0.5


lightPink : Color
lightPink =
    rgb 1 0.9 1


pink : Color
pink =
    rgb 0.9 0.4 0.9


darkPink : Color
darkPink =
    rgb 0.8 0.1 0.8


purple : Color
purple =
    rgb 0.5 0 0.7


orange : Color
orange =
    rgb 0.8 0.5 0.2


lightBrown : Color
lightBrown =
    rgb 0.7 0.7 0.3


darkBrown : Color
darkBrown =
    rgb 0.3 0.2 0.1


yellow : Color
yellow =
    rgb 0.8 0.8 0


fromType : Type -> Color
fromType t =
    case t of
        Fire ->
            red

        Grass ->
            green

        Flying ->
            lightBlue

        Normal ->
            lightPink

        Poison ->
            purple

        Water ->
            blue

        Fighting ->
            orange

        Psychic ->
            darkPink

        Ice ->
            white

        Ground ->
            lightBrown

        Rock ->
            darkBrown

        Electric ->
            yellow

        Steel ->
            midGray

        Fairy ->
            pink

        Bug ->
            lightGreen

        Dragon ->
            darkBlue

        Ghost ->
            darkGray
