module Shared.View.Nav exposing (view)

import Animation
import Animator
import Animator.Inline
import Shared.View.Colors
import Element exposing (Attribute, Element, centerX)
import Element.Border as Border
import Element.Events as Events
import Generated.Icons as Icons
import Html exposing (Html)
import Html.Attributes
import Html.Events
import Main.Msg exposing (Msg(..))
import Route exposing (Route)
import Svg exposing (Svg)
import Svg.Attributes


type alias NavLink msg =
    { route : Route
    , icon : Svg msg
    , iconDescription : String
    , label : String
    }


navLinks : List (NavLink msg)
navLinks =
    [ { route = Route.Safari, icon = Icons.pokeball [ Html.Attributes.width 60, Html.Attributes.height 60 ], iconDescription = "Pokéball", label = "Safari" }
    , { route = Route.DexOverview, icon = Icons.pokedex [ Html.Attributes.width 80, Html.Attributes.height 70 ], iconDescription = "Pokédex", label = "Pokédex" }
    , { route = Route.TrainerOverview, icon = Icons.pokecap [ Html.Attributes.width 70, Html.Attributes.height 70 ], iconDescription = "Trainer Cap", label = "Battle" }
    ]


view : Animation.Model s -> List (Element.Attribute Msg)
view model =
    Element.inFront
        (Element.el
            [ centerX
            , Element.alignBottom
            , Element.moveUp offsetBottom
            , Element.alpha 0.5
            ]
         <|
            Element.html <|
                loadingCircle model
        )
        :: List.map (Element.inFront << Element.el [ Element.alignBottom, centerX, Element.moveUp offsetBottom ]) (renderIconCircle model navLinks)
        ++ [ Element.inFront (navIcon model)
           ]


offsetBottom : number
offsetBottom =
    30


navIcon : Animation.Model a -> Element Msg
navIcon _ =
    Element.el
        (glow
            ++ [ Element.alignBottom
               , Element.moveUp offsetBottom
               , Element.centerX
               , Events.onClick ClickedNav
               ]
        )
    <|
        Element.html <|
            Icons.pokeball [ Html.Attributes.width 60, Html.Attributes.height 60 ]


radius : number
radius =
    150


renderIconCircle : Animation.Model a -> List (NavLink Msg) -> List (Element Msg)
renderIconCircle model icons =
    let
        scaling =
            -2 * pi / toFloat (List.length icons + 1)

        transform i =
            let
                p =
                    Animator.move model.timeline
                        (\{ isNavOpen } ->
                            if isNavOpen then
                                Animator.arriveSmoothly 1 <| Animator.at 1

                            else
                                Animator.leaveSmoothly 1 <| Animator.at 0
                        )
            in
            { x = radius * sin (scaling * (i + 1) * p)
            , y = radius * cos (scaling * (i + 1) * p) - radius
            }

        opacity =
            Animator.Inline.opacity model.timeline <|
                \{ isNavOpen } ->
                    if isNavOpen then
                        Animator.at 1

                    else
                        Animator.at 0

        renderOne i { route, icon, iconDescription } =
            Element.html <|
                Html.a
                    [ Animator.Inline.transform { scale = 1, rotate = 0, position = transform i }
                    , opacity
                    , Html.Attributes.href (Route.fromRoute route)
                    , Html.Attributes.alt iconDescription
                    , Html.Events.onClick ClickedNav
                    ]
                    [ icon ]
    in
    List.indexedMap (toFloat >> renderOne) icons


iconWidth : number
iconWidth =
    20


loadingCircle : Animation.Model a -> Html msg
loadingCircle model =
    let
        stroke =
            40

        normalizedRadius =
            radius - stroke + iconWidth

        circumference =
            normalizedRadius * 2 * pi

        offset i =
            (1 - i) * circumference
    in
    Svg.svg [ Html.Attributes.height ((radius + iconWidth) * 2), Html.Attributes.width ((radius + iconWidth) * 2) ]
        [ Svg.circle
            [ Html.Attributes.attribute "stroke" "white"
            , Svg.Attributes.strokeDasharray <| String.fromFloat circumference
            , Animator.Inline.style model.timeline
                "stroke-dashoffset"
                (offset >> String.fromFloat)
                (\{ isNavOpen } ->
                    if isNavOpen then
                        Animator.at 1

                    else
                        Animator.at 0
                )
            , Svg.Attributes.strokeWidth <| String.fromInt stroke
            , Svg.Attributes.fill "transparent"
            , Svg.Attributes.cx <| String.fromInt (radius + iconWidth)
            , Svg.Attributes.cy <| String.fromInt (radius + iconWidth)
            , Svg.Attributes.r <| String.fromInt normalizedRadius
            , Html.Attributes.style "transition" "stroke-dashoffset 0.1s"
            , Html.Attributes.style "transform" "rotate(90deg)"
            , Html.Attributes.style "transform-origin" "center"
            ]
            []
        ]


glow : List (Attribute msg)
glow =
    [ Border.color Shared.View.Colors.lightGray
    , Border.width 5
    , Border.rounded 50
    , Border.glow Shared.View.Colors.lightGray 5
    ]
