module Shared.View.Page404 exposing (..)

import Http
import Shared.ErrorHandling as ErrorHandling
import Shared.Types.Document exposing (Document)


view : s -> Document msg
view _ =
    { title = "Not found"
    , body = ErrorHandling.viewHttp <| Http.BadStatus 404
    }
