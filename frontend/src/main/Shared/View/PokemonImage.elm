module Shared.View.PokemonImage exposing (ImageVariation(..), showImage, showPokedex)

import Domain.DexEntry exposing (DexEntry)
import Element exposing (Element, centerX, centerY, clip, fill, height, image, px, rgb, width)
import Element.Background as Background
import Element.Border as Borders
import Element.Font as Font
import Shared.View.Colors


type alias Displayable a =
    { a | name : String, imageName : String }


listItemStyle : ( Int, Int ) -> List (Element.Attribute msg)
listItemStyle ( x, y ) =
    [ width (px x), height (px y) ]


imageStyle : Bool -> List (Element.Attribute a)
imageStyle isShiny =
    if isShiny then
        [ Background.color (rgb 1 1 0.5) ]

    else
        [ Background.color (rgb 0.5 1 0.7) ]


type ImageVariation
    = Shiny
    | Normal
    | Missing


imageSrc : ImageVariation -> String -> String
imageSrc v name =
    "/images/dex/"
        ++ (case v of
                Shiny ->
                    "shiny"

                Normal ->
                    "normal"

                Missing ->
                    "missing"
           )
        ++ "/"
        ++ name
        ++ ".png"


showImage : List (Element.Attribute msg) -> Displayable a -> ImageVariation -> Element msg
showImage attributes { imageName, name } variation =
    image attributes { src = imageSrc variation imageName, description = name }


showNumber : PokedexAttributes a -> Int -> Element a
showNumber { imageSize, missingEntryAttributes } number =
    let
        styles =
            List.concat [ missingEntryAttributes, listItemStyle imageSize, [ Background.color Shared.View.Colors.lightGray, Font.color Shared.View.Colors.midGray ] ]
    in
    Element.el styles <|
        Element.el [ centerX, centerY ] (Element.text (String.fromInt number))


show : PokedexAttributes a -> DexEntry -> Element a
show { imageSize, pokemonAttributes } dexEntry =
    let
        isShiny =
            dexEntry.amountShiny > 0

        variation =
            if isShiny then
                Shiny

            else
                Normal
    in
    showImage (List.concat [ pokemonAttributes dexEntry, listItemStyle imageSize, imageStyle isShiny ]) dexEntry variation


type alias PokedexAttributes msg =
    { imageSize : ( Int, Int )
    , pokemonAttributes : DexEntry -> List (Element.Attribute msg)
    , missingEntryAttributes : List (Element.Attribute msg)
    }


showPokedex : PokedexAttributes msg -> List DexEntry -> Element msg
showPokedex attrs entries =
    entries
        |> List.sortBy .number
        |> renderEntries attrs
        |> Element.wrappedRow [ width fill, Borders.rounded 15, clip ]


renderEntries : PokedexAttributes msg -> List DexEntry -> List (Element msg)
renderEntries attrs entries =
    let
        go n entrs =
            case entrs of
                [] ->
                    List.range n 151 |> List.map (showNumber attrs)

                entry :: rest ->
                    (List.range n (entry.number - 1)
                        |> List.map (showNumber attrs)
                    )
                        ++ show attrs entry
                        :: go (entry.number + 1) rest
    in
    go 1 entries
