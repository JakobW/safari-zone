module TrainerSelect.Api exposing (..)

import Http
import Json.Decode as D
import TrainerSelect.Types.TrainerMetadata as TrainerMetadata exposing (TrainerMetadata)
import Array exposing (Array)


getTrainerSpriteMetadata : (Result Http.Error (Array TrainerMetadata) -> msg) -> Cmd msg
getTrainerSpriteMetadata callback =
    Http.get
        { url = "/trainer_sprites.json"
        , expect = Http.expectJson callback (D.array TrainerMetadata.decoder)
        }
