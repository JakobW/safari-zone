module TrainerSelect.Detail exposing (Model, Msg, app)

import Main.SubApp exposing (SubApp)
import Shared.Effect exposing (Effect(..))
import Shared.Session exposing (Session)
import TrainerSelect.Api as Api
import TrainerSelect.Detail.Model as Model
import TrainerSelect.Detail.Msg as Msg
import TrainerSelect.Detail.Subscriptions exposing (subscriptions)
import TrainerSelect.Detail.Update exposing (update)
import TrainerSelect.Detail.View exposing (view)


type alias Msg =
    Msg.Msg


type alias Model =
    Model.Model


app : SubApp Int Msg Model Session
app =
    { init = \id s -> ( Model.init id s, CommandEffect <| Api.getTrainerSpriteMetadata Msg.GotTrainers )
    , update = update
    , view = view
    , subscriptions = subscriptions
    , getSession = Model.getSession
    , setSession = Model.setSession
    }
