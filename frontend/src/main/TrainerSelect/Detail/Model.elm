module TrainerSelect.Detail.Model exposing (..)

import Domain.Direction exposing (Direction)
import Shared.Session exposing (Session, defaultGetSession, defaultSetSession)
import Shared.Types.TrainerState as TrainerState exposing (TrainerState)
import TrainerSelect.Types.TrainerMetadata exposing (TrainerMetadata)


type alias Model =
    { trainerSpriteMetadata : Maybe TrainerMetadata
    , trainerId : Int
    , trainerStates : List TrainerState
    , session : Session
    }


createTrainerState : Direction -> TrainerState
createTrainerState dir =
    let
        s =
            TrainerState.init
    in
    { s | moving = True, direction = dir }


init : Int -> Session -> Model
init id session =
    { trainerSpriteMetadata = Nothing
    , trainerId = id
    , trainerStates = Domain.Direction.all |> List.map createTrainerState
    , session = session
    }


getSession : Model -> Session
getSession =
    defaultGetSession


setSession : Session -> Model -> Model
setSession =
    defaultSetSession
