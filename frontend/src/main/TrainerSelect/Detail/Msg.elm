module TrainerSelect.Detail.Msg exposing (..)

import Array exposing (Array)
import Http
import TrainerSelect.Types.TrainerMetadata exposing (TrainerMetadata)


type Msg
    = Frame Float
    | GotTrainers (Result Http.Error (Array TrainerMetadata))
    | SelectedTrainer
