module TrainerSelect.Detail.Subscriptions exposing (..)

import Browser.Events
import TrainerSelect.Detail.Model exposing (Model)
import TrainerSelect.Detail.Msg exposing (Msg(..))


subscriptions : Model -> Sub Msg
subscriptions _ =
    Browser.Events.onAnimationFrameDelta Frame
