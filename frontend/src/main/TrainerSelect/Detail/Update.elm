module TrainerSelect.Detail.Update exposing (..)

import Array exposing (Array)
import Route
import Shared.Effect exposing (Effect(..))
import Shared.Session as Session
import Shared.Types.TrainerState as TrainerState
import TrainerSelect.Detail.Model exposing (Model)
import TrainerSelect.Detail.Msg exposing (Msg(..))
import TrainerSelect.Types.TrainerMetadata exposing (TrainerMetadata)


update : Msg -> Model -> ( Model, Effect Msg )
update msg model =
    case msg of
        Frame _ ->
            ( { model | trainerStates = List.map TrainerState.move model.trainerStates }, NoEffect )

        GotTrainers (Ok trainers) ->
            ( findTrainer trainers model, NoEffect )

        GotTrainers (Err _) ->
            ( Session.addErrorS "http error" model, NoEffect )

        SelectedTrainer ->
            handleSelectedTrainer model


findTrainer : Array TrainerMetadata -> Model -> Model
findTrainer metadata model =
    { model | trainerSpriteMetadata = Array.get model.trainerId metadata }


handleSelectedTrainer : Model -> ( Model, Effect Msg )
handleSelectedTrainer model =
    case model.trainerSpriteMetadata of
        Just metadata ->
            ( { model | session = Session.selectTrainer ( model.trainerId, metadata ) model.session }
            , Route.navigate Route.TrainerOverview
            )

        Nothing ->
            ( model, NoEffect )
