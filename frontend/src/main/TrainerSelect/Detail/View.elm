module TrainerSelect.Detail.View exposing (..)

import Element exposing (..)
import Shared.Types.Document exposing (Document)
import Shared.View.Button as Button
import Shared.View.Colors
import Shared.View.Page404 as Page404
import TrainerSelect.Detail.Model exposing (Model)
import TrainerSelect.Detail.Msg exposing (Msg(..))
import TrainerSelect.Types.TrainerMetadata exposing (TrainerMetadata)
import TrainerSelect.View.Trainer as Trainer


view : Model -> Document Msg
view model =
    Maybe.map (viewInternal model) model.trainerSpriteMetadata
        |> Maybe.withDefault (Page404.view model.session)


viewInternal : Model -> TrainerMetadata -> Document Msg
viewInternal model metadata =
    let
        viewSprite state =
            Trainer.trainerSprite [] state metadata
                |> Trainer.coloredSpriteFrame Shared.View.Colors.darkGray
    in
    { title = "Configuration"
    , body =
        Element.column [ spacing 10, padding 10 ]
            [ Element.wrappedRow [ width fill, spacing 10 ]
                (List.map viewSprite model.trainerStates)
            , Button.accept { onPress = SelectedTrainer, label = Element.text "Select" }
            ]
    }



--[ clip, padding 10, Background.color Colors.midGray ]
