module TrainerSelect.Overview exposing (Model, Msg, app)

import Main.SubApp exposing (SubApp)
import Shared.Effect exposing (Effect(..))
import Shared.Session exposing (Session)
import TrainerSelect.Api as Api
import TrainerSelect.Overview.Model as Model
import TrainerSelect.Overview.Msg as Msg
import TrainerSelect.Overview.Subscriptions exposing (subscriptions)
import TrainerSelect.Overview.Update exposing (update)
import TrainerSelect.Overview.View exposing (view)


type alias Msg =
    Msg.Msg


type alias Model =
    Model.Model


app : SubApp () Msg Model Session
app =
    { init = \_ s -> ( Model.init s, CommandEffect <| Api.getTrainerSpriteMetadata Msg.GotTrainers )
    , update = update
    , view = view
    , subscriptions = subscriptions
    , getSession = Model.getSession
    , setSession = Model.setSession
    }
