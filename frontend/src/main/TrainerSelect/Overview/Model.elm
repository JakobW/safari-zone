module TrainerSelect.Overview.Model exposing (..)

import Array exposing (Array)
import Domain.Direction
import Shared.Session exposing (Session, defaultGetSession, defaultSetSession)
import Shared.Types.TrainerState as TrainerState exposing (TrainerState)
import TrainerSelect.Types.Tag exposing (Tag)
import TrainerSelect.Types.TrainerMetadata exposing (TrainerMetadata)


type alias Model =
    { trainerSpriteMetadata : Array TrainerMetadata
    , filters : List Tag
    , trainerState : TrainerState
    , session : Session
    }


initialTrainerState : TrainerState
initialTrainerState =
    let
        s =
            TrainerState.init
    in
    { s | moving = True, direction = Domain.Direction.D }


init : Session -> Model
init session =
    { trainerSpriteMetadata = Array.empty
    , filters = []
    , trainerState = initialTrainerState
    , session = session
    }


filteredSpriteMetadata :
    { m
        | trainerSpriteMetadata : Array { a | tags : List String }
        , filters : List Tag
    }
    -> List ( Int, { a | tags : List String } )
filteredSpriteMetadata { filters, trainerSpriteMetadata } =
    let
        activeFilters =
            (List.filter .active >> List.map .tagName) filters

        satisfiesActiveFilters tags =
            List.all (\tag -> List.member tag tags) activeFilters
    in
    Array.toIndexedList trainerSpriteMetadata
        |> List.filter (Tuple.second >> .tags >> satisfiesActiveFilters)


getSession : Model -> Session
getSession =
    defaultGetSession


setSession : Session -> Model -> Model
setSession =
    defaultSetSession
