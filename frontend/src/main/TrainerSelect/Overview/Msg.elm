module TrainerSelect.Overview.Msg exposing (..)

import Array exposing (Array)
import Http
import TrainerSelect.Types.TrainerMetadata exposing (TrainerMetadata)


type Msg
    = ToggleFilter String
    | Frame Float
    | GotTrainers (Result Http.Error (Array TrainerMetadata))
    | ClickedTrainer Int
