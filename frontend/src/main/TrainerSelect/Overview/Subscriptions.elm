module TrainerSelect.Overview.Subscriptions exposing (..)

import Browser.Events
import TrainerSelect.Overview.Model exposing (Model)
import TrainerSelect.Overview.Msg exposing (Msg(..))


subscriptions : Model -> Sub Msg
subscriptions _ =
    Browser.Events.onAnimationFrameDelta Frame
