module TrainerSelect.Overview.Update exposing (..)

import Array
import Route
import Shared.Effect exposing (Effect(..))
import Shared.Session as Session
import Shared.Types.TrainerState as TrainerState
import TrainerSelect.Overview.Model exposing (Model)
import TrainerSelect.Overview.Msg exposing (Msg(..))
import TrainerSelect.Types.Tag as Tag
import TrainerSelect.Types.TrainerMetadata as TrainerMetadata


update : Msg -> Model -> ( Model, Effect Msg )
update msg model =
    case msg of
        ToggleFilter tagName ->
            ( { model | filters = Tag.toggleTagByName tagName model.filters }, NoEffect )

        Frame _ ->
            ( { model | trainerState = TrainerState.move model.trainerState }, NoEffect )

        GotTrainers (Ok trainers) ->
            ( { model | trainerSpriteMetadata = trainers, filters = Array.toList trainers |> TrainerMetadata.collectAllTags }, NoEffect )

        GotTrainers (Err _) ->
            ( Session.addErrorS "http error" model, NoEffect )

        ClickedTrainer id ->
            ( model, Route.navigate (Route.TrainerDetail id) )
