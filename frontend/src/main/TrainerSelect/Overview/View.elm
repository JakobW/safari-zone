module TrainerSelect.Overview.View exposing (view)

import Shared.View.Colors
import Element exposing (Element, fill, padding, spacing, width)
import Element.Background as Background
import Element.Border as Border
import Element.Events as Events
import Element.Font as Font
import Shared.Types.Document exposing (Document)
import TrainerSelect.Overview.Model as Model exposing (Model)
import TrainerSelect.Overview.Msg exposing (Msg(..))
import TrainerSelect.Types.Tag exposing (Tag)
import TrainerSelect.View.Trainer as Trainer


view : Model -> Document Msg
view model =
    let
        viewSprite ( trainerId, metadata ) =
            let
                frameColor =
                    if Maybe.map Tuple.first model.session.selectedTrainer == Just trainerId then
                        Shared.View.Colors.green

                    else
                        Shared.View.Colors.darkGray
            in
            Trainer.trainerSprite [ Events.onClick (ClickedTrainer trainerId) ] model.trainerState metadata
                |> Trainer.coloredSpriteFrame frameColor
    in
    { title = "Configuration"
    , body =
        Element.column [ spacing 10, padding 10 ]
            [ Element.wrappedRow [ spacing 10 ] (List.map viewFilter model.filters)
            , Element.wrappedRow [ width fill, spacing 10 ]
                (List.map viewSprite (Model.filteredSpriteMetadata model))
            ]
    }


viewFilter : Tag -> Element Msg
viewFilter { tagName, active } =
    Element.el
        [ padding 10
        , Border.rounded 15
        , Background.color <|
            if active then
                Shared.View.Colors.darkGray

            else
                Shared.View.Colors.lightGray
        , Events.onClick (ToggleFilter tagName)
        , Font.color <|
            if active then
                Shared.View.Colors.white

            else
                Shared.View.Colors.black
        ]
    <|
        Element.text tagName
