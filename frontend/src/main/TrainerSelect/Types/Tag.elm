module TrainerSelect.Types.Tag exposing (Tag, tagNameFuzzer, toggleTagByName)

import Fuzz


type alias Tag =
    { tagName : String
    , active : Bool
    }


toggleTagByName : String -> List Tag -> List Tag
toggleTagByName tagName =
    List.map
        (\tag ->
            if tag.tagName == tagName then
                toggleTag tag

            else
                tag
        )


toggleTag : Tag -> Tag
toggleTag tag =
    { tag | active = not tag.active }


tagNameFuzzer : Fuzz.Fuzzer String
tagNameFuzzer =
    [ "male", "female", "hat", "blue" ]
        |> List.map Fuzz.constant
        |> Fuzz.oneOf
