module TrainerSelect.Types.TrainerMetadata exposing (..)

import Fuzz
import Json.Decode as D
import Json.Decode.Pipeline as D
import TrainerSelect.Types.Tag exposing (tagNameFuzzer)
import TrainerSelect.Types.Tag exposing (Tag)
import Set


type alias TrainerMetadata =
    { displayName : String
    , imagePath : String
    , tags : List String
    }


collectAllTags : List TrainerMetadata -> List Tag
collectAllTags =
    let
        inactive tagName =
            { tagName = tagName, active = False }
    in
    List.concatMap .tags
        >> Set.fromList
        >> Set.toList
        >> List.map inactive


decoder : D.Decoder TrainerMetadata
decoder =
    D.succeed TrainerMetadata
        |> D.required "displayName" D.string
        |> D.required "imagePath" D.string
        |> D.required "tags" (D.list D.string)


fuzzer : Fuzz.Fuzzer TrainerMetadata
fuzzer =
    Fuzz.map3
        TrainerMetadata
        Fuzz.string
        Fuzz.string
        (Fuzz.list tagNameFuzzer)
