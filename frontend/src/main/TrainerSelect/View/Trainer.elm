module TrainerSelect.View.Trainer exposing (coloredSpriteFrame, trainerSprite)

import Shared.View.Colors
import Element exposing (Attribute, Color, Element, clip, height, moveLeft, moveUp, padding, px, width)
import Element.Background as Background
import Element.Border as Border
import Element.Region as Region
import Shared.Types.TrainerState as TrainerState exposing (TrainerState)
import TrainerSelect.Types.TrainerMetadata exposing (TrainerMetadata)


coloredSpriteFrame : Color -> Element msg -> Element msg
coloredSpriteFrame borderColor inner =
    Element.el
        [ padding 20
        , Background.color Shared.View.Colors.white
        , Border.rounded 20
        , Border.color borderColor
        , Border.width 5
        ]
        inner


trainerSprite : List (Attribute msg) -> TrainerState -> TrainerMetadata -> Element msg
trainerSprite extraAttrs state { imagePath, displayName } =
    let
        image =
            Element.image
                [ moveLeft (toFloat <| indexToOffsetX index * 64)
                , moveUp (toFloat <| indexToOffsetY index * 64)
                ]
                { description = displayName, src = imagePath }

        index =
            TrainerState.dirToIndex state
    in
    Element.el
        (extraAttrs
            ++ [ width (px 64)
               , height (px 64)
               , Element.behindContent image
               , clip
               , Region.description displayName
               ]
        )
        Element.none


indexToOffsetX : Int -> Int
indexToOffsetX =
    modBy 4


indexToOffsetY : Int -> Int
indexToOffsetY i =
    clamp 0 3 <| i // 4
