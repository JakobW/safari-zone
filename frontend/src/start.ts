import { Authentification, setStorage } from './Login/loginView';
import { websocketPort } from './websocket';

export const startSPA = (auth: Authentification) => {
  setStorage(auth);
  import(/* webpackChunkName: "elm"*/ './main/Main.elm').then(({ Elm }) => {
    const app = Elm.Main.init({
      flags: {
        language: navigator.language,
        x: window.innerWidth,
        y: window.innerHeight,
        auth,
        backendUrl: window.location.origin,
      },
    });
    const { sendWebSocket, receiveWebSocket } = app.ports;

    websocketPort({
      subscribe: sendWebSocket.subscribe,
      send: receiveWebSocket.send,
    });
  });
};
