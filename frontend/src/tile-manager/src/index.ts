import { Authentification } from '../../Login/loginView';
import App from './components/App.svelte';
import './css/main.css';
import { setAuth } from './ts/Api';

const app = (authentification: Authentification) => {
  setAuth(authentification);
  new App({
    target: document.body,
  });
};

export default app;
