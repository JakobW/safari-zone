import { Authentification } from '../../../Login/loginView';
import { ImageMetadata, LayerData } from './mapCanvas';
import type { Pokemon, PokemonData } from './Pokemon';
import { AreaWithId, BaseArea, TileUpdate } from './Types';

let authentification: Authentification | undefined;

export const setAuth = (auth: Authentification) => {
  authentification = auth;
};

const getAuthHeaders = (): Record<string, string> => {
  if (authentification) {
    return {
      'X-Auth-Provider': authentification.provider,
      authorization: `Bearer ${authentification.token}`,
    };
  }
  return {};
};

export const getPokemon = (): Promise<PokemonData[]> =>
  fetch('/api/pokemon/pokemon', { headers: getAuthHeaders() }).then(
    (response) => response.json()
  );

export const getAreas = (): Promise<AreaWithId[]> =>
  fetch('/api/area', { headers: getAuthHeaders() }).then((response) =>
    response.json()
  );

export const getEncountersForArea = (areaId: number): Promise<Pokemon[]> =>
  fetch(`/api/encounter/area/${areaId}`, { headers: getAuthHeaders() }).then(
    (response) => response.json()
  );

export const addEncountersForArea = (
  areaId: number,
  encounter: Pokemon[]
): Promise<Response> =>
  post(
    '/api/encounter',
    encounter.map((enc) => ({
      areaId,
      pokemonId: enc.pokemonId,
      probability: enc.probability,
    }))
  );

export const deleteEncountersForArea = (
  areaId: number,
  encounters: Pokemon[]
): Promise<Response[]> =>
  Promise.all(
    encounters.map((enc) => _delete('/api/encounter', { ...enc, areaId }))
  );

export const createArea = (area: BaseArea): Promise<Response> =>
  post('/api/area', area);

export const createAreaWithPokemon = async (
  area: BaseArea & { pokemon: Pokemon[] }
) => {
  const id = await createArea(area).then((res) => Number(res.text));
  addEncountersForArea(id, area.pokemon);
};

export type LayerId = number & { _id: symbol };

export const getLayers = (): Promise<LayerId[]> =>
  get('/api/tile/layer').then((response) => response.json());

export const getTilesOfLayer = (layer: LayerId): Promise<LayerData> =>
  get(`/api/tile/layer/${layer}`).then((response) => response.json());

export const getImageMetadata = (): Promise<ImageMetadata> =>
  get('/api/images').then((response) => response.json());

export const getTileTypes = (): Promise<string[]> =>
  get('/api/tile').then((response) => response.json());

export const createOrUpdateTiles = (layer: LayerId, tiles: TileUpdate[]) =>
  put(`/api/tile/layer/${layer}`, tiles).then((response) =>
    console.log(response)
  );

const put = (url: string, body: unknown): Promise<Response> => {
  return request(url, 'PUT', body);
};

const post = (url: string, body: unknown): Promise<Response> =>
  request(url, 'POST', body);
const _delete = (url: string, body: unknown): Promise<Response> =>
  request(url, 'DELETE', body);
const get = (url: string): Promise<Response> => request(url, 'GET');

const request = (
  url: string,
  method: string,
  body?: unknown
): Promise<Response> =>
  fetch(url, {
    method,
    body: body ? JSON.stringify(body) : null,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      ...getAuthHeaders(),
    },
  });
