import { writable } from 'svelte/store';
import { getAreas } from './Api';
import { AreaWithId } from './Types';

const { set, subscribe } = writable<AreaWithId[]>([]);

const load = () => getAreas().then(set);

export const areaStore = { load, subscribe };
