import { writable } from 'svelte/store';
import { Coordinate, Mode } from './Types';

export interface GridState {
  mode: Mode;
  dragStart?: Coordinate;
  dragEnd?: Coordinate;
  isDragging: boolean;
}

const { update, subscribe } = writable<GridState>({
  mode: 'drag',
  dragStart: undefined,
  dragEnd: undefined,
  isDragging: false,
});

const toSelectMode = () => update((state) => ({ ...state, mode: 'select' }));
const toDragMode = () => update((state) => ({ ...state, mode: 'drag' }));

const startDrag = (coord: Coordinate) =>
  update((state) => ({
    ...state,
    dragStart: coord,
    dragEnd: coord,
    isDragging: true,
  }));

const endDrag = () => update((state) => ({ ...state, isDragging: false }));

const drag = (coord: Coordinate) =>
  update((state) => ({ ...state, dragEnd: coord }));

export const gridState = {
  drag,
  startDrag,
  endDrag,
  toSelectMode,
  toDragMode,
  subscribe,
};
