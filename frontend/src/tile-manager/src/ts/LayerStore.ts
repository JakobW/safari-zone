import { writable } from 'svelte/store';
import { LayerId } from './Api';

const { update, subscribe } = writable<LayerId>(0 as LayerId);

const increment = () => update((n) => (n + 1) as LayerId);
const decrement = () => update((n) => (n - 1) as LayerId);

export const layerStore = { increment, decrement, subscribe };
