import { getContext, setContext, SvelteComponent } from 'svelte';
import { writable } from 'svelte/store';

export const modalState = writable<SvelteComponent | null>(null);

export const key = {};

type State = {
  component: typeof SvelteComponent | null;
  props: Record<string, any>;
};

type Context = {
  open: (component: typeof SvelteComponent, props: Record<string, any>) => void;
  close: () => void;
};

const initialState: State = { component: null, props: {} };

export const init = () => {
  const modalState = writable<State>(initialState);

  const close = () => {
    modalState.set({ component: null, props: {} });
  };
  setContext<Context>(key, {
    open: (component: typeof SvelteComponent, props: Record<string, any>) => {
      modalState.set({ component, props });
    },
    close,
  });
  return { subscribe: modalState.subscribe, close };
};

export const getModalContext = () => getContext<Context>(key);
