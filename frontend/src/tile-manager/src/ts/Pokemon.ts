export interface Pokemon {
  pokemonId: number;
  probability: number;
}

export interface PokemonData {
  id: number;
  number: number;
  name: string;
}

export const calcDiff = (before: Pokemon[], after: Pokemon[]) => {
  const toDelete: Pokemon[] = before.filter(
    (b) => !after.find((a) => a.pokemonId === b.pokemonId)
  );
  return { toCreateOrUpdate: after, toDelete };
};
