import { writable } from 'svelte/store';
import { getPokemon } from './Api';
import { PokemonData } from './Pokemon';

const { set, subscribe } = writable<PokemonData[]>([]);

const load = () => getPokemon().then(set);

export const pokemonStore = { load, subscribe };
