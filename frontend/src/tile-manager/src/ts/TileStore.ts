import { writable } from 'svelte/store';
import { getImageMetadata, getLayers, getTilesOfLayer, LayerId } from './Api';
import { LayerData, MapCanvas } from './mapCanvas';

const { set, subscribe, update } = writable<MapCanvas | undefined>(undefined);

export const canvasId = 'background';

const loadTileDataEntry = async (
  layerName: LayerId
): Promise<[LayerId, LayerData]> => {
  const tileData = await getTilesOfLayer(layerName);
  return [layerName, tileData];
};

const loadTileDataMap = async (
  layerNames: LayerId[]
): Promise<Map<LayerId, LayerData>> =>
  Promise.all(layerNames.map(loadTileDataEntry)).then((n) => new Map(n));

const load = async (activeLayer: LayerId) => {
  const [layers, imageMetadata] = await Promise.all([
    getLayers(),
    getImageMetadata(),
  ]);
  const tileDataMap = await loadTileDataMap(layers);
  set(await MapCanvas.build(tileDataMap, imageMetadata, canvasId, activeLayer));
};

const refresh = async (layerIds: LayerId[]) => {
  const tileData = await loadTileDataMap(layerIds);
  update((canvas) => {
    canvas?.updateTileData(tileData);
    return canvas;
  });
};

export const tileStore = { load, subscribe, refresh };
