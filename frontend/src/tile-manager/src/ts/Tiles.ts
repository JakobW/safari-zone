import { createOrUpdateTiles, LayerId } from './Api';
import { GridState } from './GridStore';
import { tileStore } from './TileStore';
import { TileUpdate } from './Types';
import { grid } from './Utils';

export const createGroundTiles = async (
  { dragStart, dragEnd }: GridState,
  update: string,
  layerId: LayerId
) => {
  if (dragStart && dragEnd) {
    const tiles: TileUpdate[] = grid(dragStart, dragEnd).map(({ x, y }) => ({
      coordX: x,
      coordY: y,
      tile: update,
    }));
    await createOrUpdateTiles(layerId, tiles);
    tileStore.refresh([layerId]);
  }
};
