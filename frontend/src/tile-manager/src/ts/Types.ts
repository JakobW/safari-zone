export interface Coordinate {
  x: number;
  y: number;
}

export type Mode = 'drag' | 'select';

interface Tile {
  coordX: number;
  coordY: number;
}

export type Update = { tile: string };

export type TileUpdate = Update & Tile;

export interface BaseArea {
  name: string;
  borderLeft: number;
  borderRight: number;
  borderTop: number;
  borderBottom: number;
}

export interface AreaWithId extends BaseArea {
  id: number;
}
