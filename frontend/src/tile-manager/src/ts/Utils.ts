import { Coordinate } from './Types';

export const normalize = (m: number, n: number) => [
  Math.min(m, n),
  Math.max(m, n),
];

export const isInRange = (i: number, x: number, y: number) => {
  const [min, max] = normalize(x, y);
  return min <= i && max >= i;
};

export const range = (start: number, end: number): number[] => {
  const [min, max] = normalize(start, end);
  return Array.from({ length: max + 1 - min }).map((_, i) => min + i);
};

export const grid = (c1: Coordinate, c2: Coordinate): Coordinate[] =>
  range(c1.x, c2.x).flatMap((x) => range(c1.y, c2.y).map((y) => ({ x, y })));

export const trace = <T>(t: T): T => {
  console.log(t);
  return t;
};

export const defined = <T>(t: T | undefined): t is T => t !== undefined;
