import { LayerId } from './Api';

export interface TileData {
  columns: number;
  tiles: number[];
}

export interface LayerData {
  ground: TileData;
  overlay: TileData;
}

export interface ImageMetadata {
  atlasUrl: string;
  atlasTileSize: number;
  atlasWidth: number;
}

interface Tile {
  sourceXPx: number;
  sourceYPx: number;
  targetXTile: number;
  targetYTile: number;
}

export interface ViewArea {
  left: number;
  right: number;
  top: number;
  bottom: number;
}

const loadImage = (src: string): Promise<CanvasImageSource> => {
  const image = new Image();
  const p = new Promise<CanvasImageSource>((resolve, reject) => {
    image.onload = () => {
      resolve(image);
    };
    image.onerror = reject;
  });
  image.src = src;
  return p;
};

export class MapCanvas {
  private tileSize: number;

  static async build(
    tileData: Map<LayerId, LayerData>,
    imageMetadata: ImageMetadata,
    canvasId: string,
    activeLayer: LayerId
  ): Promise<MapCanvas> {
    const image = await loadImage(imageMetadata.atlasUrl);
    return new this(canvasId, image, tileData, imageMetadata, activeLayer);
  }

  private constructor(
    private canvasId: string,
    private image: CanvasImageSource,
    private tileData: Map<LayerId, LayerData>,
    private imageMetadata: ImageMetadata,
    private activeLayer: LayerId
  ) {
    this.draw = this.draw.bind(this);
    this.toTile = this.toTile.bind(this);
    this.drawSingleTile = this.drawSingleTile.bind(this);
    this.setActiveLayer = this.setActiveLayer.bind(this);
    this.tileSize = 32;
  }

  updateTileData(tileData: Map<LayerId, LayerData>): void {
    this.tileData = new Map([...this.tileData, ...tileData]);
  }

  setActiveLayer(activeLayer: LayerId): void {
    this.activeLayer = activeLayer;
  }

  draw(tileSize: number, viewArea: ViewArea): void {
    this.tileSize = tileSize;
    const canvas = document.getElementById(
      this.canvasId
    ) as HTMLCanvasElement | null;
    canvas?.getContext('2d')?.clearRect(0, 0, canvas.width, canvas.height);
    const { left, right, top, bottom } = viewArea;
    const offsetX = left * this.tileSize;
    const offsetY = top * this.tileSize;
    const isOnScreen = ({ targetXTile, targetYTile }: Tile) => {
      return (
        targetXTile >= Math.floor(left) &&
        targetXTile <= Math.ceil(right) &&
        targetYTile >= Math.floor(top) &&
        targetYTile <= Math.ceil(bottom)
      );
    };
    if (canvas) {
      const drawTileData = (td: TileData, alpha: number) => {
        td.tiles
          .map((tileId, i) => this.toTile(td.columns, tileId, i))
          .filter(isOnScreen)
          .forEach((tile) =>
            this.drawSingleTile(tile, canvas, offsetX, offsetY, alpha)
          );
      };
      this.tileData.forEach((td, layerId) => {
        const alpha = this.activeLayer === layerId ? 1 : 0.5;
        drawTileData(td.ground, alpha);
      });
      this.tileData.forEach((td, layerId) => {
        const alpha = this.activeLayer === layerId ? 1 : 0.5;
        drawTileData(td.overlay, alpha);
      });
    }
  }

  private toTile(columns: number, tileId: number, i: number): Tile {
    const targetYTile = Math.floor(i / columns);
    const targetXTile = i % columns;
    const sourceY =
      Math.floor(tileId / this.imageMetadata.atlasWidth) *
      this.imageMetadata.atlasTileSize;
    const sourceX =
      (tileId % this.imageMetadata.atlasWidth) *
      this.imageMetadata.atlasTileSize;
    return {
      sourceXPx: sourceX,
      sourceYPx: sourceY,
      targetXTile,
      targetYTile,
    };
  }

  private drawSingleTile(
    { sourceXPx, sourceYPx, targetXTile, targetYTile }: Tile,
    canvas: HTMLCanvasElement,
    offsetX: number,
    offsetY: number,
    alpha = 1
  ) {
    const context = canvas.getContext('2d');
    if (context) {
      context.globalAlpha = alpha;
      context.drawImage(
        this.image,
        sourceXPx,
        sourceYPx,
        this.imageMetadata.atlasTileSize,
        this.imageMetadata.atlasTileSize,
        targetXTile * this.tileSize - offsetX,
        targetYTile * this.tileSize - offsetY,
        this.tileSize,
        this.tileSize
      );
    }
  }
}
