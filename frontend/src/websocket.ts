let socket: WebSocket | undefined;

const connectWs = (path: string) => {
  const hostPath = `${
    window.location.protocol === 'https:' ? 'wss://' : 'ws://'
  }${window.location.host}/${path}`;
  return new WebSocket(hostPath);
};

export const websocketPort = ({
  subscribe,
  send,
}: Port<Elm.WebsocketOut, Elm.WebsocketIn>) => {
  let isConnecting = false;

  subscribe((msg: Elm.WebsocketOut) => {
    if (msg.tag === 'connect') {
      socket = connectWs(msg.url);
      isConnecting = true;
      socket.addEventListener('error', (err) => {
        console.error(err);
        send({
          tag: 'error',
        });
      });

      socket.addEventListener('open', () => {
        const interval = setInterval(() => {
          if (socket?.readyState === socket?.OPEN) {
            clearInterval(interval);
            socket?.send(JSON.stringify(msg.auth));
          }
        }, 50);
      });

      socket.addEventListener('message', (event) => {
        if (isConnecting) {
          send({
            tag: 'connected',
            data: event.data,
          });
          isConnecting = false;
        } else {
          send({
            tag: 'message',
            data: event.data,
          });
        }
      });

      socket.addEventListener('close', (err) => {
        send({
          tag: 'closed',
        });
        socket = undefined;
      });
    }
    if (msg.tag === 'disconnect') {
      socket?.close();
    }
    if (msg.tag === 'send') {
      socket?.send(JSON.stringify(msg.message));
    }
  });
};
