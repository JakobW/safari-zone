module Control.CameraTest exposing (..)

import Config
import Safari.Overworld.Types.Camera as Camera exposing (Camera)
import Data.Units exposing (TileUnit(..), WebGLUnit(..))
import Domain.Coordinate exposing (Coordinate)
import Expect
import Test exposing (Test, describe, test)


exampleCamera : Coordinate TileUnit -> Camera
exampleCamera player =
    Camera.mkCamera player
        { height = Config.tileSize * 10, width = Config.tileSize * 15 }
        { x = TileUnit 50, y = TileUnit 30 }


suite : Test
suite =
    describe "Camera"
        [ test "calculates the expected map offset if player is in the middle of the map" <|
            \_ ->
                Camera.calcTileOffset (exampleCamera { x = TileUnit 8, y = TileUnit 9 })
                    |> Expect.equal { x = TileUnit -0.5, y = TileUnit -4 }
        , test "does not shift the map positive values" <|
            \_ ->
                Camera.calcTileOffset (exampleCamera { x = TileUnit 2, y = TileUnit 1 })
                    |> Expect.equal { x = TileUnit 0, y = TileUnit 0 }
        , test "does not shift the map over its positive limit" <|
            \_ ->
                Camera.calcTileOffset (exampleCamera { x = TileUnit 48, y = TileUnit 28 })
                    |> Expect.equal { x = TileUnit -35, y = TileUnit -20 }
        , test "player coord has webgl offset of 0/0 if in the middle" <|
            \_ ->
                Camera.calcWebGLCoord (exampleCamera { x = TileUnit 8, y = TileUnit 9 })
                    { x = TileUnit 8, y = TileUnit 9 }
                    |> Expect.equal { x = WebGLUnit 0, y = WebGLUnit 0 }
        , test "top-left has webgl offset of -1/1" <|
            \_ ->
                Camera.calcWebGLCoord (exampleCamera { x = TileUnit 1, y = TileUnit 1 })
                    { x = TileUnit 0, y = TileUnit 0 }
                    |> Expect.equal { x = WebGLUnit -1, y = WebGLUnit 1 }
        ]
