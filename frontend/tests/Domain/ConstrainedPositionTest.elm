module Domain.ConstrainedPositionTest exposing (..)

import Data.Frame exposing (fuzzMax1Tile)
import Dict
import Domain.ConstrainedPosition as ConstrainedPosition
import Domain.Coordinate as Coord
import Domain.Direction as Direction
import Domain.MoveRule exposing (MoveRule(..), MoveRules)
import Domain.Position as Position
import Expect
import Fuzz
import Test exposing (Test, describe, fuzz2)
import Data.Units exposing (unTileUnit)


groundMoveRules : List ( Int, Int ) -> MoveRules
groundMoveRules =
    List.map (\i -> ( i, Ground )) >> Dict.fromList


suite : Test
suite =
    describe "ConstrainedPosition"
        [ fuzz2 fuzzMax1Tile Coord.fuzzer "behaves the same as regular position if relevant moverules are ground" <|
            \dt startCoord ->
                let
                    groundTiles =
                        List.map (Tuple.mapBoth ((+) startCoord.x) ((+) startCoord.y)) [ ( 0, 0 ), ( 0, 1 ) ]

                    regularPosResult =
                        Position.init startCoord
                            |> Position.move (Just Direction.D) dt
                            |> Position.getExact
                in
                ConstrainedPosition.init (groundMoveRules groundTiles) startCoord
                    |> ConstrainedPosition.move (Just Direction.D) dt
                    |> ConstrainedPosition.getExact
                    |> Expect.equal (Just regularPosResult)
        , fuzz2 Fuzz.float Coord.fuzzer "stops any movement if the surrounding coords are not allowed" <|
            \dt startCoord ->
                ConstrainedPosition.init (groundMoveRules [ Coord.toTuple startCoord ]) startCoord
                    |> ConstrainedPosition.move (Just Direction.D) dt
                    |> ConstrainedPosition.getExact
                    |> Maybe.map (Coord.map unTileUnit)
                    |> Expect.equal (Just <| Coord.toFloat startCoord)
        ]
