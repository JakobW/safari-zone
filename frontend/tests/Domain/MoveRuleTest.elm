module Domain.MoveRuleTest exposing (..)

import Domain.MoveRule as MoveRule
import Expect
import Fuzz
import Json.Decode as D
import Json.Encode as E
import Test exposing (Test, describe, fuzz, test)


suite : Test
suite =
    describe "MoveRule"
        [ test "can decode moveRules from JSON" <|
            \_ ->
                """{ "rules": [0,1,0,1], "columns": 2 }"""
                    |> D.decodeString MoveRule.decodeMoveRules
                    |> Expect.ok
        , fuzz (Fuzz.list Fuzz.int) "fails to decode moveRules from JSON if rules are not 0 - 2" <|
            \list ->
                let
                    expectation =
                        if List.all (\i -> i >= 0 && i <= 2) list then
                            Expect.ok

                        else
                            Expect.err
                in
                " { \"rules\": "
                    ++ intArrToJSON list
                    ++ ", \"columns\": 2 }"
                    |> D.decodeString MoveRule.decodeMoveRules
                    |> expectation
        , test "is able to move from a position to another if both are ground" <|
            \_ ->
                """{ "rules": [0,0], "columns": 2 }"""
                    |> D.decodeString MoveRule.decodeMoveRules
                    |> expectOkWith
                        (MoveRule.canMoveFromTo { x = 0, y = 0 } { x = 1, y = 0 } >> Expect.equal True)
        ]


expectOkWith : (a -> Expect.Expectation) -> Result D.Error a -> Expect.Expectation
expectOkWith f res =
    case res of
        Ok a ->
            f a

        Err err ->
            Expect.fail <| D.errorToString err


intArrToJSON : List Int -> String
intArrToJSON =
    E.list E.int >> E.encode 0
