module Domain.PositionTest exposing (..)

import Config
import Data.Frame exposing (fuzzMax1Tile)
import Data.Units exposing (TileUnit(..), unTileUnit)
import Domain.Coordinate as Coord
import Domain.Direction as Direction
import Domain.Position as Position
import Expect
import Test exposing (Test, describe, fuzz, fuzz2, fuzz3, test)


suite : Test
suite =
    describe "Position"
        [ fuzz3 Coord.fuzzer fuzzMax1Tile Direction.fuzzer "moving moves the position in the specified direction" <|
            \startCoord dt dir ->
                Position.init startCoord
                    |> Position.move (Just dir) dt
                    |> Position.getExact
                    |> Coord.map unTileUnit
                    |> Expect.equal
                        (Direction.directionToCoord dir
                            |> Coord.times (dt / Config.tileSize)
                            |> Coord.plus (Coord.toFloat startCoord)
                        )
        , fuzz fuzzMax1Tile "does not switch direction if the next integer position has not been reached" <|
            \dt ->
                Position.init { x = 0, y = 0 }
                    |> Position.move (Just Direction.R) (dt / 2)
                    |> Position.move (Just Direction.L) (dt / 2)
                    |> Position.getExact
                    |> Coord.map unTileUnit
                    |> Expect.equal { x = dt / Config.tileSize, y = 0 }
        , test "floors step if the next integer position has been reached" <|
            \_ ->
                Position.init { x = 0, y = 0 }
                    |> Position.move (Just Direction.R) 0.5
                    |> Position.move (Just Direction.L) (Config.tileSize * 5)
                    |> Position.getExact
                    |> Coord.map unTileUnit
                    |> Expect.equal { x = 1, y = 0 }
        , fuzz fuzzMax1Tile "is able to switch direction after the next integer position has been reached" <|
            \dt ->
                Position.init { x = 0, y = 0 }
                    |> Position.move (Just Direction.R) Config.tileSize
                    |> Position.move (Just Direction.L) dt
                    |> Position.getExact
                    |> Coord.map unTileUnit
                    |> Expect.equal { x = 1 - dt / Config.tileSize, y = 0 }
        , fuzz2 Coord.fuzzer Direction.fuzzer "can change direction without moving with dt 0" <|
            \startCoord dir ->
                Position.init startCoord
                    |> Position.move (Just dir) 0
                    |> (\pos ->
                            ( Position.getCurrentDirection pos, Position.getExact pos )
                       )
                    |> Expect.equal ( dir, Coord.map (toFloat >> TileUnit) startCoord )
        ]
