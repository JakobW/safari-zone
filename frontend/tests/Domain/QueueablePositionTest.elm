module Domain.QueueablePositionTest exposing (..)

import Config
import Data.Units exposing (unTileUnit)
import Domain.Coordinate as Coord
import Domain.Direction as Direction
import Domain.Position as Position
import Domain.QueueablePosition as QueueablePosition
import Expect exposing (FloatingPointTolerance(..))
import Fuzz
import Test exposing (Test, describe, fuzz, fuzz2, fuzz3)


suite : Test
suite =
    describe "QueueablePosition"
        [ fuzz3 Fuzz.float Coord.fuzzer (Fuzz.maybe Direction.fuzzer) "works just like Position for a single move" <|
            \dt startCoord dir ->
                let
                    posResult =
                        Position.init startCoord
                            |> Position.move dir dt
                            |> Position.getExact
                in
                QueueablePosition.init startCoord
                    |> QueueablePosition.move dir dt
                    |> QueueablePosition.getExact
                    |> Expect.equal posResult
        , fuzz Coord.fuzzer "move to coord triggers small move in the right direction for adjacent tile" <|
            \startCoord ->
                QueueablePosition.init startCoord
                    |> QueueablePosition.moveToCoord (Coord.plus { x = 0, y = 1 } startCoord)
                    |> QueueablePosition.getExact
                    |> Coord.map unTileUnit
                    |> Expect.equal (Coord.toFloat startCoord |> Coord.plus { x = 0, y = 0.1 / Config.tileSize })
        , fuzz Coord.fuzzer "works correctly for multiple moves" <|
            \startCoord ->
                QueueablePosition.init startCoord
                    |> QueueablePosition.moveToCoord (Coord.plus { x = 0, y = 1 } startCoord)
                    |> QueueablePosition.moveToCoord (Coord.plus { x = 1, y = 1 } startCoord)
                    |> QueueablePosition.getExact
                    |> Coord.map unTileUnit
                    |> Expect.all
                        [ .x >> Expect.equal (toFloat startCoord.x)
                        , .y >> Expect.within (Absolute 0.00001) (toFloat startCoord.y + 0.2 / Config.tileSize)
                        ]
        , fuzz Coord.fuzzer "speeds up movement if queue is filled" <|
            \startCoord ->
                QueueablePosition.init startCoord
                    |> QueueablePosition.moveToCoord (Coord.plus { x = 0, y = 1 } startCoord)
                    |> QueueablePosition.moveToCoord (Coord.plus { x = 1, y = 1 } startCoord)
                    |> QueueablePosition.move Nothing 0.5
                    |> QueueablePosition.getExact
                    |> Coord.map unTileUnit
                    |> Expect.all
                        [ .x >> Expect.equal (toFloat startCoord.x)
                        , .y >> Expect.within (Absolute 0.00001) (toFloat startCoord.y + 1.2 / Config.tileSize)
                        ]
        , fuzz Coord.fuzzer "move to coord jumps directly to target tile if not adjacent" <|
            \startCoord ->
                let
                    target =
                        Coord.plus { x = 1, y = 1 } startCoord
                in
                QueueablePosition.init startCoord
                    |> QueueablePosition.moveToCoord target
                    |> QueueablePosition.getExact
                    |> Coord.map unTileUnit
                    |> Expect.equal (Coord.toFloat target)
        , fuzz2 Coord.fuzzer Fuzz.float "stops at target position eventually" <|
            \startCoord dt ->
                QueueablePosition.init startCoord
                    |> QueueablePosition.moveToCoord (Coord.plus { x = 0, y = 1 } startCoord)
                    |> QueueablePosition.move Nothing dt
                    |> QueueablePosition.getExact
                    |> Coord.map unTileUnit
                    |> Expect.all
                        [ .x >> Expect.equal (toFloat startCoord.x)
                        , .y >> Expect.atMost (toFloat startCoord.y + 1)
                        ]
        ]
