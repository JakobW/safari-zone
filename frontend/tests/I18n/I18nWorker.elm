port module I18n.I18nWorker exposing (..)

import Http exposing (Error(..))
import Json.Decode as D
import Json.Decode.Pipeline as D
import Json.Encode as E
import Shared.I18n as I18n exposing (I18n)
import Shared.Types.BackendUrl as BackendUrl exposing (BackendUrl)
import Shared.Types.Language as Language exposing (Language)


port getLang : (E.Value -> msg) -> Sub msg


port sendI18nWorkerResult : { error : Bool, content : String } -> Cmd msg


type alias Request =
    { backendUrl : BackendUrl
    , language : Language
    }


type Msg
    = GotI18n (Result Http.Error I18n)
    | GetI18n (Result D.Error Request)


type alias Model =
    Maybe Language


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GetI18n (Ok req) ->
            ( Just req.language, I18n.getProperties GotI18n req )

        GetI18n (Err err) ->
            ( Nothing, D.errorToString err |> Err |> send )

        GotI18n (Ok _) ->
            ( Nothing, model |> Maybe.map Language.toString |> Result.fromMaybe "Non recognized language" |> send )

        GotI18n (Err (BadBody err)) ->
            ( Nothing, Err err |> send )

        GotI18n (Err err) ->
            ( Nothing, Err (Debug.toString err) |> send )


subscriptions : model -> Sub Msg
subscriptions _ =
    getLang (D.decodeValue requestDecoder >> GetI18n)


send : Result String String -> Cmd msg
send res =
    case res of
        Ok ok ->
            sendI18nWorkerResult { error = False, content = ok }

        Err err ->
            sendI18nWorkerResult { error = True, content = err }


main : Program () Model Msg
main =
    Platform.worker
        { init = \_ -> ( Nothing, Cmd.none )
        , update = update
        , subscriptions = subscriptions
        }


requestDecoder : D.Decoder Request
requestDecoder =
    D.succeed Request
        |> D.required "backendUrl" BackendUrl.backendUrlDecoder
        |> D.required "language" (D.string |> D.map Language.fromString)
