module I18n.PlaceholderTest exposing (..)

import Expect
import Json.Decode as D
import Shared.Types.Placeholder exposing (placeholder1, placeholder2)
import Test exposing (Test, describe, test)


suite : Test
suite =
    describe "Placeholder"
        [ test "single placeholder at the end" <|
            \_ ->
                "\"Hello {0}\""
                    |> D.decodeString placeholder1
                    |> Result.map ((|>) "World")
                    |> Expect.equal (Ok "Hello World")
        , test "single placeholder at the start" <|
            \_ ->
                "\"{0} are cool\""
                    |> D.decodeString placeholder1
                    |> Result.map ((|>) "Placeholders")
                    |> Expect.equal (Ok "Placeholders are cool")
        , test "single placeholder in the middle" <|
            \_ ->
                "\"Nobody {0} Runtime Errors\""
                    |> D.decodeString placeholder1
                    |> Result.map ((|>) "likes")
                    |> Expect.equal (Ok "Nobody likes Runtime Errors")
        , test "two placeholders" <|
            \_ ->
                "\"One: {0} and Two: {1}\""
                    |> D.decodeString placeholder2
                    |> Result.map (\f -> f "1" "2")
                    |> Expect.equal (Ok "One: 1 and Two: 2")
        , test "two swapped placeholders" <|
            \_ ->
                "\"I{1} {0} dude\""
                    |> D.decodeString placeholder2
                    |> Result.map (\f -> f "anymore" "dk")
                    |> Expect.equal (Ok "Idk anymore dude")
        ]
