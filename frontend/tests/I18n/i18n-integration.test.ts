import cors from "@koa/cors";
import { Server } from "http";
import shutdown from "http-shutdown";
import Koa from "koa";
import Static from "koa-static";
import { AddressInfo } from "net";
import path from "path";
import pkgUp from "pkg-up";
//@ts-ignore
import { Elm } from "./I18nWorker.elm";

describe("i18n", () => {
  const { ports } = Elm.I18n.I18nWorker.init();
  const sendRequest = async (language: string): Promise<string> =>
    new Promise((resolve, reject) => {
      ports.sendI18nWorkerResult.subscribe(
        ({ error, content }: { error: boolean; content: string }) => {
          ports.sendI18nWorkerResult.unsubscribe();
          if (error) {
            reject(content);
          } else {
            resolve(content);
          }
        }
      );
      const serverPort = (server.address() as AddressInfo).port;
      ports.getLang.send({
        language,
        backendUrl: `http://localhost:${serverPort}/`,
      });
    });
  let server: Server & { shutdown: (callback: () => void) => void };

  beforeAll(async () => {
    jest.setTimeout(10000);
    const rootDir = (await pkgUp()) || "";
    if (!rootDir) {
      throw new Error("No root directory found");
    }
    const app = new Koa();
    app.use(cors());
    app.use(Static(path.join(rootDir, "..", "public")));
    server = shutdown(app.listen(0));
  });

  afterAll((done) => {
    jest.setTimeout(10000);
    server.shutdown(done);
  });

  test.each(["de", "en"])(
    'i18n has the correct format for language "%s"',
    async (language) => {
      await expect(sendRequest(language)).resolves.toBe(language);
    }
  );
});
