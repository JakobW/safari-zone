module RouteTest exposing (..)

import Expect
import Fuzz exposing (Fuzzer)
import Route
import Test exposing (Test, describe, fuzz)
import Url


listFuzzer : List a -> Fuzzer a
listFuzzer =
    List.map Fuzz.constant >> Fuzz.oneOf


protocolFuzzer : Fuzzer Url.Protocol
protocolFuzzer =
    listFuzzer
        [ Url.Http
        , Url.Https
        ]


urlFuzzer : Fuzzer Url.Url
urlFuzzer =
    Fuzz.map Url.Url protocolFuzzer
        |> Fuzz.andMap Fuzz.string
        |> Fuzz.andMap (Fuzz.maybe Fuzz.int)
        |> Fuzz.andMap Fuzz.string
        |> Fuzz.andMap (Fuzz.maybe Fuzz.string)
        |> Fuzz.andMap (Fuzz.maybe Fuzz.string)


suite : Test
suite =
    describe "Route"
        [ fuzz urlFuzzer "/ is HomePage" <|
            \url ->
                let
                    expectedRoute =
                        case url.path of
                            "" ->
                                Route.DexOverview

                            "/" ->
                                Route.DexOverview

                            "/safari" ->
                                Route.Safari

                            "/safari/" ->
                                Route.Safari

                            _ ->
                                Route.NotFound url
                in
                Route.toRoute url
                    |> Expect.equal expectedRoute
        ]
