port module WebSocket.WebSocketEchoWorker exposing (..)

import Json.Decode as D
import Json.Encode as E
import Ports.WebSocket as WS
import Shared.Types.Auth


port sendWsWorkerResult : String -> Cmd msg


type Msg
    = GotWS WS.WebsocketIn


type alias Model =
    Maybe WS.Connection


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotWS (WS.Connected conn wsMsg) ->
            ( Just conn, WS.sendMessage conn <| E.string wsMsg )

        GotWS WS.Closed ->
            ( Nothing, sendWsWorkerResult "Received Close" )

        GotWS (WS.Error _) ->
            ( Nothing, sendWsWorkerResult "Received Error" )

        GotWS (WS.Message wsMsg) ->
            case model of
                Just conn ->
                    ( model, WS.sendMessage conn <| E.string wsMsg )

                Nothing ->
                    Debug.todo "Got message without being connected first"


subscriptions : model -> Sub Msg
subscriptions _ =
    WS.receive GotWS


main : Program D.Value Model Msg
main =
    Platform.worker
        { init =
            \authFlag ->
                case D.decodeValue Shared.Types.Auth.decodeAuthentification authFlag of
                    Ok auth ->
                        ( Nothing, WS.connect auth )

                    Err err ->
                        Debug.todo <| D.errorToString err
        , update = update
        , subscriptions = subscriptions
        }
