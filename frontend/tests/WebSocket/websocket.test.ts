import WS from 'jest-websocket-mock';
import { websocketPort } from '../../src/websocket';
import { Elm } from './WebSocketEchoWorker.elm';

describe('websocket', () => {
  let ws: WS;
  const wsBaseUrl = `ws://localhost:${window.location.port}/api/websocket`;

  const auth: Elm.Authentification = {
    token: '12345',
    provider: 'local',
    lifespan: 900,
  };

  let resultPort: Sub<string>;

  const initElm = () => {
    const app = Elm.WebSocket.WebSocketEchoWorker.init({ flags: auth });
    resultPort = app.ports.sendWsWorkerResult;

    websocketPort({
      subscribe: app.ports.sendWebSocket.subscribe,
      send: app.ports.receiveWebSocket.send,
    });
  };

  const expectResult = async (action: () => void, expectation: string) => {
    const promise = new Promise((resolve) => {
      resultPort.subscribe((msg: string) => {
        resultPort.unsubscribe;
        resolve(msg);
      });
    });
    action();
    await expect(promise).resolves.toBe(expectation);
  };

  beforeEach(async () => {
    ws = new WS(wsBaseUrl);
    initElm();
    await ws.connected;
  });

  afterEach(() => {
    ws.close();
  });

  it('forwards authentication correctly', async () => {
    expect(ws).toReceiveMessage(
      JSON.stringify({ ...auth, lifespan: undefined })
    );
  });

  it('can receive and send messages', () => {
    const testMessage = JSON.stringify({ echo: 'echo' });
    ws.send(testMessage);
    expect(ws).toReceiveMessage(JSON.stringify(testMessage));
  });

  it('can react to a disconnect', async () => {
    await expectResult(() => ws.close(), 'Received Close');
  });
});
