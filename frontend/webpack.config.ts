import CompressionPlugin from "compression-webpack-plugin";
import CopyPlugin from "copy-webpack-plugin";
import HtmlWebpackPlugin from "html-webpack-plugin";
import JsonMinimizerPlugin from "json-minimizer-webpack-plugin";
import MiniCssExtractPlugin from "mini-css-extract-plugin";
import path from "path";
import TerserPlugin from "terser-webpack-plugin";
import webpack from "webpack";
import webpackDevServer from "webpack-dev-server";
import zlib from "zlib";
import devServerConfig from "./webpack/dev-server";

const isProduction = process.env.NODE_ENV === "production";
const mode = isProduction ? "production" : "development";

const config: webpack.Configuration & {
  devServer: webpackDevServer.Configuration;
} = {
  entry: path.join(__dirname, "src/index.ts"),
  mode: mode,
  resolve: {
    extensions: [".ts", ".js", ".json", ".elm", ".svelte"],
  },
  target: "web",
  module: {
    rules: [
      {
        test: /\.elm$/,
        exclude: [/elm-stuff/, /node_modules/],
        use: {
          loader: "elm-webpack-loader",
          options: {
            optimize: isProduction,
            debug: false,
          },
        },
      },
      {
        test: /\.svelte$/,
        use: {
          loader: "svelte-loader",
          options: {
            compilerOptions: {
              dev: !isProduction,
            },
            emitCss: isProduction,
            hotReload: !isProduction,
            preprocess: require("svelte-preprocess")(),
          },
        },
      },
      {
        test: /\.ts$/i,
        exclude: [/elm-stuff/, /node_modules/],
        use: "ts-loader",
      },
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          { loader: "css-loader", options: { url: false } },
        ],
      },
      {
        // required to prevent errors from Svelte on Webpack 5+
        test: /node_modules\/svelte\/.*\.mjs$/,
        resolve: {
          fullySpecified: false,
        },
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      favicon: path.join(__dirname, "public/favicon.ico"),
      template: path.join(__dirname, "public/index.html"),
    }),
    new MiniCssExtractPlugin({
      filename: "[name].[contenthash].css",
    }),
  ],
  optimization: {
    minimize: isProduction,
    minimizer: [new TerserPlugin(), new JsonMinimizerPlugin()],
  },
  output: {
    filename: "[name].[contenthash].js",
    path: path.join(__dirname, "dist"),
    publicPath: "/",
  },
  devServer: devServerConfig,
};

if (isProduction) {
  config.plugins?.push(
    new CompressionPlugin({
      filename: "[path][base].br",
      algorithm: "brotliCompress",
      test: /\.(js|css|html|svg)$/,
      compressionOptions: {
        params: {
          [zlib.constants.BROTLI_PARAM_QUALITY]: 11,
        },
      } as any,
      threshold: 10240,
      minRatio: 0.8,
      deleteOriginalAssets: false,
    }) as any,
    new CompressionPlugin({
      filename: "[path][base].gz",
      algorithm: "gzip",
      test: /\.js$|\.css$|\.html$/,
      threshold: 10240,
      minRatio: 0.8,
    }) as any,
    new CopyPlugin({
      patterns: [
        { from: "public", to: "./", globOptions: { ignore: "**/index.html" } },
      ],
    })
  );
}

export default config;
