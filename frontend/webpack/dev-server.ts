import path from "path";

export default {
  static: [
    path.join(__dirname, "..", "public"),
    path.join(__dirname, "..", "..", "backend", "app", "resources"),
  ],
  compress: true,
  open: true,
  port: 3000,
  historyApiFallback: true,
  proxy: {
    "/api/websocket": {
      target: "ws://localhost:8080",
      ws: true,
    },
    "/api": "http://localhost:8080",
    "/auth": "http://localhost:8081",
  },
};
