import {
  createWsServerWithAutoAuth,
  visitWithMockWs,
} from 'cypress/support/wsServer';
import { Connection } from 'fake-websocket';

const mockTileSets = () => {
  cy.intercept('/api/images', {
    atlasTileSize: 16,
    atlasUrl: '/images/generated/tileset-result.png',
    tileMapUrls: [
      '/images/generated/layerg0.png',
      '/images/generated/layero0.png',
    ],
    atlasWidth: 8,
  });
  cy.intercept('/images/generated/layerg0.png', { fixture: 'layerg0.png' });
  cy.intercept('/images/generated/layero0.png', { fixture: 'layero0.png' });
  cy.intercept('/api/tile/moveRules', { fixture: 'moveRules.json' });
};

const holdKeyForSteps = (key: string, conn: Connection, steps: number) => {
  cy.get('body').trigger('keydown', { key, release: false });
  return new Cypress.Promise((resolve) => {
    let stepsSoFar = 0;
    const handler = (msg) => {
      console.log(msg);
      stepsSoFar += 1;
      if (stepsSoFar >= steps) {
        conn.off('message', handler);
        resolve();
      }
    };
    conn.on('message', handler);
  });
};

describe('safari page', () => {
  beforeEach(() => {
    mockTileSets();
    visitWithMockWs('/safari');
  });

  it('renders the page as expected', () => {
    cy.wrap(createWsServerWithAutoAuth({ x: 5, y: 5 }));
    cy.get('canvas').then(() => cy.matchImageSnapshot());
  });

  it('does the correct camera work on movement', () => {
    cy.wrap(createWsServerWithAutoAuth({ x: 11, y: 5 })).then((conn) => {
      cy.get('canvas');
      cy.wrap(holdKeyForSteps('ArrowRight', conn as Connection, 5)).then(() => {
        cy.wrap(holdKeyForSteps('ArrowDown', conn as Connection, 2)).then(
          () => {
            cy.wrap(holdKeyForSteps('ArrowLeft', conn as Connection, 2)).then(
              () => {
                cy.wait(100);
                cy.matchImageSnapshot();
              }
            );
          }
        );
      });
    });
  });

  it('shows pokemon and trainer on the correct position', () => {
    cy.intercept('/api/pokemon/3', venusaur);
    cy.wrap<Promise<Connection>, Connection>(
      createWsServerWithAutoAuth({ x: 5, y: 5 })
    ).then((conn) => {
      cy.get('canvas');
      conn.send(JSON.stringify(encounterMessage));
      conn.send(JSON.stringify(playerMovedMessage({ x: 3, y: 4 })));
      cy.get('canvas').then(() => cy.matchImageSnapshot());
    });
  });

  it('movement of pokemon and trainer works', () => {
    cy.intercept('/api/pokemon/3', venusaur);
    cy.wrap<Promise<Connection>, Connection>(
      createWsServerWithAutoAuth({ x: 5, y: 5 })
    ).then((conn) => {
      cy.get('canvas');
      conn.send(JSON.stringify(encounterMessage));
      conn.send(JSON.stringify(playerMovedMessage({ x: 3, y: 4 })));
      conn.send(JSON.stringify(encounterMovedMessage));
      conn.send(JSON.stringify(playerMovedMessage({ x: 2, y: 4 })));
      // TODO Note there is bug in generator-exe which makes testing the jankyness of this hard currently. The pokemon only move left/up/right/down
      // from the original location, not from the current location
      cy.wait(100);
      cy.matchImageSnapshot();
    });
  });
});

const encounterMessage = {
  tag: 'PokemonMessage',
  contents: {
    tag: 'NewEncounterMsg',
    contents: {
      position: { x: 6, y: 7 },
      encounterId: 2,
      pokemonId: 3,
      shinyStatus: 'NonShiny',
    },
  },
};

const encounterMovedMessage = {
  tag: 'PokemonMessage',
  contents: {
    tag: 'MoveEncounterMsg',
    contents: {
      position: { x: 7, y: 7 },
      encounterId: encounterMessage.contents.contents.encounterId,
    },
  },
};

const playerMovedMessage = (position: { x: number; y: number }) => ({
  tag: 'PlayerMovedMessage',
  contents: {
    id: 1,
    position,
  },
});

const venusaur = {
  id: 3,
  number: 3,
  name: 'Venusaur',
  primaryType: 'Grass',
  secondaryType: 'Poison',
  weight: 122,
  height: 1.5,
  description: 'Its a big dinosaur with a flower on its back',
  imageName: 'venusaur',
};
