import {
  createWsServerWithAutoAuth,
  visitWithMockWs,
} from 'cypress/support/wsServer';

describe('trainer overview and select', () => {
  it('trainer select works', () => {
    visitWithMockWs('/trainer');
    // cy.visit("/trainer");
    cy.wrap(createWsServerWithAutoAuth({ x: 5, y: 5 }));
    cy.get('[aria-label="Sensei"]').click();
    cy.contains('Select').click();
    cy.matchImageSnapshot();
  });
});
