const LOGIN_URL =
  'https://auth.apps.andrena.de/auth/realms/test/protocol/openid-connect/token';

const requestConfig = {
  url: LOGIN_URL,
  method: 'POST',
  form: true,
  body: {
    username: 'test',
    password: 'test',
    grant_type: 'password',
    client_id: 'testclient',
  },
};

const extractToken = (res) => res.body.access_token;

const visitUrlWithToken = (url: string, token: string) => {
  cy.wrap(token).should('have.length.greaterThan', 20);
  const hash = `#access_token=${token}`;
  cy.visit(`${url}${hash}`);
  cy.hash().should('eq', hash);
};

export const andrenaLogin = (url: string) => {
  cy.request(requestConfig)
    .then(extractToken)
    .then((token) => visitUrlWithToken(url, token));
};

export const localLogin = () => {
  cy.contains('Username').type('test_user');
  cy.contains('Password').type('test_pass');
  cy.contains(/^Login$/).click();
};

export const skipLoginWithRefreshToken = () => {
  window.localStorage.setItem('preferred_login', 'local');
  cy.intercept('/auth/refresh', {
    accessToken: 'testtoken',
    lifespan: 900,
  });
};
