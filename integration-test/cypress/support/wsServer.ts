import * as FakeWS from 'fake-websocket';

export const visitWithMockWs = (path: string) => {
  cy.intercept('/auth/refresh', {
    accessToken: 'testtoken',
    lifespan: 900,
  });
  cy.visit(path, {
    onBeforeLoad(win) {
      win.localStorage.setItem('preferred_login', 'local');
      cy.stub(win, 'WebSocket').callsFake((url) => {
        const ws = new FakeWS.WebSocket(url);
        ws.OPEN = 1;
        return ws;
      });
    },
  });
};

const baseUrl = new URL(Cypress.config('baseUrl'));

export const createWsServerWithAutoAuth = (position: {
  x: number;
  y: number;
}): Promise<FakeWS.Connection> =>
  createServer(`ws://${baseUrl.host}/api/websocket`).then(
    (conn) =>
      new Cypress.Promise((resolve) => {
        let isFirstMessage = true;
        conn.on('message', (_msg) => {
          if (isFirstMessage) {
            console.log('Sent first message');
            conn.send(JSON.stringify(position));
            isFirstMessage = false;
            resolve(conn);
          }
        });
      })
  );

export const createServer = (url: string): Promise<FakeWS.Connection> =>
  new Cypress.Promise((resolve) => {
    const mockServer = new FakeWS.WebSocketServer(url);
    mockServer.on('connection', (connection) => {
      resolve(connection);
    });
  });
