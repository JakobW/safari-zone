#!/bin/bash
: ${TEST_IMAGE:=ui-test}

(docker run --name cypress \
           -v $(pwd)/cypress:/cypress \
           --shm-size=2GB \
           --network=docker_backend \
           -e CYPRESS_BASE_URL=http://frontend \
           $TEST_IMAGE)

cypress_exit_code=$?
docker rm -f cypress
exit $cypress_exit_code           