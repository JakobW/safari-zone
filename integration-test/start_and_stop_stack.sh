#!/bin/bash

cd ../docker
if [[ ! -z "$CI_REGISTRY_IMAGE" ]]; then
  echo "DOCKER_IMAGE_HOST=${CI_REGISTRY_IMAGE}" > .env
fi

docker-compose up --detach

cd ../integration-test

/bin/sh ./run_cypress.sh

exit_code=$?

cd ../docker
docker-compose down
exit $exit_code
