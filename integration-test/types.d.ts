declare module 'fake-websocket' {
  export class WebSocket {
    OPEN: 1;
    constructor(url: string);
  }

  export class WebSocketServer {
    constructor(url: string);
    on(event: 'connection', callback: (conn: Connection) => void): void;
  }

  export interface Connection {
    on(event: 'message', callback: (msg: unknown) => void): void;
    off(event: 'message', callback: (msg: unknown) => void): void;
    send(msg: string): void;
  }
}
