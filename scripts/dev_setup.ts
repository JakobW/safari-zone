import { executeMigrations } from './lib/execute_migrations';
import { startLocalPostgres } from './lib/start_local_postgres';
import { startLocalRedis } from './lib/start_local_redis';

(async () => {
  await startLocalPostgres();
  await startLocalRedis();
  await executeMigrations();
})();
