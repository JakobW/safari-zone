import path from 'path';
import { runChildProcess } from './util';

const executeHaskellStackExecutable = (executableName: string) =>
  runChildProcess(`stack run ${executableName}`, {
    cwd: path.join(__dirname, '..', '..', 'backend'),
  });

export const executeMigrations = async () => {
  console.log('Executing app database migrations');
  await executeHaskellStackExecutable('app-migration-exe');
  console.log('Executing auth database migrations');
  await executeHaskellStackExecutable('auth-migration-exe');
};
