import waitOn from 'wait-on';
import { getContainerState, runShell } from './util';

const containerName = 'postgres_local';
const dockerImage = 'postgres';

export const startLocalPostgres = async () => {
  const containerState = await getContainerState(containerName);
  if (containerState === 'Running') {
    console.log('Postgres is already running, skipping.');
    return;
  }
  if (containerState === 'Stopped') {
    console.log('Postgres is stopped, starting...');
    await runShell(`docker start ${containerName}`);
    return;
  }
  await runShell(
    `docker run -d -p 5432:5432 -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=app --name ${containerName} ${dockerImage}`
  );
  console.log('Waiting for postgres to start');
  await waitOn({ resources: ['tcp:5432'] });
  console.log('Successfully started postgres. Setting up databases...');
  await runShell(
    `docker exec ${containerName} psql -U postgres -c "CREATE DATABASE auth"`
  );
};
