import { getContainerState, runShell } from './util';

const containerName = 'redis_local';
const dockerImage = 'redis:6.2.5-alpine';

export const startLocalRedis = async () => {
  const containerState = await getContainerState(containerName);
  if (containerState === 'Running') {
    console.log('Redis is already running, skipping.');
    return;
  }
  if (containerState === 'Stopped') {
    console.log('Redis is stopped, starting...');
    await runShell(`docker start ${containerName}`);
    return;
  }
  await runShell(
    `docker run -d -p 6379:6379 --name ${containerName} ${dockerImage}`
  );
  console.log('Successfully started Redis container!');
};
