import cp from 'child_process';

export type ContainerState = 'Running' | 'Stopped' | 'NonExistent';

// this returns the stdout that the given command produced.
// no piping is done, so output is not displayed.
export const runShell = async (command: string): Promise<string> =>
  new Promise((resolve, reject) => {
    cp.exec(command, (err, stdout) => {
      if (err) {
        reject(err);
      } else {
        resolve(stdout);
      }
    });
  });

interface ChildProcessOptions {
  cwd?: string;
}

// this pipes all logs that the given command produced to the main process.
export const runChildProcess = async (
  command: string,
  options: ChildProcessOptions = {}
): Promise<void> => {
  const [executable, ...args] = command.split(' ');
  return new Promise((resolve, reject) => {
    const child = cp.spawn(executable, args, {
      stdio: 'pipe',
      cwd: options.cwd,
    });
    child.stdout.pipe(process.stdout);
    child.stderr.pipe(process.stderr);
    const exitHandler = (exitCode: number) => {
      if (exitCode > 0) {
        reject({ code: exitCode });
      } else {
        resolve();
      }
    };
    child.on('close', exitHandler);
    child.on('exit', exitHandler);
    child.on('disconnect', reject);
    child.on('error', reject);
  });
};

export const getContainerState = async (
  containerName: string
): Promise<ContainerState> =>
  runShell(`docker container inspect -f '{{.State.Running}} ${containerName}`)
    .then((result) => (result.includes('true') ? 'Running' : 'Stopped'))
    .catch(() => 'NonExistent');
